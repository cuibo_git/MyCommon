package com.personal.dataanalyse.reportmanage.entity;

import com.personal.core.utils.CoreUtil;

/**
 * 条件信息
 * @author cuibo
 *
 */
public class ConditionInfo
{
    
    /** 维度名称 */
    private String dimensionName;
    
    /** 维度显示名(图表时使用) */
    private String dimensionDisplayName;
    
    /** 对应的表头配置 */
    private ReportHeader headerConfig;
    
    /** key */
    private String key;
    
    /** 数据类型 */
    private String dataType;
    
    /** 关系 */
    private String relation;
    
    /** 值 */
    private String value;
    
    /** 表达式 */
    private boolean expType;
    
    /** 跳跃数：类似于跳表，在某一个匹配上了，或者没有匹配上，应该直接往后跳跃，而不应该一次循环 */
    /** cb先只存储每个condition下的个行数或者列数 */
    private int skipCount;
    
    /** 是否是复合维度的条件:存在FieldConsts.LEVELTWOSPLIT分割的维度，如GROP1>1 LEVELTWOSPLIT GROP1<5 */
    private boolean complex;
    
    /**
     * 强制私有构造器，保存信息的完整性
     */
    @SuppressWarnings("unused")
    private ConditionInfo()
    {
        super();
    }

    /**
     * 
     * @param key  key
     * @param dataType  数据类型
     * @param relation  关系
     * @param value  值
     * @param expType  表达式
     * @param dimensionName  维度名称
     * @param headerConfig
     */
    public ConditionInfo(String key, String dataType, String relation, String value, boolean expType, String dimensionName, ReportHeader headerConfig)
    {
        super();
        this.key = key;
        this.dataType = dataType;
        this.relation = relation;
        this.value = value;
        this.expType = expType;
        this.dimensionName = dimensionName;
        this.headerConfig = headerConfig;
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getRelation()
    {
        return relation;
    }

    public void setRelation(String relation)
    {
        this.relation = relation;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public boolean isExpType()
    {
        return expType;
    }

    public void setExpType(boolean expType)
    {
        this.expType = expType;
    }

    public String getDataType()
    {
        return dataType;
    }

    public void setDataType(String dataType)
    {
        this.dataType = dataType;
    }

    public int getSkipCount()
    {
        return skipCount;
    }

    public void setSkipCount(int skipCount)
    {
        this.skipCount = skipCount;
    }

    public ReportHeader getHeaderConfig()
    {
        return headerConfig;
    }

    public void setHeaderConfig(ReportHeader headerConfig)
    {
        this.headerConfig = headerConfig;
    }

    public String getDimensionName()
    {
        return dimensionName;
    }

    public void setDimensionName(String dimensionName)
    {
        this.dimensionName = dimensionName;
    }

    public String getDimensionDisplayName()
    {
        return dimensionDisplayName;
    }

    public void setDimensionDisplayName(String dimensionDisplayName)
    {
        this.dimensionDisplayName = dimensionDisplayName;
    }

    public boolean isComplex()
    {
        return complex;
    }

    public void setComplex(boolean complex)
    {
        this.complex = complex;
    }
    
    public boolean customEquals(ConditionInfo other)
    {
        if (other == null)
        {
            return false;
        }
        if (CoreUtil.isEmpty(this.getKey()) && CoreUtil.isEmpty(other.getKey()))
        {
            return true;
        }
        return CoreUtil.checkEqual(this.getKey(), other.getKey()) && CoreUtil.checkEqual(this.getRelation(), other.getRelation()) && CoreUtil.checkEqual(this.getValue(), other.getValue());
    }

}
