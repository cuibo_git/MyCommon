package com.personal.dataanalyse.reportmanage.model;

import com.personal.dataanalyse.consts.FieldConsts;
import com.personal.dataanalyse.reportmanage.base.QuotaInfo;
import com.personal.dataanalyse.reportmanage.entity.DataColumnReportEx;
import com.personal.dataconvert.bean.HeaderConfig;

/**
 * 报表列
 * @author cuibo
 *
 */
public class ReportQuota extends QuotaInfo
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * 将自身信息转成DataColumnReportEx
     * @return
     * @throws Exception 
     */
    @Override
    public DataColumnReportEx toDataColumnReportEx() throws Exception
    {
        // 如果该列是在维度下面。需要拼接维度信息，并且拼接条件
        DataColumnReportEx result = super.toDataColumnReportEx();
        
        result.setAlign(this.getAlign());
        result.setWidth(this.getWidth());
        
        return result;
    }
    
    @Override
    public void createTargetValueQuotaInfo()
    {
        super.createTargetValueQuotaInfo();
        this.setWidth(FieldConsts.TARGET_VALUE_WIDTH);
        this.setAlign(HeaderConfig.CENTER);
    }


    private String align;
  
    private int width;
    
    public String getAlign()
    {
        return align;
    }

    public void setAlign(String align)
    {
        this.align = align;
    }

    public int getWidth()
    {
        return width;
    }

    public void setWidth(int width)
    {
        this.width = width;
    }


    
}
