package com.personal.dataanalyse.reportmanage.model;

import java.io.Serializable;

import com.personal.core.utils.CoreUtil;
import com.personal.dataanalyse.enums.AdditionalTypeEnum;

/**
 * 模型装饰
 * @author cuibo
 *
 */
public class ReportDecorateModel implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** 合计名 */
    private String totalName;
    
    /** 平均值名 */
    private String avgName;
    
    /** 最大值名 */
    private String maxName;
    
    /** 最小值名 */
    private String minName;
    
    /** 左页眉 */
    private String leftHeader;
    
    /** 右页眉 */
    private String rightHeader;
    
    /** 是否移除不匹配的行列 */
    private boolean removeNoMatchRowCol = false;
    
    /** 表头是否加上单位 */
    private boolean headerAddUnit = true;
    
    /**
     * 获取附加列对应的属性值
     * @param additionalType
     * @return
     */
    public String getOppositeProperty(AdditionalTypeEnum additionalType)
    {
        String result = null;
        switch (additionalType)
        {
        case 求和:
            result = totalName;
            break;
        case 求平均:
            result = avgName;
            break;
        case 求最大值:
            result = maxName;
            break;
        case 求最小值:
            result = minName;
            break;
        default:
            break;
        }
        return result;
    }
    
    /**
     * 是否有附加的行或者列
     * @return
     */
    public boolean hasAdditionalRowCol()
    {
        return !CoreUtil.isEmpty(totalName) || !CoreUtil.isEmpty(avgName) || !CoreUtil.isEmpty(maxName) || !CoreUtil.isEmpty(minName);
    }

    public String getLeftHeader()
    {
        return leftHeader;
    }

    public void setLeftHeader(String leftHeader)
    {
        this.leftHeader = leftHeader;
    }

    public String getRightHeader()
    {
        return rightHeader;
    }

    public void setRightHeader(String rightHeader)
    {
        this.rightHeader = rightHeader;
    }

    public String getTotalName()
    {
        return totalName;
    }

    public void setTotalName(String totalName)
    {
        this.totalName = totalName;
    }

    public String getAvgName()
    {
        return avgName;
    }

    public void setAvgName(String avgName)
    {
        this.avgName = avgName;
    }

    public String getMaxName()
    {
        return maxName;
    }

    public void setMaxName(String maxName)
    {
        this.maxName = maxName;
    }

    public String getMinName()
    {
        return minName;
    }

    public void setMinName(String minName)
    {
        this.minName = minName;
    }

    public boolean isRemoveNoMatchRowCol()
    {
        return removeNoMatchRowCol;
    }

    public void setRemoveNoMatchRowCol(boolean removeNoMatchRowCol)
    {
        this.removeNoMatchRowCol = removeNoMatchRowCol;
    }

    public boolean isHeaderAddUnit()
    {
        return headerAddUnit;
    }

    public void setHeaderAddUnit(boolean headerAddUnit)
    {
        this.headerAddUnit = headerAddUnit;
    }
    
}
