package com.personal.dataanalyse.reportmanage.entity;

import java.util.ArrayList;
import java.util.List;

import com.personal.core.data.DataColumn;
import com.personal.core.data.DataRow;
import com.personal.core.data.DataTable;
import com.personal.dataanalyse.consts.FieldConsts;
import com.personal.dataanalyse.enums.AdditionalTypeEnum;
import com.personal.dataanalyse.enums.FieldDataTypeEnum;
import com.personal.dataconvert.ExportData2Html;

/**
 * DataRow报表装饰
 * @author cuibo
 *
 */
public class DataRowReportEx extends DataRow
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /** 列类型 */
    private String fieldType;
    
    /** 列名称 */
    private String fieldName;
    
    /** 数据类型 */
    private String dataType;
    
    /** 字段数据：用于汇总计算时使用 */
    private String fieldDataSql;
    
    /** 字段数据：用于汇总计算时使用 */
    private String newDataSql;
    
    /** 小数位 */
    private int dotNum;
    
    /** 是否显示 */
    private boolean display;
    
    /** 单位 */
    private String fieldUnit;

    /** 条件 */
    private List<ConditionInfo> conditions;
    
    /** 是否是表达式 */
    private boolean expType;
    
    /** 合计类型 */
    private String totalType;
    
    /** 计算方式:先汇总后计算，和先计算后汇总 */
    private String calculateType;
    
    /** cb2018 02 24 为了兼容一张表中既有普通参数，又有指标参数 */
    private String targetType;
    
    /** 宽度 */
    private int width;
    
    /** 水平居中方式 */
    private String align;
    
    /** 行号*/
    private int rowNum;
    
    /** 附加行类型 */
    private AdditionalTypeEnum additionalType;
    
    /** 是否匹配上了数据（用于移除未匹配的列） */
    private boolean matchData = false;
    
    /** 该行转换之前列 */
    private DataColumnReportEx columnReportEx;
    
    public int getRowNum()
    {
		return rowNum;
	}

	public void setRowNum(int rowNum) 
	{
		this.rowNum = rowNum;
	}

	public List<ConditionInfo> getConditions()
    {
        if (conditions == null)
        {
            conditions = new ArrayList<ConditionInfo>();
        }
        return conditions;
    }

    public void setConditions(List<ConditionInfo> conditions)
    {
        this.conditions = conditions;
    }

    /**
     * 字段数据：用于汇总计算时使用:普通列，计算列，列维度指标 公用
     * @return
     */
    public String getFieldDataSql()
    {
        return fieldDataSql;
    }

    /**
     * cb 扩大该方法的访问
     */
    public void initItemMap(DataTable table)
    {
        this.dataTable = table;
        initItemClass();
    }
    
    /**
     * 初始化TD的样式
     */
    protected void initItemClass() 
    {
        if (this.dataTable == null) 
        {
            return;
        }
        // 列号
        StringBuilder builder = new StringBuilder();
        for (DataColumn column : this.dataTable.getColumns())
        {
            DataColumnReportEx columnEx = (DataColumnReportEx)column;
            if (this.getItemMap().containsKey(column.getColumnName())) 
            {
                builder.setLength(0);
                builder.append(FieldConsts.DATA_CLASS).append(FieldConsts.STR_UNDERLINE).append(this.getRowNum())
                .append(FieldConsts.STR_UNDERLINE).append(columnEx.getColNum());
                this.getItemMap().put(columnEx.getColumnName() + ExportData2Html.TDCLASS, builder.toString());
            }
        }
    }

    /**
     * 字段数据：用于汇总计算时使用:普通列，计算列，列维度指标 公用
     * @param fieldDataSql
     */
    public void setFieldDataSql(String fieldDataSql)
    {
        this.fieldDataSql = fieldDataSql;
    }

    public String getFieldType()
    {
        return fieldType;
    }

    public void setFieldType(String fieldType)
    {
        this.fieldType = fieldType;
    }

    public String getFieldName()
    {
        return fieldName;
    }

    public void setFieldName(String fieldName)
    {
        this.fieldName = fieldName;
    }

    public String getDataType()
    {
        return dataType;
    }

    public void setDataType(String dataType)
    {
        this.dataType = dataType;
    }

    public int getDotNum()
    {
        return dotNum;
    }

    public void setDotNum(int dotNum)
    {
        this.dotNum = dotNum;
    }

    public boolean isDisplay()
    {
        return display;
    }

    public void setDisplay(boolean display)
    {
        this.display = display;
    }

    public String getFieldUnit()
    {
        return fieldUnit;
    }

    public void setFieldUnit(String fieldUnit)
    {
        this.fieldUnit = fieldUnit;
    }

    public boolean isExpType()
    {
        return expType;
    }

    public void setExpType(boolean expType)
    {
        this.expType = expType;
    }

    public String getTotalType()
    {
        return totalType;
    }

    public void setTotalType(String totalType)
    {
        this.totalType = totalType;
    }

    public int getWidth()
    {
        return width;
    }

    public void setWidth(int width)
    {
        this.width = width;
    }

    public String getAlign()
    {
        return align;
    }

    public void setAlign(String align)
    {
        this.align = align;
    }

    public String getCalculateType()
    {
        return calculateType;
    }

    public void setCalculateType(String calculateType)
    {
        this.calculateType = calculateType;
    }

    public AdditionalTypeEnum getAdditionalType()
    {
        return additionalType;
    }

    public void setAdditionalType(AdditionalTypeEnum additionalType)
    {
        this.additionalType = additionalType;
    }
    
    /**
     * 是否是附加行
     * @return
     */
    public boolean isAdditionalRow()
    {
        return additionalType != null;
    }

    public boolean isMatchData()
    {
        return matchData;
    }

    public void setMatchData(boolean matchData)
    {
        this.matchData = matchData;
    }

    public DataColumnReportEx getColumnReportEx()
    {
        return columnReportEx;
    }

    public void setColumnReportEx(DataColumnReportEx columnReportEx)
    {
        this.columnReportEx = columnReportEx;
    }

    public String getTargetType()
    {
        return targetType;
    }

    public void setTargetType(String targetType)
    {
        this.targetType = targetType;
    }
    
    public String getNewDataSql()
    {
        return newDataSql;
    }

    public void setNewDataSql(String newDataSql)
    {
        this.newDataSql = newDataSql;
    }

    public boolean isDate()
    {
        return FieldDataTypeEnum.日期.toString().equals(dataTable);
    }
    
}
