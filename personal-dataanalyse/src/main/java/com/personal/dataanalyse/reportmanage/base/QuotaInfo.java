package com.personal.dataanalyse.reportmanage.base;

import java.util.ArrayList;
import java.util.List;

import com.personal.core.bean.TwoTuple;
import com.personal.core.data.DataColumns;
import com.personal.core.utils.CoreUtil;
import com.personal.dataanalyse.consts.FieldConsts;
import com.personal.dataanalyse.reportmanage.entity.DataColumnReportEx;
import com.personal.dataanalyse.reportmanage.entity.ReportHeader;

/**
 * 分析指标
 * @author cuibo
 *
 */
public class QuotaInfo extends AnalyseFactorInfo
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * 获取单列对应的DataColumnCollection
     * 区分普通列或者计算列, 维度 , 即Model的第一层Modelfield调用该方法。
     * @return
     * @throws Exception
     */
    public TwoTuple<DataColumns, List<ReportHeader>> getDataColumnCollection() throws Exception
    {
        // 普通列和计算列直接返回单列
        DataColumns columnCollection = new DataColumns();
        DataColumnReportEx columnReportEx = this.toDataColumnReportEx();
        columnCollection.add(columnReportEx);
        List<ReportHeader> headerConfigs = new ArrayList<ReportHeader>();
        headerConfigs.add(columnReportEx.toHeaderConfig());
        TwoTuple<DataColumns, List<ReportHeader>> result = new TwoTuple<DataColumns, List<ReportHeader>>(columnCollection, headerConfigs);
        calSkipCountAndWidth(result);
        return result;
    }
    
    /**
     * 将自身信息转成DataColumnReportEx
     * @return
     * @throws Exception 
     */
    public DataColumnReportEx toDataColumnReportEx() throws Exception
    {
        // 如果该列是在维度下面。需要拼接维度信息，并且拼接条件
        DataColumnReportEx result = new DataColumnReportEx();
        result.setFieldType(this.getFieldType());
        result.setFieldName(this.getFieldName());
        result.setFieldUnit(this.getFieldUnit());
        result.setColumnLable(this.getFieldName());
        //result.setColumnName(this.getFieldDataSql());
        result.setColumnName(CoreUtil.newShortUuId());
        // 计算式
        result.setFieldDataSql(this.getFieldDataSql());
        result.setNewDataSql(this.getNewDataSql());
        result.setDotNum(this.getDotNum());
        result.setExpType(this.isExpType());
        result.setDataType(this.getDataType());
        result.setFieldUnit(this.getFieldUnit());
        
        // totalType
        result.setTotalType(this.getTotalType());
        // calculateType
        result.setCalculateType(this.getCalculateType());
        result.setTargetType(this.getTargetType());
        result.setShowOrder(this.getFieldOrder());
        
        result.setDisplay(this.isDisplay());
        
        // 指标值列需要加上样式
        if (isTargetValue())
        {
            result.setStyleClass(FieldConsts.TARGET_VALUE_CSS);
        }
        
        return result;
    }
    
    /** 列类型 */
    private String fieldType;
    
    /** 小数位 */
    private int dotNum;
    
    /** 求和方式 */
    private String totalType;

    /** 计算方式 */
    private String calculateType;
    
    /** 是否是指自动添加的标值列 */
    private boolean targetValue;
    
    /** cb2018 02 24 为了兼容一张表中既有普通参数，又有指标参数 */
    private String targetType;
    
    /**
     * 列类型：
     * 列维度指标：表示的添加的是维度下的指标列，这种列是出现在维度之下的反复出现的列
     * 普通列：表示添加的单独的一个列，可以添加到某个维度之下，但只会出现一次，和“列维度指标”是有区别的
     * 计算列：表示的是表格上的具体的某一列和某一列的计算关系列，这个关系表示是具体的字段关系，
     * 比如：一级维度时建管单位，二级维度电压等级，维度下是总投资、则计算列的关系表示可能是 海口.220.总投资-三亚.220.总投资
     * 比如：一级维度年份，维度下是总投资、则计算列的关系表示可能是 年份.总投资-(年份-1).总投资 
     * @return
     */
    public String getFieldType()
    {
        return fieldType;
    }

    /**
     * 列类型：
     * 列维度指标：表示的添加的是维度下的指标列，这种列是出现在维度之下的反复出现的列
     * 普通列：表示添加的单独的一个列，可以添加到某个维度之下，但只会出现一次，和“列维度指标”是有区别的
     * 计算列：表示的是表格上的具体的某一列和某一列的计算关系列，这个关系表示是具体的字段关系，
     * 比如：一级维度时建管单位，二级维度电压等级，维度下是总投资、则计算列的关系表示可能是 海口.220.总投资-三亚.220.总投资
     * 比如：一级维度年份，维度下是总投资、则计算列的关系表示可能是 年份.总投资-(年份-1).总投资 
     * @return
     */
    public void setFieldType(String fieldType)
    {
        this.fieldType = fieldType;
    }
    
    /**
     * 小数位
     * @return
     */
    public int getDotNum()
    {
        return dotNum;
    }

    /**
     * 小数位
     * @param width
     */
    public void setDotNum(int dotNum)
    {
        this.dotNum = dotNum;
    }
    
    /**
     * 计算方式原值、求和、平均、最大值、最小值
     * @return
     */
    public String getTotalType()
    {
        return totalType;
    }

    /**
     * 计算方式原值、求和、平均、最大值、最小值
     * @param alignType
     */
    public void setTotalType(String totalType)
    {
        this.totalType = totalType;
    }
    
    /**
     * 计算方式：先计算后汇总，先汇总后计算
     * @return
     */
    public String getCalculateType()
    {
        return calculateType;
    }

    /**
     * 计算方式：先计算后汇总，先汇总后计算
     * @param calculateType
     */
    public void setCalculateType(String calculateType)
    {
        this.calculateType = calculateType;
    }

    public boolean isTargetValue()
    {
        return targetValue;
    }

    public void setTargetValue(boolean targetValue)
    {
        this.targetValue = targetValue;
    }
    
    public String getTargetType()
    {
        return targetType;
    }

    public void setTargetType(String targetType)
    {
        this.targetType = targetType;
    }

    /**
     * 创建指标值列
     */
    public void createTargetValueQuotaInfo()
    {
        this.setFieldName(CoreUtil.isEmpty(model.getHeadTargetValue()) ? FieldConsts.TARGET_VALUE_COL : model.getHeadTargetValue());
        this.setFieldDataSql(FieldConsts.TARGET_VALUE_COL);
        this.setTargetValue(true);
    }
    
    
}
