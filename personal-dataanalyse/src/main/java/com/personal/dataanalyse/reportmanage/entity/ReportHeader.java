package com.personal.dataanalyse.reportmanage.entity;

import com.personal.dataanalyse.enums.ModelFieldTypeEnum;
import com.personal.dataconvert.bean.HeaderConfig;

/**
 * 报表头部配置
 * @author cuibo
 *
 */
public class ReportHeader extends HeaderConfig
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /** 列类型 */
    private String fieldType;
    
    /** 列单位 */
    private String fieldUnit;
    
    /** 表头是否添加单位 */
    private boolean headerAddUnit = true;
    
    public ReportHeader()
    {
        super();
    }
    
    /**
     * 子节点是否有指标列（包括普通列，计算列，列维度指标）
     * @return
     */
    public boolean hasQuotaHeader()
    {
        if (getChildren() == null || getChildren().isEmpty())
        {
            return false;
        }
        for (HeaderConfig headerConfig : getChildren())
        {
            if (!(headerConfig instanceof ReportHeader))
            {
                continue;
            }
            ReportHeader header = (ReportHeader)headerConfig;
            if (ModelFieldTypeEnum.isCommonOrCalOrGroupField(header.getFieldType()))
            {
                return true;
            }
            // 递归子节点
            boolean result = header.hasQuotaHeader();
            if (result)
            {
                return result;
            }
        }
        return false;
    }

    public ReportHeader(String displayName, int deepLength, HeaderConfig parent, String path, String style, int width)
    {
        super(displayName, deepLength, parent, path, style, width);
    }

    public ReportHeader(String displayName, String value, int deepLength, String path, String style, int width)
    {
        super(displayName, value, deepLength, path, style, width);
    }
    
    public String getFieldType()
    {
        return fieldType;
    }

    public void setFieldType(String fieldType)
    {
        this.fieldType = fieldType;
    }

    public String getFieldUnit()
    {
        return fieldUnit;
    }

    public void setFieldUnit(String fieldUnit)
    {
        this.fieldUnit = fieldUnit;
    }

    public boolean isHeaderAddUnit()
    {
        return headerAddUnit;
    }

    public void setHeaderAddUnit(boolean headerAddUnit)
    {
        this.headerAddUnit = headerAddUnit;
    }
    
    
}
