package com.personal.dataanalyse.reportmanage.model;

import com.personal.dataanalyse.reportmanage.base.DimensionInfo;
import com.personal.dataanalyse.reportmanage.entity.DataColumnReportEx;

/**
 * 报表维度
 * @author cuibo
 *
 */
public class ReportDimension extends DimensionInfo
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /**
     * 将自身信息转成DataColumnReportEx
     * @return
     * @throws Exception 
     */
    @Override
    public DataColumnReportEx toDataColumnReportEx() throws Exception
    {
        // 如果该列是在维度下面。需要拼接维度信息，并且拼接条件
        DataColumnReportEx result = super.toDataColumnReportEx();
        
        result.setAlign(this.getAlign());
        result.setWidth(this.getWidth());
        
        return result;
    }
    
    private String align;
  
    private int width;
    
    public String getAlign()
    {
        return align;
    }

    public void setAlign(String align)
    {
        this.align = align;
    }

    public int getWidth()
    {
        return width;
    }

    public void setWidth(int width)
    {
        this.width = width;
    }


    
}
