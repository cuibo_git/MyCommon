package com.personal.dataanalyse.reportmanage.builder;

import com.personal.core.data.DataTable;
import com.personal.dataanalyse.reportmanage.base.DataAnalyseModel;

/**
 * 模型填充
 * @author cuibo
 *
 */
public interface ModelFiller<T extends DataAnalyseModel>
{
   /**
    * 设置结构DataTable
    * @param structTable
    */
    public void setStructTable(DataTable structTable);

    /**
     * 设置数据源DataTable
     * @param sourceTable
     */
    public void setSourceTable(DataTable sourceTable);

    /**
     * 设置分析模型
     * @param model
     */
    public void setModel(T model);
    
    /**
     * 填充操作实现
     * @throws Exception
     */
    public void fill() throws Exception;
    
}
