package com.personal.dataanalyse.reportmanage.model;

import java.util.Set;

import com.personal.core.data.DataTable;
import com.personal.dataanalyse.reportmanage.base.DataAnalyseModel;

/**
 * 数据库模型
 * @author cuibo
 *
 */
public class DbModel extends DataAnalyseModel
{

    /**
     * 
     */
    private static final long serialVersionUID = 6158030091798087891L;
    
    // 数据库相关信息

    @Override
    protected DataTable getDataByDataSource(Set<String> queryTargetNames) throws Exception
    {
        return null;
    }

}