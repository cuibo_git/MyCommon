package com.personal.dataanalyse.reportmanage.model;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.personal.core.data.DataTable;
import com.personal.core.utils.Assert;
import com.personal.core.utils.CoreUtil;
import com.personal.dataanalyse.consts.FieldConsts;
import com.personal.dataconvert.PieEchart;
import com.personal.dataconvert.util.ExcelHtmlUtil;



/**
 * 饼图模型
 * @author cuibo
 *
 */
public class PieEChartModel extends EChartModel
{
    
    /**
     * 
     */
    private static final long serialVersionUID = 6158030091798087891L;
    
    @Override
    public String toChartData(boolean animation, int width) throws Exception
    {
        // 如果没有数据，则从模型中获取
        String[] xGroupsArr = CoreUtil.split(xGroupData, FieldConsts.LEVELONESPLIT);
        Assert.isNotNullOrEmpty(xGroupsArr, "图表横向维度信息为空，图表定义信息不合法！");
        String[] fieldDatas = CoreUtil.split(fieldDataSql, FieldConsts.LEVELONESPLIT);
        Assert.isNotNullOrEmpty(fieldDatas, "图表指标信息为空，图表定义信息不合法！");
        String[] fieldDataNames = CoreUtil.split(fieldData, FieldConsts.LEVELONESPLIT);
        Assert.isNotNullOrEmpty(fieldDataNames, "图表指标信息为空，图表定义信息不合法！");
        if (fieldDatas.length != fieldDataNames.length)
        {
            throw new Exception("fieldData和fieldDataSql个数不匹配，图表定义信息不合法！");
        }
        // 如果有合计，则需要加上合计
        if (!CoreUtil.isEmpty(totalName))
        {
            xGroupsArr = CoreUtil.concat(xGroupsArr, new String[]{totalName});
        }
        
        List<Groups> xGroups = toGroups(xGroupsArr);
        Map<String, String> allMatchData = loadNoYGroupChartData(xGroups, fieldDataNames, fieldDatas);
        
        if (allMatchData == null)
        {
            allMatchData = new HashMap<String, String>();
        }
        // 饼图控制，xGroups和fieldDatas肯定有一个的长度为 1
        
        Map<String, String> pieData = new LinkedHashMap<String, String>();
        if (xGroups.size() > 1)
        {
            for (Groups x : xGroups)
            {
                if (removeNoMatchRowCol && !x.match)
                {
                    continue;
                }
                String key = x.value + ExcelHtmlUtil.REPLACEPOINTFLAG + fieldDatas[0];
                pieData.put(x.value, allMatchData.get(key));
            }
        } else
        {
            int index = 0;
            for (String field : fieldDatas)
            {
                String key =  xGroups.get(0).value + ExcelHtmlUtil.REPLACEPOINTFLAG + field;
                pieData.put(fieldDataNames[index++], allMatchData.get(key));
            }
        }
        // 构建饼模型
        PieEchart echart = new PieEchart(chartName, chartSubName);
        echart.setValues(pieData);
        echart.setAnimation(animation);
        echart.setWidth(width);
        return echart.toEchartData();
    }
    
    public PieEChartModel()
    {
        super();
    }

    public PieEChartModel(DataTable table)
    {
        super(table);
    }

}