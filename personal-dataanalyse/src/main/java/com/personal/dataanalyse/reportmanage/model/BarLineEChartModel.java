package com.personal.dataanalyse.reportmanage.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.personal.core.data.DataTable;
import com.personal.core.utils.Assert;
import com.personal.core.utils.CoreUtil;
import com.personal.dataanalyse.consts.FieldConsts;
import com.personal.dataconvert.BarLineEchart;
import com.personal.dataconvert.BarLineEchart.XValue;
import com.personal.dataconvert.util.ExcelHtmlUtil;



/**
 * 柱状折线图模型
 * @author cuibo
 *
 */
public class BarLineEChartModel extends EChartModel
{
    /**
     * 
     */
    private static final long serialVersionUID = 6158030091798087891L;
    
    /** 带有平均线 */
    private boolean avg = true;
    
    /** 带有最值线 */
    private boolean minMax = true;
    
    /** 显示类型 */
    private String lineType;
    
    /** Y轴显示名 */
    private String yDisplayName;
    
    
    @Override
    public String toChartData(boolean animation, int width) throws Exception
    {
        // 如果没有数据，则从模型中获取
        String[] xGroupsArr = CoreUtil.split(xGroupData, FieldConsts.LEVELONESPLIT);
        Assert.isNotNullOrEmpty(xGroupsArr, "图表横向维度信息为空，图表定义信息不合法！");
        String[] fieldDatas = CoreUtil.split(fieldDataSql, FieldConsts.LEVELONESPLIT);
        Assert.isNotNullOrEmpty(fieldDatas, "图表指标信息为空，图表定义信息不合法！");
        String[] fieldDataNames = CoreUtil.split(fieldData, FieldConsts.LEVELONESPLIT);
        Assert.isNotNullOrEmpty(fieldDataNames, "图表指标信息为空，图表定义信息不合法！");
        if (fieldDatas.length != fieldDataNames.length)
        {
            throw new Exception("fieldData和fieldDataSql个数不匹配，图表定义信息不合法！");
        }
        String[] lineTypes = CoreUtil.split(lineType, FieldConsts.LEVELONESPLIT);
        // 默认都是 bar
        if (lineTypes == null || lineTypes.length == 0)
        {
            lineTypes = new String[fieldDatas.length];
            for (int i = 0; i < fieldDatas.length; i++)
            {
                lineTypes[i] = "bar";
            }
        } else
        {
            if (lineTypes.length != fieldDataNames.length)
            {
                throw new Exception("lineType和fieldDataSql个数不匹配，图表定义信息不合法！");
            }
        }
        // 如果有合计，则需要加上合计
        if (!CoreUtil.isEmpty(totalName))
        {
            xGroupsArr = CoreUtil.concat(xGroupsArr, new String[]{totalName});
        }
        // 转成Groups
        List<Groups> xGroups = toGroups(xGroupsArr);
        
        String[] yGroupsArr = CoreUtil.split(yGroupData, FieldConsts.LEVELONESPLIT);
        List<Groups> yGroups = toGroups(yGroupsArr);
        Map<String, String> allMatchData = null;
        // 没有定义纵向维度
        if (yGroups == null || yGroups.isEmpty())
        {
            allMatchData = loadNoYGroupChartData(xGroups, fieldDataNames, fieldDatas);
        } else
        {
            // 定义了纵向维度
            allMatchData = loadHasYGroupChartData(xGroups, yGroups, fieldDataNames, fieldDatas);
        }
        if (allMatchData == null)
        {
            allMatchData = new HashMap<String, String>();
        }
        BarLineEchart echart = createBarLineEchart(xGroups, yGroups, fieldDatas, fieldDataNames, lineTypes, allMatchData, animation);
        echart.setWidth(width);
        return echart.toEchartData();
    }
    
    /**
     * 构建BarLineEchart模型
     * @param xGroups
     * @param yGroups
     * @param fieldDatas
     * @param fieldDataNames
     * @param lineTypes
     * @param allMatchData
     * @param animation
     * @return
     */
    private BarLineEchart createBarLineEchart(List<Groups> xGroups, List<Groups> yGroups, String[] fieldDatas,
            String[] fieldDataNames, String[] lineTypes, Map<String, String> allMatchData, boolean animation)
    {
        BarLineEchart result = new BarLineEchart(chartName, chartSubName);
        List<BarLineEchart.YValue> yValues = null;
        // 没有定义纵向维度
        if (yGroups == null || yGroups.isEmpty())
        {
            yValues = new ArrayList<BarLineEchart.YValue>();
            for (int i = 0; i < fieldDataNames.length; i++)
            {
                String fieldDataName = fieldDataNames[i];
                String fieldData = fieldDatas[i];
                BarLineEchart.YValue yValue = result.new YValue();
                yValue.setKey(fieldData);
                yValue.setName(CoreUtil.isEmpty(yGroupUnit) ? fieldDataName : fieldDataName + yGroupUnit);
                yValue.setType(lineTypes[i]);
                yValues.add(yValue);
            }
        } else
        {
            // 定义了纵向维度
            yValues = new ArrayList<BarLineEchart.YValue>();
            for (Groups yGroup : yGroups)
            {
                // 不匹配的不展示
                if (removeNoMatchRowCol && !yGroup.match)
                {
                    continue;
                }
                for (int i = 0; i < fieldDataNames.length; i++)
                {
                    // String fieldDataName = fieldDataNames[i];
                    String fieldData = fieldDatas[i];
                    BarLineEchart.YValue yValue = result.new YValue();
                    // 指标key
                    yValue.setKey(yGroup.value + ExcelHtmlUtil.REPLACEPOINTFLAG + fieldData);
                    // 显示名
                    // yValue.setName(yGroup.value + "." + fieldDataName);
                    // cb 2017 12 01 修改为只显示维度名
                    yValue.setName(CoreUtil.isEmpty(yGroupUnit) ? yGroup.value : yGroup.value + yGroupUnit);
                    yValue.setType(lineTypes[i]);
                    yValues.add(yValue);
                }
            }
        }
        
        result.setText(chartName);
        result.setSubText(chartSubName);
        result.setAvg(avg);
        result.setMinmax(minMax);
        result.setyUnit(chartUnit);
        result.setyName(yDisplayName);
        result.setxValues(toXListValues(xGroups, xGroupUnit, result));
        result.setValues(allMatchData);
        result.setyValues(yValues);
        result.setAnimation(animation);
        result.setShowLabel(showLabel);
        result.setWidth(width);
        return result;
    }
    
    /**
     * 将维度转成字符串集合（不匹配的不展示）
     * @param groups
     * @param unit
     * @return
     */
    private List<XValue> toXListValues(List<Groups> groups, String unit, BarLineEchart barLine)
    {
        if (CoreUtil.isEmpty(groups))
        {
            return null;
        }
        List<XValue> result = new ArrayList<XValue>();
        for (Groups group : groups)
        {
            if (removeNoMatchRowCol && !group.match)
            {
                continue;
            }
            result.add(barLine.new XValue(group.value, CoreUtil.isEmpty(unit) ? group.value : group.value + unit));
        }
        return result;
    }

    public BarLineEChartModel()
    {
        super();
    }

    public BarLineEChartModel(DataTable table)
    {
        super(table);
    }

    public String getLineType()
    {
        return lineType;
    }

    public void setLineType(String lineType)
    {
        this.lineType = lineType;
    }

    public boolean isAvg()
    {
        return avg;
    }

    public void setAvg(boolean avg)
    {
        this.avg = avg;
    }

    public boolean isMinMax()
    {
        return minMax;
    }

    public void setMinMax(boolean minMax)
    {
        this.minMax = minMax;
    }

    public String getyDisplayName()
    {
        return yDisplayName;
    }

    public void setyDisplayName(String yDisplayName)
    {
        this.yDisplayName = yDisplayName;
    }
    
    
    
}