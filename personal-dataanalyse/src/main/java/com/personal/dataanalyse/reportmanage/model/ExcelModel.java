package com.personal.dataanalyse.reportmanage.model;

import java.util.Set;

import com.personal.core.data.DataTable;
import com.personal.dataanalyse.reportmanage.base.DataAnalyseModel;

/**
 * Excel模型
 * @author cuibo
 *
 */
public class ExcelModel extends DataAnalyseModel
{

    /**
     * 
     */
    private static final long serialVersionUID = 6158030091798087891L;
    
    // Excel资源相关信息

    @Override
    protected DataTable getDataByDataSource(Set<String> queryTargetNames) throws Exception
    {
        return null;
    }

}