package com.personal.dataanalyse.reportmanage.base;

import java.io.Serializable;

import com.personal.core.data.DataTable;

/**
 * 基础图表模型
 * @author cuibo
 *
 */
public abstract class DataChartModel implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /** 图表的ID */
    protected String id;
    
    /** 名称  */
    protected String chartName;
    
    /** 类型 */
    protected String charType;
    
    /** 二维表转图表数据 */
    public String toChartData(boolean animation) throws Exception
    {
        return toChartData(animation, -1);
    }
    
    /** 二维表转图表数据 */
    public abstract String toChartData(boolean animation, int width) throws Exception;
    
    /** 注入数据源 */
    public abstract void setDataTable(DataTable table);
    
    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getChartName()
    {
        return chartName;
    }

    public void setChartName(String chartName)
    {
        this.chartName = chartName;
    }

    public String getCharType()
    {
        return charType;
    }

    public void setCharType(String charType)
    {
        this.charType = charType;
    }
    
}