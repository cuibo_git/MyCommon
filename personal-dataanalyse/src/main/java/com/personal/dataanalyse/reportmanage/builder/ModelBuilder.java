package com.personal.dataanalyse.reportmanage.builder;

import java.util.List;

import com.personal.core.data.DataTable;
import com.personal.dataanalyse.reportmanage.base.DataAnalyseModel;
import com.personal.dataanalyse.reportmanage.entity.ReportHeader;
import com.personal.dataconvert.bean.HtmlConfig;
import com.personal.dataconvert.bean.WorkBookConfig;
import com.personal.dataconvert.port.DataBuilder;

/**
 * 模型构建
 * @author cuibo
 *
 */
public interface ModelBuilder extends DataBuilder
{
    /**
     * 生成表头Html
     * @return
     * @throws Exception
     */
    public String buildHeaderAsHtml() throws Exception;
    
    /**
     * 构建Html
     * @param withTitle  是否在html中增加title，生产doc文档时，必须没有title，否则word生成会报错。
     * @return
     * @throws Exception
     */
    public String buildAsHtml(boolean withTitle) throws Exception;

    /**
     * 生成Json
     * @return
     * @throws Exception
     */
    public String buildAsJson() throws Exception;
    
    /**
     * 获取指定名称的图表数据
     * @param chartId
     * @param animation 是否有动画效果
     * @return
     * @throws Exception
     */
    public String buildChartData(String chartId, boolean animation) throws Exception;
    
    /**
     * 报表报表结果
     * @return
     * @throws Exception
     */
    public byte[] exportData() throws Exception;
    
    /**
     * 直接注入结果数据
     * @param table
     * @param headerConfigs
     * @throws Exception
     */
    public void setResultData(DataTable table, List<ReportHeader> headerConfigs) throws Exception;
    
    /**
     * 注入分析模型
     * @param model
     */
    public void setModel(DataAnalyseModel model);

    /**
     * 获取分析模型
     * @return
     */
    public DataAnalyseModel getModel();

    /**
     * 创建Html配置
     * @return
     * @throws Exception
     */
    public HtmlConfig createHtmlConfig() throws Exception;

    /**
     * 创建WorkBookConfig
     * @return
     * @throws Exception
     */
    public WorkBookConfig createWorkBookConfig() throws Exception;
    
}
