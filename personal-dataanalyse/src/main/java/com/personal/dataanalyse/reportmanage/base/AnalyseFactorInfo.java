package com.personal.dataanalyse.reportmanage.base;

import java.io.Serializable;
import java.util.List;

import com.personal.core.bean.TwoTuple;
import com.personal.core.data.DataColumn;
import com.personal.core.data.DataColumns;
import com.personal.dataanalyse.enums.XyTypeEnum;
import com.personal.dataanalyse.reportmanage.entity.ConditionInfo;
import com.personal.dataanalyse.reportmanage.entity.DataColumnReportEx;
import com.personal.dataanalyse.reportmanage.entity.ReportHeader;
import com.personal.dataconvert.bean.HeaderConfig;
import com.personal.dataconvert.util.ExcelHtmlUtil;

/**
 * 分析因子
 * @author cuibo
 *
 */
public abstract class AnalyseFactorInfo implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = -2212344509698036141L;
    
    /** 所属报表 */
    protected DataAnalyseModel model;

    /** 字段名称 */
    protected String fieldName;

    /** 字段数据类型 */
    protected String dataType;

    /** 字段单位 */
    protected String fieldUnit;

    /** 字段数据 */
    protected String fieldData;

    /** 字段表达式 */
    protected String fieldDataSql;
    
    /** 字段表达式 */
    protected String newDataSql;

    /** 排序*/
    protected long fieldOrder;

    /** 是否是表达式 */
    protected boolean expType;
    
    /** xy */
    private XyTypeEnum xy;
    
    /** 是否显示 */
    protected boolean display = true;

    /**
     * 抽象方法将自身转成二维列和表头结构
     * @return
     * @throws Exception
     */
    public abstract TwoTuple<DataColumns, List<ReportHeader>> getDataColumnCollection() throws Exception;

    /**
     * 计算跳跃数并计算所有父亲HeaderConfig的宽度
     * @param result
     * @return
     * @throws Exception
     */
    protected void calSkipCountAndWidth(TwoTuple<DataColumns, List<ReportHeader>> result) throws Exception
    {
        // 计算跳跃数
        if (result == null)
        {
            return;
        }
        DataColumns columnCollection = result.getA();
        List<ReportHeader> headerConfigs = result.getB();
        if (columnCollection == null || columnCollection.isEmpty())
        {
            return;
        }
        // 计算跳跃数
        calSkipCount(columnCollection);
        // 计算宽度
        ExcelHtmlUtil.calculateWidth(headerConfigs, null);
    }

    /**
     * 计算DataColumnCollection中每个DataColumn的跳跃数
     * @param columnCollection
     * @throws Exception
     */
    private static void calSkipCount(DataColumns columnCollection) throws Exception
    {
        DataColumnReportEx columnEx = null;
        for (DataColumn dataColumn : columnCollection)
        {
            columnEx = (DataColumnReportEx) dataColumn;
            if (columnEx.getConditions() == null || columnEx.getConditions().isEmpty())
            {
                continue;
            }
            for (ConditionInfo conditionInfo : columnEx.getConditions())
            {
                ReportHeader config = conditionInfo.getHeaderConfig();
                if (config == null)
                {
                    continue;
                }
                List<? extends HeaderConfig> leafs = config.getLeafHeader();
                if (leafs == null || leafs.isEmpty())
                {
                    continue;
                }
                conditionInfo.setSkipCount(leafs.size());
            }
        }
    }

    public String getFieldName()
    {
        return fieldName;
    }

    public String getFieldUnit()
    {
        return fieldUnit;
    }

    public String getFieldData()
    {
        return fieldData;
    }

    public String getFieldDataSql()
    {
        return fieldDataSql;
    }

    public long getFieldOrder()
    {
        return fieldOrder;
    }

    public boolean isExpType()
    {
        return expType;
    }

    public void setExpType(boolean expType)
    {
        this.expType = expType;
    }

    public void setFieldName(String fieldName)
    {
        this.fieldName = fieldName;
    }

    public void setFieldUnit(String fieldUnit)
    {
        this.fieldUnit = fieldUnit;
    }

    public void setFieldData(String fieldData)
    {
        this.fieldData = fieldData;
    }

    public void setFieldDataSql(String fieldDataSql)
    {
        this.fieldDataSql = fieldDataSql;
    }

    public void setFieldOrder(long fieldOrder)
    {
        this.fieldOrder = fieldOrder;
    }

    public DataAnalyseModel getModel()
    {
        return model;
    }

    public void setModel(DataAnalyseModel model)
    {
        this.model = model;
    }

    public boolean isDisplay()
    {
        return display;
    }

    public void setDisplay(boolean display)
    {
        this.display = display;
    }

    public XyTypeEnum getXy()
    {
        return xy;
    }

    public void setXy(XyTypeEnum xy)
    {
        this.xy = xy;
    }

    public String getNewDataSql()
    {
        return newDataSql;
    }

    public void setNewDataSql(String newDataSql)
    {
        this.newDataSql = newDataSql;
    }

    public String getDataType()
    {
        return dataType;
    }

    public void setDataType(String dataType)
    {
        this.dataType = dataType;
    }
    
}
