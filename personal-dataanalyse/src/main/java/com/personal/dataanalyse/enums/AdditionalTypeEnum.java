package com.personal.dataanalyse.enums;

import java.util.Map;

import com.personal.core.bean.TwoTuple;
import com.personal.core.data.DataRow;
import com.personal.core.data.DataTable;
import com.personal.core.utils.CoreUtil;
import com.personal.core.utils.ReGularUtil;
import com.personal.core.utils.StringUtil;
import com.personal.dataanalyse.reportmanage.entity.DataColumnReportEx;
import com.personal.dataanalyse.reportmanage.entity.DataRowReportEx;
import com.personal.dataanalyse.reportmanage.util.ModelUtil;

/**
 * 附加行或者附加列类型
 * @author cuibo
 *
 */
public enum AdditionalTypeEnum
{
    求和, 求平均, 求最大值, 求最小值;

    /**
     * 填充附加行
     * @param result
     * @param additionalType
     * @param columnEx
     * @param newRow
     * @param twoTuple
     */
    public static void fillAdditionalRow(DataTable result, AdditionalTypeEnum additionalType, DataColumnReportEx columnEx,
            DataRowReportEx newRow, TwoTuple<Integer, Map<String, Object>> twoTuple)
    {
        // 计算列最后计算处理
        if (ModelFieldTypeEnum.计算列.toString().equals(columnEx.getFieldType()))
        {
            return;
        }
        switch (additionalType)
        {
        case 求和: case 求平均:
            // 只有是先汇总后计算的表达式，才需要计算每个细项的汇总，然后再按照表达式计算
            if (CalculateTypeEnum.先汇总后计算.toString().equals(columnEx.getCalculateType()))
            {
                String fieldDataSql = columnEx.getFieldDataSql();
                String[] arr = ReGularUtil.EXPANDKHPATTERNANDABS.split(fieldDataSql);
                if (arr == null || arr.length == 0)
                {
                    return;
                }
                for (String single : arr)
                {
                    if (!ModelUtil.checkLegalFieldDataSql(single))
                    {
                        continue;
                    }
                    fieldDataSql = StringUtil.replaceFirst(fieldDataSql, single, columnEx.getColumnName() + single + TotalTypeEnum.求和.toString());
                }
                newRow.setValue(columnEx, CoreUtil.calculateExByAviWithAbs(fieldDataSql, twoTuple.getB()));
            } else
            {
                // 计算和
                Object hj = null;
                for (DataRow row : result.getRows())
                {
                    DataRowReportEx rowReportEx = (DataRowReportEx)row;
                    if (rowReportEx.isAdditionalRow())
                    {
                        continue;
                    }
                    hj = CoreUtil.addWithNull(hj, row.getItemMap().get(columnEx.getColumnName()));
                }
                newRow.setValue(columnEx, hj);
            }
            // 计算平均值
            if (求平均 == additionalType)
            {
                newRow.setValue(columnEx, CoreUtil.divideWithNull(newRow.getItemMap().get(columnEx.getColumnName()), twoTuple.getA()));
            }
            break;
        case 求最大值:
            for (DataRow row : result.getRows())
            {
                DataRowReportEx rowReportEx = (DataRowReportEx)row;
                if (rowReportEx.isAdditionalRow())
                {
                    continue;
                }
                if (CoreUtil.compareNumber(row.getItemMap().get(columnEx.getColumnName()), newRow.getItemMap().get(columnEx.getColumnName()) == null ? Double.MIN_VALUE : newRow.getItemMap().get(columnEx.getColumnName())) >= 0)
                {
                    newRow.setValue(columnEx, row.getItemMap().get(columnEx.getColumnName()));
                }
            }
            break;
        case 求最小值:
            for (DataRow row : result.getRows())
            {
                DataRowReportEx rowReportEx = (DataRowReportEx)row;
                if (rowReportEx.isAdditionalRow())
                {
                    continue;
                }
                if (CoreUtil.compareNumber(row.getItemMap().get(columnEx.getColumnName()), newRow.getItemMap().get(columnEx.getColumnName()) == null ? Double.MAX_VALUE : newRow.getItemMap().get(columnEx.getColumnName())) <= 0)
                {
                    newRow.setValue(columnEx, row.getItemMap().get(columnEx.getColumnName()));
                }
            }
            break;
        }
    }

}
