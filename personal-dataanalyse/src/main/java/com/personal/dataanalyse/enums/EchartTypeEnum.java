package com.personal.dataanalyse.enums;



/**
 * Echart类型
 * @author cuibo
 *
 */
public enum EchartTypeEnum
{
    柱状图,
    折线图,
    折柱图,    
    饼图,
    多维柱图;
    
    
}
