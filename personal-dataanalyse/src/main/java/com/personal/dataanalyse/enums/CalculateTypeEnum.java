package com.personal.dataanalyse.enums;


/**
 * 计算类型
 * @author cuibo
 *
 */
public enum CalculateTypeEnum
{
    先汇总后计算,
    先计算后汇总;
    
}
