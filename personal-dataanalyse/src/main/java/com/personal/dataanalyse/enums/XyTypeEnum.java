package com.personal.dataanalyse.enums;


/**
 * 模型字段或维度的X方向、Y方向
 *
 */
public enum XyTypeEnum
{
    X,Y;
    
    
    public static String getOther(String xy)
    {
    	if (X.toString().equals(xy))
    	{
    		return Y.toString();
    	}
    	else
    	{
    		return X.toString();
    	}
    }

}
