package com.personal.dataanalyse.enums;


/**
 * 折柱图的柱、线类型
 *
 */
public enum ChartLineTypeEnum
{
    柱("bar"),
    线("line");
    
    
    private ChartLineTypeEnum()
    {
    	
    }
    
    private ChartLineTypeEnum(String value)
    {
    	this.value = value;
    }
    private String value;
    
	public String getValue() 
	{
		return value;
	}
	
	
}
