package com.personal.dataanalyse.enums;

/**
 * 指标字段类型
 * @author cuibo
 *
 */
public enum ModelFieldTypeEnum
{
    列维度指标("表示的添加的是维度下的指标列，这种列是出现在维度之下的反复出现的列"),
    普通列("表示添加的单独的一个列，直接挂在报表下。"),
    计算列("示的是表格上的具体的某一列和某一列的计算关系列，这个关系表示是具体的字段关系，可能出现在维度下，也可以出现在报表下");
    
    
    /**
     * 是否是普通列或者列维度指标
     * @param fieldType
     * @return
     */
    public static boolean isCommonOrGroupField(String fieldType)
    {
        return 普通列.toString().equals(fieldType) || 列维度指标.toString().equals(fieldType);
    }
    
    /**
     * 是否是普通列或者计算列或者列维度指标
     * @param fieldType
     * @return
     */
    public static boolean isCommonOrCalOrGroupField(String fieldType)
    {
        return 普通列.toString().equals(fieldType) || 计算列.toString().equals(fieldType) || 列维度指标.toString().equals(fieldType);
    }
    
    /**
     * 是否是计算列
     * @param fieldType
     * @return
     */
    public static boolean isCalField(String fieldType)
    {
        return 计算列.toString().equals(fieldType);
    }
    
    /**
     * 是否是需要查询指标列的数据
     * @param fieldType
     * @return
     */
    public static boolean isQueryTargetNamesField(String fieldType)
    {
        return 普通列.toString().equals(fieldType) || 列维度指标.toString().equals(fieldType);
    }
    
    private ModelFieldTypeEnum(String remark)
    {
    	
    }
}
