package com.personal.dataanalyse.enums;

import com.personal.core.utils.CoreUtil;

/**
 * 数据类型
 * @author cuibo
 *
 */
public enum FieldDataTypeEnum
{
    数值 {
        @Override
        public boolean validateObjectType(Object obj)
        {
            return CoreUtil.isEmpty(obj) || CoreUtil.isNumber(obj);
        }
    },字符串 {
        @Override
        public boolean validateObjectType(Object obj)
        {
            return true;
        }
    },日期 {
        @Override
        public boolean validateObjectType(Object obj)
        {
            return CoreUtil.parseDate(obj) != null;
        }
    };
    
    /**
     * 校验obj的类型
     * @param obj
     * @return
     */
    public abstract boolean validateObjectType(Object obj);
}
