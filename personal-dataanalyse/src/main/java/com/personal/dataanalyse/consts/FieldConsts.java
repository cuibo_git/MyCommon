package com.personal.dataanalyse.consts;

/**
 * 字段分割
 * @author cuibo
 *
 */
public final class FieldConsts 
{
	private FieldConsts()
	{
	}
    
	/** 第一层分割(维度和维度之间分割符号) */    
    public static final String LEVELONESPLIT = "@1SP@";
    
    /** 第二层分割(维度内部条件分隔符) */
    public static final String LEVELTWOSPLIT = "@2SP@";
    
    /** 第三层分割(维度条件值之间的分隔符) */
    public static final String LEVELTHREESPLIT = "@3SP@";
    
    /** 大于 */
    public static final String GT = ">";
    public static final String LT = "<";
    public static final String GTEQ = ">=";
    public static final String LTEQ = "<=";
    public static final String EQ = "==";
    public static final String NE = "!=";
    
    /** 指标列 */
    public static final String TARGET_COL = "指标";
    public static final int TARGET_WIDTH = 120;
    public static final String TARGET_CSS = "targetcolumnclass";
    
    /** 指标值列 */
    public static final String TARGET_VALUE_COL = "指标值";
    public static final int TARGET_VALUE_WIDTH = 60;
    public static final String TARGET_VALUE_CSS = "targetvalueclass";
    
    /** rowCache */
    public static final String STR_CACHE = "_CACHE";
    
    /** 匹配的rowCount */
    public static final String STR_COUNT = "_COUNT";
    
    
    /**数据样式*/
    public static final String DATA_CLASS = "datatd";
    /**用户表格数据编辑时，输入框样式串联分隔符*/
    public static final String STR_UNDERLINE = "_";
    /**维度和字段的样式*/
    public static final String HEAD_CLASS = "headtd";
    
    //  附加行列的样式
    public static final String ADDITIONAL_ROW_COL = "additionalRowCol";
	
}
