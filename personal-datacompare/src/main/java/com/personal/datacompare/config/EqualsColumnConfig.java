package com.personal.datacompare.config;

import java.util.Set;

/**
 * 判断相等列
 * @author cuibo
 *
 */
public class EqualsColumnConfig extends ColumnConfig
{
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    // 差异原因的三列配置和对比的三列配置一一对应
    /** 差异原因列 */
    private String diffColumnName;
    /** 差异原因列 */
    private Set<String> diffOtherColumnNames;
    /** 差异原因列 */
    private Set<String> diffAdditiveColumnNames;
    /** 其它列名（columnName匹配上或者其他列匹配上） */
    private Set<String> otherColumnNames;
    /** 附加列名称（columnName匹配上并且附加列匹配上） */
    private Set<String> additiveColumnNames;
    /** 相似度 */
    private int similarity = 100;
    /** 其他列的相似度 */
    private int otherColSimilarity = 100;
    /** 附加列的相似度 */
    private int additiveColSimilarity = 100;

    public EqualsColumnConfig()
    {
        super();
    }

    public EqualsColumnConfig(String columnName, String columnLabel)
    {
        super(columnName, columnLabel);
    }

    public int getAdditiveColSimilarity()
    {
        return additiveColSimilarity;
    }

    public Set<String> getAdditiveColumnNames()
    {
        return additiveColumnNames;
    }

    public Set<String> getDiffAdditiveColumnNames()
    {
        return diffAdditiveColumnNames;
    }

    public String getDiffColumnName()
    {
        return diffColumnName;
    }

    public Set<String> getDiffOtherColumnNames()
    {
        return diffOtherColumnNames;
    }

    public int getOtherColSimilarity()
    {
        return otherColSimilarity;
    }

    public Set<String> getOtherColumnNames()
    {
        return otherColumnNames;
    }

    public int getSimilarity()
    {
        return similarity;
    }

    public void setAdditiveColSimilarity(int additiveColSimilarity)
    {
        if (additiveColSimilarity <= 0 || additiveColSimilarity > 100)
        {
            additiveColSimilarity = 100;
        }
        this.additiveColSimilarity = additiveColSimilarity;
    }

    public void setAdditiveColumnNames(Set<String> additiveColumnNames)
    {
        this.additiveColumnNames = additiveColumnNames;
    }

    public void setDiffAdditiveColumnNames(Set<String> diffAdditiveColumnNames)
    {
        this.diffAdditiveColumnNames = diffAdditiveColumnNames;
    }

    public void setDiffColumnName(String diffColumnName)
    {
        this.diffColumnName = diffColumnName;
    }

    public void setDiffOtherColumnNames(Set<String> diffOtherColumnNames)
    {
        this.diffOtherColumnNames = diffOtherColumnNames;
    }

    public void setOtherColSimilarity(int otherColSimilarity)
    {
        if (otherColSimilarity <= 0 || otherColSimilarity > 100)
        {
            otherColSimilarity = 100;
        }
        this.otherColSimilarity = otherColSimilarity;
    }

    public void setOtherColumnNames(Set<String> otherColumnNames)
    {
        this.otherColumnNames = otherColumnNames;
    }

    public void setSimilarity(int similarity)
    {
        if (similarity <= 0 || similarity > 100)
        {
            similarity = 100;
        }
        this.similarity = similarity;
    }
}
