package com.personal.datacompare.config;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.w3c.dom.Element;

import com.personal.core.utils.Assert;
import com.personal.core.utils.CoreUtil;
import com.personal.core.xml.XMLDoc;
import com.personal.datacompare.api.CompareDataTable;
import com.personal.datacompare.config.CompareDataTableConfig.ColumnLoopTypeEnum;
import com.personal.datacompare.config.ExtendColumnConfig.ExtendColumnTypeEnum;
import com.personal.datacompare.config.TransRowColConfig.TransColConfig;
import com.personal.datacompare.config.TransRowColConfig.TransRowConfig;
import com.personal.dataconvert.util.ExcelHtmlUtil;

/**
 * 配置管理
 * @author cuibo
 *
 */
public class CompareDataTableConfigApp
{
    private static Map<String, CompareDataTableConfig> configMap = new LinkedHashMap<String, CompareDataTableConfig>();
    private static final Object LOCK = new Object();

    /**
     * 通过配置ID获取CompareDataTableConfig
     * @param configId
     * @return
     * @throws Exception
     */
    public static CompareDataTableConfig getCompareDataTableConfig(String configId) throws Exception
    {
        if (CompareDataTableConfigApp.configMap == null)
        {
            throw new Exception("注入的导出配置文件信息为空！");
        }
        return CoreUtil.deepCopy(CompareDataTableConfigApp.configMap.get(configId));
    }

    public static Map<String, CompareDataTableConfig> load(InputStream in) throws Exception
    {
        try
        {
            XMLDoc doc = new XMLDoc();
            doc.load(in);
            Element root = doc.getRootElement();
            Assert.isNotNull(root, "获取根节点失败！");
            List<Element> books = doc.getChildList(root);
            Map<String, CompareDataTableConfig> result = new LinkedHashMap<String, CompareDataTableConfig>();
            CompareDataTableConfig tableConfig = null;
            for (Element tableElement : books)
            {
                tableConfig = CompareDataTableConfigApp.parseTablesElement(tableElement, doc);
                if (tableConfig != null)
                {
                    result.put(tableConfig.getId(), tableConfig);
                }
            }
            return result;
        } finally
        {
            ExcelHtmlUtil.release(in);
        }
    }

    /**
     * 注入导出配置流
     * @param in
     * @throws Exception
     */
    public static void setConfigStream(InputStream in) throws Exception
    {
        if (in == null)
        {
            return;
        }
        synchronized (CompareDataTableConfigApp.LOCK)
        {
            Map<String, CompareDataTableConfig> configMap = CompareDataTableConfigApp.load(in);
            if (configMap != null && !configMap.isEmpty())
            {
                CompareDataTableConfigApp.configMap.putAll(configMap);
            }
        }
    }

    private static void parseCheckColumns(CompareDataTableConfig tableConfig, XMLDoc doc, Element columnElement)
    {
        List<Element> childColumns = doc.getChildList(columnElement);
        if (childColumns == null || childColumns.isEmpty())
        {
            return;
        }
        CheckColumnConfig checkColumnConfig = null;
        List<CheckColumnConfig> checkColumnConfigs = new ArrayList<CheckColumnConfig>();
        for (Element element : childColumns)
        {
            checkColumnConfig = new CheckColumnConfig();
            checkColumnConfig.setColumnLabel(element.getAttribute("columnLabel"));
            checkColumnConfig.setColumnName(element.getAttribute("columnName"));
            checkColumnConfig.setRenameColumnName(element.getAttribute("renameColumnName"));
            checkColumnConfig.setAlign(element.getAttribute("align"));
            if (!CoreUtil.isEmpty(element.getAttribute("width")))
            {
                checkColumnConfig.setWidth(CoreUtil.parseInt(element.getAttribute("width")));
            }
            if (!CoreUtil.isEmpty(element.getAttribute("decimal")))
            {
                checkColumnConfig.setDecimal(CoreUtil.parseInt(element.getAttribute("decimal")));
            }
            if (!CoreUtil.isEmpty(element.getAttribute("showOrder")))
            {
                checkColumnConfig.setShowOrder(CoreUtil.parseInt(element.getAttribute("showOrder")));
            }
            // 是否添加合计
            if (element.hasAttribute("totalColName"))
            {
                checkColumnConfig.setTotalColName(element.getAttribute("totalColName"));
            }
            CompareDataTableConfigApp.parseCompatibleColumnNames(checkColumnConfig, element, "compatibleColumnNames");
            checkColumnConfigs.add(checkColumnConfig);
        }
        tableConfig.setCheckColumnConfigs(checkColumnConfigs);
    }

    private static void parseCombineColumns(CompareDataTableConfig tableConfig, XMLDoc doc, Element columnElement)
    {
        List<Element> childColumns = doc.getChildList(columnElement);
        if (childColumns == null || childColumns.isEmpty())
        {
            return;
        }
        CombineColumnConfig combineColumnConfig = null;
        List<CombineColumnConfig> combineColumnConfigs = new ArrayList<CombineColumnConfig>();
        for (Element element : childColumns)
        {
            combineColumnConfig = new CombineColumnConfig();
            combineColumnConfig.setColumnLabel(element.getAttribute("columnLabel"));
            combineColumnConfig.setColumnName(element.getAttribute("columnName"));
            combineColumnConfig.setRenameColumnName(element.getAttribute("renameColumnName"));
            combineColumnConfig.setAlign(element.getAttribute("align"));
            if (!CoreUtil.isEmpty(element.getAttribute("width")))
            {
                combineColumnConfig.setWidth(CoreUtil.parseInt(element.getAttribute("width")));
            }
            if (!CoreUtil.isEmpty(element.getAttribute("decimal")))
            {
                combineColumnConfig.setDecimal(CoreUtil.parseInt(element.getAttribute("decimal")));
            }
            if (!CoreUtil.isEmpty(element.getAttribute("showOrder")))
            {
                combineColumnConfig.setShowOrder(CoreUtil.parseInt(element.getAttribute("showOrder")));
            }
            CompareDataTableConfigApp.parseCompatibleColumnNames(combineColumnConfig, element, "compatibleColumnNames");
            combineColumnConfigs.add(combineColumnConfig);
        }
        tableConfig.setCombineColumnConfigs(combineColumnConfigs);
    }

    private static void parseCompareColumns(CompareDataTableConfig tableConfig, XMLDoc doc, Element columnElement)
    {
        List<Element> childColumns = doc.getChildList(columnElement);
        if (childColumns == null || childColumns.isEmpty())
        {
            return;
        }
        CompareColumnConfig compareColumnConfig = null;
        List<CompareColumnConfig> compareColumnConfigs = new ArrayList<CompareColumnConfig>();
        for (Element element : childColumns)
        {
            compareColumnConfig = new CompareColumnConfig();
            compareColumnConfig.setColumnLabel(element.getAttribute("columnLabel"));
            compareColumnConfig.setColumnName(element.getAttribute("columnName"));
            compareColumnConfig.setRenameColumnName(element.getAttribute("renameColumnName"));
            compareColumnConfig.setAlign(element.getAttribute("align"));
            if (!CoreUtil.isEmpty(element.getAttribute("width")))
            {
                compareColumnConfig.setWidth(CoreUtil.parseInt(element.getAttribute("width")));
            }
            compareColumnConfig.setCalDiff(CoreUtil.parseBoolean(element.getAttribute("calDiff")));
            compareColumnConfig.setCalDiffPercent(CoreUtil.parseBoolean(element.getAttribute("calDiffPercent")));
            if (!CoreUtil.isEmpty(element.getAttribute("decimal")))
            {
                compareColumnConfig.setDecimal(CoreUtil.parseInt(element.getAttribute("decimal")));
            }
            if (!CoreUtil.isEmpty(element.getAttribute("showOrder")))
            {
                compareColumnConfig.setShowOrder(CoreUtil.parseInt(element.getAttribute("showOrder")));
            }
            CompareDataTableConfigApp.parseCompatibleColumnNames(compareColumnConfig, element, "compatibleColumnNames");
            compareColumnConfigs.add(compareColumnConfig);
        }
        tableConfig.setCompareColumnConfigs(compareColumnConfigs);
    }

    private static void parseCompatibleColumnNames(ColumnConfig columnConfig, Element element, String attributeName)
    {
        String attributeColumnNames = element.getAttribute(attributeName);
        if (CoreUtil.isEmpty(attributeColumnNames))
        {
            return;
        }
        String[] arr = CoreUtil.split(attributeColumnNames, CompareDataTable.多列名表名分隔符);
        Set<String> set = new LinkedHashSet<String>();
        for (String string : arr)
        {
            if (CoreUtil.isEmpty(string))
            {
                continue;
            }
            set.add(string.trim());
        }
        if ("compatibleColumnNames".equals(attributeName))
        {
            columnConfig.setCompatibleColumnNames(set);
        } else if ("diffOtherColumnNames".equals(attributeName))
        {
            EqualsColumnConfig config = (EqualsColumnConfig) columnConfig;
            config.setDiffOtherColumnNames(set);
        } else if ("diffAdditiveColumnNames".equals(attributeName))
        {
            EqualsColumnConfig config = (EqualsColumnConfig) columnConfig;
            config.setDiffAdditiveColumnNames(set);
        } else if ("otherColumnNames".equals(attributeName))
        {
            EqualsColumnConfig config = (EqualsColumnConfig) columnConfig;
            config.setOtherColumnNames(set);
        } else if ("additiveColumnNames".equals(attributeName))
        {
            EqualsColumnConfig config = (EqualsColumnConfig) columnConfig;
            config.setAdditiveColumnNames(set);
        }
    }

    private static void parseCoverColumns(CompareDataTableConfig tableConfig, XMLDoc doc, Element columnElement)
    {
        List<Element> childColumns = doc.getChildList(columnElement);
        if (childColumns == null || childColumns.isEmpty())
        {
            return;
        }
        CoverColumnConfig coverColumnConfig = null;
        List<CoverColumnConfig> coverColumnConfigs = new ArrayList<CoverColumnConfig>();
        for (Element element : childColumns)
        {
            coverColumnConfig = new CoverColumnConfig();
            coverColumnConfig.setFirstColumnName(element.getAttribute("firstColumnName"));
            coverColumnConfig.setLastColumnName(element.getAttribute("lastColumnName"));
            if (!CoreUtil.isEmpty(element.getAttribute("showOrder")))
            {
                coverColumnConfig.setShowOrder(CoreUtil.parseInt(element.getAttribute("showOrder")));
            }
            CompareDataTableConfigApp.parseCompatibleColumnNames(coverColumnConfig, element, "compatibleColumnNames");
            coverColumnConfigs.add(coverColumnConfig);
        }
        tableConfig.setCoverColumnConfigs(coverColumnConfigs);
    }

    private static void parseDisplayColumns(CompareDataTableConfig tableConfig, XMLDoc doc, Element columnElement)
    {
        List<Element> childColumns = doc.getChildList(columnElement);
        if (childColumns == null || childColumns.isEmpty())
        {
            return;
        }
        DisplayColumnConfig displayColumnConfig = null;
        List<DisplayColumnConfig> displayColumnConfigs = new ArrayList<DisplayColumnConfig>();
        for (Element element : childColumns)
        {
            displayColumnConfig = new DisplayColumnConfig();
            displayColumnConfig.setColumnLabel(element.getAttribute("columnLabel"));
            displayColumnConfig.setColumnName(element.getAttribute("columnName"));
            displayColumnConfig.setRenameColumnName(element.getAttribute("renameColumnName"));
            displayColumnConfig.setAlign(element.getAttribute("align"));
            if (!CoreUtil.isEmpty(element.getAttribute("width")))
            {
                displayColumnConfig.setWidth(CoreUtil.parseInt(element.getAttribute("width")));
            }
            if (!CoreUtil.isEmpty(element.getAttribute("showOrder")))
            {
                displayColumnConfig.setShowOrder(CoreUtil.parseInt(element.getAttribute("showOrder")));
            }
            if (!CoreUtil.isEmpty(element.getAttribute("decimal")))
            {
                displayColumnConfig.setDecimal(CoreUtil.parseInt(element.getAttribute("decimal")));
            }
            displayColumnConfigs.add(displayColumnConfig);
            CompareDataTableConfigApp.parseCompatibleColumnNames(displayColumnConfig, element, "compatibleColumnNames");
        }
        tableConfig.setDisplayColumns(displayColumnConfigs);
    }

    private static void parseEqualsColumn(CompareDataTableConfig tableConfig, Element columnElement)
    {
        EqualsColumnConfig columnConfig = new EqualsColumnConfig(columnElement.getAttribute("columnName"),
                columnElement.getAttribute("columnLabel"));
        columnConfig.setDiffColumnName(columnElement.getAttribute("diffColumnName"));
        if (CoreUtil.isEmpty(columnConfig.getDiffColumnName()))
        {
            columnConfig.setDiffColumnName(columnConfig.getColumnName());
        }
        CompareDataTableConfigApp.parseCompatibleColumnNames(columnConfig, columnElement, "otherColumnNames");
        CompareDataTableConfigApp.parseCompatibleColumnNames(columnConfig, columnElement, "diffOtherColumnNames");
        if (CoreUtil.isEmpty(columnConfig.getDiffOtherColumnNames()))
        {
            columnConfig.setDiffOtherColumnNames(columnConfig.getOtherColumnNames());
        }
        CompareDataTableConfigApp.parseCompatibleColumnNames(columnConfig, columnElement, "additiveColumnNames");
        CompareDataTableConfigApp.parseCompatibleColumnNames(columnConfig, columnElement, "diffAdditiveColumnNames");
        if (CoreUtil.isEmpty(columnConfig.getDiffAdditiveColumnNames()))
        {
            columnConfig.setDiffAdditiveColumnNames(columnConfig.getAdditiveColumnNames());
        }
        if (!CoreUtil.isEmpty(columnElement.getAttribute("similarity")))
        {
            columnConfig.setSimilarity(CoreUtil.parseInt(columnElement.getAttribute("similarity")));
            if (columnConfig.getSimilarity() <= 0 || columnConfig.getSimilarity() > 100)
            {
                columnConfig.setSimilarity(100);
            }
        }
        if (!CoreUtil.isEmpty(columnElement.getAttribute("otherColSimilarity")))
        {
            columnConfig.setOtherColSimilarity(CoreUtil.parseInt(columnElement.getAttribute("otherColSimilarity")));
            if (columnConfig.getOtherColSimilarity() <= 0 || columnConfig.getOtherColSimilarity() > 100)
            {
                columnConfig.setOtherColSimilarity(100);
            }
        }
        if (!CoreUtil.isEmpty(columnElement.getAttribute("additiveColSimilarity")))
        {
            columnConfig.setAdditiveColSimilarity(
                    CoreUtil.parseInt(columnElement.getAttribute("additiveColSimilarity")));
            if (columnConfig.getAdditiveColSimilarity() <= 0 || columnConfig.getAdditiveColSimilarity() > 100)
            {
                columnConfig.setAdditiveColSimilarity(100);
            }
        }
        CompareDataTableConfigApp.parseCompatibleColumnNames(columnConfig, columnElement, "compatibleColumnNames");
        tableConfig.setEqualsColumnConfig(columnConfig);
    }

    private static void parseHandSetEquals(CompareDataTableConfig tableConfig, XMLDoc doc, Element columnElement)
    {
        List<Element> childColumns = doc.getChildList(columnElement);
        if (childColumns == null || childColumns.isEmpty())
        {
            return;
        }
        Map<String, Set<String>> handSetEquals = new HashMap<String, Set<String>>();
        for (Element element : childColumns)
        {
            if (CoreUtil.isEmpty(element.getAttribute("value")))
            {
                continue;
            }
            List<Element> childs = doc.getChildList(element);
            if (CoreUtil.isEmpty(childs))
            {
                continue;
            }
            Set<String> sts = new HashSet<String>();
            for (Element element2 : childs)
            {
                if (CoreUtil.isEmpty(element2.getAttribute("value")))
                {
                    continue;
                }
                sts.add(element2.getAttribute("value"));
            }
            if (sts.isEmpty())
            {
                continue;
            }
            handSetEquals.put(element.getAttribute("value"), sts);
        }
        tableConfig.setHandSetEquals(handSetEquals);
    }

    private static CompareDataTableConfig parseTablesElement(Element tableElement, XMLDoc doc) throws Exception
    {
        String configId = tableElement.getAttribute("id");
        Assert.isNotNullOrEmpty(configId, "报表对比合并配置中，存在配置ID为空的配置！");
        List<Element> columnElements = doc.getChildList(tableElement);
        if (columnElements == null || columnElements.isEmpty())
        {
            return null;
        }
        CompareDataTableConfig tableConfig = new CompareDataTableConfig();
        tableConfig.setBoundId(tableElement.getAttribute("boundId"));
        tableConfig.setId(configId);
        tableConfig.setLeft(tableElement.getAttribute("left"));
        tableConfig.setRight(tableElement.getAttribute("right"));
        tableConfig.setTableName(tableElement.getAttribute("tableName"));
        String compatibleTableNames = tableElement.getAttribute("compatibleTableNames");
        if (!CoreUtil.isEmpty(compatibleTableNames))
        {
            String[] arr = CoreUtil.split(compatibleTableNames, CompareDataTable.多列名表名分隔符);
            Set<String> set = new LinkedHashSet<String>();
            for (String string : arr)
            {
                if (CoreUtil.isEmpty(string))
                {
                    continue;
                }
                set.add(string.trim());
            }
            tableConfig.setCompatibleTableNames(set);
        }
        tableConfig.setTitle(tableElement.getAttribute("title"));
        tableConfig.setCompareMode(tableElement.getAttribute("compareMode"));
        tableConfig.setHasDiffReason(Boolean.valueOf(tableElement.getAttribute("hasDiffReason")));
        tableConfig.setColumnLoopType(ColumnLoopTypeEnum.valueOfIgnoreNone(tableElement.getAttribute("columnLoopType")));
        
        tableConfig.setDiffColumnKey(tableElement.getAttribute("diffColumnKey"));
        tableConfig.setDiffColumnName(tableElement.getAttribute("diffColumnName"));
        tableConfig.setDiffColumnLabel(tableElement.getAttribute("diffColumnLabel"));
        if (CoreUtil.isEmpty(tableConfig.getDiffColumnLabel()))
        {
            tableConfig.setDiffColumnLabel(tableConfig.getDiffColumnName());
        }
        if (!CoreUtil.isEmpty(tableElement.getAttribute("labelLevel")))
        {
            tableConfig.setLabelLevel(CoreUtil.parseDbl(tableElement.getAttribute("labelLevel")));
        }
        if (CoreUtil.isEmpty(tableElement.getAttribute("insertNoMatch")))
        {
            tableConfig.setInsertNoMatch(true);
        } else
        {
            tableConfig.setInsertNoMatch(Boolean.valueOf(tableElement.getAttribute("insertNoMatch")));
        }
        List<Element> columnsElements = doc.getChildList(tableElement);
        if (columnsElements == null || columnElements.isEmpty())
        {
            return null;
        }
        for (Element columnElement : columnsElements)
        {
            if ("transRowColConfig".equals(columnElement.getNodeName()))
            {
                CompareDataTableConfigApp.parseTransRowColConfig(tableConfig, doc, columnElement);
            } else if ("displayColumns".equals(columnElement.getNodeName()))
            {
                CompareDataTableConfigApp.parseDisplayColumns(tableConfig, doc, columnElement);
            } else if ("levelColumn".equals(columnElement.getNodeName()))
            {
                LevelColumnConfig columnConfig = new LevelColumnConfig(columnElement.getAttribute("columnName"),
                        columnElement.getAttribute("columnLabel"));
                columnConfig.setDiffColumnName(columnElement.getAttribute("diffColumnName"));
                tableConfig.setLevelColumnConfig(columnConfig);
                CompareDataTableConfigApp.parseCompatibleColumnNames(columnConfig, columnElement,
                        "compatibleColumnNames");
            } else if ("equalsColumn".equals(columnElement.getNodeName()))
            {
                CompareDataTableConfigApp.parseEqualsColumn(tableConfig, columnElement);
            } else if ("compareColumns".equals(columnElement.getNodeName()))
            {
                CompareDataTableConfigApp.parseCompareColumns(tableConfig, doc, columnElement);
            } else if ("combineColumns".equals(columnElement.getNodeName()))
            {
                CompareDataTableConfigApp.parseCombineColumns(tableConfig, doc, columnElement);
            } else if ("checkColumns".equals(columnElement.getNodeName()))
            {
                CompareDataTableConfigApp.parseCheckColumns(tableConfig, doc, columnElement);
            } else if ("coverColumns".equals(columnElement.getNodeName()))
            {
                CompareDataTableConfigApp.parseCoverColumns(tableConfig, doc, columnElement);
            }  else if ("extendColumns".equals(columnElement.getNodeName()))
            {
                CompareDataTableConfigApp.parseExtendColumns(tableConfig, doc, columnElement);
            } else if ("handSetEquals".equals(columnElement.getNodeName()))
            {
                CompareDataTableConfigApp.parseHandSetEquals(tableConfig, doc, columnElement);
            }
        }
        return tableConfig;
    }

    private static void parseExtendColumns(CompareDataTableConfig tableConfig, XMLDoc doc, Element columnElement)
    {
        List<Element> childColumns = doc.getChildList(columnElement);
        if (childColumns == null || childColumns.isEmpty())
        {
            return;
        }
        ExtendColumnConfig extendColumnConfig = null;
        List<ExtendColumnConfig> extendColumnConfigs = new ArrayList<ExtendColumnConfig>();
        for (Element element : childColumns)
        {
            ExtendColumnTypeEnum columnType = ExtendColumnTypeEnum.valueOfIgnoreNull(element.getAttribute("columnType"));
            if (columnType == null)
            {
                continue;
            }
            extendColumnConfig = new ExtendColumnConfig();
            extendColumnConfig.setColumnLabel(element.getAttribute("columnLabel"));
            extendColumnConfig.setColumnName(element.getAttribute("columnName"));
            extendColumnConfig.setRenameColumnName(element.getAttribute("renameColumnName"));
            extendColumnConfig.setAlign(element.getAttribute("align"));
            if (!CoreUtil.isEmpty(element.getAttribute("width")))
            {
                extendColumnConfig.setWidth(CoreUtil.parseInt(element.getAttribute("width")));
            }
            extendColumnConfig.setColumnType(columnType);
            extendColumnConfig.setRejectCurrentTree(CoreUtil.parseBoolean(element.getAttribute("rejectCurrentTree")));
            if (!CoreUtil.isEmpty(element.getAttribute("decimal")))
            {
                extendColumnConfig.setDecimal(CoreUtil.parseInt(element.getAttribute("decimal")));
            }
            if (!CoreUtil.isEmpty(element.getAttribute("showOrder")))
            {
                extendColumnConfig.setShowOrder(CoreUtil.parseInt(element.getAttribute("showOrder")));
            }
            CompareDataTableConfigApp.parseCompatibleColumnNames(extendColumnConfig, element, "compatibleColumnNames");
            
            extendColumnConfigs.add(extendColumnConfig);
        }
        tableConfig.setExtendColumnConfigs(extendColumnConfigs);
    }

    private static void parseTransRowColConfig(CompareDataTableConfig tableConfig, XMLDoc doc, Element columnElement)
    {
        TransRowColConfig rowColConfig = new TransRowColConfig();
        rowColConfig.setTblIndexColName(columnElement.getAttribute("tblIndexColName"));
        rowColConfig.setTransRowColName(columnElement.getAttribute("transRowColName"));
        List<Element> childColumns = doc.getChildList(columnElement);
        if (CoreUtil.isEmpty(childColumns))
        {
            return;
        }
        List<TransRowConfig> rows = new ArrayList<TransRowColConfig.TransRowConfig>();
        List<TransColConfig> cols = new ArrayList<TransRowColConfig.TransColConfig>();
        for (Element rowCol : childColumns)
        {
            if ("transColConfigs".equals(rowCol.getNodeName()))
            {
                for (Element element : doc.getChildList(rowCol))
                {
                    TransColConfig col = new TransColConfig();
                    col.setColumnName(element.getAttribute("columnName"));
                    if (!CoreUtil.isEmpty(element.getAttribute("compatibleColumnNames")))
                    {
                        String[] arr = CoreUtil.split(element.getAttribute("compatibleColumnNames"),
                                CompareDataTable.多列名表名分隔符);
                        Set<String> set = new LinkedHashSet<String>();
                        for (String string : arr)
                        {
                            if (CoreUtil.isEmpty(string))
                            {
                                continue;
                            }
                            set.add(string.trim());
                        }
                        col.setCompatibleColumnNames(set);
                    }
                    col.setRenameColumnName(element.getAttribute("renameColumnName"));
                    col.setTblIndex(element.getAttribute("tblIndex"));
                    cols.add(col);
                }
            } else if ("transRowConfigs".equals(rowCol.getNodeName()))
            {
                for (Element element : doc.getChildList(rowCol))
                {
                    TransRowConfig row = new TransRowConfig();
                    row.setColumnName(element.getAttribute("columnName"));
                    if (!CoreUtil.isEmpty(element.getAttribute("compatibleColumnNames")))
                    {
                        String[] arr = CoreUtil.split(element.getAttribute("compatibleColumnNames"),
                                CompareDataTable.多列名表名分隔符);
                        Set<String> set = new LinkedHashSet<String>();
                        for (String string : arr)
                        {
                            if (CoreUtil.isEmpty(string))
                            {
                                continue;
                            }
                            set.add(string.trim());
                        }
                        row.setCompatibleColumnNames(set);
                    }
                    row.setMatchValue(element.getAttribute("matchValue"));
                    if (!CoreUtil.isEmpty(element.getAttribute("compatibleMatchValues")))
                    {
                        String[] arr = CoreUtil.split(element.getAttribute("compatibleMatchValues"),
                                CompareDataTable.多列名表名分隔符);
                        Set<String> set = new LinkedHashSet<String>();
                        for (String string : arr)
                        {
                            if (CoreUtil.isEmpty(string))
                            {
                                continue;
                            }
                            set.add(string.trim());
                        }
                        row.setCompatibleMatchValues(set);
                    }
                    rows.add(row);
                }
            }
        }
        rowColConfig.setTransColConfigs(cols);
        rowColConfig.setTransRowConfigs(rows);
        if (!CoreUtil.isEmpty(cols) && !CoreUtil.isEmpty(rows))
        {
            tableConfig.setTransRowColConfig(rowColConfig);
        }
    }
}
