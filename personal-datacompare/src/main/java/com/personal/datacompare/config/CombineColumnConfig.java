package com.personal.datacompare.config;

/**
 * 合并列
 * @author cuibo
 *
 */
public class CombineColumnConfig extends ColumnConfig
{
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /** 小数位 */
    private int decimal = -1;

    public CombineColumnConfig()
    {
        super();
    }

    public CombineColumnConfig(String columnName, String columnLabel)
    {
        super(columnName, columnLabel);
    }

    public CombineColumnConfig(String columnName, String columnLabel, int width)
    {
        super(columnName, columnLabel, width);
    }

    public CombineColumnConfig(String columnName, String columnLabel, String style, int width)
    {
        super(columnName, columnLabel, style, width);
    }

    public int getDecimal()
    {
        return decimal;
    }

    public void setDecimal(int decimal)
    {
        this.decimal = decimal;
    }
}
