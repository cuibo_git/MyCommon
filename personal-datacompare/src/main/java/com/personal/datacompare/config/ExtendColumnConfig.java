package com.personal.datacompare.config;

import java.util.Map.Entry;

import com.personal.core.data.DataColumn;
import com.personal.core.data.DataRow;
import com.personal.core.data.DataTable;
import com.personal.core.utils.CoreUtil;
import com.personal.datacompare.tree.AbstractCompareTree;
import com.personal.datacompare.tree.CompareTreeNode;

/**
 * 扩展列
 * @author cuibo
 *
 */
public class ExtendColumnConfig extends ColumnConfig
{

    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** 小数位 */
    private int decimal = -1;
    
    /** 计算该扩展列是否排除当前树 */
    private boolean rejectCurrentTree;
    
    private ExtendColumnTypeEnum columnType;
    
    public int getDecimal()
    {
        return decimal;
    }

    public void setDecimal(int decimal)
    {
        this.decimal = decimal;
    }

    public ExtendColumnTypeEnum getColumnType()
    {
        return columnType;
    }

    public void setColumnType(ExtendColumnTypeEnum columnType)
    {
        this.columnType = columnType;
    }
    
    public boolean isRejectCurrentTree()
    {
        return rejectCurrentTree;
    }

    public void setRejectCurrentTree(boolean rejectCurrentTree)
    {
        this.rejectCurrentTree = rejectCurrentTree;
    }

    public enum ExtendColumnTypeEnum
    {
        求和 {
            @Override
            public void fillExtendColumn(ExtendColumnConfig columnConfig, DataRow newRow, CompareTreeNode treeNode, int otherTreesCount)
            {
                for (Entry<DataTable, CompareTreeNode> entry : treeNode.getMatchNodes().entrySet())
                {
                    // 判断是否需要当前树
                    if (columnConfig.isRejectCurrentTree())
                    {
                        if (entry.getValue() == treeNode && !treeNode.isInsertNode())
                        {
                            continue;
                        }
                    }
                    DataColumn column = AbstractCompareTree.getDataColumnByColumnConfig(entry.getValue().getRoot().getSource(), columnConfig);
                    if (column == null)
                    {
                        continue;
                    }
                    newRow.getItemMap().put(columnConfig.getCreateColumnName(), 
                            CoreUtil.add(newRow.getItemMap().get(columnConfig.getCreateColumnName()), 
                                    entry.getValue().getSource().getItemMap().get(column.getColumnName())));
                }
            }
        },
        最大值 {
            @Override
            public void fillExtendColumn(ExtendColumnConfig columnConfig, DataRow newRow, CompareTreeNode treeNode, int otherTreesCount)
            {
                Object maxValue = Double.MIN_VALUE;
                boolean found = false;
                for (Entry<DataTable, CompareTreeNode> entry : treeNode.getMatchNodes().entrySet())
                {
                    // 判断是否需要当前树
                    if (columnConfig.isRejectCurrentTree())
                    {
                        if (entry.getValue() == treeNode && !treeNode.isInsertNode())
                        {
                            continue;
                        }
                    }
                    DataColumn column = AbstractCompareTree.getDataColumnByColumnConfig(entry.getValue().getRoot().getSource(), columnConfig);
                    if (column == null)
                    {
                        continue;
                    }
                    if (CoreUtil.compareNumberNoPrecision(entry.getValue().getSource().getItemMap().get(column.getColumnName()), maxValue) > 0)
                    {
                        found = true;
                        maxValue = entry.getValue().getSource().getItemMap().get(column.getColumnName());
                    }
                }
                if (found)
                {
                    newRow.getItemMap().put(columnConfig.getCreateColumnName(), maxValue);
                }
            }
        },
        最小值 {
            @Override
            public void fillExtendColumn(ExtendColumnConfig columnConfig, DataRow newRow, CompareTreeNode treeNode, int otherTreesCount)
            {
                Object minValue = Double.MAX_VALUE;
                boolean found = false;
                for (Entry<DataTable, CompareTreeNode> entry : treeNode.getMatchNodes().entrySet())
                {
                    // 判断是否需要当前树
                    if (columnConfig.isRejectCurrentTree())
                    {
                        if (entry.getValue() == treeNode && !treeNode.isInsertNode())
                        {
                            continue;
                        }
                    }
                    DataColumn column = AbstractCompareTree.getDataColumnByColumnConfig(entry.getValue().getRoot().getSource(), columnConfig);
                    if (column == null)
                    {
                        continue;
                    }
                    if (CoreUtil.compareNumberNoPrecision(entry.getValue().getSource().getItemMap().get(column.getColumnName()), minValue) < 0)
                    {
                        found = true;
                        minValue = entry.getValue().getSource().getItemMap().get(column.getColumnName());
                    }
                }
                if (found)
                {
                    newRow.getItemMap().put(columnConfig.getCreateColumnName(), minValue);
                }
            }
        },
        平均值 {
            @Override
            public void fillExtendColumn(ExtendColumnConfig columnConfig, DataRow newRow, CompareTreeNode treeNode, int otherTreesCount)
            {
                求和.fillExtendColumn(columnConfig, newRow, treeNode, otherTreesCount);
                newRow.getItemMap().put(columnConfig.getCreateColumnName(), CoreUtil.divide(newRow.getItemMap().get(columnConfig.getCreateColumnName()), columnConfig.isRejectCurrentTree() ? otherTreesCount : otherTreesCount + 1));
            }
        };
        
        public static ExtendColumnTypeEnum valueOfIgnoreNull(String value)
        {
            if (value == null)
            {
                return null;
            }
            try
            {
                return valueOf(value);
            } catch (IllegalArgumentException e)
            {
                return null;
            }
        }

        /**
         * 填充扩展列
         * @param columnConfig
         * @param newRow
         * @param treeNode
         * @param otherTreesCount
         */
        public abstract void fillExtendColumn(ExtendColumnConfig columnConfig, DataRow newRow, CompareTreeNode treeNode, int otherTreesCount);
        
    }
    
}
