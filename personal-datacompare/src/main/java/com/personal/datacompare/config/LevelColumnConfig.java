package com.personal.datacompare.config;

/**
 * 构建层级列
 * @author cuibo
 *
 */
public class LevelColumnConfig extends ColumnConfig
{
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /** 差异原因列 */
    private String diffColumnName;

    public LevelColumnConfig()
    {
        super();
    }

    public LevelColumnConfig(String columnName, String columnLabel)
    {
        super(columnName, columnLabel);
    }

    public String getDiffColumnName()
    {
        return diffColumnName;
    }

    public void setDiffColumnName(String diffColumnName)
    {
        this.diffColumnName = diffColumnName;
    }
}
