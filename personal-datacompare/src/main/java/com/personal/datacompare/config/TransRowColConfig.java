package com.personal.datacompare.config;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * 行列转换配置
 * @author cuibo
 *
 */
public class TransRowColConfig implements Serializable
{
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /** 行列转换行 */
    private List<TransRowConfig> transRowConfigs;
    /** 行列转换列 */
    private List<TransColConfig> transColConfigs;
    /** 转换后的列名 */
    private String transRowColName;
    /** 是否添加序号列 */
    private String tblIndexColName;

    public String getTblIndexColName()
    {
        return tblIndexColName;
    }

    public List<TransColConfig> getTransColConfigs()
    {
        return transColConfigs;
    }

    public String getTransRowColName()
    {
        return transRowColName;
    }

    public List<TransRowConfig> getTransRowConfigs()
    {
        return transRowConfigs;
    }

    public void setTblIndexColName(String tblIndexColName)
    {
        this.tblIndexColName = tblIndexColName;
    }

    public void setTransColConfigs(List<TransColConfig> transColConfigs)
    {
        this.transColConfigs = transColConfigs;
    }

    public void setTransRowColName(String transRowColName)
    {
        this.transRowColName = transRowColName;
    }

    public void setTransRowConfigs(List<TransRowConfig> transRowConfigs)
    {
        this.transRowConfigs = transRowConfigs;
    }

    /**
     * 列转换
     * @author cuibo
     *
     */
    public static class TransColConfig extends ColumnConfig implements Serializable
    {
        /**
         *
         */
        private static final long serialVersionUID = 1L;
        /** 序号值 */
        private String tblIndex;
        /** 重命名的列名 */
        private String renameColumnName;
        /** 兼容的列名集合 */
        private Set<String> compatibleColumnNames;

        @Override
        public Set<String> getCompatibleColumnNames()
        {
            return compatibleColumnNames;
        }

        public String getRenameColumnName()
        {
            return renameColumnName;
        }

        public String getTblIndex()
        {
            return tblIndex;
        }

        @Override
        public void setCompatibleColumnNames(Set<String> compatibleColumnNames)
        {
            this.compatibleColumnNames = compatibleColumnNames;
        }

        public void setRenameColumnName(String renameColumnName)
        {
            this.renameColumnName = renameColumnName;
        }

        public void setTblIndex(String tblIndex)
        {
            this.tblIndex = tblIndex;
        }
    }

    /**
     * 转换
     * @author cuibo
     *
     */
    public static class TransRowConfig extends ColumnConfig implements Serializable
    {
        /**
         *
         */
        private static final long serialVersionUID = 1L;
        /** 匹配值 */
        private String matchValue;
        /** 兼容的匹配值 */
        private Set<String> compatibleMatchValues;

        public Set<String> getCompatibleMatchValues()
        {
            return compatibleMatchValues;
        }

        public String getMatchValue()
        {
            return matchValue;
        }

        public void setCompatibleMatchValues(Set<String> compatibleMatchValues)
        {
            this.compatibleMatchValues = compatibleMatchValues;
        }

        public void setMatchValue(String matchValue)
        {
            this.matchValue = matchValue;
        }
    }
}
