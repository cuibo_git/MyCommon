package com.personal.datacompare.config;

/**
 * 覆盖列
 * @author cuibo
 *
 */
public class CoverColumnConfig extends ColumnConfig
{
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /** 第一个DataTable的列名 */
    private String firstColumnName;
    /** 第二个DataTable的列名 */
    private String lastColumnName;

    public CoverColumnConfig()
    {
        super();
    }

    public CoverColumnConfig(String firstColumnName, String lastColumnName)
    {
        super();
        this.firstColumnName = firstColumnName;
        this.lastColumnName = lastColumnName;
    }

    public String getFirstColumnName()
    {
        return firstColumnName;
    }

    public String getLastColumnName()
    {
        return lastColumnName;
    }

    public void setFirstColumnName(String firstColumnName)
    {
        this.firstColumnName = firstColumnName;
    }

    public void setLastColumnName(String lastColumnName)
    {
        this.lastColumnName = lastColumnName;
    }
}
