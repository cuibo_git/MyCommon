package com.personal.datacompare.config;

/**
 * 显示列
 * @author cuibo
 *
 */
public class DisplayColumnConfig extends ColumnConfig
{
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    
    /** 小数位 */
    private int decimal = -1;

    public DisplayColumnConfig()
    {
        super();
    }

    public DisplayColumnConfig(String columnName, String columnLabel)
    {
        super(columnName, columnLabel);
    }

    public DisplayColumnConfig(String columnName, String columnLabel, int width)
    {
        super(columnName, columnLabel, width);
    }

    public DisplayColumnConfig(String columnName, String columnLabel, String style, int width)
    {
        super(columnName, columnLabel, style, width);
    }

    public int getDecimal()
    {
        return decimal;
    }

    public void setDecimal(int decimal)
    {
        this.decimal = decimal;
    }
}
