package com.personal.datacompare.config;

/**
 * 对比列
 * @author cuibo
 *
 */
public class CompareColumnConfig extends ColumnConfig
{
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /** 小数位 */
    private int decimal = -1;
    /** 计算差值 */
    private boolean calDiff;
    /** 计算百分比 */
    private boolean calDiffPercent;

    public CompareColumnConfig()
    {
        super();
    }

    public CompareColumnConfig(String columnName, String columnLabel)
    {
        super(columnName, columnLabel);
    }

    public CompareColumnConfig(String columnName, String columnLabel, int width)
    {
        super(columnName, columnLabel, width);
    }

    public CompareColumnConfig(String columnName, String columnLabel, String style, int width)
    {
        super(columnName, columnLabel, style, width);
    }

    public int getDecimal()
    {
        return decimal;
    }

    public boolean isCalDiff()
    {
        return calDiff;
    }

    public boolean isCalDiffPercent()
    {
        return calDiffPercent;
    }

    public void setCalDiff(boolean calDiff)
    {
        this.calDiff = calDiff;
    }

    public void setCalDiffPercent(boolean calDiffPercent)
    {
        this.calDiffPercent = calDiffPercent;
    }

    public void setDecimal(int decimal)
    {
        this.decimal = decimal;
    }
}
