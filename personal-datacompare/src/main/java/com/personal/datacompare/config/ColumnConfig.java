package com.personal.datacompare.config;

import java.io.Serializable;
import java.util.Set;

import com.personal.core.utils.CoreUtil;

/**
 * 列配置
 * @author cuibo
 *
 */
public abstract class ColumnConfig implements Serializable
{
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /** 列名 */
    private String columnName;
    /** 重新命名的列名 */
    private String renameColumnName;
    /** 兼容的列名集合 */
    private Set<String> compatibleColumnNames;
    /** 显示名 */
    private String columnLabel;
    /** 样式 */
    private String align;
    /** 列宽 */
    private int width;
    /** 排序 */
    private int showOrder = -1;

    public ColumnConfig()
    {
        super();
    }

    public ColumnConfig(String columnName, String columnLabel)
    {
        super();
        this.columnName = columnName;
        this.columnLabel = columnLabel;
    }

    public ColumnConfig(String columnName, String columnLabel, int width)
    {
        super();
        this.columnName = columnName;
        this.columnLabel = columnLabel;
        this.width = width;
    }

    public ColumnConfig(String columnName, String columnLabel, String align, int width)
    {
        super();
        this.columnName = columnName;
        this.columnLabel = columnLabel;
        this.align = align;
        this.width = width;
    }

    public String getColumnLabel()
    {
        return columnLabel;
    }

    public String getColumnName()
    {
        return columnName;
    }
    
    public String getCreateColumnName()
    {
        if (!CoreUtil.isEmpty(renameColumnName))
        {
            return renameColumnName;
        }
        return columnName;
    }
    
    public String getRenameColumnName()
    {
        return renameColumnName;
    }

    public void setRenameColumnName(String renameColumnName)
    {
        this.renameColumnName = renameColumnName;
    }

    public Set<String> getCompatibleColumnNames()
    {
        return compatibleColumnNames;
    }

    public int getShowOrder()
    {
        return showOrder;
    }

    public int getWidth()
    {
        return width;
    }

    public void setColumnLabel(String columnLabel)
    {
        this.columnLabel = columnLabel;
    }

    public void setColumnName(String columnName)
    {
        this.columnName = columnName;
    }

    public void setCompatibleColumnNames(Set<String> compatibleColumnNames)
    {
        this.compatibleColumnNames = compatibleColumnNames;
    }

    public void setShowOrder(int showOrder)
    {
        this.showOrder = showOrder;
    }
    
    public String getAlign()
    {
        return align;
    }

    public void setAlign(String align)
    {
        this.align = align;
    }

    public void setWidth(int width)
    {
        this.width = width;
    }
}
