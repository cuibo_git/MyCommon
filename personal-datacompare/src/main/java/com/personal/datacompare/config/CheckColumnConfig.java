package com.personal.datacompare.config;

/**
 * 查看列
 * @author cuibo
 *
 */
public class CheckColumnConfig extends ColumnConfig
{
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    /** 小数位 */
    private int decimal = -1;
    /** 合计列名（如果不为空，则添加合计） */
    private String totalColName;

    public CheckColumnConfig()
    {
        super();
    }

    public CheckColumnConfig(String columnName, String columnLabel)
    {
        super(columnName, columnLabel);
    }

    public CheckColumnConfig(String columnName, String columnLabel, int width)
    {
        super(columnName, columnLabel, width);
    }

    public CheckColumnConfig(String columnName, String columnLabel, String style, int width)
    {
        super(columnName, columnLabel, style, width);
    }

    public int getDecimal()
    {
        return decimal;
    }

    public String getTotalColName()
    {
        return totalColName;
    }

    public void setDecimal(int decimal)
    {
        this.decimal = decimal;
    }

    public void setTotalColName(String totalColName)
    {
        this.totalColName = totalColName;
    }
}
