package com.personal.datacompare.factory;

/**
 * 对比模式枚举
 * @author cuibo
 *
 */
public enum CompareModeEnum
{
    普通对比("不构建层级，依据对比字段匹配。适应于表一"), 普通层级对比("构建层级，依据对比字段匹配。适应于表四，表七"), 有根层级对比("构建层级，依据对比字段匹配。适应于表二，表三");
    /** 说明 */
    private String comment;

    private CompareModeEnum(String comment)
    {
        this.comment = comment;
    }

    public String getComment()
    {
        return comment;
    }
}
