package com.personal.datacompare.factory;

/**
 * 相似度类别枚举
 * @author cuibo
 *
 */
public enum SimilarityEnum
{
    EQUALSSIMILARITY, OTHERCOLSIMILARITY, ADDITIVECOLSIMILARITY;
}
