package com.personal.datacompare.factory;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.w3c.dom.Element;

import com.personal.core.utils.Assert;
import com.personal.core.utils.FileUtil;
import com.personal.core.xml.XMLDoc;
import com.personal.datacompare.config.CompareDataTableConfig;
import com.personal.datacompare.tree.AbstractCompareTree;
import com.personal.datacompare.tree.CommonCompareTree;
import com.personal.datacompare.tree.LevelCompareTree;
import com.personal.datacompare.tree.LevelRootCompareTree;

/**
 * 对比树工厂
 * @author cuibo
 *
 */
public class CompareTreeFactory
{
    /** 外部配置 */
    private static Map<String, String> configMap = new ConcurrentHashMap<String, String>();

    /**
     * 由配置信息获取树的实现
     * @param config
     * @return
     * @throws Exception
     */
    public static AbstractCompareTree getTreeImplByConfig(CompareDataTableConfig config) throws Exception
    {
        Assert.isNotNull(config, "对比合并配置信息为空！");
        Assert.isNotNullOrEmpty(config.getCompareMode(), "对比合并配置模式信息为空！");
        // 优先从配置中读取
        if (!CompareTreeFactory.configMap.isEmpty())
        {
            for (Entry<String, String> entry : CompareTreeFactory.configMap.entrySet())
            {
                if (entry.getKey().equals(config.getCompareMode()))
                {
                    String classPath = entry.getValue();
                    Object obj = Class.forName(classPath).newInstance();
                    AbstractCompareTree result = (AbstractCompareTree) obj;
                    result.setTableConfig(config);
                    return result;
                }
            }
        }
        if (CompareModeEnum.普通对比.toString().equals(config.getCompareMode()))
        {
            return new CommonCompareTree(null, config);
        } else if (CompareModeEnum.普通层级对比.toString().equals(config.getCompareMode()))
        {
            return new LevelCompareTree(null, config);
        } else if (CompareModeEnum.有根层级对比.toString().equals(config.getCompareMode()))
        {
            return new LevelRootCompareTree(null, config);
        } else
        {
            throw new Exception("暂不支持" + config.getCompareMode() + "对比合并模式的对比合并！");
        }
    }

    /**
     * 注入
     * @param in
     * @throws Exception
     */
    public static void setConfig(InputStream in) throws Exception
    {
        try
        {
            XMLDoc doc = new XMLDoc();
            doc.load(in);
            List<Element> elements = doc.getChildList(doc.getRootElement());
            if (elements == null || elements.isEmpty())
            {
                return;
            }
            for (Element element : elements)
            {
                CompareTreeFactory.configMap.put(element.getAttribute("mode"), element.getAttribute("class"));
            }
        } finally
        {
            FileUtil.release(in);
        }
    }
    
    public static void setCompareClassPath(String compareModel, String classPath)
    {
        Assert.isNotNull(compareModel, "对比模式不能为空！");
        Assert.isNotNull(classPath, "类路径不能为空！");
        CompareTreeFactory.configMap.put(compareModel, classPath);
    }
}
