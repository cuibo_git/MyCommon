package com.personal.datacompare.tree;

import com.personal.core.data.DataRow;
import com.personal.core.data.DataTable;
import com.personal.datacompare.config.CompareDataTableConfig;

/**
 * 无层级对比
 * @author cuibo
 *
 */
public class CommonCompareTree extends AbstractCompareTree
{
    /**
     *
     */
    private static final long serialVersionUID = -6672533750428536758L;

    public CommonCompareTree()
    {
        super();
    }

    public CommonCompareTree(CompareDataTableConfig tableConfig)
    {
        super(tableConfig);
    }

    /**
     *
     * @param source      数据源
     * @param tableConfig  配置信息
     * @throws Exception
     */
    public CommonCompareTree(DataTable source, CompareDataTableConfig tableConfig)
    {
        super(source, tableConfig);
    }

    @Override
    public int getDataRowDeepLength(DataRow row, boolean isFirst, boolean isLast)
    {
        return 1;
    }
}
