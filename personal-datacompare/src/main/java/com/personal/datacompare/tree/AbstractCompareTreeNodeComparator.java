package com.personal.datacompare.tree;

import java.util.Comparator;

import com.personal.datacompare.config.CompareDataTableConfig;

/**
 * 抽象树节点比较器
 * @author cuibo
 *
 */
public abstract class AbstractCompareTreeNodeComparator implements Comparator<CompareTreeNode>
{
    /** 对比配置 */
    private CompareDataTableConfig compareDataTableConfig;

    public CompareDataTableConfig getCompareDataTableConfig()
    {
        return compareDataTableConfig;
    }

    void setCompareDataTableConfig(CompareDataTableConfig compareDataTableConfig)
    {
        this.compareDataTableConfig = compareDataTableConfig;
    }
}
