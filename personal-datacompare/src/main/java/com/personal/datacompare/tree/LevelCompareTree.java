package com.personal.datacompare.tree;

import com.personal.core.data.DataColumn;
import com.personal.core.data.DataRow;
import com.personal.core.data.DataTable;
import com.personal.core.utils.Assert;
import com.personal.core.utils.CoreUtil;
import com.personal.core.utils.ReGularUtil;
import com.personal.datacompare.config.CompareDataTableConfig;

/**
 * 无根层级对比（如表四，表七）
 * @author cuibo
 *
 */
public class LevelCompareTree extends AbstractCompareTree
{
    /**
     *
     */
    private static final long serialVersionUID = 2459763884113456940L;

    public LevelCompareTree()
    {
    }

    public LevelCompareTree(CompareDataTableConfig tableConfig)
    {
        super(tableConfig);
    }

    /**
     *
     * @param source      数据源
     * @param tableConfig  配置信息
     * @throws Exception
     */
    public LevelCompareTree(DataTable source, CompareDataTableConfig tableConfig)
    {
        super(source, tableConfig);
    }

    @Override
    public int getDataRowDeepLength(DataRow row, boolean isFirst, boolean isLast) throws Exception
    {
        Assert.isNotNull(tableConfig.getLevelColumnConfig(), "层级配置信息为空！");
        Assert.isNotNull(tableConfig.getLevelColumnConfig().getColumnName(), "层级配置信息为空！");
        DataColumn column = getDataColumnByColumnConfig(source, tableConfig.getLevelColumnConfig());
        String levelInfo = column == null ? null : CoreUtil.parseStr(row.getItemMap().get(column.getColumnName()));
        if (CoreUtil.isEmpty(levelInfo))
        {
            // 最后一层空的才当做第一层级
            return isLast ? 1 : -1;
        }
        // 层级列为空：1  中文汉字：2  带有括号的中文汉字：3  普通阿拉数字：4   带有"." 的阿拉伯数字 每多一个在 4的基础上加1
        if (ReGularUtil.CHINESENUMBER.matcher(levelInfo).matches())
        {
            return 1;
        } else if (ReGularUtil.CHINESENUMBERANDKH.matcher(levelInfo).matches())
        {
            return 2;
        } else if (CoreUtil.isNumber(levelInfo) && !levelInfo.contains("."))
        {
            return 3;
        }
        if (levelInfo.contains("."))
        {
            return 3 + levelInfo.split("\\.").length - 1;
        } else
        {
            // 不能识别，当做遍历到哪个节点的子集
            return -1;
        }
    }
}
