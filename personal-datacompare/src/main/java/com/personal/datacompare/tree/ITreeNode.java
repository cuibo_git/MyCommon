package com.personal.datacompare.tree;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.personal.core.port.HaveDeepAndChildren;
import com.personal.core.utils.DeepChildrenUtil;

/**
 * 树节点
 * @author cuibo
 *
 * @param <T>
 * @param <V>
 */
public abstract class ITreeNode<T, V> implements Serializable, HaveDeepAndChildren
{
    /**
     *
     */
    private static final long serialVersionUID = -5536764188132733034L;
    /** 行数据 */
    protected V source;
    /** 节点深度（深度为1的父亲节点就是空的） */
    protected int deepLength = -1;
    /** 获取树根 */
    protected ITree<T, V> root;
    /** 父亲节点 */
    protected ITreeNode<T, V> parent;
    /** 子节点 */
    protected List<ITreeNode<T, V>> children;

    public ITreeNode(V source)
    {
        super();
        this.source = source;
    }

    /**
     * 获取兄弟节点（包括自己本身）
     * @return
     */
    public List<? extends ITreeNode<T, V>> getBrothers()
    {
        if (this.getParent() == null)
        {
            return root.getChilds();
        } else
        {
            return this.getParent().getChildren();
        }
    }

    @Override
    public List<ITreeNode<T, V>> getChildren()
    {
        if (children == null)
        {
            children = new ArrayList<ITreeNode<T, V>>();
        }
        return children;
    }

    @Override
    public int getDeepLength()
    {
        return deepLength;
    }

    /**
     * 获取兄长节点
     * @return
     */
    public List<ITreeNode<T, V>> getElderBrothers()
    {
        List<? extends ITreeNode<T, V>> brothers = getBrothers();
        if (brothers == null || brothers.isEmpty())
        {
            return null;
        }
        List<ITreeNode<T, V>> result = new ArrayList<ITreeNode<T, V>>();
        for (ITreeNode<T, V> iTreeNode : brothers)
        {
            if (iTreeNode == this)
            {
                break;
            }
            result.add(iTreeNode);
        }
        return result;
    }

    @Override
    public ITreeNode<T, V> getParent()
    {
        return parent;
    }

    public ITree<T, V> getRoot()
    {
        return root;
    }

    public V getSource()
    {
        return source;
    }

    /**
     * 是否是叶子节点
     * @return
     */
    @Override
    public boolean isLeaf()
    {
        // 子节点为空则为叶子节点
        return children == null || children.isEmpty();
    }

    /**
     * 递归获取所有子节点
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<? extends ITreeNode<T, V>> recursionGetTreeNodes()
    {
        if (isLeaf())
        {
            return null;
        }
        List<? extends HaveDeepAndChildren> tempResult = DeepChildrenUtil.recursionGetChildren(this);
        if (tempResult == null || tempResult.isEmpty())
        {
            return null;
        }
        List<ITreeNode<T, V>> result = new ArrayList<ITreeNode<T, V>>();
        for (HaveDeepAndChildren iTreeNode : tempResult)
        {
            result.add((ITreeNode<T, V>) iTreeNode);
        }
        return result;
    }

    public void setChildren(List<ITreeNode<T, V>> children)
    {
        this.children = children;
    }

    @Override
    public void setDeepLength(int deepLength)
    {
        this.deepLength = deepLength;
    }

    public void setParent(ITreeNode<T, V> parent)
    {
        this.parent = parent;
    }

    public void setRoot(ITree<T, V> root)
    {
        this.root = root;
    }

    public void setSource(V source)
    {
        this.source = source;
    }
}
