package com.personal.jdbc.operator;

import java.util.Map;

import javax.sql.DataSource;

import com.personal.core.copyonwrite.CopyOnWriteHashMap;
import com.personal.core.utils.Assert;

/**
 * Jdbc操作入口
 * @author qq
 *
 */
public class JdbcApp
{
	
	private static final String DEFAULT = "DEFAULT";
	
	private static Map<String, JdbcOperator> operators = new CopyOnWriteHashMap<>();
	
	/**
	 * 获取Jdbc操作
	 * @return
	 */
	public static JdbcOperator getOperator()
	{
		return operators.get(DEFAULT);
	}
	
	/**
	 * 获取Jdbc操作
	 * @param name
	 * @return
	 */
	public static JdbcOperator getOperator(String name)
	{
		return operators.get(name);
	}
	
	/**
	 * 注册数据源
	 * @param name
	 * @param dataSource
	 */
	public static boolean registerDataSource(String name, DataSource dataSource)
	{
		Assert.isNotNull(name, "数据源名称不能为空！");
		Assert.isNotNull(dataSource, "数据源不能为空！");
		return operators.put(name, new JdbcOperator(dataSource)) != null;
	}
	
	/**
     * 注册数据源
     * @param name
     * @param dataSource
     */
    public static boolean registerDataSource(DataSource dataSource)
    {
        Assert.isNotNull(dataSource, "数据源不能为空！");
        return operators.put(DEFAULT, new JdbcOperator(dataSource)) != null;
    }
	
}
