package com.personal.jdbc.dialect;

/**
 * Postgre
 * @author qq
 *
 */
public class PostgreDialect extends JdbcDialect
{
    public String makePagging(String sql, int start, int limit)
    {
        StringBuilder result = new StringBuilder();
        result.append(sql).append(" limit ").append(limit).append(" offset ").append(start);
        return result.toString();
    }

    public String makeCount(String sql)
    {
        return "select count(1) from (" + sql + ") tab_name";
    }
}