package com.personal.jdbc.dialect;

/**
 * oracle
 * @author qq
 *
 */
public class Oracle10GDialect extends JdbcDialect
{
    public String makePagging(String sql, int start, int limit)
    {
        StringBuilder result = new StringBuilder();
        if (start == 0)
        {
            result.append("select * from (").append(sql).append(") where rownum <= ").append(limit);
        } else
        {
            result.append("select * from ( select row_.*, rownum rownum_ from (").append(sql)
                .append(") row_ where rownum <= ").append(start + limit).append(") where rownum_ > ").append(start);
        }
        return result.toString();
    }

    public String makeCount(String sql)
    {
        return "select count(1) from (" + sql + ") tab_name";
    }
}