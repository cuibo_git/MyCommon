package com.personal.jdbc.dialect;

/**
 * 达梦
 * @author qq
 *
 */
public class DmDialect extends JdbcDialect
{
    public String makePagging(String sql, int start, int limit)
    {
        StringBuilder result = new StringBuilder("select * from (");
        result.append(sql).append(") limit ").append(limit).append(" offset ").append(start);
        return result.toString();
    }

    public String makeCount(String sql)
    {
        return "select count(1) from (" + sql + ")";
    }
}