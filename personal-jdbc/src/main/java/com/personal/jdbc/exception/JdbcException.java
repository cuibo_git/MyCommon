package com.personal.jdbc.exception;

import org.springframework.dao.DataAccessException;

/**
 * JdbcException
 * @author qq
 *
 */
public class JdbcException extends DataAccessException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JdbcException(String msg)
	{
		super(msg);
	}

	public JdbcException(String msg, Throwable cause)
	{
		super(msg, cause);
	}

}
