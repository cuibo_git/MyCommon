package com.personal.jdbc.test;

import java.util.Date;

import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.personal.core.data.DataRow;
import com.personal.core.data.DataTable;
import com.personal.core.utils.CoreUtil;
import com.personal.jdbc.operator.JdbcApp;

public class Test
{
    public static void main(String[] args)
    {
//        sys.db.driver=oracle.jdbc.driver.OracleDriver
//        sys.db.url=jdbc:oracle:thin:@192.168.0.52:1521:ORCL
//        sys.db.username=GXDWPG
//        sys.db.password=PASSWORD
        
        try
        {
            SingleConnectionDataSource source = new SingleConnectionDataSource();
//            Properties properties = new Properties();
//            properties.load(Test.class.getResourceAsStream("dbcp.properties"));
//            source.setConnectionProperties(properties);
            source.setDriverClassName("com.mysql.cj.jdbc.Driver");
//            source.setUrl("jdbc:mysql://127.0.0.1:13306/zyhpj_test?useServerPrepStmts=false&rewriteBatchedStatements=true");
//            source.setUsername("root");
//            source.setPassword("cuibo123456");
            source.setUrl("jdbc:mysql://localhost:3306/ale?useUnicode=true&characterEncoding=UTF-8");
            source.setUsername("root");
            source.setPassword("12345678");
            JdbcApp.registerDataSource(source);
            System.out.println(JdbcApp.getOperator().getConnection());
            
            DataTable table = new DataTable("obj_test_user");
            table.addNewColumn("id");
            table.addNewColumn("name");
            table.addNewColumn("age");    
            table.addNewColumn("createdate");
            table.addNewColumn("remark");
            table.addNewColumn("money");
            table.addNewColumn("showorder");
            
            
            for (int i = 0; i < 1000; i++)
            {
                DataRow newRow = table.addNewRow();
                newRow.setValue("id", CoreUtil.newUuId());
                newRow.setValue("name", "name" + i);
                newRow.setValue("age", i);
                newRow.setValue("createdate", new Date());
                newRow.setValue("remark", "remark");
                newRow.setValue("money", 11111.1111);
                newRow.setValue("showorder", System.currentTimeMillis());
            }
            long time = System.currentTimeMillis();
             System.out.println(JdbcApp.getOperator().batchInsert(table));
            // System.out.println("批量插入：" + (System.currentTimeMillis() - time));
            
           // System.out.println(JdbcApp.getOperator().queryDataCount("select * from obj_test_user"));
            
//           DataTable table = JdbcApp.getOperator().queryForDataTable("select id,name,age from obj_test_user ");
//           
//           System.out.println(table.getRows().size());
            
//            time = System.currentTimeMillis();
//            for (int i = 0; i < 10000; i++)
//            {
//                JdbcApp.getOperator().queryForList("select * from obj_test_user");
//            }
            System.out.println("查询：" + (System.currentTimeMillis() - time));
//           

//            System.out.println(JdbcApp.getOperator().queryForPageAndTotalCount("select id,name,age from obj_test_user ", 0, 100).getTotalCount());
            
//            System.out.println(JdbcApp.getOperator().executeNonQuerySQL("delete from obj_test_user"));
            
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}

