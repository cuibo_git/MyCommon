package com.personal.dao.exception;

/**
 * ClassNotInjectionException
 * @author qq
 *
 */
public class ClassNotInjectionException extends DaoException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ClassNotInjectionException()
    {
	    
    }
	

	public ClassNotInjectionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }


    public ClassNotInjectionException(Throwable cause)
    {
        super(cause);
    }



    public ClassNotInjectionException(String msg)
	{
		super(msg);
	}

	public ClassNotInjectionException(String msg, Throwable cause)
	{
		super(msg, cause);
	}

}
