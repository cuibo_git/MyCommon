package com.personal.dao.exception;

/**
 * DaoException
 * @author qq
 *
 */
public abstract class DaoException extends RuntimeException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public DaoException()
    {
	    
    }
	

	public DaoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }


    public DaoException(Throwable cause)
    {
        super(cause);
    }



    public DaoException(String msg)
	{
		super(msg);
	}

	public DaoException(String msg, Throwable cause)
	{
		super(msg, cause);
	}

}
