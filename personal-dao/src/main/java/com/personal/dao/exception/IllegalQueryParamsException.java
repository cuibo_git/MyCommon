package com.personal.dao.exception;

/**
 * IllegalQueryParamsException
 * @author qq
 *
 */
public class IllegalQueryParamsException extends DaoException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public IllegalQueryParamsException()
    {
	    
    }
	

	public IllegalQueryParamsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }


    public IllegalQueryParamsException(Throwable cause)
    {
        super(cause);
    }



    public IllegalQueryParamsException(String msg)
	{
		super(msg);
	}

	public IllegalQueryParamsException(String msg, Throwable cause)
	{
		super(msg, cause);
	}

}
