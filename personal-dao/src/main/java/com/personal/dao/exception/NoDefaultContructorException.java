package com.personal.dao.exception;

/**
 * IllegalQueryParamsException
 * @author qq
 *
 */
public class NoDefaultContructorException extends DaoException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NoDefaultContructorException()
    {
	    
    }
	

	public NoDefaultContructorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }


    public NoDefaultContructorException(Throwable cause)
    {
        super(cause);
    }



    public NoDefaultContructorException(String msg)
	{
		super(msg);
	}

	public NoDefaultContructorException(String msg, Throwable cause)
	{
		super(msg, cause);
	}

}
