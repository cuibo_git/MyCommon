package com.personal.dao.exception;

/**
 * IllegalQueryParamsException
 * @author qq
 *
 */
public class GetDialectErrorException extends DaoException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public GetDialectErrorException()
    {
	    
    }
	

	public GetDialectErrorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }


    public GetDialectErrorException(Throwable cause)
    {
        super(cause);
    }



    public GetDialectErrorException(String msg)
	{
		super(msg);
	}

	public GetDialectErrorException(String msg, Throwable cause)
	{
		super(msg, cause);
	}

}
