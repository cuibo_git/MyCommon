package com.personal.dao.exception;

/**
 * ValidatorNotPassException
 * @author qq
 *
 */
public class ValidatorNotPassException extends DaoException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ValidatorNotPassException()
    {
	    
    }
	

	public ValidatorNotPassException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }


    public ValidatorNotPassException(Throwable cause)
    {
        super(cause);
    }



    public ValidatorNotPassException(String msg)
	{
		super(msg);
	}

	public ValidatorNotPassException(String msg, Throwable cause)
	{
		super(msg, cause);
	}

}
