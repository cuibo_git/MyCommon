package com.personal.dao.exception;

/**
 * BatchUpdateException
 * @author qq
 *
 */
public class BatchUpdateException extends DaoException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public BatchUpdateException()
    {
	    
    }
	

	public BatchUpdateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }


    public BatchUpdateException(Throwable cause)
    {
        super(cause);
    }



    public BatchUpdateException(String msg)
	{
		super(msg);
	}

	public BatchUpdateException(String msg, Throwable cause)
	{
		super(msg, cause);
	}

}
