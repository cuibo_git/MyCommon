package com.personal.dao.exception;

/**
 * TransactionException
 * @author qq
 *
 */
public class TransactionException extends DaoException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public TransactionException()
    {
	    
    }
	

	public TransactionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }


    public TransactionException(Throwable cause)
    {
        super(cause);
    }



    public TransactionException(String msg)
	{
		super(msg);
	}

	public TransactionException(String msg, Throwable cause)
	{
		super(msg, cause);
	}

}
