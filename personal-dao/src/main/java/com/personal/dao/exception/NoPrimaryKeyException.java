package com.personal.dao.exception;

/**
 * IllegalQueryParamsException
 * @author qq
 *
 */
public class NoPrimaryKeyException extends DaoException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NoPrimaryKeyException()
    {
	    
    }
	

	public NoPrimaryKeyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }


    public NoPrimaryKeyException(Throwable cause)
    {
        super(cause);
    }



    public NoPrimaryKeyException(String msg)
	{
		super(msg);
	}

	public NoPrimaryKeyException(String msg, Throwable cause)
	{
		super(msg, cause);
	}

}
