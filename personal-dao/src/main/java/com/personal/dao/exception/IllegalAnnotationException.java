package com.personal.dao.exception;

/**
 * IllegalQueryParamsException
 * @author qq
 *
 */
public class IllegalAnnotationException extends DaoException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public IllegalAnnotationException()
    {
	    
    }
	

	public IllegalAnnotationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }


    public IllegalAnnotationException(Throwable cause)
    {
        super(cause);
    }



    public IllegalAnnotationException(String msg)
	{
		super(msg);
	}

	public IllegalAnnotationException(String msg, Throwable cause)
	{
		super(msg, cause);
	}

}
