package com.personal.dao.exception;

/**
 * IllegalQueryParamsException
 * @author qq
 *
 */
public class NoPrimaryKeyAnnotationException extends DaoException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NoPrimaryKeyAnnotationException()
    {
	    
    }
	

	public NoPrimaryKeyAnnotationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }


    public NoPrimaryKeyAnnotationException(Throwable cause)
    {
        super(cause);
    }



    public NoPrimaryKeyAnnotationException(String msg)
	{
		super(msg);
	}

	public NoPrimaryKeyAnnotationException(String msg, Throwable cause)
	{
		super(msg, cause);
	}

}
