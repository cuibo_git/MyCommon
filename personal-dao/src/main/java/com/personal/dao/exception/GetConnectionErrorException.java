package com.personal.dao.exception;

/**
 * GetConnectionErrorException
 * @author qq
 *
 */
public class GetConnectionErrorException extends DaoException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public GetConnectionErrorException()
    {
	    
    }
	

	public GetConnectionErrorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }


    public GetConnectionErrorException(Throwable cause)
    {
        super(cause);
    }



    public GetConnectionErrorException(String msg)
	{
		super(msg);
	}

	public GetConnectionErrorException(String msg, Throwable cause)
	{
		super(msg, cause);
	}

}
