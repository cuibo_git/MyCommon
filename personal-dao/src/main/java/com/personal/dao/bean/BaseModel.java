package com.personal.dao.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * 数据库对象基础类
 * @author cuibo
 *
 */
public abstract class BaseModel implements Serializable, Cloneable
{
    /**
     * 
     */
    private static final long serialVersionUID = -4612512757343955789L;
    /** 排序字段 */
    protected String orderByClause;
    /** distinct查询 */
    protected boolean distinct;
    /** 复杂条件 */
    protected List<Criteria> oredCriteria;
    /** 自定义需要查询的列 */
    protected Set<String> queryFieldNames;
    /** 分页开始 */
    protected Integer start;
    /** 分页结束 */
    protected Integer limit;
    /** 是否查询大字段 */
    protected boolean selectBigData = true;

    public List<Criteria> getOredCriteria()
    {
        if (oredCriteria == null)
        {
            oredCriteria = new ArrayList<Criteria>(1);
        }
        return oredCriteria;
    }

    /**
     * 添加至oredCriteria中
     * @param criteria
     * @return
     */
    public boolean addCriteria(Criteria criteria)
    {
        return getOredCriteria().add(criteria);
    }
    
    /**
     * 添加查询列
     * @param fieldName
     * @return
     */
    public boolean addQueryFieldName(String fieldName)
    {
        if (queryFieldNames == null)
        {
            queryFieldNames = new LinkedHashSet<>();
        }
        return queryFieldNames.add(fieldName);
    }
    
    /**
     * 创建一个并加到oredCriteria中
     * @return
     */
    public Criteria createCriteria()
    {
        Criteria criteria = new Criteria();
        getOredCriteria().add(criteria);
        return criteria;
    }

    public void clear()
    {
        oredCriteria = null;
        orderByClause = null;
        distinct = false;
        start = null;
        limit = null; 
        queryFieldNames = null;
    }

    public String getOrderByClause()
    {
        return orderByClause;
    }

    public void setOrderByClause(String orderByClause)
    {
        this.orderByClause = orderByClause;
    }

    public boolean isDistinct()
    {
        return distinct;
    }

    public void setDistinct(boolean distinct)
    {
        this.distinct = distinct;
    }

    public void setOredCriteria(List<Criteria> oredCriteria)
    {
        this.oredCriteria = oredCriteria;
    }

    public Integer getStart()
    {
        return start;
    }

    public void setStart(Integer start)
    {
        this.start = start;
    }

    public Integer getLimit()
    {
        return limit;
    }

    public void setLimit(Integer limit)
    {
        this.limit = limit;
    }

    public Set<String> getQueryFieldNames()
    {
        return queryFieldNames;
    }

    public void setQueryFieldNames(Set<String> queryFieldNames)
    {
        this.queryFieldNames = queryFieldNames;
    }

    public boolean isSelectBigData()
    {
        return selectBigData;
    }

    public void setSelectBigData(boolean selectBigData)
    {
        this.selectBigData = selectBigData;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }
    
}
