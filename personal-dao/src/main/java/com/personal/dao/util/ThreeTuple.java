package com.personal.dao.util;

/**
 * 三元组
 * @author qq
 *
 * @param <A>
 * @param <B>
 * @param <C>
 */
public class ThreeTuple<A,B,C> extends TwoTuple<A, B>
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private C c;
    

    public ThreeTuple()
    {
        super();
    }

    public ThreeTuple(A a, B b, C c)
    {
    	super(a, b);
        this.c = c;
    }

	public C getC()
	{
		return c;
	}

	public void setC(C c)
	{
		this.c = c;
	}
}
