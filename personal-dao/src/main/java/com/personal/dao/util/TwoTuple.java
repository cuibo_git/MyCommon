package com.personal.dao.util;

import java.io.Serializable;

/**
 * 二元组
 * @author cuibo
 * @param <A>
 * @param <B>
 */
public class TwoTuple<A,B> implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private A a;
    
    private B b;

    public TwoTuple()
    {
        super();
    }

    public TwoTuple(A a, B b)
    {
        this.a = a;
        this.b = b;
    }
    
    public A getA()
    {
        return a;
    }

    public void setA(A a)
    {
        this.a = a;
    }

    public B getB()
    {
        return b;
    }

    public void setB(B b)
    {
        this.b = b;
    }
}
