package com.personal.dao.util;

import java.util.Collection;
import java.util.Map;

/**
 * DaoUtil
 * @author cuibo
 *
 */
public class DaoUtil
{
    /**
     * 集合是否为空
     * @param coll
     * @return
     */
    public static boolean isEmpty(Collection<?> coll)
    {
        return coll == null || coll.isEmpty();
    }

    public static boolean isEmpty(Object obj)
    {
        String str = obj == null ? null : obj.toString();
        return str == null || str.length() == 0 || str.trim().length() == 0;
    }
    
    public static boolean isEmpty(String obj)
    {
        return obj == null || obj.length() == 0 || obj.trim().length() == 0;
    }

    public static boolean isEmpty(Object[] arr)
    {
        return arr == null || arr.length == 0;
    }

    /**
     * Map是否为空
     * @param coll
     * @return
     */
    public static boolean isEmpty(Map<?, ?> coll)
    {
        return coll == null || coll.isEmpty();
    }
}
