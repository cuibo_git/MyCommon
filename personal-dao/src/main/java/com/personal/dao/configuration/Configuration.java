package com.personal.dao.configuration;

import java.util.List;

import com.personal.dao.handler.BatchSqlHandler;
import com.personal.dao.handler.SqlHandler;
import com.personal.dao.handler.SqlHandlerFactory;
import com.personal.dao.metadata.DataBaseTableMetadata;
import com.personal.dao.metadata.DataBaseTableMetadataRegistry;
import com.personal.dao.reflection.DefaultReflectorFactory;
import com.personal.dao.reflection.MetaClass;
import com.personal.dao.reflection.MetaObject;
import com.personal.dao.reflection.ReflectorFactory;
import com.personal.dao.reflection.factory.DefaultObjectFactory;
import com.personal.dao.reflection.factory.ObjectFactory;
import com.personal.dao.reflection.wrapper.DefaultObjectWrapperFactory;
import com.personal.dao.reflection.wrapper.ObjectWrapperFactory;
import com.personal.dao.type.TypeHandlerRegistry;

/**
 * Configuration
 * @author cuibo
 *
 */
public class Configuration
{

    private Configuration()
    {
    }

    private static final Configuration CONFIGURATION = new Configuration();

    /** 元数据注册 */
    private final DataBaseTableMetadataRegistry metadataRegistry = new DataBaseTableMetadataRegistry();
    
    /** 反射工厂 */
    private final ReflectorFactory reflectorFactory = new DefaultReflectorFactory();

    /** 事物 */
    private final ObjectFactory objectFactory = new DefaultObjectFactory();

    /** 事物转换工厂 */
    private final ObjectWrapperFactory objectWrapperFactory = new DefaultObjectWrapperFactory();
    
    /** Sql处理工厂 */
    private final SqlHandlerFactory sqlHandlerFactory = new SqlHandlerFactory();

    /** 类型注册 */
    private final TypeHandlerRegistry typeHandlerRegistry = new TypeHandlerRegistry();
    
    /**
     * 获取实例
     * @return
     */
    public static Configuration getInstance()
    {
        return CONFIGURATION;
    }

    /**
     * 创建对象的MetaObject
     * @param object
     * @return
     */
    public MetaObject newMetaObject(Object object)
    {
        return MetaObject.forObject(object, objectFactory, objectWrapperFactory, reflectorFactory);
    }

    /**
     * 创建类型的MetaClass
     * @param type
     * @return
     */
    public MetaClass newMetaClass(Class<?> type)
    {
        return MetaClass.forClass(type, reflectorFactory);
    }

    /**
     * 获取默认的查询Long处理
     * @return
     */
    public SqlHandler<Long> getDefaultQueryLongHandler()
    {
        return sqlHandlerFactory.getDefaultQueryLongHandler();
    }

    /**
     * 获取默认的更新Integer处理
     * @return
     */
    public SqlHandler<Integer> getDefaultUpdateIntegerHandler()
    {
        return sqlHandlerFactory.getDefaultUpdateIntegerHandler();
    }

    /**
     * 获取默认的批量更新Integer处理
     * @return
     */
    public BatchSqlHandler<Integer> getDefaultBatchUpdateIntegerHandler()
    {
        return sqlHandlerFactory.getDefaultBatchUpdateIntegerHandler();
    }

    /**
     * 新建查询列表处理
     * @param metadata
     * @return
     */
    public <T> SqlHandler<List<T>> newQueryListHandler(DataBaseTableMetadata<T> metadata)
    {
        return sqlHandlerFactory.newQueryListHandler(metadata);
    }

    /**
     * 新建查询单个实体处理
     * @param metadata
     * @return
     */
    public <T> SqlHandler<T> newQueryObjectHandler(DataBaseTableMetadata<T> metadata)
    {
        return sqlHandlerFactory.newQueryObjectHandler(metadata);
    }

    /**
     * 获取指定类型的元数据
     * @param t
     * @return
     */
    public <T> DataBaseTableMetadata<T> getDataBaseTableMetadata(Class<T> t)
    {
        return metadataRegistry.getDataBaseTableMetadata(t);
    }

    /**
     * 把指定类型的类，注册到缓存
     * @param t
     * @return
     */
    public <T> DataBaseTableMetadata<T> registerDataBaseTableMetadata(Class<T> t)
    {
        return metadataRegistry.registerDataBaseTableMetadata(t);
    }

    public ReflectorFactory getReflectorFactory()
    {
        return reflectorFactory;
    }

    public ObjectFactory getObjectFactory()
    {
        return objectFactory;
    }

    public ObjectWrapperFactory getObjectWrapperFactory()
    {
        return objectWrapperFactory;
    }

    public TypeHandlerRegistry getTypeHandlerRegistry()
    {
        return typeHandlerRegistry;
    }

}
