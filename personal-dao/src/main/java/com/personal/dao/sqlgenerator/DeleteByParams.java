package com.personal.dao.sqlgenerator;

import com.personal.dao.bean.BaseModel;
import com.personal.dao.dialect.DaoDialect;
import com.personal.dao.metadata.DataBaseTableMetadata;
import com.personal.dao.sql.BaseSql;
import com.personal.dao.sql.Sql;

/**
 * 通过条件查询个数
 * @author cuibo
 *
 */
public class DeleteByParams<T extends BaseModel> extends BaseSqlGenerator<T>
{

    public DeleteByParams(T params, DataBaseTableMetadata<T> metadata, DaoDialect dialect)
    {
        super(params, metadata, dialect);
    }


    @Override
    public Sql generate()
    {
        BaseSql sql = generateCondition();
        StringBuilder builder = new StringBuilder("delete from ").append(metadata.getTableName());
        if (sql != null)
        {
        	builder.append(" where ").append(sql.getSql());
        	sql.setSql(builder.toString());
            return sql;
        } else
        {
            return new BaseSql(builder.toString());
        }
    }


}
