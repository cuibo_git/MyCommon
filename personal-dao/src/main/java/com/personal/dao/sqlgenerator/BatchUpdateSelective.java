package com.personal.dao.sqlgenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.personal.dao.bean.BaseModel;
import com.personal.dao.configuration.Configuration;
import com.personal.dao.exception.NoPrimaryKeyException;
import com.personal.dao.metadata.DataBaseFieldMetadata;
import com.personal.dao.metadata.DataBaseTableMetadata;
import com.personal.dao.reflection.MetaObject;
import com.personal.dao.sql.BaseBatchSql;

/**
 * 批量插入
 * @author cuibo
 *
 */
public class BatchUpdateSelective<T extends BaseModel> extends BaseBatchSqlGenerator<T>
{

    public BatchUpdateSelective(List<T> records, DataBaseTableMetadata<T> metadata)
    {
        super(records, metadata);
    }

    @Override
    public BaseBatchSql generate()
    {
        // 次操作需要校验主键
        checkPrimaryKey();
        // 查找出最大的属性实体
        Set<DataBaseFieldMetadata> maxMetadats = new LinkedHashSet<>();
        // 所有的数据缓存
        Map<T, Map<DataBaseFieldMetadata, Object>> allDatas = new HashMap<>();
        for (T t : records)
        {
        	MetaObject metaObject = Configuration.getInstance().newMetaObject(t);
        	// 校验
        	SqlValidator.validate(metadata, t, metaObject);
            Map<DataBaseFieldMetadata, Object> data = new HashMap<>();
            // 加上主键
            Object pk = metaObject.getValue(metadata.getPrimaryKey().getFieldName());
            if (pk == null)
            {
                throw new NoPrimaryKeyException(t + "的主键为空！");
            }
            data.put(metadata.getPrimaryKey(), pk);
            for (DataBaseFieldMetadata field : metadata.getFields())
            {
                Object value = metaObject.getValue(field.getFieldName());
                if (value != null)
                {
                    data.put(field, value);
                    maxMetadats.add(field);
                }
            }
            allDatas.put(t, data);
        }
        
        List<Object[]> datas = new ArrayList<>(records.size());
        StringBuilder sql = new StringBuilder("update ").append(metadata.getTableName());
        sql.append(" set ");
        int index = -1;
        for (T record : records)
        {
            index ++;
            Object[] data = new Object[maxMetadats.size() + 1];
            int colIndex = 0;
            Map<DataBaseFieldMetadata, Object> local = allDatas.get(record);
            for (DataBaseFieldMetadata field : maxMetadats)
            {
                Object value = local.get(field);
                data[colIndex ++] = value;
                if (index == 0)
                {
                    sql.append(field.getColumnName()).append(" = ?,");
                }
            }
            // 所有的data都加上主键
            data[data.length - 1] = local.get(metadata.getPrimaryKey());
            datas.add(data);
        }
        // 如果值不为空，则截取最后的,
        sql.deleteCharAt(sql.length() - 1);
        sql.append(" where ").append(metadata.getPrimaryKey().getColumnName()).append(" = ?");
        return new BaseBatchSql(sql.toString(), datas);
    }

}
