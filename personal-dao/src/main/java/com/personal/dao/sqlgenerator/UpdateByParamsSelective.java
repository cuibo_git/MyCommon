package com.personal.dao.sqlgenerator;

import java.util.ArrayList;
import java.util.List;

import com.personal.dao.bean.BaseModel;
import com.personal.dao.dialect.DaoDialect;
import com.personal.dao.metadata.DataBaseTableMetadata;
import com.personal.dao.sql.BaseSql;
import com.personal.dao.sql.Sql;

/**
 * 通过条件查询个数
 * @author cuibo
 *
 */
public class UpdateByParamsSelective<T extends BaseModel> extends BaseSqlGenerator<T>
{
 
    public UpdateByParamsSelective(T value, T params, DataBaseTableMetadata<T> metadata, DaoDialect dialect)
    {
        super(params, metadata, dialect);
        this.value = value;
    }
    
    
    @Override
    public Sql generate()
    {
        List<Object> all = new ArrayList<Object>();
        BaseSql update = generateValue(true, true);
        all.addAll(update.getValues());
        BaseSql condition = generateCondition();
        StringBuilder sql = new StringBuilder("update ").append(metadata.getTableName());
        sql.append(" set ").append(update.getSql());
        if (condition != null)
        {
            sql.append(" where ").append(condition.getSql());
            all.addAll(condition.getValues());
        }
        return new BaseSql(sql.toString(), all);
    }

}
