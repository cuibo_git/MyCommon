package com.personal.dao.sqlgenerator;

import com.personal.dao.bean.BaseModel;
import com.personal.dao.dialect.DaoDialect;
import com.personal.dao.metadata.DataBaseTableMetadata;
import com.personal.dao.sql.BaseSql;
import com.personal.dao.sql.Sql;

/**
 * 通过条件查询个数
 * @author cuibo
 *
 */
public class DeleteByPrimaryKey<T extends BaseModel> extends BaseSqlGenerator<T>
{

    public DeleteByPrimaryKey(Object primaryKey, DataBaseTableMetadata<T> metadata, DaoDialect dialect)
    {
        super(primaryKey, metadata, dialect);
    }


    @Override
    public Sql generate()
    {
        checkPrimaryKey();
        StringBuilder sql = new StringBuilder("delete from ").append(metadata.getTableName());
        sql.append(" where ").append(metadata.getPrimaryKey().getColumnName()).append(" = ?");
        Sql result = new BaseSql(sql.toString(), primaryKey);
        return result;
    }


}
