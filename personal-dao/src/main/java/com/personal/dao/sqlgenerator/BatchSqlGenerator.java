package com.personal.dao.sqlgenerator;

import com.personal.dao.sql.BatchSql;

/**
 * 条件生成器
 * @author cuibo
 *
 */
public interface BatchSqlGenerator
{
    public abstract BatchSql generate();
    
}
