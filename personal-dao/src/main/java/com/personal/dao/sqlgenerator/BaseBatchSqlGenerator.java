package com.personal.dao.sqlgenerator;

import java.util.List;

import com.personal.dao.bean.BaseModel;
import com.personal.dao.exception.NoPrimaryKeyAnnotationException;
import com.personal.dao.metadata.DataBaseTableMetadata;

/**
 * 基础的Sql生成器
 * 
 * @author cuibo
 *
 */
public abstract class BaseBatchSqlGenerator<T extends BaseModel> implements BatchSqlGenerator
{

	/** 主键 */
	protected List<T> records;

	/** 元数据 */
	protected DataBaseTableMetadata<T> metadata;

	// public BaseBatchSqlGenerator()
	// {
	// super();
	// }

	public BaseBatchSqlGenerator(List<T> records, DataBaseTableMetadata<T> metadata)
	{
		super();
		this.records = records;
		this.metadata = metadata;
	}

	protected void checkPrimaryKey()
	{
		if (metadata.getPrimaryKey() == null)
		{
			throw new NoPrimaryKeyAnnotationException(metadata.getClass() + "对应的主键配置伪空，不支持此方法的操作！");
		}
	}

}
