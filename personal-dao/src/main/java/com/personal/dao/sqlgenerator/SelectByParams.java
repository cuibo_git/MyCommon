package com.personal.dao.sqlgenerator;

import com.personal.dao.bean.BaseModel;
import com.personal.dao.dialect.DaoDialect;
import com.personal.dao.metadata.DataBaseTableMetadata;
import com.personal.dao.sql.BaseSql;
import com.personal.dao.sql.Sql;

/**
 * 通过条件查询个数
 * @author cuibo
 *
 */
public class SelectByParams<T extends BaseModel> extends BaseSqlGenerator<T>
{

    public SelectByParams(T params, DataBaseTableMetadata<T> metadata, DaoDialect dialect)
    {
        super(params, metadata, dialect);
    }


    @Override
    public Sql generate()
    {
        BaseSql sql = generateCondition();
        StringBuilder sqlBuilder = new StringBuilder("select ").append(generateQuery()).append(" from ").append(metadata.getTableName());
        if (sql != null)
        {
        	sqlBuilder.append(" where ").append(sql.getSql());
            // 追加分页和Orderby
            appendPageAndOrderby(sqlBuilder);
            sql.setSql(sqlBuilder.toString());
            sql.setReadCache(true);
            return sql;
        } else
        {
        	// 追加分页和Orderby
            appendPageAndOrderby(sqlBuilder);
            return new BaseSql(sqlBuilder.toString(), true);
        }
    }

}
