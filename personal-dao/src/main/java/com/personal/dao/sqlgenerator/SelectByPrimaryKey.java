package com.personal.dao.sqlgenerator;

import com.personal.dao.bean.BaseModel;
import com.personal.dao.dialect.DaoDialect;
import com.personal.dao.metadata.DataBaseTableMetadata;
import com.personal.dao.sql.BaseSql;
import com.personal.dao.sql.Sql;

/**
 * 通过条件查询个数
 * @author cuibo
 *
 */
public class SelectByPrimaryKey<T extends BaseModel> extends BaseSqlGenerator<T>
{

    public SelectByPrimaryKey(Object primaryKey, DataBaseTableMetadata<T> metadata, DaoDialect dialect)
    {
        super(primaryKey, metadata, dialect);
    }


    @Override
    public Sql generate()
    {
        checkPrimaryKey();
        StringBuilder sql = new StringBuilder("select * from ").append(metadata.getTableName());
        sql.append(" where ").append(metadata.getPrimaryKey().getColumnName()).append(" = ?");
        BaseSql result = new BaseSql(sql.toString(), primaryKey);
        result.setReadCache(true);
        return result;
    }

}
