package com.personal.dao.sqlgenerator;

import com.personal.dao.sql.Sql;

/**
 * Sql生成器
 * @author cuibo
 *
 */
public interface SqlGenerator
{

    public Sql generate();
    
    
}
