package com.personal.dao.sqlgenerator;

import com.personal.dao.bean.BaseModel;
import com.personal.dao.metadata.DataBaseTableMetadata;
import com.personal.dao.sql.BaseSql;
import com.personal.dao.sql.Sql;

/**
 * 通过条件查询个数
 * @author cuibo
 *
 */
public class SelectAll<T extends BaseModel> extends BaseSqlGenerator<T>
{


	public SelectAll(DataBaseTableMetadata<T> metadata)
	{
		super(metadata);
	}
	

    @Override
    public Sql generate()
    {
        return new BaseSql("select * from " + metadata.getTableName(), true);
    }

}
