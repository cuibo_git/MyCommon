package com.personal.dao.cache;

/**
 * 读缓存
 * @author qq
 *
 */
public interface ReadCache
{
	boolean isReadCache();
}
