package com.personal.dao.cache;

/**
 * 刷新缓存
 * @author qq
 *
 */
public interface FlushCache
{
	boolean isFlushCache();
}
