package com.personal.dao.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.personal.dao.enums.MatchModelEnum;
import com.personal.dao.enums.OperatorEnum;

/**
 * 数据库字段注解
 * 用于表示实体的属性是数据库字段
 * @author qq
 *
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface DataBaseField
{
	String columnName() default "";
	
	String caption() default "";
	
	OperatorEnum operator() default OperatorEnum.EQ;
	
	MatchModelEnum matchModel() default MatchModelEnum.ALL;
	
	boolean bigData() default false;
	
}
