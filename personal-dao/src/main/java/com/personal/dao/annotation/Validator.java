package com.personal.dao.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 校验
 * @author qq
 *
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface Validator
{
	int maxLength() default -1;
	
	String errorMessage() default ""; 
}
