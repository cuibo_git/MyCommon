package com.personal.dao.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * DataBaseTable注解
 * @author qq
 *
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface DataBaseTable
{
	String tableName() default "";
	
}
