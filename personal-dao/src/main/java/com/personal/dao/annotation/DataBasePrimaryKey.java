package com.personal.dao.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.personal.dao.enums.PrimaryKeyGeneratorEnum;

/**
 * 主键注解
 * @author qq
 *
 */
@Retention(RUNTIME)
@Target(FIELD)
public @interface DataBasePrimaryKey
{
    String columnName() default "";
    
    String caption() default "";
    
    PrimaryKeyGeneratorEnum generator() default PrimaryKeyGeneratorEnum.UUID;
}
