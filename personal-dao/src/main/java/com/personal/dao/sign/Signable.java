package com.personal.dao.sign;

import java.util.List;

/**
 * 具有标记性的：用做缓存的key的生成
 * @author qq
 *
 */
public interface Signable
{
	/** 获取标记 */
	List<Object> getSignFlags();
}
