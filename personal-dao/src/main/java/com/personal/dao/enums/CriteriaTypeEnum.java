package com.personal.dao.enums;

/**
 * 条件类型枚举
 * @author cuibo
 *
 */
public enum CriteriaTypeEnum
{
    无值条件,
    单值条件,
    前后范围值条件,
    多个范围值条件,
    自定义条件;
    
}
