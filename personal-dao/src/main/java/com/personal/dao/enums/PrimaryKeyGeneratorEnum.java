package com.personal.dao.enums;

import java.util.UUID;

/**
 * 主键生成
 * @author cuibo
 *
 */
public enum PrimaryKeyGeneratorEnum
{
    UUID {
		@Override
		public Object generatorPrimaryKey()
		{
			UUID uuid = java.util.UUID.randomUUID();
	        String primaryKey = uuid.toString().replace("-", "");
	        return primaryKey;
		}
	};
	
	public abstract Object generatorPrimaryKey();
}
