package com.personal.dao.enums;


/**
 * 匹配模式
 * @author cuibo
 *
 */
public enum MatchModelEnum
{
    ALL,
    BEFORE,
    AFTER;
    
}
