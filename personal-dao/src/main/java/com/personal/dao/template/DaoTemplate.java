package com.personal.dao.template;

import org.springframework.jdbc.core.JdbcTemplate;

import com.personal.dao.cache.CacheKey;
import com.personal.dao.dialect.DaoDialect;
import com.personal.dao.handler.BatchSqlHandler;
import com.personal.dao.handler.SqlHandler;
import com.personal.dao.sign.Signable;
import com.personal.dao.sql.BatchSql;
import com.personal.dao.sql.Sql;

/**
 * Dao的操作模板
 * @author cuibo
 *
 */
public interface DaoTemplate
{
    
    <V> V execute(Sql sql, SqlHandler<V> handler);
    
    <V> V execute(BatchSql sql, BatchSqlHandler<V> handler);
    
    DaoDialect getDaoDialect();
    
    JdbcTemplate geJdbcTemplate();
    
    <V> CacheKey createCacheKey(Signable... signables);

}
