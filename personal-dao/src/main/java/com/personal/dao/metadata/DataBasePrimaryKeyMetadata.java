package com.personal.dao.metadata;

import java.io.Serializable;
import java.lang.reflect.Field;

import com.personal.dao.annotation.DataBasePrimaryKey;
import com.personal.dao.enums.PrimaryKeyGeneratorEnum;
import com.personal.dao.util.DaoUtil;

/**
 * 主键元数据
 * 
 * @author cuibo
 *
 */
public class DataBasePrimaryKeyMetadata extends DataBaseFieldMetadata implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 主键生成方式 */
	private PrimaryKeyGeneratorEnum generator;

	public PrimaryKeyGeneratorEnum getGenerator()
	{
		return generator;
	}

	void setGenerator(PrimaryKeyGeneratorEnum generator)
	{
		this.generator = generator;
	}

	public void init(Field field, DataBasePrimaryKey primaryKey)
	{
		setFieldName(field.getName());
		setColumnName(primaryKey.columnName());
		setCaption(primaryKey.caption());
		setC(field.getType());
		if (DaoUtil.isEmpty(getColumnName()))
		{
			setColumnName(getFieldName());
		}
		if (DaoUtil.isEmpty(getCaption()))
		{
			setCaption(getFieldName());
		}
		setGenerator(primaryKey.generator());
	}

}
