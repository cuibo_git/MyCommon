package com.personal.dao.metadata;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Locale;

import com.personal.dao.annotation.DataBaseField;
import com.personal.dao.enums.MatchModelEnum;
import com.personal.dao.enums.OperatorEnum;
import com.personal.dao.util.DaoUtil;

/**
 * 列元数据
 * 
 * @author cuibo
 *
 */
public class DataBaseFieldMetadata implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 类 */
    private Class<?> c;
    
	/** 字段名 */
	private String fieldName;

	/** 列名 */
	private String columnName;

	/** 备注 */
	private String caption;

	/** 操作方式 */
	private OperatorEnum operator;

	/** 匹配模式 */
	private MatchModelEnum matchModel;

	/** 是否是大字段 */
	private boolean bigData;

	/** 校验 */
	private ValidatorMetadata validator;
	
	/** 是否过期 */
	private boolean deprecated;

	public String getFieldName()
	{
		return fieldName;
	}

	void setFieldName(String fieldName)
	{
		this.fieldName = fieldName;
	}

	public String getColumnName()
	{
		return columnName;
	}

	void setColumnName(String columnName)
	{
		this.columnName = columnName;
	}

	public String getCaption()
	{
		return caption;
	}

	void setCaption(String caption)
	{
		this.caption = caption;
	}

	public OperatorEnum getOperator()
	{
		return operator;
	}

	void setOperator(OperatorEnum operator)
	{
		this.operator = operator;
	}

	public MatchModelEnum getMatchModel()
	{
		return matchModel;
	}

	void setMatchModel(MatchModelEnum matchModel)
	{
		this.matchModel = matchModel;
	}

	public boolean isBigData()
	{
		return bigData;
	}

	void setBigData(boolean bigData)
	{
		this.bigData = bigData;
	}

	public ValidatorMetadata getValidator()
	{
		return validator;
	}

	void setValidator(ValidatorMetadata validator)
	{
		this.validator = validator;
	}
	
	public Class<?> getC()
    {
        return c;
    }

    void setC(Class<?> c)
    {
        this.c = c;
    }

    public boolean isDeprecated()
	{
		return deprecated;
	}

	void setDeprecated(boolean deprecated)
	{
		this.deprecated = deprecated;
	}

	public void init(Field field, DataBaseField dataBaseField, Deprecated deprecated)
	{
		setFieldName(field.getName());
		setColumnName(dataBaseField.columnName());
		setCaption(dataBaseField.caption());
		setOperator(dataBaseField.operator());
		setMatchModel(dataBaseField.matchModel());
		setBigData(dataBaseField.bigData());
		setC(field.getType());
		if (DaoUtil.isEmpty(getColumnName()))
		{
			setColumnName(getFieldName());
		}
		if (DaoUtil.isEmpty(getCaption()))
		{
			setCaption(getFieldName());
		}
		// 列名统一转小写
        setColumnName(getColumnName().toLowerCase(Locale.ENGLISH));
        // 是否过期
		if (deprecated != null)
		{
			setDeprecated(true);
		}
	}

}
