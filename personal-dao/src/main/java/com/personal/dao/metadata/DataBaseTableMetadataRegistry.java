package com.personal.dao.metadata;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.personal.dao.copyonwrite.CopyOnWriteHashMap;

/**
 * DataBaseTableMetadataRegistry
 * @author cuibo
 *
 */
public class DataBaseTableMetadataRegistry
{
    
    private static final Log LOG = LogFactory.getLog(DataBaseTableMetadataRegistry.class);
    
    /** 元数据缓存 */
    private final Map<String, DataBaseTableMetadata<?>> cache = new CopyOnWriteHashMap<>();
    
    /**
     * 获取指定类型的元数据
     * @param t
     * @return
     */
	@SuppressWarnings("unchecked")
	public <T> DataBaseTableMetadata<T> getDataBaseTableMetadata(Class<T> t)
    {
        return (DataBaseTableMetadata<T>) cache.get(t.getName());
    }

    /**
     * 把指定类型的类，注册到缓存
     * @param t
     * @return
     */
    public <T> DataBaseTableMetadata<T> registerDataBaseTableMetadata(Class<T> t)
    {
        DataBaseTableMetadata<T> metadata = new DataBaseTableMetadata<T>();
        metadata.initByBaseModel(t);
        cache.put(t.getName(), metadata);
        LOG.info("注册：" + t + "成功！", null);
        return metadata;
    }
}
