package com.personal.dao.metadata;

import java.io.Serializable;

import com.personal.dao.annotation.Validator;

/**
 * 校验元数据
 * @author qq
 *
 */
public class ValidatorMetadata implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int maxLength;
	
	private String errorMessage;

	public void init(Validator validator)
	{
		setMaxLength(validator.maxLength());
		setErrorMessage(validator.errorMessage());
	}

	public int getMaxLength()
	{
		return maxLength;
	}

	void setMaxLength(int maxLength)
	{
		this.maxLength = maxLength;
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

}
