package com.personal.dao.rowmapper;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.personal.dao.configuration.Configuration;
import com.personal.dao.handler.HandlerUtil;
import com.personal.dao.metadata.DataBaseTableMetadata;

public class BaseModelRowMapper<T> implements RowMapper<T>
{

    private DataBaseTableMetadata<T> metadata;
    
    public BaseModelRowMapper(DataBaseTableMetadata<T> metadata)
    {
        super();
        this.metadata = metadata;
    }

    @Override
    public T mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        // 获取元数据
        ResultSetMetaData sqlmetaData = rs.getMetaData();
        // 生成对象
        T result = Configuration.getInstance().getObjectFactory().create(metadata.getC());
        HandlerUtil.setResultData2MetaObject(rs, sqlmetaData, metadata, result);
        return result;
    }

}
