package com.personal.dao.base;

import java.util.List;

import com.personal.dao.bean.Page;
import com.personal.dao.handler.BatchSqlHandler;
import com.personal.dao.handler.SqlHandler;
import com.personal.dao.metadata.DataBaseTableMetadata;
import com.personal.dao.sql.BatchSql;
import com.personal.dao.sql.Sql;
import com.personal.dao.template.DaoTemplate;

/**
 * 基础Dao
 * @author qq
 *
 */
public interface IBaseDao<T>
{
    long countByParams(T params);

    int deleteByParams(T params);

    int deleteByPrimaryKey(Object primaryKey);

    int insert(T record);

    int insertSelective(T record);

    List<T> selectByParams(T params);
    
    List<T> selectAll();
    
    Page<T> selectPageByParams(T params);

    T selectByPrimaryKey(Object primaryKey);
    
    T selectByPrimaryKeyWithoutBigData(Object primaryKey);

    int updateByParamsSelective(T record, T params);
    
    int updateByParams(T record, T params);

    int updateByPrimaryKeySelective(T record);

    int updateByPrimaryKey(T record);
    
    int batchInsert(List<T> records);
    
    int batchInsertSelective(List<T> records);
    
    int batchUpdate(List<T> records);
    
    int batchUpdateSelective(List<T> records);
    
    <V> V execute(Sql sql, SqlHandler<V> handler);
    
    <V> V execute(BatchSql sql, BatchSqlHandler<V> handler);
    
    DataBaseTableMetadata<T> getDataBaseTableMetadata();
    
    DaoTemplate getDaoTemplate();
    
}
