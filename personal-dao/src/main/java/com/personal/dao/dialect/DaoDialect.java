package com.personal.dao.dialect;

import java.util.Locale;

/**
 * Jdbc方言
 * @author qq
 *
 */
public abstract class DaoDialect
{
    public abstract String makePagging(String sql, int start, int limit);

    public abstract String makeCount(String sql);

    public static DaoDialect getDialect(String dialect)
    {
        dialect = dialect.toLowerCase(Locale.ENGLISH);
        // if ("Oracle".equalsIgnoreCase(dialect))
        if (dialect.contains("oracle"))
        {
            return new Oracle10GDialect();
        }
        // if ("MYSQL".equalsIgnoreCase(dialect))
        if (dialect.contains("mysql"))
        {
            return new MySqlDialect();
        }
        // if ("Microsoft SQL Server".equalsIgnoreCase(dialect))
        if (dialect.contains("sqlserver") || dialect.contains("sql server"))
        {
            return new SqlServerDialect();
        }
        // if ("PostgreSQL".equalsIgnoreCase(dialect))
        if (dialect.contains("postgresql"))
        {
            return new PostgreDialect();
        }
        // if ("DM DBMS".equalsIgnoreCase(dialect))
        if (dialect.contains("dm dbms"))
        {
            return new DmDialect();
        }
        return null;
    }
}