package com.personal.dao.dialect;

import java.util.Locale;

/**
 * Mysql
 * @author qq
 *
 */
public class MySqlDialect extends DaoDialect
{
	public String makePagging(String sql, int start, int limit)
	{
		StringBuilder result = new StringBuilder();
		result.append(sql).append(" limit ").append(start).append(" , ").append(limit);
		return result.toString();
	}

	public String makeCount(String sql)
	{
	    String temp = sql.toLowerCase(Locale.ENGLISH);
		return temp.startsWith("select * from ") ? "select count(1) from " + sql.substring(14)
				: "select count(1) from (" + sql + ") tab_name";
	}
}