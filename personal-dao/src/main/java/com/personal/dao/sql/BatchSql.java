package com.personal.dao.sql;

import java.util.List;

import com.personal.dao.cache.ReadCache;
import com.personal.dao.sign.Signable;

/**
 * 批量语句条件
 * @author qq
 *
 */
public interface BatchSql extends Signable, ReadCache
{

	public String getSql();
	
    public List<Object[]> getParams();
	
}
