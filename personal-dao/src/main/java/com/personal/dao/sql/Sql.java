package com.personal.dao.sql;

import com.personal.dao.cache.ReadCache;
import com.personal.dao.sign.Signable;

/**
 * 条件
 * @author qq
 *
 */
public interface Sql extends Signable, ReadCache
{
	/** 获取执行的语句 */
	public String getSql();
	
	/** 获取语句的参数值 */
    public Object[] getParams();
    

}
