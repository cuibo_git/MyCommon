package com.personal.dao.sql;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.personal.dao.util.DaoUtil;

/**
 * 条件
 * @author qq
 *
 */
public class BaseSql implements Sql, Serializable
{

    /**
	 * 
	 */
	private static final long serialVersionUID = -8677339307513309823L;

	protected static Object[] EMPTY = new Object[]{};
    
    protected String sql;
    
    protected List<Object> values;
    
    protected boolean readCache;
    
    public BaseSql(String sql)
    {
        super();
        this.sql = sql;
    }
    
    public BaseSql(String sql, Object... values)
    {
        super();
        this.sql = sql;
        if (!DaoUtil.isEmpty(values))
        {
            this.values = new ArrayList<Object>();
            for (Object object : values)
            {
                this.values.add(object);
            }
        }
    }
    
    public BaseSql(String sql, List<Object> values, boolean readCache)
	{
		super();
		this.sql = sql;
		this.values = values;
		this.readCache = readCache;
	}

	public BaseSql(String sql, boolean readCache)
	{
		super();
		this.sql = sql;
		this.readCache = readCache;
	}

	public BaseSql(String sql, List<Object> values)
    {
        super();
        this.sql = sql;
        this.values = values;
    }
    
    /**
     * 获取签名：标识唯一
     * @return
     */
	@Override
    public List<Object> getSignFlags()
    {
    	List<Object> result = new ArrayList<>();
    	result.add(getSql());
    	Object[] local = getParams();
    	if (!DaoUtil.isEmpty(local))
		{
    		for (Object object : local)
			{
    			result.add(object);
			}
		}
    	return result;
    }
    
    @Override
	public Object[] getParams()
	{
		return values == null ? EMPTY : values.toArray(new Object[]{values.size()});
	}

	public List<Object> getValues()
	{
		if (values == null)
		{
			values = new ArrayList<>();
		}
		return values;
	}

	public void setValues(List<Object> values)
	{
		this.values = values;
	}

	@Override
    public String getSql()
    {
        return sql;
    }

    public void setSql(String sql)
    {
        this.sql = sql;
    }

    @Override
	public boolean isReadCache()
	{
		return readCache;
	}

	public void setReadCache(boolean readCache)
	{
		this.readCache = readCache;
	}

}
