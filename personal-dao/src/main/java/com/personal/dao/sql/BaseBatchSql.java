package com.personal.dao.sql;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.personal.dao.util.DaoUtil;

/**
 * 批量条件
 * 
 * @author qq
 *
 */
public class BaseBatchSql implements BatchSql, Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected String sql;

	protected boolean readCache;

	protected List<Object[]> values;

	public BaseBatchSql(String sql)
	{
		this.sql = sql;
	}

	public BaseBatchSql(String sql, List<Object[]> values)
	{
		this.sql = sql;
		this.values = values;
	}

	public BaseBatchSql(String sql, List<Object[]> values, boolean readCache)
	{
		this.sql = sql;
		this.values = values;
		this.readCache = readCache;
	}

	/**
	 * 获取签名：标识唯一
	 * 
	 * @return
	 */
	@Override
	public List<Object> getSignFlags()
	{
		List<Object> result = new ArrayList<>();
		result.add(sql);
		if (!DaoUtil.isEmpty(values))
		{
			for (Object[] objects : values)
			{
				if (!DaoUtil.isEmpty(objects))
				{
					for (Object object : objects)
					{
						result.add(object);
					}
				}
			}
		}
		return result;
	}

	@SuppressWarnings("unchecked")
    @Override
	public List<Object[]> getParams()
	{
		return values == null ? Collections.EMPTY_LIST : values;
	}

	@Override
	public String getSql()
	{
		return sql;
	}

	public void setSql(String sql)
	{
		this.sql = sql;
	}

	@Override
	public boolean isReadCache()
	{
		return readCache;
	}

}
