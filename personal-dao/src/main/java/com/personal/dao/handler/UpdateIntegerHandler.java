package com.personal.dao.handler;

import com.personal.dao.sql.Sql;
import com.personal.dao.template.DaoTemplate;

/**
 * 更新返回Integer
 * @author cuibo
 *
 */
public class UpdateIntegerHandler extends BaseSqlHandler<Integer>
{

    @Override
    public Integer handleResult(DaoTemplate template, Sql sql)
    {
        return template.geJdbcTemplate().update(sql.getSql(), sql.getParams());
    }

    @Override
    public boolean isFlushCache()
    {
        return true;
    }

}
