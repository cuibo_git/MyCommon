/**
 *    Copyright 2009-2015 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package com.personal.dao.handler;

import com.personal.dao.cache.FlushCache;
import com.personal.dao.sign.Signable;
import com.personal.dao.sql.Sql;
import com.personal.dao.template.DaoTemplate;

/**
 * 结果处理
 * @author cuibo
 */
public interface SqlHandler<T> extends Signable, FlushCache
{

    T handleResult(DaoTemplate template, Sql sql);
    
}
