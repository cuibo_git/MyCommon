package com.personal.dao.handler;

import java.util.ArrayList;
import java.util.List;
 
/**
 * 批处理
 */
public abstract class BaseBatchSqlHandler<T> implements BatchSqlHandler<T>
{

	@Override
	public List<Object> getSignFlags()
	{
		List<Object> result = new ArrayList<>(1);
		result.add(getClass().getName());
		return result;
	}

}
