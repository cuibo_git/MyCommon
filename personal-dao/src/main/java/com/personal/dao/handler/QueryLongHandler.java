package com.personal.dao.handler;

import com.personal.dao.sql.Sql;
import com.personal.dao.template.DaoTemplate;

/**
 * 查询返回Long
 * @author cuibo
 *
 */
public class QueryLongHandler extends BaseSqlHandler<Long>
{

    @Override
    public Long handleResult(DaoTemplate template, Sql sql)
    {
        return template.geJdbcTemplate().queryForObject(sql.getSql(), Long.class, sql.getParams());
    }

    @Override
    public boolean isFlushCache()
    {
        return false;
    }

}
