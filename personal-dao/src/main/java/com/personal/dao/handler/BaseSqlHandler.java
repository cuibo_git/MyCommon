package com.personal.dao.handler;

import java.util.ArrayList;
import java.util.List;

/**
 * 结果处理
 * @author cuibo
 */
public abstract class BaseSqlHandler<T> implements SqlHandler<T>
{

	@Override
	public List<Object> getSignFlags()
	{
		List<Object> result = new ArrayList<>(1);
		result.add(getClass().getName());
		return result;
	}


}
