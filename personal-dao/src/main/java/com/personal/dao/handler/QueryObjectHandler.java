package com.personal.dao.handler;

import com.personal.dao.extractor.BaseModelResultSetExtractor;
import com.personal.dao.metadata.DataBaseTableMetadata;
import com.personal.dao.sql.Sql;
import com.personal.dao.template.DaoTemplate;

/** * 查询返回Long
 * @author cuibo
 *
 */
public class QueryObjectHandler<T> extends BaseSqlHandler<T>
{

    private DataBaseTableMetadata<T> metadata;
    
    public QueryObjectHandler(DataBaseTableMetadata<T> metadata)
    {
        this.metadata = metadata;
    }
    
    @Override
    public T handleResult(DaoTemplate template, Sql sql)
    {
        return template.geJdbcTemplate().query(sql.getSql(), sql.getParams(), new BaseModelResultSetExtractor<>(metadata));
    }

    @Override
    public boolean isFlushCache()
    {
        return false;
    }

}
