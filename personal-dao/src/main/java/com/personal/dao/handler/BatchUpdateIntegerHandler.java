package com.personal.dao.handler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.core.JdbcTemplate;

import com.personal.dao.exception.BatchUpdateException;
import com.personal.dao.exception.GetConnectionErrorException;
import com.personal.dao.sql.BatchSql;
import com.personal.dao.template.DaoTemplate;
import com.personal.dao.util.JdbcUtil;

/**
 * 更新返回Integer
 * @author cuibo
 *
 */
public class BatchUpdateIntegerHandler extends BaseBatchSqlHandler<Integer>
{
    
    private static final Log LOG = LogFactory.getLog(BatchUpdateIntegerHandler.class);
    
    private static final int BATCH_COUNT = 2000;

    @Override
    public Integer handleResult(DaoTemplate template, BatchSql sql)
    {
        // 不使用Jdbctemplate,性能较慢
        // template.batchUpdate(sql, batchArgs)
        return batchUpdate(template.geJdbcTemplate(), sql.getSql(), sql.getParams());
    }
    
    public int batchUpdate(JdbcTemplate template, String sql, List<Object[]> batchArgs)
    {
        Connection connection = null;
        PreparedStatement statement = null;
        try
        {
            connection = JdbcUtil.getConnection(template);
            if (connection == null)
            {
                throw new GetConnectionErrorException("获取数据连接失败！");
            }
            // 判断是否开启了事物注解
            boolean isConnectionTransactional = JdbcUtil.isConnectionTransactional(connection, template);
            JdbcUtil.begainTransaction(connection);
            statement = connection.prepareStatement(sql);
            int result = 0;
            for (int i = 0; i < batchArgs.size(); i++)
            {
                Object[] objArr = batchArgs.get(i);
                for (int j = 0; j < objArr.length; j++)
                {
                    statement.setObject(j + 1, objArr[j]);
                }
                statement.addBatch();
                if ((i+1) % BATCH_COUNT == 0)
                {
                    int[] arr = statement.executeBatch();
                    for (int j : arr)
                    {
                        if (j == Statement.SUCCESS_NO_INFO)
                        {
                            result++;
                        }
                    }
                    statement.clearBatch();
                }
            }
            int[] arr = statement.executeBatch();
            for (int j : arr)
            {
                if (j == Statement.SUCCESS_NO_INFO)
                {
                    result++;
                }
            }
            statement.clearBatch();
            // 开启事物注解则不能提交
            if (!isConnectionTransactional)
            {
                connection.commit();
            }
            return result;
        } catch (SQLException e)
        {
            LOG.error(e);
            JdbcUtil.rollbackTransaction(connection);
            throw new BatchUpdateException(e.getMessage(), e);
        } finally
        {
            JdbcUtil.releaseResource(template, connection, statement, null);
        }
    }

    @Override
    public boolean isFlushCache()
    {
        return true;
    }

}
