package com.personal.dao.handler;

import com.personal.dao.cache.FlushCache;
import com.personal.dao.sign.Signable;
import com.personal.dao.sql.BatchSql;
import com.personal.dao.template.DaoTemplate;
 
/**
 * 批处理
 */
public interface BatchSqlHandler<T> extends Signable, FlushCache
{

    T handleResult(DaoTemplate template, BatchSql sql);
    

}
