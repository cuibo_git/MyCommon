package com.personal.dao.handler;

import java.util.List;

import com.personal.dao.configuration.Configuration;
import com.personal.dao.exception.NoDefaultContructorException;
import com.personal.dao.metadata.DataBaseTableMetadata;
import com.personal.dao.reflection.MetaClass;
import com.personal.dao.rowmapper.BaseModelRowMapper;
import com.personal.dao.sql.Sql;
import com.personal.dao.template.DaoTemplate;

/**
 * 查询返回Long
 * @author cuibo
 *
 */
public class QueryListHandler<T> extends BaseSqlHandler<List<T>>
{

	private DataBaseTableMetadata<T> metadata;
    
    public QueryListHandler(DataBaseTableMetadata<T> metadata)
    {
        this.metadata = metadata;
    }
    
    @Override
    public List<T> handleResult(DaoTemplate template, Sql sql)
    {
    	Class<T> c = metadata.getC();
        MetaClass metaClass = Configuration.getInstance().newMetaClass(c);
        if (!metaClass.hasDefaultConstructor())
        {
            throw new NoDefaultContructorException("请给" + c + "提供默认的构造器！");
        }
        return template.geJdbcTemplate().query(sql.getSql(), sql.getParams(), new BaseModelRowMapper<T>(metadata));
    }

    @Override
    public boolean isFlushCache()
    {
        return false;
    }

}
