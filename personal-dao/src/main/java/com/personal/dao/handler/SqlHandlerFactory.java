package com.personal.dao.handler;

import java.util.List;

import com.personal.dao.metadata.DataBaseTableMetadata;

/**
 * 处理类工厂
 * @author cuibo
 *
 */
public class SqlHandlerFactory
{

    /** 查询Long处理 */
    private final SqlHandler<Long> queryLongHandler = new QueryLongHandler();

    /** 更新Integer处理 */
    private final SqlHandler<Integer> updateIntegerHandler = new UpdateIntegerHandler();

    /** 批量更新Integer处理 */
    private final BatchSqlHandler<Integer> batchUpdateIntegerHandler = new BatchUpdateIntegerHandler();
    
    /**
     * 获取默认的查询Long处理
     * @return
     */
    public SqlHandler<Long> getDefaultQueryLongHandler()
    {
        return queryLongHandler;
    }

    /**
     * 获取默认的更新Integer处理
     * @return
     */
    public SqlHandler<Integer> getDefaultUpdateIntegerHandler()
    {
        return updateIntegerHandler;
    }

    /**
     * 获取默认的批量更新Integer处理
     * @return
     */
    public BatchSqlHandler<Integer> getDefaultBatchUpdateIntegerHandler()
    {
        return batchUpdateIntegerHandler;
    }

    /**
     * 新建查询列表处理
     * @param metadata
     * @return
     */
    public <T> SqlHandler<List<T>> newQueryListHandler(DataBaseTableMetadata<T> metadata)
    {
        return new QueryListHandler<>(metadata);
    }

    /**
     * 新建查询单个实体处理
     * @param metadata
     * @return
     */
    public <T> SqlHandler<T> newQueryObjectHandler(DataBaseTableMetadata<T> metadata)
    {
        return new QueryObjectHandler<>(metadata);
    }
    
}
