package com.personal.excel.config;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;

import com.personal.core.utils.Assert;
import com.personal.core.utils.CoreUtil;
import com.personal.core.utils.FileUtil;
import com.personal.core.xml.XMLConvertUtil;
import com.personal.core.xml.XMLDoc;

/**
 * Sheet页规则解析
 * @author cuibo
 *
 */
public class SheetRuleParser
{

    /**
     * 解析规则
     * @param in
     * @return
     * @throws Exception
     */
    public static List<SheetRule> parseXml(InputStream in) throws Exception
    {
        Assert.isNotNull(in, "配置流不能为空！");
        try
        {
            XMLDoc doc = new XMLDoc();
            doc.load(in);
            List<Element> rules = doc.getChildList(doc.getRootElement());
            if (CoreUtil.isEmpty(rules))
            {
                return null;
            }
            List<SheetRule> result = new ArrayList<>();
            for (Element element : rules)
            {
                SheetRule rule = XMLConvertUtil.elementToObject(element, SheetRule.class);
                result.add(rule);
            }
            return result;
        } finally
        {
            FileUtil.release(in);
        }
    }
    
}
