package com.personal.excel.config;

import java.io.Serializable;

/**
 * 单个Sheet页的解析规则
 * @author cuibo
 *
 */
public class SheetRule implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** 头部开始 */
    private int headStart;
    
    /** 头部结束 */
    private int headEnd;
    
    /** Sheet页名称 */
    private String sheetName;
    
    /** Sheet页号 */
    private int sheetIndex = -1;
    
    /** 多表头分割 */
    private String multiSplit = ".";
    
    /** 是否是规则 */
    private boolean regular = true;
    
    /** 是否过滤空行 */
    private boolean filterNullRow = true;

    public int getHeadStart()
    {
        return headStart;
    }

    public void setHeadStart(int headStart)
    {
        this.headStart = headStart;
    }

    public int getHeadEnd()
    {
        return headEnd;
    }

    public void setHeadEnd(int headEnd)
    {
        this.headEnd = headEnd;
    }

    public String getSheetName()
    {
        return sheetName;
    }

    public void setSheetName(String sheetName)
    {
        this.sheetName = sheetName;
    }

    public int getSheetIndex()
    {
        return sheetIndex;
    }

    public void setSheetIndex(int sheetIndex)
    {
        this.sheetIndex = sheetIndex;
    }

    public String getMultiSplit()
    {
        return multiSplit;
    }

    public void setMultiSplit(String multiSplit)
    {
        this.multiSplit = multiSplit;
    }

    public boolean isRegular()
    {
        return regular;
    }

    public void setRegular(boolean regular)
    {
        this.regular = regular;
    }

    public boolean isFilterNullRow()
    {
        return filterNullRow;
    }

    public void setFilterNullRow(boolean filterNullRow)
    {
        this.filterNullRow = filterNullRow;
    }

}
