package com.personal.validate.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import com.personal.core.utils.CoreUtil;
import com.personal.core.utils.StringUtil;
import com.personal.validate.consts.LanguageTypeEnum;
import com.personal.validate.consts.ValidationTypeEnum;

/**
 * 正则实现
 * UUID001 MATCH reg
 * @author cuibo
 *
 */
public class MATCH extends BaseValidation
{
    
    private static final String MATCH = "MATCH";
    private static final String 匹配 = "匹配";

    @Override
    public List<ValidationResult> getAllValidationResult(LanguageTypeEnum language) throws Exception
    {
        List<ValidationResult> result = new ArrayList<ValidationResult>();
        ValidationResult single = getValidationResult(language);
        if (single != null)
        {
            result.add(single);
        }
        return result;
    }

    @Override
    public String[] getKeyInfo()
    {
        return new String[]
        { MATCH };
    }

    @Override
    public ValidationResult getValidationResult(LanguageTypeEnum language) throws Exception
    {
        if (language != null)
        {
            languageChange(language, LanguageTypeEnum.英文);
        }
        checkCanValidate(true);
        ValidationResult result = new ValidationResult();
        String[] arr = StringUtil.split(validationInfo, MATCH);
        // 计算待校验数据
        Object[] data = calculateExp(arr[0]);
        if (CoreUtil.isEmpty(arr[1]))
        {
            throw new Exception(validationInfo + "正则校验，正则规则为空！");
        }
        if (data != null && data.length == 1)
        {
            if (data[0] == null)
            {
                data[0] = "";
            }
            Pattern pattern = Pattern.compile(arr[1]);
            if (pattern.matcher(CoreUtil.parseStr(data[0])).matches())
            {
                return result;
            }
        }
        result.setPass(false);
        result.setErrorMessage(arr[0] + "的格式不合法！");
        return result;
    }

    @Override
    public ValidationTypeEnum getValidationType()
    {
        return ValidationTypeEnum.判断校验;
    }
    
    @Override
    protected Map<String, String> getLanguageChangeRelMap(LanguageTypeEnum source, LanguageTypeEnum target)
    {
        Map<String, String> result = new HashMap<String, String>();
        switch (source)
        {
        case 中文:
            switch (target)
            {
            case 英文:
                result.put(匹配, MATCH);
                break;
            default:
                break;
            }
            break;
        case 英文:
            switch (target)
            {
            case 中文:
                result.put(MATCH, 匹配);
                break;

            default:
                break;
            }
            break;

        default:
            break;
        }
        return result;
    }

}
