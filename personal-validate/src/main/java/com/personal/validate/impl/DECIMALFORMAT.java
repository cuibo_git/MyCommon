package com.personal.validate.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.personal.core.utils.Assert;
import com.personal.core.utils.CoreUtil;
import com.personal.core.utils.StringUtil;
import com.personal.validate.consts.LanguageTypeEnum;
import com.personal.validate.consts.ValidationTypeEnum;

/**
 * 小数位格式化，不做校验
 * @author cuibo
 *
 */
public class DECIMALFORMAT extends BaseValidation
{

    private static final String DECIMALFORMAT = "DECIMALFORMAT";
    private static final String 小数位 = "小数位";
    
    @Override
    public List<ValidationResult> getAllValidationResult(LanguageTypeEnum language) throws Exception
    {
        List<ValidationResult> result = new ArrayList<ValidationResult>();
        ValidationResult single = getValidationResult(language);
        if (single != null)
        {
            result.add(single);
        }
        return result;
    }

    @Override
    public ValidationResult getValidationResult(LanguageTypeEnum language) throws Exception
    {
        if (language != null)
        {
            languageChange(language, LanguageTypeEnum.英文);
        }
        checkCanValidate(true);
        ValidationResult result = new ValidationResult();
        String[] arr = StringUtil.split(validationInfo, DECIMALFORMAT); 

        Assert.isNotNullOrEmpty(arr[0], "待校验指标名为空！");
        Assert.isNotNullOrEmpty(arr[1], "数字格式化内容为空！");
        String targetName = arr[0].trim();
        // 小数位个数
        int xsw = CoreUtil.parseInt(arr[1]);
        // 指标值
        String targetValue = getTargetValue(targetName);
        validationValue.put(targetName, CoreUtil.parseDblStr(targetValue, xsw));
        return result;
    }

    @Override
    public String[] getKeyInfo()
    {
        return new String[]
        { DECIMALFORMAT };
    }

    @Override
    public ValidationTypeEnum getValidationType()
    {
        return ValidationTypeEnum.操作校验;
    }

    @Override
    protected Map<String, String> getLanguageChangeRelMap(LanguageTypeEnum source, LanguageTypeEnum target)
    {
        Map<String, String> result = new HashMap<String, String>();
        switch (source)
        {
        case 中文:
            switch (target)
            {
            case 英文:
                result.put(小数位, DECIMALFORMAT);
                break;
            default:
                break;
            }
            break;
        case 英文:
            switch (target)
            {
            case 中文:
                result.put(DECIMALFORMAT, 小数位);
                break;

            default:
                break;
            }
            break;

        default:
            break;
        }
        return result;
    }

}
