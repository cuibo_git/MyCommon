package com.personal.validate.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.personal.core.utils.Assert;
import com.personal.core.utils.StringUtil;
import com.personal.validate.consts.LanguageTypeEnum;
import com.personal.validate.consts.ValidationTypeEnum;

/**
 * 值等于操作
 * UUID001 VALUEEQ exp/ UUID002
 * @author cuibo
 *
 */
public class VALUEEQ extends BaseValidation
{

    private static final String VALUEEQ = "VALUEEQ";
    private static final String 值等于 = "值等于";
    
    @Override
    public List<ValidationResult> getAllValidationResult(LanguageTypeEnum language) throws Exception
    {
        List<ValidationResult> result = new ArrayList<ValidationResult>();
        ValidationResult single = getValidationResult(language);
        if (single != null)
        {
            result.add(single);
        }
        return result;
    }

    @Override
    public String[] getKeyInfo()
    {
        return new String[]
        { VALUEEQ };
    }

    @Override
    public ValidationResult getValidationResult(LanguageTypeEnum language) throws Exception
    {
        if (language != null)
        {
            languageChange(language, LanguageTypeEnum.英文);
        }
        checkCanValidate();
        String[] arr = StringUtil.split(validationInfo, VALUEEQ);
        Assert.isNotNullOrEmpty(arr[0], "待校验指标名为空！");
        Object[] calResult = calculateExp(arr[1]);
        // 将值改为设置的值
        if (calResult != null && calResult.length == 1)
        {
            validationValue.put(arr[0].trim(), calResult[0]);
        }
        // 通过
        return new ValidationResult();
    }

    @Override
    public ValidationTypeEnum getValidationType()
    {
        return ValidationTypeEnum.操作校验;
    }
    
    @Override
    protected Map<String, String> getLanguageChangeRelMap(LanguageTypeEnum source, LanguageTypeEnum target)
    {
        Map<String, String> result = new HashMap<String, String>();
        switch (source)
        {
        case 中文:
            switch (target)
            {
            case 英文:
                result.put(值等于, VALUEEQ);
                break;
            default:
                break;
            }
            break;
        case 英文:
            switch (target)
            {
            case 中文:
                result.put(VALUEEQ, 值等于);
                break;

            default:
                break;
            }
            break;

        default:
            break;
        }
        return result;
    }

}
