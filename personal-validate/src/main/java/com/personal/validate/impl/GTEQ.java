package com.personal.validate.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.personal.core.utils.Assert;
import com.personal.core.utils.CoreUtil;
import com.personal.core.utils.ReGularUtil;
import com.personal.core.utils.StringUtil;
import com.personal.validate.consts.LanguageTypeEnum;
import com.personal.validate.consts.ValidationTypeEnum;

/**
 * 大于等于校验
 * UUID001+UUID001 GTEQ 325
 * @author cuibo
 *
 */
public class GTEQ extends BaseValidation
{
    private static final String GTEQ = "GTEQ";
    private static final String 大于等于 = "大于等于";
    
    @Override
    public List<ValidationResult> getAllValidationResult(LanguageTypeEnum language) throws Exception
    {
        List<ValidationResult> result = new ArrayList<ValidationResult>();
        ValidationResult single = getValidationResult(language);
        if (single != null)
        {
            result.add(single);
        }
        return result;
    }

    @Override
    public String[] getKeyInfo()
    {
        return new String[]
        { GTEQ };
    }

    @Override
    public ValidationResult getValidationResult(LanguageTypeEnum language) throws Exception
    {
        if (language != null)
        {
            languageChange(language, LanguageTypeEnum.英文);
        }
        checkCanValidate(true);
        ValidationResult result = new ValidationResult();
        // 通过 GT 分割
        String[] arr = StringUtil.split(validationInfo, GTEQ); 
        Assert.isNotNullOrEmpty(arr[0], "待校验指标名为空！");
        // 第一第二个操作数都有可能是表达式
        Object[] calResult = calculateExp(arr);
        if (calResult != null && calResult.length == 2)
        {
            // 兼容时间类型
            if (ReGularUtil.ISDATE.matcher(CoreUtil.parseStr(calResult[0])).matches()
                    || ReGularUtil.ISDATE.matcher(CoreUtil.parseStr(calResult[1])).matches())
            {
                Date dateOne = CoreUtil.parseDate(calResult[0]);
                Date dateTwo = CoreUtil.parseDate(calResult[1]);
                if (CoreUtil.compareDate(dateOne, dateTwo) >= 0)
                {
                    return result;
                }
            } else
            {
                // 都是数字
                if (CoreUtil.compareNumber(calResult[0], calResult[1]) >= 0)
                {
                    return result;
                }
            }
        }
        result.setPass(false);
        result.setErrorMessage(arr[0] + "的指标值必须大于等于" + arr[1] + "的值");
        return result;
    }

    @Override
    public ValidationTypeEnum getValidationType()
    {
        return ValidationTypeEnum.判断校验;
    }

    @Override
    protected Map<String, String> getLanguageChangeRelMap(LanguageTypeEnum source, LanguageTypeEnum target)
    {
        Map<String, String> result = new HashMap<String, String>();
        switch (source)
        {
        case 中文:
            switch (target)
            {
            case 英文:
                result.put(大于等于, GTEQ);
                break;
            default:
                break;
            }
            break;
        case 英文:
            switch (target)
            {
            case 中文:
                result.put(GTEQ, 大于等于);
                break;

            default:
                break;
            }
            break;

        default:
            break;
        }
        return result;
    }
    
}
