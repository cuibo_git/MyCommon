package com.personal.validate.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.personal.core.utils.Assert;
import com.personal.core.utils.CoreUtil;
import com.personal.core.utils.StringUtil;
import com.personal.validate.consts.LanguageTypeEnum;
import com.personal.validate.consts.ValidationTypeEnum;

/**
 * 为空校验
 * ISNULL UUID001
 * @author cuibo
 *
 */
public class ISNULL extends BaseValidation
{
    
    private static final String ISNULL = "ISNULL";
    private static final String 为空 = "为空";

    @Override
    public List<ValidationResult> getAllValidationResult(LanguageTypeEnum language) throws Exception
    {
        List<ValidationResult> result = new ArrayList<ValidationResult>();
        ValidationResult single = getValidationResult(language);
        if (single != null)
        {
            result.add(single);
        }
        return result;
    }

    @Override
    public String[] getKeyInfo()
    {
        return new String[]
        { ISNULL };
    }

    @Override
    public ValidationResult getValidationResult(LanguageTypeEnum language) throws Exception
    {
        if (language != null)
        {
            languageChange(language, LanguageTypeEnum.英文);
        }
        checkCanValidate(true);
        ValidationResult result = new ValidationResult();
        String targetName = StringUtil.replaceFirst(validationInfo, ISNULL, "");
        Assert.isNotNullOrEmpty(targetName, "待校验指标名为空！");
        if (validationValue.containsKey(targetName))
        {
            if (CoreUtil.isEmpty(validationValue.get(targetName)))
            {
                return result;
            }
        } else
        {
            return result;
        }
        result.setPass(false);
        result.setErrorMessage(targetName + "的指标值必须为空！");
        return result;
    }

    @Override
    public ValidationTypeEnum getValidationType()
    {
        return ValidationTypeEnum.判断校验;
    }
    
    @Override
    protected Map<String, String> getLanguageChangeRelMap(LanguageTypeEnum source, LanguageTypeEnum target)
    {
        Map<String, String> result = new HashMap<String, String>();
        switch (source)
        {
        case 中文:
            switch (target)
            {
            case 英文:
                result.put(为空, ISNULL);
                break;
            default:
                break;
            }
            break;
        case 英文:
            switch (target)
            {
            case 中文:
                result.put(ISNULL, 为空);
                break;

            default:
                break;
            }
            break;

        default:
            break;
        }
        return result;
    }
}
