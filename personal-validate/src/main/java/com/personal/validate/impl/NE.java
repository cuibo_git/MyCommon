package com.personal.validate.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.personal.core.utils.Assert;
import com.personal.core.utils.CoreUtil;
import com.personal.core.utils.StringUtil;
import com.personal.validate.consts.LanguageTypeEnum;
import com.personal.validate.consts.ValidationTypeEnum;

/**
 * 不等于校验
 * UUID001+UUID001 NE 325
 * @author cuibo
 *
 */
public class NE extends BaseValidation
{

    private static final String NE = "NE";
    private static final String 不等于 = "不等于";
    
    @Override
    public List<ValidationResult> getAllValidationResult(LanguageTypeEnum language) throws Exception
    {
        List<ValidationResult> result = new ArrayList<ValidationResult>();
        ValidationResult single = getValidationResult(language);
        if (single != null)
        {
            result.add(single);
        }
        return result;
    }

    @Override
    public String[] getKeyInfo()
    {
        return new String[]
        { NE };
    }

    @Override
    public ValidationResult getValidationResult(LanguageTypeEnum language) throws Exception
    {
        if (language != null)
        {
            languageChange(language, LanguageTypeEnum.英文);
        }
        checkCanValidate(true);
        ValidationResult result = new ValidationResult();
        // 通过 NE 分割
        String[] arr = StringUtil.split(validationInfo, NE);
        // 第一第二个操作数都有可能是表达式
        Assert.isNotNullOrEmpty(arr[0], "待校验指标名为空！");
        Object[] calResult = calculateExp(arr);
        if (calResult != null && calResult.length == 2)
        {
            if (CoreUtil.isNumber(calResult[0]) && CoreUtil.isNumber(calResult[1]))
            {
                // 都是数字
                if (CoreUtil.subtract(calResult[0], calResult[1]) != 0.0)
                {
                    return result;
                }
            } else
            {
                // 非数字
                if (calResult[0] != null && !calResult[0].equals(calResult[1]))
                {
                    return result;
                } else if (CoreUtil.isEmpty(calResult[0]) && !CoreUtil.isEmpty(calResult[1]))
                {
                    return result;
                }
            }
        }
        result.setPass(false);
        result.setErrorMessage(arr[0] + "的指标值必须不等于" + arr[1] + "的值");
        return result;
    }

    @Override
    public ValidationTypeEnum getValidationType()
    {
        return ValidationTypeEnum.判断校验;
    }
    
    @Override
    protected Map<String, String> getLanguageChangeRelMap(LanguageTypeEnum source, LanguageTypeEnum target)
    {
        Map<String, String> result = new HashMap<String, String>();
        switch (source)
        {
        case 中文:
            switch (target)
            {
            case 英文:
                result.put(不等于, NE);
                break;
            default:
                break;
            }
            break;
        case 英文:
            switch (target)
            {
            case 中文:
                result.put(NE, 不等于);
                break;

            default:
                break;
            }
            break;

        default:
            break;
        }
        return result;
    }

}
