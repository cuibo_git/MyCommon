package com.personal.validate.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.personal.core.utils.Assert;
import com.personal.core.utils.CoreUtil;
import com.personal.core.utils.StringUtil;
import com.personal.validate.consts.LanguageTypeEnum;
import com.personal.validate.consts.ValidationTypeEnum;

/**
 * 为非数字校验
 * NOTNUMBER UUID001
 * @author cuibo
 *
 */
public class NOTNUMBER extends BaseValidation
{
    
    private static final String NOTNUMBER = "NOTNUMBER";
    private static final String 不为数字 = "不为数字";

    @Override
    public List<ValidationResult> getAllValidationResult(LanguageTypeEnum language) throws Exception
    {
        List<ValidationResult> result = new ArrayList<ValidationResult>();
        ValidationResult single = getValidationResult(language);
        if (single != null)
        {
            result.add(single);
        }
        return result;
    }

    @Override
    public String[] getKeyInfo()
    {
        return new String[]
        { NOTNUMBER };
    }

    @Override
    public ValidationResult getValidationResult(LanguageTypeEnum language) throws Exception
    {
        if (language != null)
        {
            languageChange(language, LanguageTypeEnum.英文);
        }
        checkCanValidate(true);
        ValidationResult result = new ValidationResult();
        String targetName = StringUtil.replaceFirst(validationInfo, NOTNUMBER, "").trim();
        Assert.isNotNullOrEmpty(targetName, "待校验指标名为空！");
        if (CoreUtil.isNumber(validationValue.get(targetName)))
        {
            result.setPass(false);
            result.setErrorMessage(targetName + "的指标值必须为非数字！");
        }
        return result;
    }

    @Override
    public ValidationTypeEnum getValidationType()
    {
        return ValidationTypeEnum.判断校验;
    }
    
    @Override
    protected Map<String, String> getLanguageChangeRelMap(LanguageTypeEnum source, LanguageTypeEnum target)
    {
        Map<String, String> result = new HashMap<String, String>();
        switch (source)
        {
        case 中文:
            switch (target)
            {
            case 英文:
                result.put(不为数字, NOTNUMBER);
                break;
            default:
                break;
            }
            break;
        case 英文:
            switch (target)
            {
            case 中文:
                result.put(NOTNUMBER, 不为数字);
                break;

            default:
                break;
            }
            break;

        default:
            break;
        }
        return result;
    }

}
