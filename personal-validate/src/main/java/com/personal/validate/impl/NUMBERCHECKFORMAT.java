package com.personal.validate.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.personal.core.utils.Assert;
import com.personal.core.utils.CoreUtil;
import com.personal.core.utils.StringUtil;
import com.personal.validate.consts.LanguageTypeEnum;
import com.personal.validate.consts.ValidationTypeEnum;
import com.personal.validate.inlet.Validator;

/**
 * 数据校验并且格式化
 * 数字格式化校验, targetName/exp NUMBERCHECKFORMAT 7,9     targetName/exp NUMBERCHECKFORMAT 11
 * 只对整数位的长度进行校验，小数位自动格式化
 * @author cuibo
 *
 */
public class NUMBERCHECKFORMAT extends BaseValidation
{

    private static final String NUMBERCHECKFORMAT = "NUMBERCHECKFORMAT";
    private static final String 数字校验格式化 = "数字校验格式化";
    
    @Override
    public List<ValidationResult> getAllValidationResult(LanguageTypeEnum language) throws Exception
    {
        List<ValidationResult> result = new ArrayList<ValidationResult>();
        ValidationResult single = getValidationResult(language);
        if (single != null)
        {
            result.add(single);
        }
        return result;
    }

    @Override
    public String[] getKeyInfo()
    {
        return new String[]
        { NUMBERCHECKFORMAT };
    }

    @Override
    public ValidationResult getValidationResult(LanguageTypeEnum language) throws Exception
    {
        if (language != null)
        {
            languageChange(language, LanguageTypeEnum.英文);
        }
        checkCanValidate(true);
        ValidationResult result = new ValidationResult();
        String[] arr = StringUtil.split(validationInfo, NUMBERCHECKFORMAT);
        // 第一第二个操作数都有可能是表达式
        Assert.isNotNullOrEmpty(arr[0], "待校验指标名为空！");
        Assert.isNotNullOrEmpty(arr[1], "数字格式化内容为空！");
        String targetName = arr[0].trim();
        String formatInfo = arr[1].trim();

        // 整数位和小数位
        int zsw = -1;
        int xsw = -1;

        // 合法性判断
        arr = formatInfo.split(",", 2);
        if (arr != null && arr.length == 1)
        {
            zsw = CoreUtil.parseInt(arr[0]);
        } else if (arr != null && arr.length == 2)
        {
            zsw = CoreUtil.parseInt(arr[0]);
            xsw = CoreUtil.parseInt(arr[1]);
        } else
        {
            throw new Exception(validationInfo + "数字格式化语句不合法！");
        }
        if (zsw <= -1)
        {
            throw new Exception(validationInfo + "数字格式化语句不合法！");
        }

        // 计算出结果
        Object[] objarr = calculateExp(arr[0]);
        if (objarr != null && objarr.length == 1)
        {
            String targetValue = CoreUtil.parseStr(objarr[0]);
            // 必须为数字
            ValidationResult single = Validator.validateSingle("ISNUMBER " + targetName, validationValue);
            if (single.isPass())
            {
                // 格式化结果
                if (targetValue.contains("."))
                {
                    int valuezsw = targetValue.indexOf(".");
                    if (zsw > valuezsw)
                    {
                        result.setPass(false);
                        result.setErrorMessage(targetName + "的值整数位最大为" + zsw);
                        return result;
                    }
                    // 格式化小数位
                    if (xsw > -1)
                    {
                        validationValue.put(targetName, CoreUtil.parseDblStr(targetValue, xsw));
                    }
                } else
                {
                    if (zsw > targetValue.length())
                    {
                        result.setPass(false);
                        result.setErrorMessage(targetName + "的值整数位最大为" + zsw);
                        return result;
                    }
                }
            } else
            {
                result.setPass(false);
                result.setErrorMessage(targetName + "必须为数字！");
                return result;
            }
        } else
        {
            throw new Exception(validationInfo + "数字格式化语句不合法！");
        }
        return result;
    }

    @Override
    public ValidationTypeEnum getValidationType()
    {
        return ValidationTypeEnum.操作校验;
    }
    
    @Override
    protected Map<String, String> getLanguageChangeRelMap(LanguageTypeEnum source, LanguageTypeEnum target)
    {
        Map<String, String> result = new HashMap<String, String>();
        switch (source)
        {
        case 中文:
            switch (target)
            {
            case 英文:
                result.put(数字校验格式化, NUMBERCHECKFORMAT);
                break;
            default:
                break;
            }
            break;
        case 英文:
            switch (target)
            {
            case 中文:
                result.put(NUMBERCHECKFORMAT, 数字校验格式化);
                break;

            default:
                break;
            }
            break;

        default:
            break;
        }
        return result;
    }

}
