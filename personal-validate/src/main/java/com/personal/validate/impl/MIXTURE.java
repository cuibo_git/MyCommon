package com.personal.validate.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.personal.core.utils.CoreUtil;
import com.personal.validate.consts.LanguageTypeEnum;
import com.personal.validate.consts.ValidationTypeEnum;
import com.personal.validate.inlet.Validator;

/**
 * 混合句
 * 语句;语句
 * @author cuibo
 *
 */
public class MIXTURE extends BaseValidation
{

    private static final String EFLAG = ";";
    private static final String CFLAG = "；";

    @Override
    public List<ValidationResult> getAllValidationResult(LanguageTypeEnum language) throws Exception
    {
        if (language != null)
        {
            languageChange(language, LanguageTypeEnum.英文);
        }
        // 切割混合句
        checkCanValidate();
        List<ValidationResult> result = new ArrayList<ValidationResult>();
        String[] arr = CoreUtil.split(validationInfo, EFLAG);
        List<ValidationResult> singleResults = null;
        for (String string : arr)
        {
            singleResults = Validator.validateAll(string, validationValue, language); 
            if (singleResults != null && singleResults.size() > 0)
            {
                result.addAll(singleResults);
            }
        }
        return result;
    }

    @Override
    public String[] getKeyInfo()
    {
        return new String[]
        { EFLAG };
    }

    @Override
    public ValidationResult getValidationResult(LanguageTypeEnum language) throws Exception
    {
        if (language != null)
        {
            languageChange(language, LanguageTypeEnum.英文);
        }
        // 切割混合句
        checkCanValidate();
        String[] arr = validationInfo.split(EFLAG);
        List<ValidationResult> singleResults = null;
        for (String string : arr)
        {
            singleResults = Validator.validateAll(string, validationValue, language);
            if (singleResults == null || singleResults.isEmpty())
            {
                continue;
            }
            for (ValidationResult validationResult : singleResults)
            {
                if (!validationResult.isPass())
                {
                    return validationResult;
                }
            }
        }
        // 全部通过
        return new ValidationResult();
    }

    @Override
    public ValidationTypeEnum getValidationType()
    {
        return ValidationTypeEnum.混合句;
    }
    
    @Override
    protected Map<String, String> getLanguageChangeRelMap(LanguageTypeEnum source, LanguageTypeEnum target)
    {
        Map<String, String> result = new HashMap<String, String>();
        switch (source)
        {
        case 中文:
            switch (target)
            {
            case 英文:
                result.put(CFLAG, EFLAG);
                break;
            default:
                break;
            }
            break;
        case 英文:
            switch (target)
            {
            case 中文:
                result.put(EFLAG, CFLAG);
                break;

            default:
                break;
            }
            break;

        default:
            break;
        }
        return result;
    }

}
