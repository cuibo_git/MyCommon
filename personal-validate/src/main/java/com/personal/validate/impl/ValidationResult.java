package com.personal.validate.impl;

/**
 * 校验结果
 * @author cuibo
 *
 */
public class ValidationResult
{

    /** 错误信息 */
    private String errorMessage = "";

    private boolean pass = true;
    
    public String getErrorMessage()
    {
        return errorMessage;
    }

    public boolean isPass()
    {
        return pass;
    }

    public void setErrorMessage(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public void setPass(boolean pass)
    {
        this.pass = pass;
    }

    @Override
    public String toString()
    {
        if (pass)
        {
            return "校验通过！";
        } else
        {
            return "校验不通过：" + errorMessage + "！";
        }
    }

}
