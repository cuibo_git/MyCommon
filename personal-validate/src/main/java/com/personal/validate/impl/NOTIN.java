package com.personal.validate.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.personal.core.utils.Assert;
import com.personal.core.utils.CoreUtil;
import com.personal.core.utils.StringUtil;
import com.personal.validate.consts.LanguageTypeEnum;
import com.personal.validate.consts.ValidationTypeEnum;

/**
 * 值范围校验 (不要写括号)
 * UUID002 NOTIN 213,253
 * @author cuibo
 *
 */
public class NOTIN extends BaseValidation
{

    private static final String NOTIN = "NOTIN";
    private static final String 值不在范围 = "值不在范围";
    
    @Override
    public List<ValidationResult> getAllValidationResult(LanguageTypeEnum language) throws Exception
    {
        List<ValidationResult> result = new ArrayList<ValidationResult>();
        ValidationResult single = getValidationResult(language);
        if (single != null)
        {
            result.add(single);
        }
        return result;
    }

    @Override
    public String[] getKeyInfo()
    {
        return new String[]
        { NOTIN };
    }

    @Override
    public ValidationResult getValidationResult(LanguageTypeEnum language) throws Exception
    {
        if (language != null)
        {
            languageChange(language, LanguageTypeEnum.英文);
        }
        checkCanValidate(true);
        ValidationResult result = new ValidationResult();
        // 通过 NOTIN 分割
        String[] arr = StringUtil.split(validationInfo, NOTIN);
        // 第一个为待校验值，第二个为值范围
        Assert.isNotNullOrEmpty(arr[0], "待校验指标名为空！");
        Assert.isNotNullOrEmpty(arr[1], "检查范围值为空！");
        String[] calResult = replaceTargetName(arr);
        if (calResult != null && calResult.length == 2)
        {
            // 均为空，则视为包含，则返回false
            if (CoreUtil.isEmpty(calResult[1]) && CoreUtil.isEmpty(calResult[0]))
            { 
                result.setPass(false);
                result.setErrorMessage(arr[0] + "的指标值必须不在" + arr[1] + "之中！");
                return result;
            } else
            {
                // 通过 , 切割
                if (calResult[1] != null)
                {
                    String[] values = calResult[1].split(",");
                    for (String string : values)
                    {
                        if (string != null && string.equals(calResult[0]))
                        {
                            result.setPass(false);
                            result.setErrorMessage(arr[0] + "的指标值必须不在" + arr[1] + "之中！");
                            return result;
                        }
                    }
                }
            }
        }
        return result;
    }

    @Override
    public ValidationTypeEnum getValidationType()
    {
        return ValidationTypeEnum.判断校验;
    }
    
    @Override
    protected Map<String, String> getLanguageChangeRelMap(LanguageTypeEnum source, LanguageTypeEnum target)
    {
        Map<String, String> result = new HashMap<String, String>();
        switch (source)
        {
        case 中文:
            switch (target)
            {
            case 英文:
                result.put(值不在范围, NOTIN);
                break;
            default:
                break;
            }
            break;
        case 英文:
            switch (target)
            {
            case 中文:
                result.put(NOTIN, 值不在范围);
                break;

            default:
                break;
            }
            break;

        default:
            break;
        }
        return result;
    }

}
