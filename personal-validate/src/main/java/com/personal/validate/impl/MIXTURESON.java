package com.personal.validate.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.personal.core.utils.Assert;
import com.personal.core.utils.CoreUtil;
import com.personal.core.utils.StringUtil;
import com.personal.validate.consts.LanguageTypeEnum;
import com.personal.validate.consts.ValidationTypeEnum;
import com.personal.validate.inlet.Validator;

/**
 * 混合子句
 * 语句 &&/|| 语句
 * @author cuibo
 *
 */
public class MIXTURESON extends BaseValidation
{

    private static final String EFLAGONE = "&&";

    private static final String EFLAGTWO = "||";
    
    private static final String CFLAGONE = "并且";

    private static final String CFLAGTWO = "或者";

    @Override
    public List<ValidationResult> getAllValidationResult(LanguageTypeEnum language) throws Exception
    {
        List<ValidationResult> results = new ArrayList<ValidationResult>();
        ValidationResult result = getValidationResult(language);
        if (result != null)
        {
            results.add(result);
        }
        return results;
    }

    @Override
    public String[] getKeyInfo()
    {
        return new String[]
        { EFLAGONE, EFLAGTWO };
    }
    
    @Override
    public ValidationResult getValidationResult(LanguageTypeEnum language) throws Exception
    {
        if (language != null)
        {
            languageChange(language, LanguageTypeEnum.英文);
        }
        // 切割混合句
        checkCanValidate();
        // 拷贝一份
        String tempValidationInfo = validationInfo;
        // 去除所有括号
        validationInfo = validationInfo.replaceAll("\\(", "").replaceAll("\\)", "");
        // 按照 FLAG 切割
        String[] arrOne = CoreUtil.split(validationInfo, EFLAGONE); 
        // 所有的子句
        List<String> allSub = new ArrayList<String>();

        String[] arrTwo = null;
        if (arrOne != null && arrOne.length > 0)
        {
            for (String string : arrOne)
            {
                Assert.isNotNullOrEmpty(string, tempValidationInfo + "校验语句格式不合法");
                arrTwo = CoreUtil.split(string, EFLAGTWO);
                if (arrTwo != null && arrTwo.length > 0)
                {
                    for (String str : arrTwo)
                    {
                        Assert.isNotNullOrEmpty(str, tempValidationInfo + "校验语句格式不合法");
                        allSub.add(str);
                    }
                }
            }
        }
        // 对allSub校验
        Assert.isNotNullOrEmpty(allSub, tempValidationInfo + "校验语句格式不合法");
        Map<String, Boolean> cahResult = new HashMap<String, Boolean>();
        ValidationResult single = null;
        for (String string : allSub)
        {
            single =  Validator.validateSingle(string, validationValue, language);
            if (single != null)
            {
                // 处理 VALUEIN 和 IN 的括号问题
                cahResult.put(string, single.isPass());
            }
        }
        String message = tempValidationInfo;
        // 将原 validationInfo 的 子句全部用  cahResult 代替
        for (Entry<String, Boolean> entry : cahResult.entrySet())
        {
            tempValidationInfo = StringUtil.replaceFirst(tempValidationInfo, entry.getKey(), entry.getValue() + "");
        }
        // 计算该表达式是 true 还是  false
        boolean flag = CoreUtil.calExpFlag(tempValidationInfo);
        ValidationResult result = new ValidationResult();
        // 校验通过
        if (flag)
        {
            return result;
        } else
        {
            result.setPass(false);
            message = StringUtil.replace(message, EFLAGONE, "并且");
            message = StringUtil.replace(message, EFLAGTWO, "或者");
            result.setErrorMessage(message);
            return result;
        }
    }

    @Override
    public ValidationTypeEnum getValidationType()
    {
        return ValidationTypeEnum.复合句;
    }
    
    @Override
    protected Map<String, String> getLanguageChangeRelMap(LanguageTypeEnum source, LanguageTypeEnum target)
    {
        Map<String, String> result = new HashMap<String, String>();
        switch (source)
        {
        case 中文:
            switch (target)
            {
            case 英文:
                result.put(CFLAGONE, EFLAGONE);
                result.put(CFLAGTWO, EFLAGTWO);
                break;
            default:
                break;
            }
            break;
        case 英文:
            switch (target)
            {
            case 中文:
                result.put(EFLAGONE, CFLAGONE);
                result.put(EFLAGTWO, CFLAGTWO);
                break;

            default:
                break;
            }
            break;

        default:
            break;
        }
        return result;
    }

}
