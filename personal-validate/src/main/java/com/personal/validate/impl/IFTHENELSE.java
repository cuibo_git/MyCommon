package com.personal.validate.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.personal.core.utils.Assert;
import com.personal.core.utils.CoreUtil;
import com.personal.core.utils.StringUtil;
import com.personal.validate.consts.LanguageTypeEnum;
import com.personal.validate.consts.ValidationTypeEnum;
import com.personal.validate.inlet.Validator;

/**
 * if then 条件句
 * IF UUID001+UUID002 GT UUID003 THEN UUID004 VALUEEQ UUID002 ELSE UUID004 VALUEEQ UUID003
 * @author cuibo
 *
 */
public class IFTHENELSE extends BaseValidation
{

    private static final String IF = "IF";
    private static final String THEN = "THEN";
    private static final String ELSE = "ELSE";
    
    private static final String 如果 = "如果";
    private static final String 那么 = "那么";
    private static final String 否则 = "否则";
    
    @Override
    public List<ValidationResult> getAllValidationResult(LanguageTypeEnum language) throws Exception
    {
        if (language != null)
        {
            languageChange(language, LanguageTypeEnum.英文);
        }
        checkCanValidate();
        // 通过 IF  THEN  ELSE 切割
        String[] arr = StringUtil.split(validationInfo, THEN); 
        if (arr == null || arr.length != 2)
        {
            throw new Exception(validationInfo + "表达式不合法");
        }
        // 条件语句
        String condition = arr[0];
        if (CoreUtil.isEmpty(condition) || !condition.contains(IF))
        {
            throw new Exception(validationInfo + "表达式不合法");
        }
        // 操作语句
        String operate = arr[1];
        if (CoreUtil.isEmpty(operate))
        {
            throw new Exception(validationInfo + "表达式不合法");
        }
        // 条件一定是复合子句
        condition = StringUtil.replaceFirst(condition, IF, "");
        // 获取条件的执行结果
        ValidationResult singleResult = Validator.validateSingle(condition, validationValue, language);
        Assert.isNotNull(singleResult, validationInfo + "的条件执行失败！");

        if (singleResult.isPass())
        {
            // 检验操作语句是否包含 ELSE
            if (operate.contains(ELSE))
            {
                // 只执行 THEN
                arr = StringUtil.split(operate, ELSE); 
                return Validator.validateAll(arr[0], validationValue, language);
            } else
            {
                return Validator.validateAll(operate, validationValue, language);
            }
        } else
        {
            // 检验操作语句是否包含 ELSE
            if (operate.contains(ELSE))
            {
                // 只执行 ELSE
                arr = StringUtil.split(operate, ELSE);
                return Validator.validateAll(arr[1], validationValue, language);
            } else
            {
                // 没有 ELSE ，直接通过
                List<ValidationResult> results = new ArrayList<ValidationResult>();
                results.add(new ValidationResult());
                return results;
            }
        }
    }

    @Override
    public String[] getKeyInfo()
    {
        return new String[]
        { IF, THEN, ELSE };
    }

    @Override
    public ValidationResult getValidationResult(LanguageTypeEnum language) throws Exception
    {
        if (language != null)
        {
            languageChange(language, LanguageTypeEnum.英文);
        }
        checkCanValidate();
        // 通过 IF  THEN  ELSE 切割
        String[] arr = StringUtil.split(validationInfo, THEN); 
        if (arr == null || arr.length != 2)
        {
            throw new Exception(validationInfo + "表达式不合法");
        }
        // 条件语句
        String condition = arr[0];
        if (CoreUtil.isEmpty(condition) || !condition.contains(IF))
        {
            throw new Exception(validationInfo + "表达式不合法");
        }
        // 操作语句
        String operate = arr[1];
        if (CoreUtil.isEmpty(operate))
        {
            throw new Exception(validationInfo + "表达式不合法");
        }
        // 条件一定是复合子句
        condition = StringUtil.replaceFirst(condition, IF, "");
        // 获取条件的校验结果
        ValidationResult singleResult = Validator.validateSingle(condition, validationValue, language); 
        Assert.isNotNull(singleResult, validationInfo + "的条件执行失败！");

        ValidationResult result = new ValidationResult();
        if (singleResult.isPass())
        {
            // 检验操作语句是否包含 ELSE
            if (operate.contains(ELSE))
            {
                // 只执行 THEN
                arr = StringUtil.split(operate, ELSE);
                result = Validator.validateSingle(arr[0], validationValue, language);
            } else
            {
                result = Validator.validateSingle(operate, validationValue, language);
            }
        } else
        {
            // 检验操作语句是否包含 ELSE
            if (operate.contains(ELSE))
            {
                // 只执行 ELSE
                arr = StringUtil.split(operate, ELSE);
                result = Validator.validateSingle(arr[1], validationValue, language);
            } else
            {
                // 没有 ELSE ，直接通过
                return new ValidationResult();
            }
        }
        // 没有通过则处理提示信息
        if (result != null && !result.isPass())
        {
            result.setErrorMessage(StringUtil.replace(
                    StringUtil.replace(StringUtil.replace(validationInfo, IF, "如果"), THEN, "那么"), ELSE, "否则"));
        }
        return result;
    }

    @Override
    public ValidationTypeEnum getValidationType()
    {
        return ValidationTypeEnum.复合句;
    }
    
    @Override
    protected Map<String, String> getLanguageChangeRelMap(LanguageTypeEnum source, LanguageTypeEnum target)
    {
        Map<String, String> result = new HashMap<String, String>();
        switch (source)
        {
        case 中文:
            switch (target)
            {
            case 英文:
                result.put(如果, IF);
                result.put(那么, THEN);
                result.put(否则, ELSE);
                break;
            default:
                break;
            }
            break;
        case 英文:
            switch (target)
            {
            case 中文:
                result.put(IF, 如果);
                result.put(THEN, 那么);
                result.put(ELSE, 否则);
                break;

            default:
                break;
            }
            break;

        default:
            break;
        }
        return result;
    }

}
