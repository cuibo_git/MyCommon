package com.personal.validate.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.personal.core.utils.Assert;
import com.personal.core.utils.CoreUtil;
import com.personal.core.utils.StringUtil;
import com.personal.validate.consts.LanguageTypeEnum;
import com.personal.validate.consts.ValidationTypeEnum;

/**
 * 大于校验
 * UUID001 LENGTHGT 20
 * @author cuibo
 *
 */
public class LENGTHGT extends BaseValidation
{
    
    private static final String LENGTHGT = "LENGTHGT";
    private static final String 长度大于 = "长度大于";

    @Override
    public List<ValidationResult> getAllValidationResult(LanguageTypeEnum language) throws Exception
    {
        List<ValidationResult> result = new ArrayList<ValidationResult>();
        ValidationResult single = getValidationResult(language);
        if (single != null)
        {
            result.add(single);
        }
        return result;
    }

    @Override
    public String[] getKeyInfo()
    {
        return new String[]
        { LENGTHGT };
    }

    @Override
    public ValidationResult getValidationResult(LanguageTypeEnum language) throws Exception
    {
        if (language != null)
        {
            languageChange(language, LanguageTypeEnum.英文);
        }
        checkCanValidate(true);
        ValidationResult result = new ValidationResult();
        // 通过 LENGTHGT 分割
        String[] arr = StringUtil.split(validationInfo, LENGTHGT);
        // 第一第二个操作数都有可能是表达式
        Assert.isNotNullOrEmpty(arr[0], "待校验指标名为空！");
        Assert.isNotNullOrEmpty(arr[1], "长度校验值为空！");
        String value = getTargetValue(arr[0].trim());
        if (CoreUtil.isEmpty(value) && CoreUtil.parseInt(arr[1]) < 0)
        {
            return result;
        } else
        {
            if (value != null && value.length() > CoreUtil.parseInt(arr[1]))
            {
                return result;
            }
        }
        result.setPass(false);
        result.setErrorMessage(arr[0] + "的值长度必须大于" + CoreUtil.parseInt(arr[1]));
        return result;
    }

    @Override
    public ValidationTypeEnum getValidationType()
    {
        return ValidationTypeEnum.判断校验;
    }
    
    @Override
    protected Map<String, String> getLanguageChangeRelMap(LanguageTypeEnum source, LanguageTypeEnum target)
    {
        Map<String, String> result = new HashMap<String, String>();
        switch (source)
        {
        case 中文:
            switch (target)
            {
            case 英文:
                result.put(长度大于, LENGTHGT);
                break;
            default:
                break;
            }
            break;
        case 英文:
            switch (target)
            {
            case 中文:
                result.put(LENGTHGT, 长度大于);
                break;

            default:
                break;
            }
            break;

        default:
            break;
        }
        return result;
    }

}
