package com.personal.validate.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.personal.core.utils.Assert;
import com.personal.core.utils.CoreUtil;
import com.personal.core.utils.StringUtil;
import com.personal.validate.consts.LanguageTypeEnum;
import com.personal.validate.consts.ValidationConst;
import com.personal.validate.consts.ValidationTypeEnum;

/**
 * 值等于操作 (不要写括号)
 * UUID001 VALUEIN 1,2,3
 * @author cuibo
 *
 */
public class VALUEIN extends BaseValidation
{

    private static final String VALUEIN = "VALUEIN";
    private static final String 值取值 = "值取值";
    
    @Override
    public List<ValidationResult> getAllValidationResult(LanguageTypeEnum language) throws Exception
    {
        List<ValidationResult> result = new ArrayList<ValidationResult>();
        ValidationResult single = getValidationResult(language);
        if (single != null)
        {
            result.add(single);
        }
        return result;
    }

    @Override
    public String[] getKeyInfo()
    {
        return new String[]
        { VALUEIN };
    }

    @Override
    public ValidationResult getValidationResult(LanguageTypeEnum language) throws Exception
    {
        if (language != null)
        {
            languageChange(language, LanguageTypeEnum.英文);
        }
        checkCanValidate();
        String[] arr = StringUtil.split(validationInfo, VALUEIN);
        Assert.isNotNullOrEmpty(arr[0], "待校验指标名为空！");
        Assert.isNotNullOrEmpty(arr[1], "指标范围值为空！");
        Object[] calResult = calculateExp(arr[1]);
        if (calResult != null && calResult.length == 1)
        {
            String value = CoreUtil.parseStr(calResult[0]);
            if (!CoreUtil.isEmpty(value))
            {
                // 如果前后有括号则去除前后括号
                if (value.startsWith("("))
                {
                    value = value.substring(1);
                }
                if (value.endsWith(")"))
                {
                    value = value.substring(0, value.length() - 1);
                }
                String[] arrs = value.split(",");
                // 更新下拉框的值
                validationValue.put(arr[0].trim() + ValidationConst.SELECT, Arrays.asList(arrs));
            }
        }
        // 通过
        return new ValidationResult();
    }

    @Override
    public ValidationTypeEnum getValidationType()
    {
        return ValidationTypeEnum.操作校验;
    }
    
    @Override
    protected Map<String, String> getLanguageChangeRelMap(LanguageTypeEnum source, LanguageTypeEnum target)
    {
        Map<String, String> result = new HashMap<String, String>();
        switch (source)
        {
        case 中文:
            switch (target)
            {
            case 英文:
                result.put(值取值, VALUEIN);
                break;
            default:
                break;
            }
            break;
        case 英文:
            switch (target)
            {
            case 中文:
                result.put(VALUEIN, 值取值);
                break;

            default:
                break;
            }
            break;

        default:
            break;
        }
        return result;
    }

}
