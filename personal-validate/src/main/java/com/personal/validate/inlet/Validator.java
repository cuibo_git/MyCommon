package com.personal.validate.inlet;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.personal.core.utils.Assert;
import com.personal.core.utils.CoreUtil;
import com.personal.core.utils.ReGularUtil;
import com.personal.validate.consts.LanguageTypeEnum;
import com.personal.validate.factory.ValidatorFactory;
import com.personal.validate.impl.ValidationResult;
import com.personal.validate.port.Validation;

/**
 * 校验入口
 * @author cuibo
 *
 */
public class Validator
{
    
    public static void main(String[] args)
    {
      Map<String, Object> map = new HashMap<String, Object>();
      map.put("UUID001", "213");
      map.put("UUID002", "213");
      map.put("UUID003", "426");
      //map.put("UUID005", "");
      for (int i = 0; i < 400; i++)
      {
          map.put("UUIDrrrr" + i, 456);
      }
      map.put("UUID004", null);
      try
      {
//          for (int i = 0; i < 1; i++)
//          {
//              validateSingle("IF (UUID001+UUID002 GT UUID003) || (UUID001+UUID002 LT UUID003) THEN " + 
//                      " UUID004 VALUEEQ UUID002 && UUID005 VALUEEQ UUID003  ELSE UUID004 VALUEEQ UUID003 && UUID005 VALUEEQ UUID002", map);
//          }
          String chinese = " (UUID001+UUID002)/UUID001 大于 1 并且 (UUID001+UUID002)/UUID001 小于 3 ";
          long time = System.currentTimeMillis();
          for (int i = 0; i < 1; i++)
          {
//              validateSingle("IF (UUID001+UUID002 GT UUID003) || (UUID001+UUID002 LT UUID003) THEN " + 
//                      " UUID004 VALUEEQ UUID002 && UUID005 VALUEEQ UUID003  ELSE UUID004 VALUEEQ UUID003 && UUID005 VALUEEQ UUID002", map);
              System.out.println(validateSingle(chinese, map, LanguageTypeEnum.中文));
          }
          System.out.println("校验花费时间：" + (System.currentTimeMillis() - time));
      } catch (Exception e)
      {
          e.printStackTrace();
      }
    }
    
    /**
     * 校验 validationInfo  非短路校验
     * @param validationInfo   校验语句
     * @param validationValue  待校验值
     * @return
     * @throws Exception
     */
    public static List<ValidationResult> validateAll(String validationInfo, Map<String, Object> validationValue) throws Exception
    {
        return validateAll(validationInfo, validationValue, null);
    }
    
    /**
     * 校验 validationInfo  非短路校验
     * @param validationInfo   校验语句
     * @param validationValue  待校验值
     * @param language  语言环境
     * @return
     * @throws Exception
     */
    public static List<ValidationResult> validateAll(String validationInfo, Map<String, Object> validationValue, LanguageTypeEnum language) throws Exception
    {
        Validation validation = ValidatorFactory.getValidation(validationInfo, validationValue, language);
        Assert.isNotNull(validation, "获取" + validationInfo + "的校验者失败！");
        validation.setValidationInfo(validationInfo);
        validation.setValidationValue(validationValue);
        return validation.getAllValidationResult(language);
    }

    /**
     * 校验 validationInfo  短路校验
     * @param validationInfo   校验语句
     * @param validationValue   待校验值
     * @return
     * @throws Exception
     */
    public static ValidationResult validateSingle(String validationInfo, Map<String, Object> validationValue) throws Exception
    {
        return validateSingle(validationInfo, validationValue, null);
    }
    
    /**
     * 校验 validationInfo  短路校验
     * @param validationInfo   校验语句
     * @param validationValue   待校验值
     * @param language  语言环境
     * @return
     * @throws Exception
     */
    public static ValidationResult validateSingle(String validationInfo, Map<String, Object> validationValue, LanguageTypeEnum language) throws Exception
    {
        Validation validation = ValidatorFactory.getValidation(validationInfo, validationValue, language);
        Assert.isNotNull(validation, "获取" + validationInfo + "的校验者失败！");
        validation.setValidationInfo(validationInfo);
        validation.setValidationValue(validationValue);
        return validation.getValidationResult(language);
    }
    
    /**
     * 必填校验
     * @param requiredCheck
     * @param valuetitle
     * @param ...value
     * @throws Exception 
     */
    public static boolean[] checkRequired(boolean requiredCheck, String ...value)
    {
        
    	if (requiredCheck)
		{	//必填校验
    		if (value != null && value.length > 0)
    		{
    		    boolean requiredCheck2[] = new boolean[value.length];
    		    int i = 0;
    			for (String str:value)
    			{
    				if (CoreUtil.isEmpty(str))
    				{
    				    requiredCheck2[i] = false; 
    				} else
    				{
    				    requiredCheck2[i] = true; 
    				}
    				i++;
    			}
    			return requiredCheck2;
    		}
		}
    	return null;
    }
    
    /**
     * 长度校验
     * @param lengthCheck
     * @param value
     * @param title
     * @throws Exception 
     */
    public static boolean[] checkLength(String lengthCheck, String ...value) throws Exception
    {
    	if (lengthCheck.contains(","))
		{	//数字填写校验
			String []str = lengthCheck.split(",");
			if (str.length != 2)
			{
				throw new Exception("配置文件填写不正确！"); 
			} else
			{
				if (value != null && value.length > 0)
	    		{
				    boolean [] errors = new boolean[value.length];
				    int i = 0;
	    			for (String string:value)
	    			{
	    				if ((!CoreUtil.isEmpty(string) && !CoreUtil.isNumber(string)))
	    				{
	    				    errors[i] = false;
	    				} else
	    				{
	    				    errors[i] = checkNum(string,str);
	    				}
	    				i++;
	    			}
	    			return errors;
	    		}
			}
		} else if("整数".equals(lengthCheck))
		{	
		    if (value != null && value.length > 0)
            {
		        boolean [] errors = new boolean[value.length];
                int i = 0;
                for (String string:value)
                {
                    if (!CoreUtil.isEmpty(string) && !ReGularUtil.INTEGER.matcher(string).matches())
                    {
                        errors[i] = false;
                    } else
                    {
                        errors[i] = true;
                    }
                    i++;
                }
                return errors;
            }
		} else 
		{
		    //字符串填写校验
            int leng = CoreUtil.parseInt(lengthCheck);
            if (value != null && value.length > 0)
            {
                boolean [] errors = new boolean[value.length];
                int i = 0;
                for (String string:value)
                {
                    if (!CoreUtil.isEmpty(string) && string.length() > leng)
                    {
                        errors [i] = false; 
                    } else
                    {
                        errors [i] = true; 
                    }
                    i++;
                }
                return errors;
            }
		}
    	return null;
    }
    
    public static boolean checkLength(String value, int leng)
    {
        if (!CoreUtil.isEmpty(value) && value.length() > leng)
        {
            return false;
        } else
        {
            return true;
        }
    }
    
    /**
	 * 数字整数与小数位的检测
	 * @param targetInfoValue
	 * @param str
	 * @throws Exception
	 */
	private static boolean checkNum(String targetInfoValue, String []str)
	{
		if (!CoreUtil.isEmpty(targetInfoValue))
		{
			if (targetInfoValue.contains("."))
			{	//123456.78
				int start = targetInfoValue.indexOf(".");
				if (start > CoreUtil.parseInt(str[0]))
				{
					return false; 
				}
			} else
			{
				if (targetInfoValue.length() > CoreUtil.parseInt(str[0]))
				{
				    return false; 
				}
			}
		}
		return true;
	}
	
}
