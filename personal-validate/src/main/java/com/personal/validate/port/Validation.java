package com.personal.validate.port;

import java.util.List;
import java.util.Map;

import com.personal.validate.consts.LanguageTypeEnum;
import com.personal.validate.consts.ValidationTypeEnum;
import com.personal.validate.impl.ValidationResult;

/**
 * 复杂校验接口
 * @author cuibo
 *
 */
public interface Validation
{

    /**
     * 获取所有校验结果 （即语句全部校验）
     */
    public List<ValidationResult> getAllValidationResult() throws Exception;
    
    /**
     * 获取所有校验结果 （即语句全部校验）
     * @param language
     */
    public List<ValidationResult> getAllValidationResult(LanguageTypeEnum language) throws Exception;

    /**
     * 获取校验的关键字，即通过这些关键字可以作为校验的唯一标示
     */
    public String[] getKeyInfo();

    /** 获取校验结果
     * @throws Exception
     */
    public ValidationResult getValidationResult() throws Exception;
    
    /** 获取校验结果
     * @param language
     * @throws Exception
     */
    public ValidationResult getValidationResult(LanguageTypeEnum language) throws Exception;

    /**
     *  获取检验类型
     */
    public ValidationTypeEnum getValidationType();

    /**
     * 设置校验语句
     * @param validationValue
     */
    public void setValidationInfo(String validationInfo);

    /**
     * 设置校验值
     * @param validationValue
     */
    public void setValidationValue(Map<String, Object> validationValue);
    
    /**
     * 从source语言环境切换至target
     * @param source
     * @param target
     */
    public void languageChange(LanguageTypeEnum source, LanguageTypeEnum target);
}
