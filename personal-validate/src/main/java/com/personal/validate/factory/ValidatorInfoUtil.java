package com.personal.validate.factory;

import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.w3c.dom.Element;

import com.personal.core.utils.Assert;
import com.personal.core.utils.CoreUtil;
import com.personal.core.utils.FileUtil;
import com.personal.core.xml.XMLDoc;

/**
 * 校验信息读取工具类
 * @author cuibo
 *
 */
public class ValidatorInfoUtil
{
    private ValidatorInfoUtil()
    {
    }
    
    private static final Map<String, String> ALLVALIDATOR_ENGLISH = new LinkedHashMap<String, String>();
    private static final Map<String, String> ALLVALIDATOR_CHINESE = new LinkedHashMap<String, String>();

    private static final Lock LOCK = new ReentrantLock();
    
    
    /**
     * 获取所以的注册校验
     * @return
     * @throws Exception
     */
    static Map<String, String> getChineseAllValidator() throws Exception
    {
        if (CoreUtil.isEmpty(ALLVALIDATOR_CHINESE))
        {
            try
            {
                LOCK.lock();
                if (!CoreUtil.isEmpty(ALLVALIDATOR_CHINESE))
                {
                    return ALLVALIDATOR_CHINESE;
                }
                if (ValidatorInfoUtil.ALLVALIDATOR_CHINESE == null || ValidatorInfoUtil.ALLVALIDATOR_CHINESE.isEmpty())
                {
                    ValidatorInfoUtil.loadChinese();
                }
            } finally
            {
                LOCK.unlock();
            }
        }
        return ALLVALIDATOR_CHINESE;
    }

    private static void loadChinese() throws Exception
    {
        InputStream in = null;
        try
        {
            XMLDoc xmlDoc = new XMLDoc();
            in = ValidatorInfoUtil.class.getResourceAsStream("校验实现注册_中文.xml");
            xmlDoc.load(in);
            Element root = xmlDoc.getRootElement();
            Assert.isNotNull(root, "获取根节点失败！");
            List<Element> validators = xmlDoc.getChildList(root);
            if (validators != null && validators.size() > 0)
            {
                for (Element element : validators)
                {
                    if (CoreUtil.isEmpty(element.getAttribute("keyInfo")))
                    {
                        throw new Exception("存在关键字为空的校验实现");
                    }
                    if (ValidatorInfoUtil.ALLVALIDATOR_CHINESE.containsKey(element.getAttribute("keyInfo")))
                    {
                        throw new Exception("存在相同关键字" + element.getAttribute("keyInfo") + "的校验实现");
                    }
                    ValidatorInfoUtil.ALLVALIDATOR_CHINESE.put(element.getAttribute("keyInfo"),
                            element.getAttribute("class"));
                }
            }
        } finally
        {
            FileUtil.release(in);
        }
    }
    
    
    /**
     * 获取所以的注册校验
     * @return
     * @throws Exception
     */
    static Map<String, String> getEgnlishAllValidator() throws Exception
    {
        if (CoreUtil.isEmpty(ALLVALIDATOR_ENGLISH))
        {
            try
            {
                LOCK.lock();
                if (!CoreUtil.isEmpty(ALLVALIDATOR_ENGLISH))
                {
                    return ALLVALIDATOR_ENGLISH;
                }
                if (ValidatorInfoUtil.ALLVALIDATOR_ENGLISH == null || ValidatorInfoUtil.ALLVALIDATOR_ENGLISH.isEmpty())
                {
                    ValidatorInfoUtil.loadEnglish();
                }
            } finally
            {
                LOCK.unlock();
            }
        }
        return ALLVALIDATOR_ENGLISH;
    }

    private static void loadEnglish() throws Exception
    {
        InputStream in = null;
        try
        {
            XMLDoc xmlDoc = new XMLDoc();
            in = ValidatorInfoUtil.class.getResourceAsStream("校验实现注册_英文.xml");
            xmlDoc.load(in);
            Element root = xmlDoc.getRootElement();
            Assert.isNotNull(root, "获取根节点失败！");
            List<Element> validators = xmlDoc.getChildList(root);
            if (validators != null && validators.size() > 0)
            {
                for (Element element : validators)
                {
                    if (CoreUtil.isEmpty(element.getAttribute("keyInfo")))
                    {
                        throw new Exception("存在关键字为空的校验实现");
                    }
                    if (ValidatorInfoUtil.ALLVALIDATOR_ENGLISH.containsKey(element.getAttribute("keyInfo")))
                    {
                        throw new Exception("存在相同关键字" + element.getAttribute("keyInfo") + "的校验实现");
                    }
                    if ("AND".equals(element.getAttribute("keyInfo")))
                    {
                        ValidatorInfoUtil.ALLVALIDATOR_ENGLISH.put("&&", element.getAttribute("class"));
                    } else
                    {
                        ValidatorInfoUtil.ALLVALIDATOR_ENGLISH.put(element.getAttribute("keyInfo"),
                                element.getAttribute("class"));
                    }
                }
            }
        } finally
        {
            FileUtil.release(in);
        }
    }
}
