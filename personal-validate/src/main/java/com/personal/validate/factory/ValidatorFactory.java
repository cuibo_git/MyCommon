package com.personal.validate.factory;

import java.util.Map;
import java.util.Map.Entry;

import com.personal.core.utils.Assert;
import com.personal.validate.consts.LanguageTypeEnum;
import com.personal.validate.port.Validation;

/**
 * 校验工厂
 * 获取各种类型的校验实现
 * @author cuibo
 *
 */
public abstract class ValidatorFactory
{
    /**
     * 通过校验语句获取校验者
     * @param validationInfo
     * @return
     * @throws Exception
     */
    public static Validation getValidation(String validationInfo, Map<String, Object> validationValue) throws Exception
    {
        Validation result = ValidatorFactory.getValidation(validationInfo, LanguageTypeEnum.英文);
        result.setValidationInfo(validationInfo);
        result.setValidationValue(validationValue);
        return result;
    }
    
    /**
     * 通过校验语句获取校验者
     * @param validationInfo
     * @param validationValue
     * @param language
     * @return
     * @throws Exception
     */
    public static Validation getValidation(String validationInfo, Map<String, Object> validationValue, LanguageTypeEnum language) throws Exception
    {
        Validation result = ValidatorFactory.getValidation(validationInfo, language);
        result.setValidationInfo(validationInfo);
        result.setValidationValue(validationValue);
        return result;
    }
    
    /**
     * 通过校验语句获取校验者
     * @param validationInfo
     * @param language
     * @return
     * @throws Exception
     */
    private static Validation getValidation(String validationInfo, LanguageTypeEnum language) throws Exception
    {
        Assert.isNotNullOrEmpty(validationInfo, "校验语句为空！");
        Map<String, String> allValidator = null;
        if (language == null)
        {
            allValidator = ValidatorInfoUtil.getEgnlishAllValidator();
        } else
        {
            switch (language)
            {
            case 中文:
                allValidator = ValidatorInfoUtil.getChineseAllValidator();
                break;
            case 英文:
                allValidator = ValidatorInfoUtil.getEgnlishAllValidator();
                break;
            default:
                allValidator = ValidatorInfoUtil.getEgnlishAllValidator();
                break;
            }
        }
        String keyInfo = "";
        String[] arr = null;
        ok: for (Entry<String, String> entry : allValidator.entrySet())
        {
            keyInfo = entry.getKey();
            // 关键字可能为多个
            arr = keyInfo.split(",");
            if (arr != null && arr.length > 0)
            {
                for (String key : arr)
                {
                    if (!validationInfo.contains(key))
                    {
                        continue ok;
                    }
                }
            }
            Object obj = null;
            try
            {
                obj = Class.forName(entry.getValue()).newInstance();
            } catch (InstantiationException e)
            {
            } catch (IllegalAccessException e)
            {
            } catch (ClassNotFoundException e)
            {
            }
            if (obj instanceof Validation)
            {
                return (Validation) obj;
            }
            throw new Exception("获取校验语句" + validationInfo + "的校验实现失败！");
        }
        throw new Exception("获取校验语句" + validationInfo + "的校验实现失败！");
    }

}
