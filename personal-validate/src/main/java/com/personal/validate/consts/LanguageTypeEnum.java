package com.personal.validate.consts;

/**
 * 语言类型(默认是英文)
 * @author cuibo
 *
 */
public enum LanguageTypeEnum
{
    中文, 英文;
}
