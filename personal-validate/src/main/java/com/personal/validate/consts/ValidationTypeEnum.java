package com.personal.validate.consts;

/**
 * 获取校验类型
 * @author cuibo
 *
 */
public enum ValidationTypeEnum
{
    操作校验, 复合句, 混合句, 判断校验,
}
