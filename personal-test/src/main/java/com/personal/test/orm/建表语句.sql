-- Create table
create table A_TEST_USER
(
  ID         VARCHAR2(40) not null,
  NAME       VARCHAR2(200),
  AGE        NUMBER,
  CREATEDATE TIMESTAMP(6),
  REMARK     CLOB,
  MONEY      NUMBER,
  SHOWORDER  NUMBER,
  FILEDATA   BLOB
)
tablespace GXDWPGDB
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table A_TEST_USER
  add primary key (ID)
  using index 
  tablespace GXDWPGDB
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
