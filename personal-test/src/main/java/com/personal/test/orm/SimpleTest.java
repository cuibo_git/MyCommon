package com.personal.test.orm;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.personal.dao.base.IBaseDao;

/**
 * Unit test for simple App.
 * https://www.jianshu.com/p/c553169c5921
 */
public class SimpleTest
{
    
    /**
     * 增
     */
	@Test
    public void testAdd()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml", SimpleTest.class);
        IBaseDao<User> userDao = (IBaseDao<User>) context.getBean("userDao");
        
        // 单个新增
        User add = new User();
        // add.setId(UUID.randomUUID().toString());
        add.setName("崔波");
        add.setAge(18);
        add.setCreatedate(new Timestamp(System.currentTimeMillis()));
        add.setRemark("备注");
        add.setMoney(111.11);
        add.setShoworder(System.currentTimeMillis());
        try
        {
            add.setFileData(FileUtil.readInputStream(SimpleTest.class.getResourceAsStream("spring.xml")));
        } catch (IOException e)
        {
        }
        // 选择性插入
        // userDao.insertSelective(add);
        if (userDao.insert(add) == 1)
        {
            System.out.println(add + "新增成功");
        }
        
        long time = System.currentTimeMillis();
        
        // 批量新增
        List<User> users = new ArrayList<>();
        for (int i = 0; i < 100; i++)
        {
            User local = new User();
            // add.setId(UUID.randomUUID().toString());
            local.setName("崔波");
            local.setAge(18);
            local.setCreatedate(new Timestamp(System.currentTimeMillis()));
            local.setRemark("备注");
            local.setMoney(111.11);
            local.setShoworder(System.currentTimeMillis());
            try
			{
            	local.setFileData(FileUtil.readInputStream(SimpleTest.class.getResourceAsStream("spring.xml")));
			} catch (IOException e)
			{
				e.printStackTrace();
			}
            users.add(local);
        }
        // 选择性批量插入，取所有记录里面非空字段的并集
        // userDao.batchInsertSelective(records);
        if (userDao.batchInsert(users) == users.size())
        {
            System.out.println("新增" + users.size() + "个用户成功：耗时：" + (System.currentTimeMillis() - time));
        }
    }
    
    public void testDelete()
    {
    	ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml", SimpleTest.class);
        IBaseDao<User> userDao = (IBaseDao<User>) context.getBean("userDao");
        
        // 主键删除
        User params = new User();
        params.setStart(0);
        params.setLimit(1);
        List<User> users = userDao.selectByParams(params);
        
        System.out.println("一共" + userDao.countByParams(params));
        if (userDao.deleteByPrimaryKey(users.get(0).getId()) == 1)
		{
			System.out.println(users.get(0) + "删除成功！");
		}
        System.out.println("一共" + userDao.countByParams(params));
        
        // 条件删除
        params.setName("batch");
        System.out.println("一共删除：" + userDao.deleteByParams(params));
        
    }
    
    public void testUpdate()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml", SimpleTest.class);
        IBaseDao<User> userDao = (IBaseDao<User>) context.getBean("userDao");
        
        // 主键更新
        User params = new User();
        params.setStart(0);
        params.setLimit(1);
        List<User> users = userDao.selectByParams(params);
        
        User user = users.get(0);
        // user.setName("update");
        user.setName("up");
        user.setAge(123456);
        // userDao.updateByPrimaryKeySelective(user);
        if (userDao.updateByPrimaryKey(user) == 1)
        {
            System.out.println(user + "更新成功！");
            System.out.println(userDao.selectByPrimaryKey(user.getId()));
        }
        
        // 条件更新
        params = new User();
        params.setName("崔波");
        User update = new User();
        update.setName("up");
        System.out.println("一共更新：" + userDao.updateByParamsSelective(update, params) + "条数据");
        // userDao.updateByParamsSelective(params, update);
        
        
        // 批量更新
        params = new User();
        List<User> allUsers = userDao.selectAll();
        
        for (User user2 : allUsers)
		{
        	user2.setName("batch");
		}
        System.out.println("一共更新：" + userDao.batchUpdate(allUsers) + "条数据");
        
    }
    
    public static void main(String[] args)
    {
    	ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml", SimpleTest.class);
        IBaseDao<User> userDao = (IBaseDao<User>) context.getBean("userDao");
        userDao.selectAll();
        long time = System.currentTimeMillis();
    	userDao.selectAll();
    	System.out.println(System.currentTimeMillis() - time);
    	time = System.currentTimeMillis();
    	userDao.selectAll();
    	System.out.println(System.currentTimeMillis() - time);
    	time = System.currentTimeMillis();
    	userDao.selectAll();
    	System.out.println(System.currentTimeMillis() - time);
    	time = System.currentTimeMillis();
    	userDao.selectAll();
    	System.out.println(System.currentTimeMillis() - time);
    	time = System.currentTimeMillis();
    	userDao.selectAll();
    	System.out.println(System.currentTimeMillis() - time);
//    	userDao.selectByPrimaryKey(null);
//    	userDao.selectByPrimaryKeyWithoutBigData(null);
//    	userDao.selectPageByParams(params);
    	
    }
    
}
