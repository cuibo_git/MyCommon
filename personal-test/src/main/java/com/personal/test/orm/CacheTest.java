package com.personal.test.orm;

import java.util.List;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.personal.dao.base.IBaseDao;

/**
 * Unit test for simple App.
 * https://www.jianshu.com/p/c553169c5921
 */
public class CacheTest
{
    // 默认使用DaoTemplateBean查询，当在实体上配置了缓存注解时，使用
	// CachingDaoTemplateBean 装饰 DaoTemplateBean
	// 每个实体对应自己的缓存区域，
	// 缓存Key的生产依赖Sql和SqlHandler实现Signable借口
	// Sql是否读取缓存，SqlHandler是否刷新缓存依赖他们实现了ReadCache和FlushCache接口
    @Test
    public void testCache()
    {
    	ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml", CacheTest.class);
        IBaseDao<User> userDao = (IBaseDao<User>) context.getBean("userDao");
        
        // 默认匹配方式使用注解上的配置
        User parms = new User();
        parms.setName("波");
        
        List<User> users1 = userDao.selectAll();
        // userDao.insert(new User());
        List<User> users2 = userDao.selectAll();
        
        System.out.println(users1 == users2);
    }
    
}
