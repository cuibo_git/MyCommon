package com.personal.test.orm;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.personal.dao.base.IBaseDao;
import com.personal.dao.handler.BaseSqlHandler;
import com.personal.dao.metadata.DataBaseTableMetadata;
import com.personal.dao.sql.BaseSql;
import com.personal.dao.sql.Sql;
import com.personal.dao.template.DaoTemplate;

/**
 * Unit test for simple App.
 * https://www.jianshu.com/p/c553169c5921
 */
public class IdefinedTest
{
	public static void main(String[] args)
    {
    	ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml", IdefinedTest.class);
        IBaseDao<User> userDao = (IBaseDao<User>) context.getBean("userDao");
        DataBaseTableMetadata metadate = userDao.getDataBaseTableMetadata();
        Long hj1 = userDao.execute(new BaseSql("select sum(age) as hj from " + metadate.getTableName(), true), new BaseSqlHandler<Long>()
		{

			@Override
			public boolean isFlushCache()
			{
				return false;
			}
			
			@Override
			public List<Object> getSignFlags()
			{
				return null;
			}

			@Override
			public Long handleResult(DaoTemplate template, Sql sql)
			{
				return template.geJdbcTemplate().queryForObject(sql.getSql(), Long.class, sql.getParams());
			}
		});
        
//        userDao.execute(new BaseSql("xxxx") , new BaseSqlHandler<Boolean>()
//		{
//
//			@Override
//			public Boolean handleResult(DaoTemplate template, Sql sql)
//			{
//				System.out.println("dosomething");
//				return true;
//			}
//
//			@Override
//			public boolean isFlushCache()
//			{
//				return true;
//			}
//	
//		});
        
        Long hj2 = userDao.execute(new BaseSql("select sum(age) as hj from " + metadate.getTableName(), true), new BaseSqlHandler<Long>()
		{

			@Override
			public boolean isFlushCache()
			{
				return false;
			}
			
			@Override
			public List<Object> getSignFlags()
			{
				return null;
			}

			@Override
			public Long handleResult(DaoTemplate template, Sql sql)
			{
				return template.geJdbcTemplate().queryForObject(sql.getSql(), Long.class, sql.getParams());
			}
		});
        
        System.out.println(hj1 == hj2);
        
    }
    
    
    
    
    
}
