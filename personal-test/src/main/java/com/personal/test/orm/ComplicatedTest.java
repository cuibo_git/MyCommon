package com.personal.test.orm;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.personal.dao.base.IBaseDao;
import com.personal.dao.bean.Criteria;

/**
 * Unit test for simple App.
 * https://www.jianshu.com/p/c553169c5921
 */
public class ComplicatedTest
{
    // 复杂条件查询
    @Test
    public void testQuery()
    {
    	ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml", ComplicatedTest.class);
        IBaseDao<User> userDao = (IBaseDao<User>) context.getBean("userDao");
        
        // 默认匹配方式使用注解上的配置
        User parms = new User();
        parms.setName("波");
        System.out.println("一共：" + userDao.countByParams(parms));
        
        parms.setName(null);
        Criteria criteria = parms.createCriteria();
//        criteria.andFieldEqualTo(fieldName, value)
//        criteria.andFieldIsNotNull(fieldName)
//        criteria.andFieldIsNull(fieldName)
//        criteria.andFieldGreaterThan(fieldName, value);
//        criteria.andFieldBetween(fieldName, value1, value2);
//        criteria.andFieldIn(fieldName, values)
        criteria.andFieldIn("name", "崔", "波");
        criteria.andFieldGreaterThanOrEqualTo("age", 45);
        userDao.selectByParams(parms);
    	
    }
    
}
