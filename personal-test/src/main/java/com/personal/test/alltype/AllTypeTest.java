package com.personal.test.alltype;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.personal.dao.base.IBaseDao;
import com.personal.test.orm.FileUtil;

/**
 * AllTypeTest
 * @author cuibo
 *
 */
public class AllTypeTest
{
    public void testSqlserver()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml", AllTypeTest.class);
        IBaseDao<SqlServerAlltype> dao = (SqlServerAlltypeDaoImpl) context.getBean("SqlServerAlltypeDaoImpl");
        
        SqlServerAlltype add = new SqlServerAlltype();
        add.setaName(1L);
        add.setbName("bName");
        add.setcName(new Date());
        add.setdName(new Date());
        add.seteName(new Date());
        add.setfName(new Date());
        add.setgName(1L);
        add.sethName(1.2);
        add.setiName(111);
        add.setjName(12.2356);
        add.setkName("kName");
        add.setlName("lName");
        add.setmName(1L);
        add.setnName("nName");
        add.setoName((short)1);
        add.setpName(new BigDecimal("1234.567"));
        add.setqName("qName");
        add.setrName(new Date());
        try
        {
            add.setsName(FileUtil.readInputStream(AllTypeTest.class.getResourceAsStream("spring.xml")));
        } catch (IOException e)
        {
        }
        add.settName((byte)12);
        System.out.println(dao.insert(add));
        
        List<SqlServerAlltype> types = dao.selectAll();
        System.out.println(types.size());
        
        
    }
    
    public void testMysql()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml", AllTypeTest.class);
        IBaseDao<MySqlAlltype> dao = (MySqlAlltypeDaoImpl) context.getBean("MySqlAlltypeDaoImpl");
        
        MySqlAlltype add = new MySqlAlltype();
        add.setAname((byte)1);
        add.setBname((short)1);
        add.setCname(1);
        add.setDname(23);
        add.setEname(45);
        add.setFname(4L);
        add.setGname(12.56);
        add.setHname(56.7F);
        add.setIname(4L);
        add.setJname(4L);
        add.setKname("kName");
        add.setLname("lName");
        add.setMname(new Date());
        add.setNname(new Date());
        add.setOname(new Date());
        add.setPname(new Date());
        try
        {
            add.setQname(FileUtil.readInputStream(AllTypeTest.class.getResourceAsStream("spring.xml")));
        } catch (IOException e)
        {
        }
        add.setRname("rName");
        
        System.out.println(dao.insert(add));
        
        List<MySqlAlltype> types = dao.selectAll();
        System.out.println(types.size());
        
        
    }
    
    @Test
    public void testOracle()
    {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml", AllTypeTest.class);
        IBaseDao<OracleAllType> dao = (OracleAlltypeDaoImpl) context.getBean("OracleAlltypeDaoImpl");
        
        OracleAllType add = new OracleAllType();
        add.setA(123456);
        add.setB(3213);
        try
        {
            add.setC(FileUtil.readInputStream(AllTypeTest.class.getResourceAsStream("spring.xml")));
        } catch (IOException e)
        {
        }
        add.setD("d");
        add.setE("e");
        add.setF(new Date());
        add.setG("g");
        add.setH("ccc");
        add.setI((short)1);
        add.setJ("123");
        add.setK(new Date());
        add.setL("l");
        
        System.out.println(dao.insert(add));
        
        List<OracleAllType> types = dao.selectAll();
        
        System.out.println(types.size());
        
        
    }
}
