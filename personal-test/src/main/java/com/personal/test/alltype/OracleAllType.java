package com.personal.test.alltype;

import java.util.Date;

import com.personal.dao.annotation.DataBaseField;
import com.personal.dao.annotation.DataBasePrimaryKey;
import com.personal.dao.annotation.DataBaseTable;
import com.personal.dao.bean.BaseModel;
import com.personal.dao.enums.PrimaryKeyGeneratorEnum;

@DataBaseTable(tableName="ORACLEALLTYPE")
public class OracleAllType extends BaseModel
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @DataBasePrimaryKey(generator=PrimaryKeyGeneratorEnum.UUID)
    private String id;

    @DataBaseField
    private Object a;

    @DataBaseField
    private Object b;
    
    @DataBaseField
    private byte[] c;

    @DataBaseField
    private String d;

    @DataBaseField
    private String e;
    
    @DataBaseField
    private Date f;
    
    @DataBaseField
    private String g;

    @DataBaseField
    private Object h;

    @DataBaseField
    private Short i;

    @DataBaseField
    private Object j;

    @DataBaseField
    private Date k;

    @DataBaseField
    private String l;
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Object getA() {
        return a;
    }

    public void setA(Object a) {
        this.a = a;
    }

    public Object getB() {
        return b;
    }

    public void setB(Object b) {
        this.b = b;
    }

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e == null ? null : e.trim();
    }

    public Date getF() {
        return f;
    }

    public void setF(Date f) {
        this.f = f;
    }

    public Object getH() {
        return h;
    }

    public void setH(Object h) {
        this.h = h;
    }

    public Short getI() {
        return i;
    }

    public void setI(Short i) {
        this.i = i;
    }

    public Object getJ() {
        return j;
    }

    public void setJ(Object j) {
        this.j = j;
    }

    public Date getK() {
        return k;
    }

    public void setK(Date k) {
        this.k = k;
    }

    public String getL() {
        return l;
    }

    public void setL(String l) {
        this.l = l == null ? null : l.trim();
    }

    public byte[] getC()
    {
        return c;
    }

    public void setC(byte[] c)
    {
        this.c = c;
    }

    public String getD()
    {
        return d;
    }

    public void setD(String d)
    {
        this.d = d;
    }

    public String getG()
    {
        return g;
    }

    public void setG(String g)
    {
        this.g = g;
    }
    
}