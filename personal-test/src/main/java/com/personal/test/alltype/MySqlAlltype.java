package com.personal.test.alltype;

import java.util.Date;

import com.personal.dao.annotation.DataBaseField;
import com.personal.dao.annotation.DataBasePrimaryKey;
import com.personal.dao.annotation.DataBaseTable;
import com.personal.dao.bean.BaseModel;
import com.personal.dao.enums.PrimaryKeyGeneratorEnum;

@DataBaseTable(tableName="mysqlalltype")
public class MySqlAlltype  extends BaseModel
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @DataBasePrimaryKey(generator=PrimaryKeyGeneratorEnum.UUID)
    private String id;

    @DataBaseField
    private Byte aname;

    @DataBaseField
    private Short bname;

    @DataBaseField
    private Integer cname;

    @DataBaseField
    private Integer dname;

    @DataBaseField
    private Integer ename;

    @DataBaseField
    private Long fname;

    @DataBaseField
    private Double gname;

    @DataBaseField
    private Float hname;

    @DataBaseField
    private Long iname;

    @DataBaseField
    private Long jname;

    @DataBaseField
    private String kname;

    @DataBaseField
    private String lname;

    @DataBaseField
    private Date mname;

    @DataBaseField
    private Date nname;

    @DataBaseField
    private Date oname;

    @DataBaseField
    private Date pname;
    
    @DataBaseField
    private byte[] qname;

    @DataBaseField
    private String rname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Byte getAname() {
        return aname;
    }

    public void setAname(Byte aname) {
        this.aname = aname;
    }

    public Short getBname() {
        return bname;
    }

    public void setBname(Short bname) {
        this.bname = bname;
    }

    public Integer getCname() {
        return cname;
    }

    public void setCname(Integer cname) {
        this.cname = cname;
    }

    public Integer getDname() {
        return dname;
    }

    public void setDname(Integer dname) {
        this.dname = dname;
    }

    public Integer getEname() {
        return ename;
    }

    public void setEname(Integer ename) {
        this.ename = ename;
    }

    public Long getFname() {
        return fname;
    }

    public void setFname(Long fname) {
        this.fname = fname;
    }

    public Double getGname() {
        return gname;
    }

    public void setGname(Double gname) {
        this.gname = gname;
    }

    public Float getHname() {
        return hname;
    }

    public void setHname(Float hname) {
        this.hname = hname;
    }

    public Long getIname() {
        return iname;
    }

    public void setIname(Long iname) {
        this.iname = iname;
    }

    public Long getJname() {
        return jname;
    }

    public void setJname(Long jname) {
        this.jname = jname;
    }

    public String getKname() {
        return kname;
    }

    public void setKname(String kname) {
        this.kname = kname == null ? null : kname.trim();
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname == null ? null : lname.trim();
    }

    public Date getMname() {
        return mname;
    }

    public void setMname(Date mname) {
        this.mname = mname;
    }

    public Date getNname() {
        return nname;
    }

    public void setNname(Date nname) {
        this.nname = nname;
    }

    public Date getOname() {
        return oname;
    }

    public void setOname(Date oname) {
        this.oname = oname;
    }

    public Date getPname() {
        return pname;
    }

    public void setPname(Date pname) {
        this.pname = pname;
    }

    public byte[] getQname()
    {
        return qname;
    }

    public void setQname(byte[] qname)
    {
        this.qname = qname;
    }

    public String getRname()
    {
        return rname;
    }

    public void setRname(String rname)
    {
        this.rname = rname;
    }
    
}