package com.personal.test.alltype;

import java.math.BigDecimal;
import java.util.Date;

import com.personal.dao.annotation.DataBaseField;
import com.personal.dao.annotation.DataBasePrimaryKey;
import com.personal.dao.annotation.DataBaseTable;
import com.personal.dao.bean.BaseModel;
import com.personal.dao.enums.PrimaryKeyGeneratorEnum;

@DataBaseTable(tableName="sqlserveralltype")
public class SqlServerAlltype extends BaseModel 
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @DataBasePrimaryKey(generator=PrimaryKeyGeneratorEnum.UUID)
    private String id;

    @DataBaseField
    private Long aName;

    @DataBaseField
    private String bName;

    @DataBaseField
    private Date cName;

    @DataBaseField
    private Date dName;

    @DataBaseField
    private Date eName;

    @DataBaseField
    private Object fName;

    @DataBaseField
    private Long gName;

    @DataBaseField
    private Double hName;

    @DataBaseField
    private Integer iName;

    @DataBaseField
    private double jName;

    @DataBaseField
    private String kName;

    @DataBaseField
    private String lName;
    
    @DataBaseField
    private Long mName;

    @DataBaseField
    private String nName;

    @DataBaseField
    private Short oName;
    
    @DataBaseField
    private BigDecimal pName;
    
    @DataBaseField
    private String qName;

    @DataBaseField
    private Date rName;

    @DataBaseField
    private byte[] sName;
    
    @DataBaseField
    private Byte tName;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Long getaName()
    {
        return aName;
    }

    public void setaName(Long aName)
    {
        this.aName = aName;
    }

    public String getbName()
    {
        return bName;
    }

    public void setbName(String bName)
    {
        this.bName = bName;
    }

    public Date getcName()
    {
        return cName;
    }

    public void setcName(Date cName)
    {
        this.cName = cName;
    }

    public Date getdName()
    {
        return dName;
    }

    public void setdName(Date dName)
    {
        this.dName = dName;
    }

    public Date geteName()
    {
        return eName;
    }

    public void seteName(Date eName)
    {
        this.eName = eName;
    }

    public Object getfName()
    {
        return fName;
    }

    public void setfName(Object fName)
    {
        this.fName = fName;
    }

    public Long getgName()
    {
        return gName;
    }

    public void setgName(Long gName)
    {
        this.gName = gName;
    }

    public Double gethName()
    {
        return hName;
    }

    public void sethName(Double hName)
    {
        this.hName = hName;
    }

    public Integer getiName()
    {
        return iName;
    }

    public void setiName(Integer iName)
    {
        this.iName = iName;
    }

    public double getjName()
    {
        return jName;
    }

    public void setjName(double jName)
    {
        this.jName = jName;
    }

    public String getkName()
    {
        return kName;
    }

    public void setkName(String kName)
    {
        this.kName = kName;
    }

    public String getlName()
    {
        return lName;
    }

    public void setlName(String lName)
    {
        this.lName = lName;
    }

    public Long getmName()
    {
        return mName;
    }

    public void setmName(Long mName)
    {
        this.mName = mName;
    }

    public String getnName()
    {
        return nName;
    }

    public void setnName(String nName)
    {
        this.nName = nName;
    }

    public Short getoName()
    {
        return oName;
    }

    public void setoName(Short oName)
    {
        this.oName = oName;
    }

    public BigDecimal getpName()
    {
        return pName;
    }

    public void setpName(BigDecimal pName)
    {
        this.pName = pName;
    }

    public String getqName()
    {
        return qName;
    }

    public void setqName(String qName)
    {
        this.qName = qName;
    }

    public Date getrName()
    {
        return rName;
    }

    public void setrName(Date rName)
    {
        this.rName = rName;
    }

    public byte[] getsName()
    {
        return sName;
    }

    public void setsName(byte[] sName)
    {
        this.sName = sName;
    }

    public Byte gettName()
    {
        return tName;
    }

    public void settName(Byte tName)
    {
        this.tName = tName;
    }

   
}