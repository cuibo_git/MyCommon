package com.personal.test.txt;

import java.util.List;

public interface IUserService
{
    int insert(User record);

    int updateByParams(User record, User params);
    
    int batchInsert(List<User> records);
    
    int batchUpdate(List<User> records);
    
    List<User> selectAll();
    
}
