package com.personal.test.txt;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.personal.dao.base.IBaseDao;

public class UserServiceImpl implements IUserService
{

    private IBaseDao<User> userDao;
    
    @Transactional
    @Override
    public int insert(User record)
    {
        int i = userDao.insert(record);
        // throw new RuntimeException("");
        return i;
    }

    @Transactional
    @Override
    public int updateByParams(User record, User params)
    {
        return userDao.updateByParams(record, params);
    }

    @Transactional
    @Override
    public int batchInsert(List<User> records)
    {
        int i = userDao.batchInsert(records);
//        throw new RuntimeException();
         return i;
    }

    @Transactional
    @Override
    public int batchUpdate(List<User> records)
    {
        return userDao.batchUpdate(records);
    }

    public IBaseDao<User> getUserDao()
    {
        return userDao;
    }

    public void setUserDao(IBaseDao<User> userDao)
    {
        this.userDao = userDao;
    }

    @Override
    public List<User> selectAll()
    {
        return userDao.selectAll();
    }
    
    

}
