package com.personal.test.txt;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Unit test for simple App.
 * https://www.jianshu.com/p/c553169c5921
 */
public class TxTTest
{
    public static void main(String[] args)
    {
    	ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring.xml", TxTTest.class);
        IUserService userService = (IUserService) context.getBean("userService");
        
        userService.selectAll();
        
        long time = System.currentTimeMillis();
        List<User> users = new ArrayList<>();
        for (int i = 0; i < 10000; i++)
        {
            User user = new User();
            user.setId(i + "");
            users.add(new User());
        }
        userService.batchInsert(users);
        System.out.println("新增" + users.size() + "个用户成功：耗时：" + (System.currentTimeMillis() - time));
        
    }
    
}
