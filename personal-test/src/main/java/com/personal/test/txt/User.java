package com.personal.test.txt;

import java.util.Date;

import com.personal.dao.annotation.CacheNamespace;
import com.personal.dao.annotation.DataBaseField;
import com.personal.dao.annotation.DataBasePrimaryKey;
import com.personal.dao.annotation.DataBaseTable;
import com.personal.dao.annotation.Validator;
import com.personal.dao.bean.BaseModel;
import com.personal.dao.enums.MatchModelEnum;
import com.personal.dao.enums.OperatorEnum;

/**
 * 用户
 * @author cuibo
 *
 */
// 缓存配置，配置缓存的大小，策略（如FIFO，LRU），时间等，不配置该配置则不启用缓存
// 可自定义缓存实现，使用外部的缓存
@CacheNamespace(flushInterval = 10000000, readWrite = false)
// 该实体类对应数据库表名
@DataBaseTable(tableName="a_test_user")
public class User extends BaseModel
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    // 主键配置: 配置数据列名（默认和属性名一致），备注，自动生成方式的
    // @DataBasePrimaryKey(columnName="columnName", caption="主键", generator=PrimaryKeyGeneratorEnum.UUID);
    @DataBasePrimaryKey
    private String id;
    
    // 该字段的校验配置（暂只实现了长度，后续可以扩展）：不配置则不校验
    @Validator(maxLength=40, errorMessage="名称最大长度为3")
    // 该字段的配置：配置字段列明（默认和字段属性名一致），备注，查询时的匹配方式和匹配模式，是否是大字段（作用于不需要经常查询的字段如blob）
    // @DataBaseField(columnName="columnName", caption="姓名", operator=OperatorEnum.LIKE, matchModel=MatchModelEnum.ALL, bigData=true)
    @DataBaseField(operator=OperatorEnum.LIKE)
    private String name;
    
    @DataBaseField()
    private Integer age;
    
    @DataBaseField
    private Date createdate;
    
    @DataBaseField(operator=OperatorEnum.LIKEIGNORECASE, matchModel = MatchModelEnum.ALL)
    private String remark;
    
    @DataBaseField
    private Double money;
    
    @DataBaseField
    private Long showorder;
    
    @DataBaseField
    private byte[] fileData;
    
    @Override
    public String toString()
    {
    	return "id:" + id + "," + "name:" + name + ", age:" + age + ",createdate:" + createdate; 
    }
    
    public String toString2()
    {
    	return super.toString();
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Integer getAge()
    {
        return age;
    }

    public void setAge(Integer age)
    {
        this.age = age;
    }

    public Date getCreatedate()
    {
        return createdate;
    }

    public void setCreatedate(Date createdate)
    {
        this.createdate = createdate;
    }

    public String getRemark()
    {
        return remark;
    }

    public void setRemark(String remark)
    {
        this.remark = remark;
    }

    public Double getMoney()
    {
        return money;
    }

    public void setMoney(Double money)
    {
        this.money = money;
    }

    public Long getShoworder()
    {
        return showorder;
    }

    public void setShoworder(Long showorder)
    {
        this.showorder = showorder;
    }

    public byte[] getFileData()
    {
        return fileData;
    }

    public void setFileData(byte[] fileData)
    {
        this.fileData = fileData;
    }


}
