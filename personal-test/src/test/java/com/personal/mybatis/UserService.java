package com.personal.mybatis;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.apache.ibatis.session.SqlSession;

public class UserService
{
    public static void main(String[] args) {
        // insertUser();
        // deleteUser(1);
        //selectUserById(2);
        //selectAllUser();
        // selectAllUser();
    	selectAllUser();
        long time = System.currentTimeMillis();
//        for (int i = 0; i < 1000; i++)
//        {
//            selectAllUser();
//        }
        // insertBatchUser();
        selectAllUser();
            // selectAllUser();
        System.out.println("查询：" + (System.currentTimeMillis() - time));
    }

    
    /**
     * 新增用户
     */
    private static boolean  insertBatchUser(){
        SqlSession session = DBTools.getBacthSession();
        
        List<User> users = new ArrayList<>();
        for (int i = 0; i < 10000; i++)
        {
            User user = new User();
            user.setId(UUID.randomUUID().toString());
            user.setName("cuibo");
            user.setAge(12);
            user.setRemark("remark");
            user.setCreatedate(new Date());
            user.setMoney(1234.567 + new Random().nextInt(31231));
            user.setShoworder(System.currentTimeMillis());
            users.add(user);
        }
        UserMapper mapper = session.getMapper(UserMapper.class);
        
        
        int batch = 0;
        for (User record : users)
        {
            try
            {
                mapper.insertUser(record);
            } catch (Exception e)
            {
                e.printStackTrace();
            }
            batch++;
            if (batch == 1000)
            {
                session.commit();
                session.clearCache();
                batch = 0;
            }
        }
        session.commit();
        session.close();
        return true;
    }
    
    /**
     * 新增用户
     */
    private static boolean  insertUser(){
        SqlSession session = DBTools.getSession();
        UserMapper mapper = session.getMapper(UserMapper.class);
        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setName("cuibo");
        user.setAge(12);
        user.setRemark("remark");
        user.setCreatedate(new Date());
        user.setMoney(1234.567 + new Random().nextInt(31231));
        user.setShoworder(System.currentTimeMillis());
        try {
            int index=mapper.insertUser(user);
            boolean bool=index>0?true:false;
             session.commit();
             return bool;
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
            return false;
        }finally{
            session.close();
        }
    }
    
    
    /**
     * 删除用户
     * @param id 用户ID
     */
    private static boolean deleteUser(int id){
        SqlSession session=DBTools.getSession();
        UserMapper mapper=session.getMapper(UserMapper.class);
        try {
            int index=mapper.deleteUser(id); 
            boolean bool=index>0?true:false;
            session.commit(); 
            return bool;
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback(); 
            return false;
        }finally{
            session.close();
        }
    }
    
    
    /**
     * 根据id查询用户
     * @param id
     */
    private static void selectUserById(int id){
        SqlSession session=DBTools.getSession();
        UserMapper mapper=session.getMapper(UserMapper.class);
        try {
        User user= mapper.selectUserById(id);
        session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        }finally{
            session.close();
        }
    }
    
    /**
     * 查询所有的用户
     */
    private static void selectAllUser(){
        SqlSession session=DBTools.getSession();
        UserMapper mapper=session.getMapper(UserMapper.class);
        try {
        List<User> users = mapper.selectAllUser();
        session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
        }finally{
            session.close();
        }
    }
}
