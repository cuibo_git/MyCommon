package com.personal.mybatis;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class DBTools
{   
 public static SqlSessionFactory sessionFactory;
    
    static{
        try {
            //构建sqlSession的工厂
            sessionFactory = new SqlSessionFactoryBuilder().build(DBTools.class.getResourceAsStream("mybatis.cfg.xml"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    //创建能执行映射文件中sql的sqlSession
    public static SqlSession getSession(){
        return sessionFactory.openSession();
    }
    
    public static SqlSession getBacthSession(){
        return sessionFactory.openSession(ExecutorType.BATCH, false);
    }
}
