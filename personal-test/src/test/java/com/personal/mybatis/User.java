package com.personal.mybatis;

import java.util.Date;

/**
 * 用户
 * @author cuibo
 *
 */
public class User
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    private String id;
    
    private String name;
    
    private Integer age;
    
    private Date createdate;
    
    private String remark;
    
    private Double money;
    
    private Long showorder;
    
    private byte[] fileData;
    
    public byte[] getFileData()
	{
		return fileData;
	}

	public void setFileData(byte[] fileData)
	{
		this.fileData = fileData;
	}

	public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Integer getAge()
    {
        return age;
    }

    public void setAge(Integer age)
    {
        this.age = age;
    }

    public Date getCreatedate()
    {
        return createdate;
    }

    public void setCreatedate(Date createdate)
    {
        this.createdate = createdate;
    }

    public String getRemark()
    {
        return remark;
    }

    public void setRemark(String remark)
    {
        this.remark = remark;
    }

    public Double getMoney()
    {
        return money;
    }

    public void setMoney(Double money)
    {
        this.money = money;
    }

    public Long getShoworder()
    {
        return showorder;
    }

    public void setShoworder(Long showorder)
    {
        this.showorder = showorder;
    }
    
    

}
