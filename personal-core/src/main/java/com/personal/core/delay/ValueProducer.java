package com.personal.core.delay;

/**
 * V生成
 * @author cuibo
 *
 * @param <V>
 */
public interface ValueProducer<V>
{
    public V produce();
    
}
