package com.personal.core.enums;

/**
 * 数据类型
 * @author cuibo
 *
 */
public enum DataTypeEnum
{
    数字,
    字符,
    日期;
}
