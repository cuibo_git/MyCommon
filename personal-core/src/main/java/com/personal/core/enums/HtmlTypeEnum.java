package com.personal.core.enums;

import com.personal.core.htmldata.HtmlIrregularData;
import com.personal.core.htmldata.InputIrregularData;
import com.personal.core.htmldata.InputTextIrregularData;
import com.personal.core.htmldata.OutputIrregularData;

/**
 * Html组件类别枚举
 * @author cuibo
 *
 */
public enum HtmlTypeEnum
{
    INPUT,
    OUTPUT,
    INPUTTEXT,;

    /**
     * 获取htmlType 对应的类型实现
     * @param htmlType
     * @return
     */
    public static HtmlIrregularData getHtmlImplByType(String htmlType)
    {
        HtmlIrregularData result = null;
        if (HtmlTypeEnum.INPUT.toString().equals(htmlType))
        {
            result = new InputIrregularData();
        } else if (HtmlTypeEnum.OUTPUT.toString().equals(htmlType))
        {
            result = new OutputIrregularData();
        } else if (HtmlTypeEnum.INPUTTEXT.toString().equals(htmlType))
        {
            result = new InputTextIrregularData();
        } else
        {
            // 默认还是输出框
            result = new OutputIrregularData();
        }
        return result;
    }
    
}
