package com.personal.core.port;

import java.util.List;


/**
 * 有子节点并且有深度
 * @author cuibo
 *
 */
public interface HaveDeepAndChildren
{
    public HaveDeepAndChildren getParent();
    
    public List<? extends HaveDeepAndChildren> getChildren();
    
    public void setDeepLength(int deepLength);
    
    public int getDeepLength();
    
    public boolean isLeaf();
    
}
