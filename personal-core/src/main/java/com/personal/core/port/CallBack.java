package com.personal.core.port;

/**
 * 
 * 回调
 * @author cuibo
 *
 * @param <V>
 */
public interface CallBack<V>
{

    /**
     * 执行结束
     * @param v
     */
    public void onComplete(V v);
    
    /**
     * 执行成功
     * @param v
     */
    public void onSuccess(V v);
    
    /**
     * 执行失败
     * @param cause
     */
    public void onError(Throwable cause);
    
    
}
