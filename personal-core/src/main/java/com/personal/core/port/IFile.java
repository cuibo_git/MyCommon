package com.personal.core.port;

import java.io.IOException;
import java.io.InputStream;

/**
 * 文件
 * @author cuibo
 *
 */
public interface IFile
{
    public String getFileName();
    
    public InputStream getStreamData() throws IOException;
    
    public byte[] getByteData() throws IOException;
    
    public long getSize() throws IOException;
    
    public void relase() throws Exception;
    
}
