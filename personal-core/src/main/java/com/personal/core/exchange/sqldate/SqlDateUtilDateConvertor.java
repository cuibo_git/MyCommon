package com.personal.core.exchange.sqldate;

import com.personal.core.exchange.Convertor;

public class SqlDateUtilDateConvertor extends Convertor<java.sql.Date, java.util.Date>
{
  public java.util.Date exchange(java.sql.Date o)
  {
    return new java.util.Date(o.getTime());
  }
}