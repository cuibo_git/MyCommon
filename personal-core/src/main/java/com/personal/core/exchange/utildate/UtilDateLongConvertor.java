package com.personal.core.exchange.utildate;

import java.util.Date;

import com.personal.core.exchange.Convertor;

public class UtilDateLongConvertor extends Convertor<Date, Long>
{
  public Long exchange(Date o)
  {
    return Long.valueOf(o.getTime());
  }
}