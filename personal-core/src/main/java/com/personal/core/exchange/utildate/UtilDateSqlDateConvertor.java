package com.personal.core.exchange.utildate;

import com.personal.core.exchange.Convertor;

public class UtilDateSqlDateConvertor extends Convertor<java.util.Date, java.sql.Date>
{
  public java.sql.Date exchange(java.util.Date o)
  {
    return new java.sql.Date(o.getTime());
  }
}