package com.personal.core.exchange.utildate;

import java.sql.Timestamp;
import java.util.Date;

import com.personal.core.exchange.Convertor;

public class UtilDateTimeConvertor extends Convertor<Date, Timestamp>
{
  public Timestamp exchange(Date o)
  {
    return new Timestamp(o.getTime());
  }
}