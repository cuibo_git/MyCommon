package com.personal.core.exchange.decimal;

import java.math.BigDecimal;

import com.personal.core.exchange.Convertor;

public class BigDecimalLongConvertor extends Convertor<BigDecimal, Long>
{
  public Long exchange(BigDecimal o)
  {
    return Long.valueOf(o.longValue());
  }
}