package com.personal.core.exchange.time;

import java.sql.Date;
import java.sql.Timestamp;

import com.personal.core.exchange.Convertor;

public class TimeSqlDateConvertor extends Convertor<Timestamp, Date>
{
  public Date exchange(Timestamp o)
  {
    return new Date(o.getTime());
  }
}