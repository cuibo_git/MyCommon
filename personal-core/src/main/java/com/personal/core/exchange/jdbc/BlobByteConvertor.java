package com.personal.core.exchange.jdbc;

import java.io.InputStream;
import java.sql.Blob;

import com.personal.core.exchange.Convertor;
import com.personal.core.utils.FileUtil;

public class BlobByteConvertor extends Convertor<Blob, byte[]>
{
  public byte[] exchange(Blob o)
  {
    InputStream stream = null;
    try
    {
      stream = o.getBinaryStream();
      byte[] data = new byte[stream.available()];
      stream.read(data);
      return data;
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    finally {
      FileUtil.release(stream);
    }
    return null;
  }
}