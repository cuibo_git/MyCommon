package com.personal.core.exchange.string;

import java.text.SimpleDateFormat;

import com.personal.core.exchange.Convertor;
import com.personal.core.exchange.exception.ExchangeException;

public class StrSqlDateConvertor extends Convertor<String, java.sql.Date>
{
  public java.sql.Date exchange(String o)
  {
    try
    {
      return new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.ms").parse(o).getTime());
    }
    catch (Exception localException)
    {
      try
      {
        return new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(o).getTime());
      }
      catch (Exception localException1)
      {
        try
        {
          return new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd").parse(o).getTime());
        }
        catch (Exception localException2)
        {
          try
          {
            return new java.sql.Date((long)Double.parseDouble(o)); } catch (Exception localException3) {  }
        }
      }
    }
    throw new ExchangeException("传入的字符[" + o + "]无法转换为java.sql.Date");
  }
}