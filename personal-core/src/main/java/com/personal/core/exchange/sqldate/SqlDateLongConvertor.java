package com.personal.core.exchange.sqldate;

import java.sql.Date;

import com.personal.core.exchange.Convertor;

public class SqlDateLongConvertor extends Convertor<Date, Long>
{
  public Long exchange(Date o)
  {
    return Long.valueOf(o.getTime());
  }
}