package com.personal.core.exchange.doubletype;

import java.sql.Timestamp;

import com.personal.core.exchange.Convertor;

public class DoubleTimeConvertor extends Convertor<Double, Timestamp>
{
  public Timestamp exchange(Double o)
  {
    return new Timestamp(o.longValue());
  }
}