package com.personal.core.exchange.string;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.personal.core.exchange.Convertor;
import com.personal.core.exchange.exception.ExchangeException;

public class StrDateConvertor extends Convertor<String, Date>
{
  public Date exchange(String o)
  {
    try
    {
      return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.ms").parse(o);
    }
    catch (Exception localException)
    {
      try
      {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(o);
      }
      catch (Exception localException1)
      {
        try
        {
          return new SimpleDateFormat("yyyy-MM-dd").parse(o);
        }
        catch (Exception localException2)
        {
          try
          {
            return new Date((long)Double.parseDouble(o)); 
        } catch (Exception localException3) {  }
        }
      }
    }
    throw new ExchangeException("传入的字符[" + o + "]无法转换为java.util.Date");
  }
}