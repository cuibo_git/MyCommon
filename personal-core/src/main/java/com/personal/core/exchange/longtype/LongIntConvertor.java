package com.personal.core.exchange.longtype;

import com.personal.core.exchange.Convertor;

public class LongIntConvertor extends Convertor<Long, Integer>
{
  public Integer exchange(Long o)
  {
    return Integer.valueOf(o.intValue());
  }
}