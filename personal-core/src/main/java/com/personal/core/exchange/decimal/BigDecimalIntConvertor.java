package com.personal.core.exchange.decimal;

import java.math.BigDecimal;

import com.personal.core.exchange.Convertor;

public class BigDecimalIntConvertor extends Convertor<BigDecimal, Integer>
{
  public Integer exchange(BigDecimal o)
  {
    return Integer.valueOf(o.intValue());
  }
}