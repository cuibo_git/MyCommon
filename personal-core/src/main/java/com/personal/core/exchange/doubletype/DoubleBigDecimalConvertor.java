package com.personal.core.exchange.doubletype;

import java.math.BigDecimal;

import com.personal.core.exchange.Convertor;

public class DoubleBigDecimalConvertor extends Convertor<Double, BigDecimal>
{
  public BigDecimal exchange(Double o)
  {
    return new BigDecimal(o.doubleValue());
  }
}