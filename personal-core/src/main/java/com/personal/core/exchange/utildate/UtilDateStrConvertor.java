package com.personal.core.exchange.utildate;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.personal.core.exchange.Convertor;
import com.personal.core.exchange.ExchangeContext;

public class UtilDateStrConvertor extends Convertor<Date, String>
{
  public String exchange(Date o)
  {
    String format = (String)ExchangeContext.getParam("DATE_FORMAT");

    if (format == null)
    {
      format = "yyyy-MM-dd HH:mm:ss";
    }

    return new SimpleDateFormat(format).format(o);
  }
}