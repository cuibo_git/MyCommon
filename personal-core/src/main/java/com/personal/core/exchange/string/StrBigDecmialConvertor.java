package com.personal.core.exchange.string;

import java.math.BigDecimal;

import com.personal.core.exchange.Convertor;
import com.personal.core.exchange.exception.ExchangeException;

public class StrBigDecmialConvertor extends Convertor<String, BigDecimal>
{
  public BigDecimal exchange(String o)
  {
    try
    {
      return new BigDecimal(o);
    }
    catch (Exception e) {
      throw new ExchangeException(e);
    }
  }
}