package com.personal.core.exchange.doubletype;

import java.sql.Date;

import com.personal.core.exchange.Convertor;

public class DoubleSqlDateConvertor extends Convertor<Double, Date>
{
  public Date exchange(Double o)
  {
    return new Date(o.longValue());
  }
}