package com.personal.core.exchange.integer;

import java.sql.Timestamp;

import com.personal.core.exchange.Convertor;

public class IntTimeConvertor extends Convertor<Integer, Timestamp>
{
  public Timestamp exchange(Integer o)
  {
    return new Timestamp(o.intValue());
  }
}