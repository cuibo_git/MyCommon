package com.personal.core.exchange.integer;

import com.personal.core.exchange.Convertor;

public class IntStrConvertor extends Convertor<Integer, String>
{
  public String exchange(Integer o)
  {
    return o.toString();
  }
}