package com.personal.core.exchange.sqldate;

import java.sql.Date;
import java.text.SimpleDateFormat;

import com.personal.core.exchange.Convertor;
import com.personal.core.exchange.ExchangeContext;

public class SqlDateStrConvertor extends Convertor<Date, String>
{
  public String exchange(Date o)
  {
    String format = (String)ExchangeContext.getParam("DATE_FORMAT");

    if (format == null)
    {
      format = "yyyy-MM-dd HH:mm:ss";
    }

    return new SimpleDateFormat(format).format(o);
  }
}