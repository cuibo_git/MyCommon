package com.personal.core.exchange.longtype;

import java.util.Date;

import com.personal.core.exchange.Convertor;

public class LongDateConvertor extends Convertor<Long, Date>
{
  public Date exchange(Long o)
  {
    return new Date(o.longValue());
  }
}