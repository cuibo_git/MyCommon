package com.personal.core.exchange.jdbc;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.SQLException;

import com.personal.core.exchange.Convertor;
import com.personal.core.exchange.ExchangeContext;
import com.personal.core.exchange.exception.ExchangeException;

public class StrBlobConvertor extends Convertor<String, Blob>
{
  public Blob exchange(String o)
  {
    Connection conn = (Connection)ExchangeContext.getParam("CONNECTION");

    if (conn == null)
    {
      throw new ExchangeException("将String类型转换为Blob类型需获取数据库连接对象，请调用ExchangeContext.setParam(\"CONNECTION\", conn)方法注入");
    }

    try
    {
      Blob blob = conn.createBlob();
      blob.setBytes(0L, o.getBytes());
      return blob;
    }
    catch (SQLException e) {
      throw new ExchangeException(e);
    }
  }
}