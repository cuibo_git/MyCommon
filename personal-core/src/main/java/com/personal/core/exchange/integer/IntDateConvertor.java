package com.personal.core.exchange.integer;

import java.util.Date;

import com.personal.core.exchange.Convertor;

public class IntDateConvertor extends Convertor<Integer, Date>
{
  public Date exchange(Integer o)
  {
    return new Date(o.intValue());
  }
}