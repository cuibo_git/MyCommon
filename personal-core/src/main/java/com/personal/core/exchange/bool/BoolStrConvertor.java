package com.personal.core.exchange.bool;

import com.personal.core.exchange.Convertor;

public class BoolStrConvertor extends Convertor<Boolean, String>
{
  public String exchange(Boolean o)
  {
    return o.toString();
  }
}