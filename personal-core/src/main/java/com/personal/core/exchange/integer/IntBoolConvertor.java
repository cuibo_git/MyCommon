package com.personal.core.exchange.integer;

import com.personal.core.exchange.Convertor;

public class IntBoolConvertor extends Convertor<Integer, Boolean>
{
  public Boolean exchange(Integer o)
  {
    return o.intValue() == 0 ? Boolean.FALSE : Boolean.TRUE;
  }
}