package com.personal.core.exchange.longtype;

import com.personal.core.exchange.Convertor;

public class LongStrConvertor extends Convertor<Long, String>
{
  public String exchange(Long o)
  {
    return o.toString();
  }
}