package com.personal.core.exchange.decimal;

import java.math.BigDecimal;

import com.personal.core.exchange.Convertor;

public class BigDecimalDoubleConvertor extends Convertor<BigDecimal, Double>
{
  public Double exchange(BigDecimal o)
  {
    return Double.valueOf(o.doubleValue());
  }
}