package com.personal.core.exchange.decimal;

import java.math.BigDecimal;

import com.personal.core.exchange.Convertor;

public class BigDecimalStrConvertor extends Convertor<BigDecimal, String>
{
  public String exchange(BigDecimal o)
  {
    return o.toEngineeringString();
  }
}