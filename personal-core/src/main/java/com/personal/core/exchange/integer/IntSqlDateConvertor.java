package com.personal.core.exchange.integer;

import java.sql.Date;

import com.personal.core.exchange.Convertor;

public class IntSqlDateConvertor extends Convertor<Integer, Date>
{
  public Date exchange(Integer o)
  {
    return new Date(o.intValue());
  }
}