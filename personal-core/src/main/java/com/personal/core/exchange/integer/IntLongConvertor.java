package com.personal.core.exchange.integer;

import com.personal.core.exchange.Convertor;

public class IntLongConvertor extends Convertor<Integer, Long>
{
  public Long exchange(Integer o)
  {
    return Long.valueOf(o.longValue());
  }
}