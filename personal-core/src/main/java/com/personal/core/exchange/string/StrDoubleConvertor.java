package com.personal.core.exchange.string;

import com.personal.core.exchange.Convertor;
import com.personal.core.exchange.exception.ExchangeException;

public class StrDoubleConvertor extends Convertor<String, Double>
{
  public Double exchange(String o)
  {
    try
    {
      return Double.valueOf(Double.parseDouble(o));
    }
    catch (Exception e) {
      throw new ExchangeException(e);
    }
  }
}