package com.personal.core.exchange.time;

import java.sql.Timestamp;
import java.util.Date;

import com.personal.core.exchange.Convertor;

public class TimeUtilDateConvertor extends Convertor<Timestamp, Date>
{
  public Date exchange(Timestamp o)
  {
    return new Date(o.getTime());
  }
}