package com.personal.core.exchange.doubletype;

import com.personal.core.exchange.Convertor;

public class DoubleLongConvertor extends Convertor<Double, Long>
{
  public Long exchange(Double o)
  {
    return Long.valueOf(o.longValue());
  }
}