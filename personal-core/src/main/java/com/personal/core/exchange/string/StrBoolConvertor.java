package com.personal.core.exchange.string;

import com.personal.core.exchange.Convertor;

public class StrBoolConvertor extends Convertor<String, Boolean>
{
  public Boolean exchange(String o)
  {
    return Boolean.valueOf(o);
  }
}