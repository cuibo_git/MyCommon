package com.personal.core.exchange.decimal;

import java.math.BigDecimal;
import java.sql.Date;

import com.personal.core.exchange.Convertor;

public class BigDecimalSqlDateConvertor extends Convertor<BigDecimal, Date>
{
  public Date exchange(BigDecimal o)
  {
    return new Date(o.longValue());
  }
}