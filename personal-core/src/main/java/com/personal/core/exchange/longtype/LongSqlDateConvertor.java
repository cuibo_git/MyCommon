package com.personal.core.exchange.longtype;

import java.sql.Date;

import com.personal.core.exchange.Convertor;

public class LongSqlDateConvertor extends Convertor<Long, Date>
{
  public Date exchange(Long o)
  {
    return new Date(o.longValue());
  }
}