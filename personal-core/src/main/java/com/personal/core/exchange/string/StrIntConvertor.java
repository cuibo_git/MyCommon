package com.personal.core.exchange.string;

import com.personal.core.exchange.Convertor;
import com.personal.core.exchange.exception.ExchangeException;

public class StrIntConvertor extends Convertor<String, Integer>
{
  public Integer exchange(String o)
  {
    try
    {
      return Integer.valueOf(Integer.parseInt(o));
    }
    catch (Exception e) {
      throw new ExchangeException(e);
    }
  }
}