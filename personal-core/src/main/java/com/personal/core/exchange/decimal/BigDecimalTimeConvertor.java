package com.personal.core.exchange.decimal;

import java.math.BigDecimal;
import java.sql.Timestamp;

import com.personal.core.exchange.Convertor;

public class BigDecimalTimeConvertor extends Convertor<BigDecimal, Timestamp>
{
  public Timestamp exchange(BigDecimal o)
  {
    return new Timestamp(o.longValue());
  }
}