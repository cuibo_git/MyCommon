package com.personal.core.exchange.jdbc;

import java.io.InputStream;
import java.sql.Clob;

import com.personal.core.exchange.Convertor;
import com.personal.core.utils.FileUtil;

public class ClobStrConvertor extends Convertor<Clob, String>
{
  public String exchange(Clob o)
  {
    InputStream stream = null;
    try
    {
      stream = o.getAsciiStream();
      byte[] data = new byte[stream.available()];
      stream.read(data);
      return new String(data);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    finally {
      FileUtil.release(stream);
    }
    return null;
  }
}