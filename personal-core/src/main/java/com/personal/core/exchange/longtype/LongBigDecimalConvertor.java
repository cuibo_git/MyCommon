package com.personal.core.exchange.longtype;

import java.math.BigDecimal;

import com.personal.core.exchange.Convertor;

public class LongBigDecimalConvertor extends Convertor<Long, BigDecimal>
{
  public BigDecimal exchange(Long o)
  {
    return new BigDecimal(o.longValue());
  }
}