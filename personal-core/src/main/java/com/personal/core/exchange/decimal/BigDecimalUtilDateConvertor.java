package com.personal.core.exchange.decimal;

import java.math.BigDecimal;
import java.util.Date;

import com.personal.core.exchange.Convertor;

public class BigDecimalUtilDateConvertor extends Convertor<BigDecimal, Date>
{
  public Date exchange(BigDecimal o)
  {
    return new Date(o.longValue());
  }
}