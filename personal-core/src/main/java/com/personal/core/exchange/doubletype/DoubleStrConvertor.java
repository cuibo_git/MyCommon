package com.personal.core.exchange.doubletype;

import com.personal.core.exchange.Convertor;

public class DoubleStrConvertor extends Convertor<Double, String>
{
  public String exchange(Double o)
  {
    return o.toString();
  }
}