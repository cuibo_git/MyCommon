package com.personal.core.exchange.jdbc;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;

import com.personal.core.exchange.Convertor;
import com.personal.core.exchange.ExchangeContext;
import com.personal.core.exchange.exception.ExchangeException;

public class StrClobConvertor extends Convertor<String, Clob>
{
  public Clob exchange(String o)
  {
    Connection conn = (Connection)ExchangeContext.getParam("CONNECTION");

    if (conn == null)
    {
      throw new ExchangeException("将String类型转换为Clob类型需获取数据库连接对象，请调用ExchangeContext.setParam(\"CONNECTION\", conn)方法注入");
    }

    try
    {
      Clob clob = conn.createClob();
      clob.setString(0L, o);
      return clob;
    }
    catch (SQLException e) {
      throw new ExchangeException(e);
    }
  }
}