package com.personal.core.exchange.sqldate;

import java.sql.Date;
import java.sql.Timestamp;

import com.personal.core.exchange.Convertor;

public class SqlDateTimeConvertor extends Convertor<Date, Timestamp>
{
  public Timestamp exchange(Date o)
  {
    return new Timestamp(o.getTime());
  }
}