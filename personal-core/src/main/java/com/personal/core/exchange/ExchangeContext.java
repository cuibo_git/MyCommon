package com.personal.core.exchange;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ExchangeContext
  implements Serializable
{
  private static final long serialVersionUID = 8856273796705160937L;
  private static ThreadLocal<ExchangeContext> local = new ThreadLocal<ExchangeContext>();

  private Map<String, Object> context = new HashMap<String, Object>();

  public static void addParam(String key, Object value)
  {
    ExchangeContext context = (ExchangeContext)local.get();
    if (context == null)
    {
      context = new ExchangeContext();
      local.set(context);
    }
    context.context.put(key, value);
  }

  public static Object getParam(String key)
  {
    ExchangeContext context = (ExchangeContext)local.get();
    if (context == null)
    {
      return null;
    }
    return context.context.get(key);
  }
}