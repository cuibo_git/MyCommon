package com.personal.core.exchange.string;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import com.personal.core.exchange.Convertor;
import com.personal.core.exchange.exception.ExchangeException;

public class StrTimeConvertor extends Convertor<String, Timestamp>
{
  public Timestamp exchange(String o)
  {
    try
    {
      return new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.ms").parse(o).getTime());
    }
    catch (Exception localException)
    {
      try
      {
        return new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(o).getTime());
      }
      catch (Exception localException1)
      {
        try
        {
          return new Timestamp(new SimpleDateFormat("yyyy-MM-dd").parse(o).getTime());
        }
        catch (Exception localException2)
        {
          try
          {
            return new Timestamp((long)Double.parseDouble(o)); } catch (Exception localException3) {  }
        }
      }
    }
    throw new ExchangeException("传入的字符[" + o + "]无法转换为java.sql.TimeStamp");
  }
}