package com.personal.core.exchange.bool;

import com.personal.core.exchange.Convertor;

public class BoolIntConvertor extends Convertor<Boolean, Integer>
{
  public Integer exchange(Boolean o)
  {
    return Integer.valueOf(o.booleanValue() ? 1 : 0);
  }
}