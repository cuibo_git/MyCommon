package com.personal.core.exchange.longtype;

import com.personal.core.exchange.Convertor;

public class LongDoubleConvertor extends Convertor<Long, Double>
{
  public Double exchange(Long o)
  {
    return Double.valueOf(o.doubleValue());
  }
}