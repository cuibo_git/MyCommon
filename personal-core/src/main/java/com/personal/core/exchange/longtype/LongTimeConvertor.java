package com.personal.core.exchange.longtype;

import java.sql.Timestamp;

import com.personal.core.exchange.Convertor;

public class LongTimeConvertor extends Convertor<Long, Timestamp>
{
  public Timestamp exchange(Long o)
  {
    return new Timestamp(o.longValue());
  }
}