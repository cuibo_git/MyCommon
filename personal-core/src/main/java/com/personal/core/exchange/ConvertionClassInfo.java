package com.personal.core.exchange;

public class ConvertionClassInfo
{
  private Class<?> orginalClass;
  private Class<?> transformClass;

  public Class<?> getOrginalClass()
  {
    return this.orginalClass;
  }

  public void setOrginalClass(Class<?> orginalClass)
  {
    this.orginalClass = orginalClass;
  }

  public Class<?> getTransformClass()
  {
    return this.transformClass;
  }

  public void setTransformClass(Class<?> transformClass)
  {
    this.transformClass = transformClass;
  }
}