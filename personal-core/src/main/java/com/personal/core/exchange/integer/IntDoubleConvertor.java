package com.personal.core.exchange.integer;

import com.personal.core.exchange.Convertor;

public class IntDoubleConvertor extends Convertor<Integer, Double>
{
  public Double exchange(Integer o)
  {
    return Double.valueOf(o.doubleValue());
  }
}