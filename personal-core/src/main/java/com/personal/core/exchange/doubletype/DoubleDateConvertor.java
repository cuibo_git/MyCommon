package com.personal.core.exchange.doubletype;

import java.util.Date;

import com.personal.core.exchange.Convertor;

public class DoubleDateConvertor extends Convertor<Double, Date>
{
  public Date exchange(Double o)
  {
    return new Date(o.longValue());
  }
}