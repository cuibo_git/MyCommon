package com.personal.core.exchange.exception;

public class ExchangeException extends RuntimeException
{
  private static final long serialVersionUID = 6290317936399219300L;
  private String message;

  public ExchangeException(Throwable throwable)
  {
    super(throwable);
    this.message = throwable.getMessage();
  }

  public ExchangeException(String message)
  {
    super(message);
    this.message = message;
  }

  public String getMessage()
  {
    return this.message;
  }
}