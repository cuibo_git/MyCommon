package com.personal.core.exchange.time;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import com.personal.core.exchange.Convertor;
import com.personal.core.exchange.ExchangeContext;

public class TimeStrConvertor extends Convertor<Timestamp, String>
{
  public String exchange(Timestamp o)
  {
    String format = (String)ExchangeContext.getParam("DATE_FORMAT");

    if (format == null)
    {
      format = "yyyy-MM-dd HH:mm:ss";
    }

    return new SimpleDateFormat(format).format(o);
  }
}