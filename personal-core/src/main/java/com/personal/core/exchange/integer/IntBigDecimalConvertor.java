package com.personal.core.exchange.integer;

import java.math.BigDecimal;

import com.personal.core.exchange.Convertor;

public class IntBigDecimalConvertor extends Convertor<Integer, BigDecimal>
{
  public BigDecimal exchange(Integer o)
  {
    return new BigDecimal(o.intValue());
  }
}