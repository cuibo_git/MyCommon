package com.personal.core.exchange.time;

import java.sql.Timestamp;

import com.personal.core.exchange.Convertor;

public class TimeLongConvertor extends Convertor<Timestamp, Long>
{
  public Long exchange(Timestamp o)
  {
    return Long.valueOf(o.getTime());
  }
}