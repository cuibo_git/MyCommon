package com.personal.core.exchange.doubletype;

import com.personal.core.exchange.Convertor;

public class DoubleIntConvertor extends Convertor<Double, Integer>
{
  public Integer exchange(Double o)
  {
    return Integer.valueOf(o.intValue());
  }
}