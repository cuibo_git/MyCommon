package com.personal.core.exchange.string;

import com.personal.core.exchange.Convertor;
import com.personal.core.exchange.exception.ExchangeException;

public class StrLongConvertor extends Convertor<String, Long>
{
  public Long exchange(String o)
  {
    try
    {
      return Long.valueOf(Long.parseLong(o));
    }
    catch (Exception e) {
      throw new ExchangeException(e);
    }
  }
}