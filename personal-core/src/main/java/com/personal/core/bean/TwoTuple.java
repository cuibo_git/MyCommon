package com.personal.core.bean;

import java.io.Serializable;

/**
 * 二元组
 * @author cuibo
 * @param <A>
 * @param <B>
 */
public class TwoTuple<A,B> extends OneTuple<A> implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private B b;

    public TwoTuple()
    {
        super();
    }

    public TwoTuple(A a, B b)
    {
        super(a);
        this.b = b;
    }

    public B getB()
    {
        return b;
    }

    public void setB(B b)
    {
        this.b = b;
    }
}
