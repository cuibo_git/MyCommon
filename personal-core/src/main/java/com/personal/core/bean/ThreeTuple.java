package com.personal.core.bean;

/**
 * 三元组
 * @author cuibo
 * @param <A>
 * @param <B>
 * @param <C>
 */
public class ThreeTuple<A,B,C> extends TwoTuple<A, B>
{
    
    public ThreeTuple()
    {
        super();
    }

    public ThreeTuple(A a, B b, C c)
    {
        super(a, b);
        this.c = c;
    }
    
    private C c;
    
    public C getC()
    {
        return c;
    }

    public void setC(C c)
    {
        this.c = c;
    }
    
}
