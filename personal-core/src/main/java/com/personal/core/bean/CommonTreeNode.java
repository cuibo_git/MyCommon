package com.personal.core.bean;

import java.util.List;

import com.personal.core.port.HaveDeepAndChildren;
import com.personal.core.utils.CoreUtil;

/**
 * 通用的树节点
 * @author cuibo
 *
 * @param <T>
 */
public class CommonTreeNode<T> implements HaveDeepAndChildren
{

    private T source;

    private CommonTreeNode<T> parent;

    /** 子节点集合 */
    private List<CommonTreeNode<T>> children;

    /** 节点深度 */
    private int deepLength;

    public CommonTreeNode()
    {
        super();
    }

    public CommonTreeNode(T source)
    {
        super();
        this.source = source;
    }

    public T getSource()
    {
        return source;
    }

    public void setSource(T source)
    {
        this.source = source;
    }

    @Override
    public HaveDeepAndChildren getParent()
    {
        return parent;
    }

    @Override
    public List<? extends HaveDeepAndChildren> getChildren()
    {
        return children;
    }

    @Override
    public void setDeepLength(int deepLength)
    {
        this.deepLength = deepLength;
    }

    @Override
    public int getDeepLength()
    {
        return deepLength;
    }

    @Override
    public boolean isLeaf()
    {
        return CoreUtil.isEmpty(getChildren());
    }

}
