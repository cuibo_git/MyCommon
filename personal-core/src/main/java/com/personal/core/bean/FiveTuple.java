package com.personal.core.bean;

/**
 * 五元组
 * @author cuibo
 * @param <A>
 * @param <B>
 * @param <C>
 * @param <D>
 * @param <E>
 */
public class FiveTuple<A,B,C,D,E> extends FourTuple<A, B, C, D>
{

    public FiveTuple()
    {
        super();
    }

    public FiveTuple(A a, B b, C c, D d, E e)
    {
        super(a, b, c, d);
        this.e = e;
    }

    private E e;

    public E getE()
    {
        return e;
    }

    public void setE(E e)
    {
        this.e = e;
    }

    
    
}
