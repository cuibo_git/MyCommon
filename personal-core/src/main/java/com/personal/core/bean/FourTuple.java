package com.personal.core.bean;

/**
 * 四元组
 * @author cuibo
 * @param <A>
 * @param <B>
 * @param <C>
 * @param <D>
 */
public class FourTuple<A,B,C,D> extends ThreeTuple<A, B, C>
{

    public FourTuple()
    {
        super();
    }

    public FourTuple(A a, B b, C c, D d)
    {
        super(a, b, c);
        this.d = d;
    }

    private D d;

    public D getD()
    {
        return d;
    }

    public void setD(D d)
    {
        this.d = d;
    }
    
    
}
