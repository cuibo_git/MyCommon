package com.personal.core.bean;

import java.io.Serializable;

/**
 * @author cuibo
 * @param <A>
 */
public class OneTuple<A> implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = -6227123599857553586L;
    private A a;
    
    public OneTuple()
    {
        super();
    }

    public OneTuple(A a)
    {
        super();
        this.a = a;
    }

    public A getA()
    {
        return a;
    }

    public void setA(A a)
    {
        this.a = a;
    }

}
