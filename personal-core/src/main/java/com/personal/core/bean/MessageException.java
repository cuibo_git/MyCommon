package com.personal.core.bean;

/**
 * 消息异常
 * 使用规范：只限用于提示型的消息抛出，在生产环境会抛弃异常栈信息
 * @author cuibo
 *
 */
public class MessageException extends Exception
{

    /**
     * 
     */
    private static final long serialVersionUID = 4225795602734491381L;

    public static void main(String[] args)
    {
        
    }
    
    public MessageException()
    {
        super();
    }

    public MessageException(String arg0, Throwable arg1)
    {
        super(arg0, arg1);
    }

    public MessageException(String arg0)
    {
        super(arg0);
    }

    public MessageException(Throwable arg0)
    {
        super(arg0);
    }
    
    private static boolean fillInStackTrace;
    
    static
    {
        try
        {
            fillInStackTrace = "Development".equals(System.getProperties().get("com.booway.zj.common.pojo.messageexception.fillInStackTrace"));
        } catch (Throwable e)
        {
        }
    }

    @Override
    public synchronized Throwable fillInStackTrace()
    {
        if (fillInStackTrace)
        {
            return super.fillInStackTrace();
        }
        return this;
    }
    

}
