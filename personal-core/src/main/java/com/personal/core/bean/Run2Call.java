package com.personal.core.bean;

import java.util.concurrent.Callable;

/**
 * Runable转Callable
 * @author cuibo
 *
 */
public class Run2Call<T> implements Callable<T>
{
    private Runnable run;
    
    public Run2Call(Runnable run)
    {
        super();
        this.run = run;
    }

    @Override
    public T call()
    {
        run.run();
        return null;
    }
}
