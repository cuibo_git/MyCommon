package com.personal.core.bean;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.personal.core.port.IFile;
import com.personal.core.utils.FileUtil;

/**
 * 内存中的文件数据
 * @author cuibo
 *
 */
public class FileInfo implements IFile
{

    private String fileName;
    
    private InputStream streamData;
    
    public FileInfo()
    {
        super();
    }

    public FileInfo(String fileName, byte[] byteData)
    {
        super();
        this.fileName = fileName;
        this.streamData = new ByteArrayInputStream(byteData);
    }
    
    public FileInfo(String fileName, InputStream streamData) throws IOException
    {
        super();
        this.fileName = fileName;
        this.streamData = streamData;
    }

    public String getFileName()
    {
        return fileName;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public InputStream getStreamData() throws IOException
    {
        return streamData;
    }
    
    public void setStreamData(InputStream streamData)
    {
        this.streamData = streamData;
    }

    public byte[] getByteData() throws IOException
    {
        return FileUtil.readInputStream(getStreamData());
    }

    public long getSize() throws IOException
    {
        InputStream stream = getStreamData();
        return stream == null ? -1L : stream.available();
    }
    
    @Override
	protected void finalize() throws Throwable 
	{
        if (streamData != null)
        {
            FileUtil.release(streamData);
        }
		super.finalize();
	}

    @Override
    public void relase() throws Exception
    {
        FileUtil.release(streamData);
    }
    
}
