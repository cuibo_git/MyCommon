package com.personal.core.bean;

/**
 * 计数器
 * 非线程安全
 * @author cuibo
 *
 */
public class CountEntity
{

    /** 用于计数 */
    private int count;

    public CountEntity()
    {

    }

    public CountEntity(int count)
    {
        this.count = count;
    }

    /**
     * 加一
     */
    public void add()
    {
        count += 1;
    }
    
    /**
     * 减一
     */
    public void sub()
    {
        count += 1;
    }


    /**
     * 获取计数器结果
     * @return
     */
    public int getCount()
    {
        return count;
    }
    
    /**
     * 获取计数器结果然后加1
     * @return
     */
    public int getCountAndAdd()
    {
        int result = count;
        add();
        return result;
    }
    
    /**
     * 加1然后获取计数器结果
     * @return
     */
    public int addAndGetCount()
    {
        add();
        return count;
    }

    public void setCount(int count)
    {
        this.count = count;
    }

}
