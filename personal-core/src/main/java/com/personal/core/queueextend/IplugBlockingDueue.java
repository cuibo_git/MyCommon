package com.personal.core.queueextend;

import java.util.concurrent.BlockingDeque;
import java.util.concurrent.TimeUnit;

/**
 * 可插队的阻塞队列
 * @author cuibo
 *
 */
public interface IplugBlockingDueue<T> extends BlockingDeque<T>
{
    /**
     * 指定位置添加(抛异常)
     * @param index
     * @param t
     * @return
     */
    public boolean add(int index, T t);
    
    /**
     * 指定位置添加(特殊值)
     * @param index
     * @param t
     * @return
     */
    public boolean offer(int index, T t);
    
    /**
     * 指定位置添加
     * @param index
     * @param t
     * @throws InterruptedException
     */
    public void put(int index, T t) throws InterruptedException;
          
    /**
     * 指定位置添加
     * @param index
     * @param t
     * @param paramLong
     * @param paramTimeUnit
     * @return
     * @throws InterruptedException
     */
    public abstract boolean offer(int index, T t, long paramLong, TimeUnit paramTimeUnit)
            throws InterruptedException;
    
    /**
     * coordinate位置之前添加(抛异常)
     * @param coordinate
     * @param t
     * @return
     */
    public boolean addBefore(T coordinate, T t);
    
    /**
     * coordinate位置之前添加(特殊值)
     * @param coordinate
     * @param t
     * @return
     */
    public boolean offerBefore(T coordinate, T t);
    
    /**
     * coordinate位置之前添加
     * @param coordinate
     * @param t
     * @throws InterruptedException
     */
    public void putBefore(T coordinate, T t) throws InterruptedException;
          
    /**
     * coordinate位置之前添加
     * @param coordinate
     * @param t
     * @param paramLong
     * @param paramTimeUnit
     * @return
     * @throws InterruptedException
     */
    public abstract boolean offerBefore(T coordinate, T t, long paramLong, TimeUnit paramTimeUnit)
            throws InterruptedException;
    
    /**
     * coordinate位置之后添加(抛异常)
     * @param coordinate
     * @param t
     * @return
     */
    public boolean addAfter(T coordinate, T t);
    
    /**
     * coordinate位置之后添加(特殊值)
     * @param coordinate
     * @param t
     * @return
     */
    public boolean offerAfter(T coordinate, T t);
    
    /**
     * coordinate位置之后添加
     * @param coordinate
     * @param t
     * @throws InterruptedException
     */
    public void putAfter(T coordinate, T t) throws InterruptedException;
          
    /**
     * coordinate位置之后添加
     * @param coordinate
     * @param t
     * @param paramLong
     * @param paramTimeUnit
     * @return
     * @throws InterruptedException
     */
    public abstract boolean offerAfter(T coordinate, T t, long paramLong, TimeUnit paramTimeUnit)
            throws InterruptedException;
    
    /**
     * 获取指定位置
     * @param index
     * @return
     */
    public T get(int index);
    
    /**
     * 指定位置移除(抛异常)
     * @param index
     * @return
     */
    public T remove(int index);
    
    /**
     * 指定位置移除(特殊值)
     * @param index
     * @return
     */
    public T poll(int index);
    
    /**
     * 指定位置移除(阻塞)
     * @param index
     * @return
     * @throws InterruptedException
     */
    public T take(int index) throws InterruptedException;
    
    /**
     * 指定位置移除(超时阻塞)
     * @param index
     * @param time
     * @param unit
     * @return
     * @throws InterruptedException
     */
    public T take(int index, long time, TimeUnit unit) throws InterruptedException;
    
    /**
     * 查找位置索引
     * @param t
     * @return
     */
    public int indexOf(T t);
    
    /**
     * 位置交换
     * @param paramOne
     * @param paramTwo
     * @return
     */
    public boolean exchange(T paramOne, T paramTwo);
    
    /**
     * 位置交换
     * @param indexOne
     * @param indexTwo
     * @return
     */
    public boolean exchange(int indexOne, int indexTwo);
    
    /**
     * 将t移动到coordinate前面
     * @param t
     * @param coordinate
     * @return
     */
    public boolean moveBefore(T t, T coordinate);
    
    /**
     * 将t移动到coordinate后面
     * @param t
     * @param coordinate
     * @return
     */
    public boolean moveAfter(T t, T coordinate);
}
