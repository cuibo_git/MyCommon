package com.personal.core.htmldata;

import com.personal.core.enums.HtmlTypeEnum;


/**
 * 输出框不规则单元数据
 * @author cuibo
 *
 */
public class OutputIrregularData extends HtmlIrregularData
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /** 其他Html实现转成Output保存原来的对象信息  */
    private HtmlIrregularData originalData;

    @Override
    public String getTdHtml(boolean withTitle)
    {
        StringBuilder result = new StringBuilder();
        result.append(getTdStartHtml(withTitle)).append(getData()).append("</td>");
        return result.toString();
    }

    @Override
    public HtmlTypeEnum getHtmlType()
    {
        return HtmlTypeEnum.OUTPUT;
    }
    
    @Override
    public OutputIrregularData returnEdit()
    {
        this.setOriginalData(this);
        return this;
    }
    
    @Override
    public HtmlIrregularData toEdit()
    {
        return this.getOriginalData() == null ? this : this.getOriginalData();
    }
    
    @Override
    public String getParamKey()
    {
        // 输出框没有form表单的域
        return null;
    }

    /**
     * 切换至非编辑状态
     * @return
     */
    public HtmlIrregularData getOriginalData()
    {
        if (originalData == null)
        {
            return this;
        }
        return originalData;
    }

    public void setOriginalData(HtmlIrregularData originalData)
    {
        this.originalData = originalData;
    }

    @Override
    public boolean isFormArea()
    {
        return false;
    }

}
