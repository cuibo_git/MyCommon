package com.personal.core.htmldata;

import com.personal.core.enums.HtmlTypeEnum;


/**
 * 输入域不规则单元数据
 * @author cuibo
 *
 */
public class InputTextIrregularData extends HtmlIrregularData
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Override
    public String getTdHtml(boolean withTitle)
    {
        StringBuilder result = new StringBuilder();
        result.append(getTdStartHtml(withTitle));
        // 拼接输入框
        result.append("<textarea class=\"irregulartextarea\" ").append("name=\"").append(getParamKey()).append("\" > ").append(getData()).append("</textarea>");
        result.append("</td>");
        return result.toString();
    }
    
    @Override
    public HtmlTypeEnum getHtmlType()
    {
        return HtmlTypeEnum.INPUTTEXT;
    }
    
    @Override
    public OutputIrregularData returnEdit()
    {
        OutputIrregularData result = new OutputIrregularData();
        result.copyProperties(this);
        result.setOriginalData(this);
        return result;
    }

    @Override
    public boolean isFormArea()
    {
        return true;
    }
    
}
