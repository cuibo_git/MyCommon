package com.personal.core.htmldata;

import com.personal.core.data.IrregularData;
import com.personal.core.enums.HtmlTypeEnum;
import com.personal.core.utils.CoreUtil;


/**
 * Html 不规则单元数据
 * @author cuibo
 *
 */
public abstract class HtmlIrregularData extends IrregularData
{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** 宽度 */
    private int width;
    
    /** 高度 */
    private int height;
    
    /** 水平居中样式 */
    private String align;
    
    /** 垂直居中样式 */
    private String valign;
    
    /** 单元格样式 */
    private String style = "";
    
    /** 每个单元格的错误提示 */
    private String rederror;
    
    /** 表单域的key */
    private String paramKey;
    
    /** 获取tdHtml */
    public abstract String getTdHtml(boolean withTitle);
    
    /** 获取其类型 */
    public abstract HtmlTypeEnum getHtmlType();
    
    /** 切换至编辑状态 */
    public HtmlIrregularData toEdit()
    {
        return this;
    }
    
    /** 切换至非编辑状态 */
    public abstract OutputIrregularData returnEdit();
    
    /** 是否是表单域 */
    public abstract boolean isFormArea();
    
    /** 获取表单参数的key */
    public String getParamKey()
    {
        if (!CoreUtil.isEmpty(paramKey))
        {
            return paramKey;
        }
        // 使用行号+列号：获取其key
        return "irregular" + getRowIndex() + "," + getColIndex();
    }
    
    /**
     * 从source中拷贝属性
     * @param source
     */
    public void copyProperties(HtmlIrregularData source)
    {
        this.setRowIndex(source.getRowIndex());
        this.setColIndex(source.getColIndex());
        this.setData(source.getData());
        this.setRowSpan(source.getRowSpan());
        this.setColSpan(source.getColSpan());
        this.setWidth(source.getWidth());
        this.setHeight(source.getHeight());
        this.setAlign(source.getAlign());
        this.setValign(source.getValign());
        this.setStyle(source.getStyle());
        this.setRederror(source.getRederror());
    }
    
    /**
     * 获取td的开始节点：<td xxxx>
     * @return
     */
    protected String getTdStartHtml(boolean withTitle)
    {
        StringBuilder result = new StringBuilder();
        // 也要加上行号，列号信息
        result.append("<td ").append("rowIndex=\"").append(getRowIndex()).append("\" colIndex=\"").append(getColIndex())
            .append("\" width=\"").append(getWidth()).append("px\"")
            .append(" height=\"").append(getHeight()).append("px\"").append(" rowspan=\"")
            .append(getRowSpan()).append("\" colspan=\"").append(getColSpan()).append("\"")
            .append(" align=\"").append(getAlign()).append("\" valign=\"").append(getValign()).append("\"");
        // 加上class="datatd_0_1" 为了引用一致
        result.append(" class = \"datatd_").append(getRowIndex()).append("_").append(getColIndex()).append("\" ");
        // 内容
        if (withTitle)
        {
            result.append(" title=\"").append(getData()).append("\"");
        }
        if (!CoreUtil.isEmpty(rederror))
        {
            result.append(" rederror=\"").append(getRederror()).append("\"");
        }
        result.append(" style=\"").append(getStyle()).append("\" >");
        return result.toString();
    }
    

    public String getStyle()
    {
        return style;
    }

    public void setStyle(String style)
    {
        this.style = style;
    }

    public int getWidth()
    {
        return width;
    }

    public void setWidth(int width)
    {
        this.width = width;
    }

    public int getHeight()
    {
        return height;
    }

    public void setHeight(int height)
    {
        this.height = height;
    }

    public String getAlign()
    {
        return align;
    }

    public void setAlign(String align)
    {
        this.align = align;
    }

    public String getValign()
    {
        return valign;
    }

    public void setValign(String valign)
    {
        this.valign = valign;
    }

    public String getRederror()
    {
        return rederror;
    }

    public void setRederror(String rederror)
    {
        this.rederror = rederror;
    }

    public void setParamKey(String paramKey)
    {
        this.paramKey = paramKey;
    }
    
}
