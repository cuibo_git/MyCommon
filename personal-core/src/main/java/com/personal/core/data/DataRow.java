package com.personal.core.data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 数据表行管理
 * 
 * @author shijiaokun
 */
public class DataRow implements Serializable {

    private static final long   serialVersionUID = -4964515718789457729L;

    protected DataTable         dataTable;

    private Map<String, Object> itemMap;

    protected DataRow(){
        this.itemMap = new HashMap<String, Object>();
    }

    public DataTable getDataTable() {
        return dataTable;
    }

    /**
     * 获取列值
     * 
     * @param columnName 列名
     * @return
     */
    public Object getValue(String columnName) {
        // 查找列
        return itemMap.get(columnName);
    }

    /**
     * 设置列值
     * 
     * @param columnName 列名
     * @param value 列值
     */
    public void setValue(String columnName, Object value) {
        // 设置值
        itemMap.put(columnName, value);
    }

    /**
     * 获取列值
     * 
     * @param columnName 列名
     * @return
     */
    public Object getValue(DataColumn column) {
        if (column == null) {
            return null;
        }
        // 查找列
        return itemMap.get(column.getColumnName());
    }

    /**
     * 设置列值
     * 
     * @param columnName 列名
     * @param value 列值
     */
    public void setValue(DataColumn column, Object value) {
        if (column == null) {
            return;
        }
        // 设置值
        itemMap.put(column.getColumnName(), value);
    }

    /**
     * 获取行数据集合
     * 
     * @return 行数据的副本
     */
    public Map<String, Object> getItemMap() {
        if (itemMap == null) {
            return new HashMap<String, Object>(0);
        }
        return itemMap;
    }

}
