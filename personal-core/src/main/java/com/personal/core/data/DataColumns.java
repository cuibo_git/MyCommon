package com.personal.core.data;

import java.util.ArrayList;

/**
 * 数据表列集合
 * 
 * @author cuibo
 */
public class DataColumns extends ArrayList<DataColumn> {

    private static final long serialVersionUID = -7479928637350982636L;

    DataTable                 dataTable;

    public DataColumns(){
        super();
    }

    public DataColumns(DataTable dataTable){
        super();
        this.dataTable = dataTable;
    }

}
