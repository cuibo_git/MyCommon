package com.personal.core.data;

import java.io.Serializable;
import java.util.List;

/**
 * 分页数据
 * @author qq
 *
 */
public class Page<T> implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int start;
	
	private int limit;
	
	private long totalCount;
	
	private List<T> data;

	public List<T> getData()
	{
		return data;
	}

	public void setData(List<T> data)
	{
		this.data = data;
	}

	public int getStart()
	{
		return start;
	}

	public void setStart(int start)
	{
		this.start = start;
	}

	public int getLimit()
	{
		return limit;
	}

	public void setLimit(int limit)
	{
		this.limit = limit;
	}

	public long getTotalCount()
	{
		return totalCount;
	}

	public void setTotalCount(long totalCount)
	{
		this.totalCount = totalCount;
	}
	
}
