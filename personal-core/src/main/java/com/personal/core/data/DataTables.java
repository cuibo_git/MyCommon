package com.personal.core.data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 数据集对象 description 存储数据的集合，包含多个DataTable
 * 
 * @author dengbo createTime 2014-3-28
 */
public class DataTables extends ArrayList<DataTable> implements Serializable {

    private static final long serialVersionUID = -7901753771591459267L;

    private Object            tag              = null;

    public boolean addTable(DataTable dataTable) {
        // 如果已经存在同名的表，删除原来同名的表
        if (this.contains(dataTable.getTableName())) {
            DataTable table = this.getDataTable(dataTable.getTableName());

            if (table != null) {
                this.removeTable(table);
            }
        }
        return this.add(dataTable);
    }

    public boolean removeTable(DataTable dataTable) {
        return this.remove(dataTable);
    }

    public boolean contians(String tableName) {
        return getDataTable(tableName) != null;
    }

    @Override
    public void clear() {
        super.clear();
    }

    public DataTable getDataTable(String tableName) {

        for (DataTable table : this) {
            if (table.getTableName().equals(tableName)) {
                return table;
            }
        }
        return null;
    }

    public Object getTag() {
        return tag;
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }
}
