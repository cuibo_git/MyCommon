package com.personal.core.data;

import java.util.ArrayList;

/**
 * 数据表行集合
 * 
 * @author shijiaokun
 */
public class DataRows extends ArrayList<DataRow> {

    private static final long serialVersionUID = -6306029891822281003L;

    DataTable                 dataTable;

    public DataRows(){
    }

    public DataRows(DataTable dataTable){
        this.dataTable = dataTable;
    }

}
