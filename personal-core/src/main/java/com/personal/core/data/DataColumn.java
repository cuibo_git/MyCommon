package com.personal.core.data;

import java.io.Serializable;
import java.sql.Types;

/**
 * 数据表列管理
 * 
 * @author cuibo
 */
public class DataColumn implements Serializable {

    private static final long serialVersionUID = -5513634240671235859L;

    /** 名称 */
    private String            columnName;

    /** 标识 */
    private String            columnLable;

    /** 数据类型 */
    private String            dataType;

    /** 宽度 */
    private int               width            = -1;

    /** 水平样式 */
    private String            align;

    /** sql数据类型 */
    private int               sqlType          = Types.NULL;

    /** 是否显示 */
    private boolean           display          = Boolean.TRUE;

    /** 小数位 */
    private int               dotNum           = -1;

    /** 单位 */
    private String            fieldUnit;

    private Object            tag;

    protected DataTable       dataTable;

    public DataColumn(){
    }

    public DataColumn(String columnName){
        this.columnName = columnName;
        this.columnLable = columnName;
    }

    public DataColumn(String columnName, String columnLable){
        this.columnName = columnName;
        this.columnLable = columnLable;
    }

    public String getColumnName() {
        return columnName;
    }

    public String getColumnLable() {
        return columnLable;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public void setColumnLable(String columnLable) {
        this.columnLable = columnLable;
    }

    public DataTable getDataTable() {
        return dataTable;
    }

    public Object getTag() {
        return tag;
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getAlign() {
        return align;
    }

    public void setAlign(String align) {
        this.align = align;
    }

    public int getSqlType() {
        return sqlType;
    }

    public void setSqlType(int sqlType) {
        this.sqlType = sqlType;
    }

    public boolean isDisplay() {
        return display;
    }

    public void setDisplay(boolean display) {
        this.display = display;
    }

    public void setDataTable(DataTable dataTable) {
        this.dataTable = dataTable;
    }

    public int getDotNum() {
        return dotNum;
    }

    public void setDotNum(int dotNum) {
        this.dotNum = dotNum;
    }

    public String getFieldUnit() {
        return fieldUnit;
    }

    public void setFieldUnit(String fieldUnit) {
        this.fieldUnit = fieldUnit;
    }

}
