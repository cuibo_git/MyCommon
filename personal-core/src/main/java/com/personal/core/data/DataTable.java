package com.personal.core.data;

import java.io.Serializable;

/**
 * 内存数据表对象
 * 
 * @author shijiaokun
 */
public class DataTable implements Serializable, Cloneable {

    private static final long serialVersionUID = -6090445434572249084L;

    private String            tableName;
    private DataRows          rows;
    private DataColumns       columns;
    private Object            tag              = null;

    /** 左中右页眉 */
    private String            leftHeader;
    private String            middleHeader;
    private String            rightHeader;

    /** 标题 */
    private String            title;

    public DataTable(){
        // 初始化行列
        rows = new DataRows(this);
        columns = new DataColumns(this);
    }

    public DataTable(String tableName){
        this.tableName = tableName;

        // 初始化行列
        rows = new DataRows(this);
        columns = new DataColumns(this);
    }

    /**
     * 数据表新数据行（未添加到行集合中）
     * 
     * @return
     */
    public DataRow newRow() {
        DataRow row = new DataRow();
        row.dataTable = this;
        return row;
    }

    /**
     * 数据表新数据行（并添加到行集合中）
     * 
     * @return
     */
    public DataRow addNewRow() {
        DataRow row = new DataRow();
        row.dataTable = this;
        if (rows.add(row)) {
            return row;
        }
        return null;
    }

    /**
     * 获取数据表名
     * 
     * @return
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * 设置内存表名
     * 
     * @param tableName
     */
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    /**
     * 获取数据行集合
     * 
     * @return
     */
    public DataRows getRows() {
        return rows;
    }

    /**
     * 获取数据列集合（列名大小写不敏感）
     * 
     * @return
     */
    public DataColumns getColumns() {
        return columns;
    }

    /**
     * 根据列名获取列对象
     * 
     * @param columnName 列名
     * @return
     */
    public DataColumn getColumn(String columnName) {
        for (DataColumn column : columns) {
            if (column.getColumnName().equals(columnName)) {
                return column;
            }
        }
        return null;
    }

    /**
     * 根据列名获取列所索引
     * 
     * @param columnName 列名
     * @return 没有改列，返回-1，否则返回列索引
     */
    public int indexOf(String columnName) {
        for (DataColumn column : columns) {
            if (column.getColumnName().equals(columnName)) {
                return columns.indexOf(column);
            }
        }

        return -1;
    }

    /**
     * 添加新列
     * 
     * @param columnName
     * @return
     */
    public DataColumn addNewColumn(String columnName) {
        // 是否有列
        DataColumn dataColumn = this.getColumn(columnName);
        if (dataColumn != null) {
            return dataColumn;
        }
        dataColumn = new DataColumn(columnName);
        // 添加列
        if (!columns.add(dataColumn)) {
            return null;
        }
        return dataColumn;
    }

    /**
     * 添加新列
     * 
     * @param columnName
     * @param columnLable
     * @return
     */
    public DataColumn addNewColumn(String columnName, String columnLable) {
        // 是否有列
        DataColumn dataColumn = this.getColumn(columnName);
        if (dataColumn != null) {
            return dataColumn;
        }
        dataColumn = new DataColumn(columnName, columnLable);
        // 添加列
        if (!columns.add(dataColumn)) {
            return null;
        }
        return dataColumn;
    }

    /**
     * 移除列
     * 
     * @param columnName 列名称
     * @return
     */
    public boolean removeColumn(String columnName) {
        // 是否有列
        DataColumn dataColumn = this.getColumn(columnName);
        if (dataColumn == null) {
            return true;
        }
        if (!columns.remove(dataColumn)) {
            return false;
        }
        return true;
    }

    public Object getTag() {
        return tag;
    }

    public void setTag(Object tag) {
        this.tag = tag;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getLeftHeader() {
        return leftHeader;
    }

    public void setLeftHeader(String leftHeader) {
        this.leftHeader = leftHeader;
    }

    public String getMiddleHeader() {
        return middleHeader;
    }

    public void setMiddleHeader(String middleHeader) {
        this.middleHeader = middleHeader;
    }

    public String getRightHeader() {
        return rightHeader;
    }

    public void setRightHeader(String rightHeader) {
        this.rightHeader = rightHeader;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 初始化不规则表的所有列
     */
    public void initIrregularDataColumns() {
        columns.clear();
        columns.add(new DataColumn(IrregularData.DATA));
        columns.add(new DataColumn(IrregularData.ROWINDEX));
        columns.add(new DataColumn(IrregularData.COLINDEX));
        columns.add(new DataColumn(IrregularData.ROWSPAN));
        columns.add(new DataColumn(IrregularData.COLSPAN));
    }

}
