package com.personal.core.data;

import java.io.Serializable;

/**
 * 不规则表数据
 * 
 * @author cuibo
 */
public class IrregularData implements Serializable {

    /**
     * 
     */
    private static final long  serialVersionUID = 1L;

    /** 行号 */
    protected int              rowIndex;
    public static final String ROWINDEX         = "ROWINDEX";

    /** 列号 */
    protected int              colIndex;
    public static final String COLINDEX         = "COLINDEX";

    /** 数据 */
    protected String           data;
    public static final String DATA             = "DATA";

    /** 跨行 */
    protected int              rowSpan;
    public static final String ROWSPAN          = "ROWSPAN";

    /** 跨列 */
    protected int              colSpan;
    public static final String COLSPAN          = "COLSPAN";

    public IrregularData(){

    }

    public int getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public int getColIndex() {
        return colIndex;
    }

    public void setColIndex(int colIndex) {
        this.colIndex = colIndex;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getRowSpan() {
        return rowSpan;
    }

    public void setRowSpan(int rowSpan) {
        this.rowSpan = rowSpan;
    }

    public int getColSpan() {
        return colSpan;
    }

    public void setColSpan(int colSpan) {
        this.colSpan = colSpan;
    }

}
