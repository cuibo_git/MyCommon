package com.personal.core.utils;

import java.text.DecimalFormat;

/**
 * 数字格式化
 * @author cuibo
 *
 */
public class DecimalFormatHolder
{
    private static final String ZEROCARRY = "0";
    private static final String ONECARRY = "0.#";
    private static final String TWOCARRY = "0.##";
    private static final String THREECARRY = "0.###";
    private static final String FOURCARRY = "0.####";
    private static final String FIVECARRY = "0.#####";
    private static final String SIXCARRY = "0.######";
    private static final String SEVENCARRY = "0.#######";
    private static final String EIGHTCARRY = "0.########";
    
    private static final String ZEROCARRYE = "0";
    private static final String ONECARRYE = "0.0";
    private static final String TWOCARRYE = "0.00";
    private static final String THREECARRYE = "0.000";
    private static final String FOURCARRYE = "0.0000";
    private static final String FIVECARRYE = "0.00000";
    private static final String SIXCARRYE = "0.000000";
    private static final String SEVENCARRYE = "0.0000000";
    private static final String EIGHTCARRYE = "0.00000000";
    
    public static DecimalFormat getDecimalFormat(int carryNum)
    {
        DecimalFormat result = null;
        switch (carryNum)
        {
        case 0:
            result = ZEROLOCAL.get();
            break;
        case 1:
            result = ONELOCAL.get();
            break;
        case 2:
            result = TWOLOCAL.get();
            break;
        case 3:
            result = THREELOCAL.get();
            break;
        case 4:
            result = FOURLOCAL.get();
            break;
        case 5:
            result = FIVELOCAL.get();
            break;
        case 6:
            result = SIXLOCAL.get();
            break;
        case 7:
            result = SEVENLOCAL.get();
            break;
        case 8:
            result = EIGHTLOCAL.get();
            break;
        default:
            result = EIGHTLOCAL.get();
            break;
        }
        return result;
    }
    
    public static DecimalFormat getDecimalFormatXsw(int carryNum)
    {
        DecimalFormat result = null;
        switch (carryNum)
        {
        case 0:
            result = ZEROLOCALE.get();
            break;
        case 1:
            result = ONELOCALE.get();
            break;
        case 2:
            result = TWOLOCALE.get();
            break;
        case 3:
            result = THREELOCALE.get();
            break;
        case 4:
            result = FOURLOCALE.get();
            break;
        case 5:
            result = FIVELOCALE.get();
            break;
        case 6:
            result = SIXLOCALE.get();
            break;
        case 7:
            result = SEVENLOCALE.get();
            break;
        case 8:
            result = EIGHTLOCALE.get();
            break;
        default:
            result = EIGHTLOCALE.get();
            break;
        }
        return result;
    }
    
    // 1.8 使用lambda
    private static final ThreadLocal<DecimalFormat> ZEROLOCAL = new ThreadLocal<DecimalFormat>()
    {
        @Override
        protected DecimalFormat initialValue()
        {
            DecimalFormat decFormat = new DecimalFormat();
            decFormat.setMinimumFractionDigits(0);
            decFormat.setMaximumFractionDigits(0);
            // 设置格式化字符串
            decFormat.applyPattern(ZEROCARRY);
            return decFormat;
        }
    };
    // 1.8 使用lambda
    private static final ThreadLocal<DecimalFormat> ONELOCAL = new ThreadLocal<DecimalFormat>()
    {
        @Override
        protected DecimalFormat initialValue()
        {
            DecimalFormat decFormat = new DecimalFormat();
            decFormat.setMinimumFractionDigits(1);
            decFormat.setMaximumFractionDigits(1);
            // 设置格式化字符串
            decFormat.applyPattern(ONECARRY);
            return decFormat;
        }
    };
    // 1.8 使用lambda
    private static final ThreadLocal<DecimalFormat> TWOLOCAL = new ThreadLocal<DecimalFormat>()
    {
        @Override
        protected DecimalFormat initialValue()
        {
            DecimalFormat decFormat = new DecimalFormat();
            decFormat.setMinimumFractionDigits(2);
            decFormat.setMaximumFractionDigits(2);
            // 设置格式化字符串
            decFormat.applyPattern(TWOCARRY);
            return decFormat;
        }
    };
    // 1.8 使用lambda
    private static final ThreadLocal<DecimalFormat> THREELOCAL = new ThreadLocal<DecimalFormat>()
    {
        @Override
        protected DecimalFormat initialValue()
        {
            DecimalFormat decFormat = new DecimalFormat();
            decFormat.setMinimumFractionDigits(3);
            decFormat.setMaximumFractionDigits(3);
            // 设置格式化字符串
            decFormat.applyPattern(THREECARRY);
            return decFormat;
        }
    };
    // 1.8 使用lambda
    private static final ThreadLocal<DecimalFormat> FOURLOCAL = new ThreadLocal<DecimalFormat>()
    {
        @Override
        protected DecimalFormat initialValue()
        {
            DecimalFormat decFormat = new DecimalFormat();
            decFormat.setMinimumFractionDigits(4);
            decFormat.setMaximumFractionDigits(4);
            // 设置格式化字符串
            decFormat.applyPattern(FOURCARRY);
            return decFormat;
        }
    };
    // 1.8 使用lambda
    private static final ThreadLocal<DecimalFormat> FIVELOCAL = new ThreadLocal<DecimalFormat>()
    {
        @Override
        protected DecimalFormat initialValue()
        {
            DecimalFormat decFormat = new DecimalFormat();
            decFormat.setMinimumFractionDigits(5);
            decFormat.setMaximumFractionDigits(5);
            // 设置格式化字符串
            decFormat.applyPattern(FIVECARRY);
            return decFormat;
        }
    };
    // 1.8 使用lambda
    private static final ThreadLocal<DecimalFormat> SIXLOCAL = new ThreadLocal<DecimalFormat>()
    {
        @Override
        protected DecimalFormat initialValue()
        {
            DecimalFormat decFormat = new DecimalFormat();
            decFormat.setMinimumFractionDigits(6);
            decFormat.setMaximumFractionDigits(6);
            // 设置格式化字符串
            decFormat.applyPattern(SIXCARRY);
            return decFormat;
        }
    };
    // 1.8 使用lambda
    public static final ThreadLocal<DecimalFormat> SEVENLOCAL = new ThreadLocal<DecimalFormat>()
    {
        @Override
        protected DecimalFormat initialValue()
        {
            DecimalFormat decFormat = new DecimalFormat();
            decFormat.setMinimumFractionDigits(7);
            decFormat.setMaximumFractionDigits(7);
            // 设置格式化字符串
            decFormat.applyPattern(SEVENCARRY);
            return decFormat;
        }
    };
    // 1.8 使用lambda
    private static final ThreadLocal<DecimalFormat> EIGHTLOCAL = new ThreadLocal<DecimalFormat>()
    {
        @Override
        protected DecimalFormat initialValue()
        {
            DecimalFormat decFormat = new DecimalFormat();
            decFormat.setMinimumFractionDigits(8);
            decFormat.setMaximumFractionDigits(8);
            // 设置格式化字符串
            decFormat.applyPattern(EIGHTCARRY);
            return decFormat;
        }
    };
    
    
    
    
    // 1.8 使用lambda
    private static final ThreadLocal<DecimalFormat> ZEROLOCALE = new ThreadLocal<DecimalFormat>()
    {
        @Override
        protected DecimalFormat initialValue()
        {
            DecimalFormat decFormat = new DecimalFormat();
            decFormat.setMinimumFractionDigits(0);
            decFormat.setMaximumFractionDigits(0);
            // 设置格式化字符串
            decFormat.applyPattern(ZEROCARRYE);
            return decFormat;
        }
    };
    // 1.8 使用lambda
    private static final ThreadLocal<DecimalFormat> ONELOCALE = new ThreadLocal<DecimalFormat>()
    {
        @Override
        protected DecimalFormat initialValue()
        {
            DecimalFormat decFormat = new DecimalFormat();
            decFormat.setMinimumFractionDigits(1);
            decFormat.setMaximumFractionDigits(1);
            // 设置格式化字符串
            decFormat.applyPattern(ONECARRYE);
            return decFormat;
        }
    };
    // 1.8 使用lambda
    private static final ThreadLocal<DecimalFormat> TWOLOCALE = new ThreadLocal<DecimalFormat>()
    {
        @Override
        protected DecimalFormat initialValue()
        {
            DecimalFormat decFormat = new DecimalFormat();
            decFormat.setMinimumFractionDigits(2);
            decFormat.setMaximumFractionDigits(2);
            // 设置格式化字符串
            decFormat.applyPattern(TWOCARRYE);
            return decFormat;
        }
    };
    // 1.8 使用lambda
    private static final ThreadLocal<DecimalFormat> THREELOCALE = new ThreadLocal<DecimalFormat>()
    {
        @Override
        protected DecimalFormat initialValue()
        {
            DecimalFormat decFormat = new DecimalFormat();
            decFormat.setMinimumFractionDigits(3);
            decFormat.setMaximumFractionDigits(3);
            // 设置格式化字符串
            decFormat.applyPattern(THREECARRYE);
            return decFormat;
        }
    };
    // 1.8 使用lambda
    private static final ThreadLocal<DecimalFormat> FOURLOCALE = new ThreadLocal<DecimalFormat>()
    {
        @Override
        protected DecimalFormat initialValue()
        {
            DecimalFormat decFormat = new DecimalFormat();
            decFormat.setMinimumFractionDigits(4);
            decFormat.setMaximumFractionDigits(4);
            // 设置格式化字符串
            decFormat.applyPattern(FOURCARRYE);
            return decFormat;
        }
    };
    // 1.8 使用lambda
    private static final ThreadLocal<DecimalFormat> FIVELOCALE = new ThreadLocal<DecimalFormat>()
    {
        @Override
        protected DecimalFormat initialValue()
        {
            DecimalFormat decFormat = new DecimalFormat();
            decFormat.setMinimumFractionDigits(5);
            decFormat.setMaximumFractionDigits(5);
            // 设置格式化字符串
            decFormat.applyPattern(FIVECARRYE);
            return decFormat;
        }
    };
    // 1.8 使用lambda
    private static final ThreadLocal<DecimalFormat> SIXLOCALE = new ThreadLocal<DecimalFormat>()
    {
        @Override
        protected DecimalFormat initialValue()
        {
            DecimalFormat decFormat = new DecimalFormat();
            decFormat.setMinimumFractionDigits(6);
            decFormat.setMaximumFractionDigits(6);
            // 设置格式化字符串
            decFormat.applyPattern(SIXCARRYE);
            return decFormat;
        }
    };
    // 1.8 使用lambda
    public static final ThreadLocal<DecimalFormat> SEVENLOCALE = new ThreadLocal<DecimalFormat>()
    {
        @Override
        protected DecimalFormat initialValue()
        {
            DecimalFormat decFormat = new DecimalFormat();
            decFormat.setMinimumFractionDigits(7);
            decFormat.setMaximumFractionDigits(7);
            // 设置格式化字符串
            decFormat.applyPattern(SEVENCARRYE);
            return decFormat;
        }
    };
    // 1.8 使用lambda
    private static final ThreadLocal<DecimalFormat> EIGHTLOCALE = new ThreadLocal<DecimalFormat>()
    {
        @Override
        protected DecimalFormat initialValue()
        {
            DecimalFormat decFormat = new DecimalFormat();
            decFormat.setMinimumFractionDigits(8);
            decFormat.setMaximumFractionDigits(8);
            // 设置格式化字符串
            decFormat.applyPattern(EIGHTCARRYE);
            return decFormat;
        }
    };
}
