package com.personal.core.utils;

import java.util.regex.Pattern;


/**
 * 正则相关工具类（预编译常用正则）
 * @author cuibo
 *
 */
public final class ReGularUtil
{
    public static final Pattern EXPPATTERN = Pattern.compile("[\\+\\-\\*\\/]");
    
    public static final Pattern EXPANDKHPATTERN = Pattern.compile("[\\+\\-\\*\\/\\(\\)]");
    
    public static final Pattern EXPANDKHPATTERNANDABS = Pattern.compile("[\\+\\-\\*\\/\\(\\)\\|]");
    
    /** 加减 */
    public static final Pattern JJPPATTERN = Pattern.compile("[\\+\\-]");
    
    public static final Pattern NULLPATTERN = Pattern.compile("\\s");
    
    /** 简单的日期正则，不考虑合法性 */
    public static final Pattern ISDATE = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}$|^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}$");
    
    /** 中文数字正则 */
    public static final Pattern CHINESENUMBER = Pattern.compile("^[零一二三四五六七八九十百千万亿]+$");
    
    /** 中文数字外面有括号的正则 */
    public static final Pattern CHINESENUMBERANDKH = Pattern.compile("^[(（][零一二三四五六七八九十]+[)）]$");
    
    /** 数字正则 */
    public static final Pattern NUMBER_PATTERN = Pattern.compile("^[-\\+]?\\d{1,}$|^[-\\+]?\\d{1,}\\.\\d{1,}$");
    
    /** htmltitle */
    public static final Pattern HTMLTITLE = Pattern.compile("^title=''$");
    
    /** 整数 */
    public static final Pattern INTEGER = Pattern.compile("[1-9][0-9]*|0");
    
    /** 千分位数字 */
    public static final Pattern THOUSANDS_PATTERN = Pattern.compile("^[-\\+]?\\d{1,3}(,\\d{3})*(\\.(\\d*))?$");
    
    /** 逗号 */
    public static final Pattern DH = Pattern.compile(",|，");
    
    /** 层级正则(1.1.1.1) */
    public static final Pattern LEVEL_PATTERN = Pattern.compile("^(\\d{1,}.){0,}\\d{1,}$");
    
}
