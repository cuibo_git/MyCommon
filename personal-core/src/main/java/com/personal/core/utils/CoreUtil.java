package com.personal.core.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import com.googlecode.aviator.AviatorEvaluator;
import com.personal.core.data.DataColumn;
import com.personal.core.data.DataRow;
import com.personal.core.data.DataTables;
import com.personal.core.data.DataTable;

import static java.lang.System.out;

/**
 * 造价工具类
 * @author cuibo
 */
public class CoreUtil
{
    /** 浮点型零值（判断"0.0"值）*/
    private static final double FLOAT_ZERO_VALUE = 0.00000001;
    private static final String[] NUMBERARR =
    { "零", "一", "二", "三", "四", "五", "六", "七", "八", "九" };
    private static final String[] UNITARR =
    { "", "十", "百", "千", "万", "十", "百", "千", "亿", "十" };
    private static final double SYSTEMPRECISION;
    private static final char[] CHNNUMCHAR =
    { '零', '一', '二', '三', '四', '五', '六', '七', '八', '九' };
    private static final Map<Character, Integer> CHARINTMAP = new ConcurrentHashMap<Character, Integer>();
    static
    {
        for (int i = 0; i < CHNNUMCHAR.length; i++)
        {
            CHARINTMAP.put(CHNNUMCHAR[i], i);
        }
        CHARINTMAP.put('十', 10);
        CHARINTMAP.put('百', 100);
        CHARINTMAP.put('千', 1000);
        // 系统精度
        if (!isEmpty(System.getProperty("systemprecision")))
        {
            SYSTEMPRECISION = Double.parseDouble(System.getProperty("systemprecision"));
        } else
        {
            SYSTEMPRECISION = 0.0001D;
        }
    }
    
    /**
     * cuibo:SimpleDateFormat非线程安全
     */
    public static final ThreadLocal<DateFormat> LOCALONE = new ThreadLocal<DateFormat>()
    {
        @Override
        protected DateFormat initialValue()
        {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
    };

    public static final ThreadLocal<DateFormat> LOCALTWO = new ThreadLocal<DateFormat>()
    {
        @Override
        protected DateFormat initialValue()
        {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }
    };

    /** 设置一个大的时间 用于查询 **/
    public static final Date DATETIME_QUERY_MAX = parseDate("9999-01-01 00:00:00");
    public static final Date DATETIME_QUERY_MIN = parseDate("1753-01-01 00:00:00");

    /**
     * obj 转 str
     * @param obj
     * @return
     */
    public static String formatDate(Object obj)
    {
        if (obj == null)
        {
            return null;
        }
        if (obj instanceof Date || obj instanceof Number)
        {
            return LOCALONE.get().format(obj);
        } else
        {
            Date date = parseDate(obj);
            if (date != null)
            {
                return LOCALONE.get().format(date);
            }
            return null;
        }
    }

    /**
     * obj 转 str
     * @param obj
     * @return
     */
    public static String formatDateWithSfm(Object obj)
    {
        if (obj == null)
        {
            return null;
        }
        if (obj instanceof Date || obj instanceof Number)
        {
            return LOCALTWO.get().format(obj);
        } else
        {
            Date date = parseDate(obj);
            if (date != null)
            {
                return LOCALTWO.get().format(date);
            }
            return null;
        }
    }

    /**
     * obj 转 str
     * @param obj
     * @return
     * @throws ParseException 
     */
    public static Date parseDate(Object obj)
    {
        if (obj == null)
        {
            return null;
        }
        if (obj instanceof Date)
        {
            return (Date) obj;
        }
        if (obj instanceof Number)
        {
            return new Date(parseLong(obj));
        }
        if (isNumber(obj))
        {
            return new Date(parseLong(obj));
        }
        try
        {
            return LOCALTWO.get().parse(obj.toString());
        } catch (ParseException e)
        {
        }
        try
        {
            return LOCALONE.get().parse(obj.toString());
        } catch (ParseException e)
        {
        }
        return null;
    }

    /**
     * @author cuibo 普通简单表达式的计算,且支持 -- += *- /- 这种操作符和负号在一起的情况
     * @param expression 表达式
     * @return
     * @throws Exception
     */
    public static String calculateEx(String expression)
    {
        // 2017 03 01 使用IKE表达式处理
        Object result = calculateExpByAvi(expression);
        if (result == null)
        {
            return null;
        }
        return CoreUtil.parseDblStr(result, 8);
    }

    /**
     * 通过AviatorEvaluator来计算表达式
     * @param exp
     * @return
     */
    public static String calculateExpByAvi(String exp)
    {
        if (isEmpty(exp))
        {
            return null;
        }
        try
        {
            exp = StringUtil.replace(StringUtil.replace(exp, "（", "("), "）", ")");
            // cb AviatorEvaluator会自动转型，如两个int相除会取整
            if (checkIsExp(exp))
            {
                String[] arr = ReGularUtil.EXPANDKHPATTERN.split(exp);
                for (String string : arr)
                {
                    // 是数字，转成DOUBLE
                    if (isNumber(string))
                    {
                        // 转为double
                        exp = StringUtil.replaceFirst(exp, string, parseDblStrWithXsw(string, 8, true));
                        // 此处一定只能替换一次就要跳出循环，替换多次可能会导致把前面的部分替换掉
                        break;
                    }
                }
            }
            return parseStr(AviatorEvaluator.execute(exp));
        } catch (Exception e)
        {
            return null;
        }
    }

    public static boolean calExpFlag(String validationInfo, Map<String, Object> map)
    {
        if (isEmpty(validationInfo))
        {
            return false;
        }
        try
        {
            return parseBoolean(AviatorEvaluator.execute(validationInfo, map));
        } catch (Exception e)
        {
            return false;
        }
    }

    /**
     * 计算一个表达式是 true 还是 false 也可以直接调用 calculateExpByJs 来计算
     * @param validationInfo (true&&false)||(false||true)
     * @return
     */
    public static boolean calExpFlag(String validationInfo)
    {
        if (isEmpty(validationInfo))
        {
            return false;
        }
        try
        {
            return parseBoolean(AviatorEvaluator.execute(validationInfo));
        } catch (Exception e)
        {
            return false;
        }
    }

    public static void main1(String[] args) {
        String str = "v1&&v2";
        Map<String, Object> map = new HashMap<>();
        map.put("v1", false);
        out.println(AviatorEvaluator.execute(str, map));
    }


    public static void main(String[] args) {
        A a = new A();
        B b = new B();
        Map<String, Object> map = new HashMap<>();
        map.put("a", a);
        map.put("b", b);
        AviatorEvaluator.execute("a.pb(b.c)", map);

    }

    public static class A {
        public void pb(String  v) {
            out.println(v);
        }
    }

    public static class B {
        public String c = "1";
    }

    /**
     * 判断包含
     * @param s1
     * @param s2
     * @return
     * @throws Exception
     */
    public static boolean checkContains(Object v1, Object v2)
    {
        String s1 = CoreUtil.converKH(v1);
        String s2 = CoreUtil.converKH(v2);
        if (!isEmpty(s1) && s1.contains(s2))
        {
            return true;
        }
        return false;
    }

    /**
     * 检查浮点型数据是否为"0.0"值
     * @param dblIn
     * @return
     */
    public static boolean checkDblZero(double dblIn)
    {
        return dblIn < CoreUtil.FLOAT_ZERO_VALUE && dblIn > -CoreUtil.FLOAT_ZERO_VALUE;
    }

    /**
     * 判断是否为浮点数，包括double和float
     * @param str 传入的字符串
     * @return 是浮点数返回true,否则返回false
     */
    public static boolean checkDouble(String str)
    {
        if (isEmpty(str))
        {
            return false;
        }
        return ReGularUtil.NUMBER_PATTERN.matcher(str).matches();
    }

    /**
     * 判断相同
     * @param s1
     * @param s2
     * @param nullIsEq
     * @return
     * @throws Exception
     */
    public static boolean checkEqual(Object v1, Object v2, boolean... nullIsEq)
    {
        if (v1 == null && v2 == null)
        {
            return (nullIsEq == null || nullIsEq.length == 0) ? true : nullIsEq[0];
        }
        if (v1 == v2)
        {
            return true;
        }
        String s1 = parseStr(v1);
        String s2 = parseStr(v2);
        if (s1.length() == 0 && s2.length() == 0)
        {
            return (nullIsEq == null || nullIsEq.length == 0) ? true : nullIsEq[0];
        }
        return s1.equals(s2);
    }

    /**
     * 判断相同忽略大小写
     * @param s1
     * @param s2
     * @param nullIsEq
     * @return
     * @throws Exception
     */
    public static boolean checkEqualIgnoreDxx(Object v1, Object v2, boolean... nullIsEq)
    {
        if (v1 == null && v2 == null)
        {
            return (nullIsEq == null || nullIsEq.length == 0) ? true : nullIsEq[0];
        }
        if (v1 == v2)
        {
            return true;
        }
        String s1 = parseStr(v1);
        String s2 = parseStr(v2);
        if (s1.length() == 0 && s2.length() == 0)
        {
            return (nullIsEq == null || nullIsEq.length == 0) ? true : nullIsEq[0];
        }
        // 均为空视为相等
        return s1.toUpperCase().equals(s2.toUpperCase());
    }

    /**
     * 判断相同
     * @param s1
     * @param s2
     * @return
     * @throws Exception
     */
    public static boolean checkEqual(List<?> col1, List<?> col2)
    {
        if (col1 == null && col2 == null)
        {
            return true;
        }
        if (col1 != null && col2 == null)
        {
            return false;
        } else if (col1 == null && col2 != null)
        {
            return false;
        }
        if (col1.size() != col2.size())
        {
            return false;
        }
        for (int i = 0; i < col1.size(); i++)
        {
            if (!checkEqual(col1.get(i), col2.get(i)))
            {
                return false;
            }
        }
        return true;
    }

    /**
     * 判断相同（忽略括号）
     * @param s1
     * @param s2
     * @param nullIsEq
     * @return
     * @throws Exception
     */
    public static boolean checkEqualIgnoreKh(Object v1, Object v2, boolean... nullIsEq)
    {
        // 均为空视为相等
        if (v1 == null && v2 == null)
        {
            return (nullIsEq == null || nullIsEq.length == 0) ? true : nullIsEq[0];
        }
        if (v1 == v2)
        {
            return true;
        }
        String s1 = CoreUtil.converKH(v1);
        String s2 = CoreUtil.converKH(v2);
        if (s1.length() == 0 && s2.length() == 0)
        {
            return (nullIsEq == null || nullIsEq.length == 0) ? true : nullIsEq[0];
        }
        return s1.equals(s2);
    }

    /**
     * 判断相同（忽略括号和大小写）
     * @param s1
     * @param s2
     * @param nullIsEq
     * @return
     * @throws Exception
     */
    public static boolean checkEqualIgnoreKhAndDxx(Object v1, Object v2, boolean... nullIsEq)
    {
        // 均为空视为相等
        if (v1 == null && v2 == null)
        {
            return (nullIsEq == null || nullIsEq.length == 0) ? true : nullIsEq[0];
        }
        if (v1 == v2)
        {
            return true;
        }
        String s1 = CoreUtil.converKH(v1);
        String s2 = CoreUtil.converKH(v2);
        if (s1.length() == 0 && s2.length() == 0)
        {
            return (nullIsEq == null || nullIsEq.length == 0) ? true : nullIsEq[0];
        }
        return s1.toUpperCase().equals(s2.toUpperCase());
    }

    /**
     * 判断相同(忽略大小写，忽略号大小写，忽略所有的空格)
     * @param s1
     * @param s2
     * @param nullIsEq  空是否相等
     * @return
     * @throws Exception
     */
    public static boolean checkEqualIgnoreAllInfo(Object v1, Object v2, boolean... nullIsEq)
    {
        // 均为空视为相等
        if (v1 == null && v2 == null)
        {
            return (nullIsEq == null || nullIsEq.length == 0) ? true : nullIsEq[0];
        }
        if (v1 == v2)
        {
            return true;
        }
        String s1 = CoreUtil.converKH(v1);
        String s2 = CoreUtil.converKH(v2);
        // 去除所有空格
        s1 = ReGularUtil.NULLPATTERN.matcher(s1).replaceAll("");
        s2 = ReGularUtil.NULLPATTERN.matcher(s2).replaceAll("");
        if (s1.length() == 0 && s2.length() == 0)
        {
            return (nullIsEq == null || nullIsEq.length == 0) ? true : nullIsEq[0];
        }
        return s1.toUpperCase().equals(s2.toUpperCase());
    }

    /**
     * 判断相同_忽略空格
     * @param s1
     * @param s2
     * @param nullIsEq  空是否相等
     * @return
     * @throws Exception
     */
    public static boolean checkEqualIgnoreSpace(Object v1, Object v2, boolean... nullIsEq)
    {
        // 均为空视为相等
        if (v1 == null && v2 == null)
        {
            return (nullIsEq == null || nullIsEq.length == 0) ? true : nullIsEq[0];
        }
        if (v1 == v2)
        {
            return true;
        }
        String s1 = parseStr(v1);
        String s2 = parseStr(v2);
        // 去除所有空格
        s1 = ReGularUtil.NULLPATTERN.matcher(s1).replaceAll("");
        s2 = ReGularUtil.NULLPATTERN.matcher(s2).replaceAll("");
        if (s1.length() == 0 && s2.length() == 0)
        {
            return (nullIsEq == null || nullIsEq.length == 0) ? true : nullIsEq[0];
        }
        return s1.equals(s2);
    }

    /**
     * 判断包含(忽略大小写，忽略号大小写，忽略所有的空格)
     * @param s1
     * @param s2
     * @param nullIsEq
     * @return
     * @throws Exception
     */
    public static boolean checkContainsIgnoreAllInfo(Object v1, Object v2, boolean... nullIsEq)
    {
        // 均为空视为相等
        if (v1 == null && v2 == null)
        {
            return (nullIsEq == null || nullIsEq.length == 0) ? true : nullIsEq[0];
        }
        if (v1 == v2)
        {
            return true;
        }
        String s1 = CoreUtil.converKH(v1);
        String s2 = CoreUtil.converKH(v2);
        // 去除所有空格
        s1 = ReGularUtil.NULLPATTERN.matcher(s1).replaceAll("");
        s2 = ReGularUtil.NULLPATTERN.matcher(s2).replaceAll("");
        if (s1.length() == 0 && s2.length() == 0)
        {
            return (nullIsEq == null || nullIsEq.length == 0) ? true : nullIsEq[0];
        }
        return s1.toUpperCase().contains(s2.toUpperCase());
    }

    /**
     * 判断相同或者包含
     * @param v1 
     * @param v2 逗号分隔
     * @return
     * @throws Exception
     */
    public static boolean checkIn(Object v1, Object v2)
    {
        if (v1 == null && v2 == null)
        {
            return false;
        }
        String s2 = CoreUtil.converKH(v2);
        String[] arr = split(s2, ",");
        if (arr == null || arr.length == 0)
        {
            return false;
        }
        boolean result = false;
        for (String string : arr)
        {
            result = checkEqual(v1, string);
            if (result)
            {
                return result;
            }
        }
        return result;
    }

    /**
     * 比较时间大小
     * @param dateOne
     * @param dateTwo
     * @return dateOne 毫秒数大于 dateTwo 1  dateOne 毫秒数小于 dateTwo -1  null视为最大值
     */
    public static int compareDate(Object dateOne, Object dateTwo)
    {
        return compareDate(parseDate(dateOne), parseDate(dateTwo));
    }

    /**
     * 比较时间大小
     * @param dateOne
     * @param dateTwo
     * @return dateOne 毫秒数大于 dateTwo 1  dateOne 毫秒数小于 dateTwo -1  null视为最大值
     */
    public static int compareDate(Date dateOne, Date dateTwo)
    {
        if (dateOne == null && dateTwo == null)
        {
            return 0;
        } else if (dateOne == null && dateTwo != null)
        {
            return 1;
        } else if (dateOne != null && dateTwo == null)
        {
            return -1;
        } else
        {
            long local = dateOne.getTime() - dateTwo.getTime();
            return local > 0 ? local == 0 ? 0 : 1 : -1;
        }
    }

    /**
     * 比较数字大小(不考虑精度)
     * @param objOne
     * @param objTwo
     * @return objOne < objTwo 返回 -1  objOne > objTwo 返回  1   objOne == objTwo 返回0  
     */
    public static int compareNumberNoPrecision(Object objOne, Object objTwo)
    {
        return Double.compare(subtract(objOne, objTwo), 0.0);
    }

    /**
     * 比较数字大小(系统默认精度)
     * @param objOne
     * @param objTwo
     * @return objOne < objTwo 返回 -1  objOne > objTwo 返回  1   objOne == objTwo 返回0  
     */
    public static int compareNumber(Object objOne, Object objTwo)
    {
        return compareNumber(objOne, objTwo, SYSTEMPRECISION);
    }

    /**
     * 比较数字大小
     * @param objOne
     * @param objTwo
     * @param precision 精度
     * @return objOne < objTwo 返回 -1  objOne > objTwo 返回  1   objOne == objTwo 返回0  
     */
    public static int compareNumber(Object objOne, Object objTwo, double precision)
    {
        double cz = subtract(objOne, objTwo);
        // 差值的绝对值小于等于SYSTEMPRECISION 均视为相等
        if (Double.compare(Math.abs(cz), precision) < 1)
        {
            return 0;
        }
        return Double.compare(cz, 0.0);
    }

    /**
     * DOUBLE转汉字,整数部分限制为INT类型
     * @param d
     * @return
     */
    public static String converToChinese(double d)
    {
        String ds = parseDblStr(d, 10);
        if (ds.contains("."))
        {
            String[] arr = ds.split("\\.");
            if (arr.length == 1)
            {
                return converToChinese(parseInt(arr[0]));
            }
            return converToChinese(parseInt(arr[0])) + "点" + converXsw(arr[1]);
        } else
        {
            return converToChinese((int) d);
        }
    }

    /**
     * 转小数位
     * @param str
     * @return
     */
    private static String converXsw(String str)
    {
        if (StringUtil.isEmpty(str))
        {
            return "";
        }
        char ch;
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < str.length(); i++)
        {
            ch = str.charAt(i);
            result.append(NUMBERARR[ch - '0']);
        }
        return result.toString();
    }

    /**
     * 整数转汉字
     * @param i
     * @return
     */
    public static String converToChinese(int i)
    {
        boolean isFs = i < 0;
        String result = "";
        i = Math.abs(i);
        if (i < 10)
        {
            result = NUMBERARR[i];
        }
        StringBuilder build = new StringBuilder();
        int j = 0;
        while (i / 10 != 0)
        {
            build.append(UNITARR[j]);
            build.append(NUMBERARR[i % 10]);
            i = i / 10;
            j++;
        }
        build.append(UNITARR[j]);
        build.append(NUMBERARR[i]);
        result = build.reverse().toString();
        if (result.endsWith("零"))
        {
            result = result.substring(0, result.length() - 1);
        }
        // 处理零万
        if (result.contains("零万"))
        {
            result = StringUtil.replace(result, "零万", "万");
        }
        // 处理零亿
        if (result.contains("零亿"))
        {
            result = StringUtil.replace(result, "零亿", "亿");
        }
        result = result.replaceAll("零[\\S]{1}", "零");
        result = result.replaceAll("[零]+", "零");

        if (result.contains("百零万"))
        {
            result = StringUtil.replace(result, "百零万", "百万");
        }
        if (result.contains("千零万"))
        {
            result = StringUtil.replace(result, "千零万", "千万");
        }
        // 仍有零万
        if (result.contains("零万"))
        {
            result = StringUtil.replace(result, "零万", "");
        }
        if (result.endsWith("零"))
        {
            result = result.substring(0, result.length() - 1);
        }
        //以一十开头的变成十开头
        if (result.startsWith("一十"))
        {
            result = result.replaceFirst("一十", "十");
        }
        return isFs ? "负" + result : result;
    }

    /**
     * 转化括号，去除空格
     * @param value
     * @return
     * @throws Exception
     */
    public static String converKH(Object s)
    {
        String value = parseStr(s);
        if (isEmpty(value))
        {
            return value;
        }
        if (value.contains("（"))
        {
            value = value.replace("（", "(").trim();
        }
        if (value.contains("）"))
        {
            value = value.replace("）", ")").trim();
        }
        return value;
    }

    /**
     * 序列化深拷贝
     * @param source
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    @SuppressWarnings("unchecked")
    public static <T> T deepCopy(T source) throws IOException, ClassNotFoundException
    {
        ObjectOutputStream out = null;
        ObjectInputStream in = null;
        try
        {
            if (source == null)
            {
                return null;
            }
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            out = new ObjectOutputStream(new BufferedOutputStream(bos));
            out.writeObject(source);
            out.flush();

            ByteArrayInputStream bis = new ByteArrayInputStream(bos.toByteArray());
            in = new ObjectInputStream(new BufferedInputStream(bis));
            Object obj = in.readObject();
            if (obj == null)
            {
                return null;
            }
            return (T) obj;
        } finally
        {
            release(in);
            release(out);
        }
    }

    /**
     * 当前年份的第一天
     * @return
     */
    public static Date getCurrentYearFirstDay()
    {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        return calendar.getTime();
    }

    /**
     * 当前年份的第一天
     * @return
     */
    public static Date getCurrentYearLastDay()
    {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        calendar.add(Calendar.YEAR, 1);
        calendar.add(Calendar.MILLISECOND, -1);
        return calendar.getTime();
    }

    public static DataTable getDataTable(DataTables dt, String tableName)
    {
        if (dt == null || dt.isEmpty() || isEmpty(tableName))
        {
            return null;
        }
        String name = "";
        for (DataTable dataTable : dt)
        {
            name = dataTable.getTableName();
            if (CoreUtil.checkEqualOrContains(tableName, name))
            {
                return dataTable;
            }
        }
        return null;
    }

    /**
     * 判断相同或者包含
     * @param s1  其中包含 like  in
     * @param s2
     * @return
     * @throws Exception
     */
    private static boolean checkEqualOrContains(Object v1, Object v2)
    {
        if (v1 == null && v2 == null)
        {
            return true;
        }
        String s1 = CoreUtil.converKH(v1);
        String s2 = CoreUtil.converKH(v2);
        if (s1.contains("like"))
        {
            s1 = s1.replaceAll("like", "").trim();
            return s2 != null && s2.contains(s1);
        }
        if (s1.contains("in"))
        {
            return s1 != null && s1.contains(s2);
        }
        return s1.equals(s2);
    }

    /**
     * 获取格式化字符
     * @param carryNum
     * @return
     */
    public static String getFormatPatternString(int carryNum)
    {
        StringBuilder patternStr = new StringBuilder("0");
        for (int index = 1; index <= carryNum; index++)
        {
            if (index == 1)
            {
                patternStr.append(".");
            }
            patternStr.append("#");
        }
        return patternStr.toString();
    }

    /**
     * 解析浮点型字符串(对应值为零则返回空)
     * 小数位为两位
     */
    public static String parseDblStr(Object objIn)
    {
        return CoreUtil.parseDblStr(objIn, 2, false);
    }

    public static int parseInt(Object objIn)
    {
        if (objIn == null)
        {
            return 0;
        }
        return (int) parseDbl(objIn);
    }

    public static long parseLong(Object objIn)
    {
        if (objIn == null)
        {
            return 0L;
        }
        return (long) parseDbl(objIn);
    }

    public static float parseFloat(Object objIn)
    {
        if (objIn == null)
        {
            return 0L;
        }
        return (float) parseDbl(objIn);
    }

    public static String parseStr(Object objIn)
    {
        return objIn == null ? "" : objIn.toString();
    }

    public static double parseDbl(Object objIn)
    {
        double dbl = 0.0D;
        if (objIn == null)
        {
            return dbl;
        }
        try
        {
            dbl = Double.parseDouble(parseStr(objIn));
        } catch (NumberFormatException localException)
        {
        }

        return dbl;
    }

    public static boolean parseBoolean(Object objIn)
    {
        return Boolean.valueOf(parseStr(objIn));
    }

    /**
     * 解析浮点型字符串(对应值为零则返回空)
     * 小数位为两位
     */
    public static String parseDblStr(Object objIn, boolean bDisplayZero)
    {
        return CoreUtil.parseDblStr(objIn, 2, bDisplayZero);
    }

    /**
     * 解析浮点型字符串(对应值为零则返回空)
     */
    public static String parseDblStr(Object objIn, int carryNum)
    {
        return CoreUtil.parseDblStr(objIn, carryNum, false);
    }

    /**
     * 解析浮点型字符串
     * @param objIn
     * @param carryNum
     * @param displayZero  为0时展示0还是""的区分  true时显示零 false显示空
     * @return
     */
    public static String parseDblStr(Object objIn, int carryNum, boolean displayZero)
    {
        if (isEmpty(objIn))
        {
            // 为空百分百返回"";
            return "";
        }
        Double dbl = parseDbl(objIn);
        if (!displayZero && CoreUtil.checkDblZero(dbl))
        {
            return "";
        }
        BigDecimal result = null;
        try
        {
            result = new BigDecimal(parseStr(objIn));
            result = result.setScale(carryNum, BigDecimal.ROUND_HALF_UP);
            return DecimalFormatHolder.getDecimalFormat(carryNum).format(result);
        } catch (Exception e)
        {
            return "";
        }
    }

    /**
     * 解析浮点型字符串
     * 百分百保留对应的小数位
     */
    public static String parseDblStrWithXsw(Object objIn, int carryNum)
    {
        return parseDblStrWithXsw(objIn, carryNum, true);
    }

    /**
     * 将double转Str为空时返回0
     * 一般用于图表的生成
     * @param objIn
     * @return
     */
    public static String parseDblStrWithZero(Object objIn)
    {
        return parseDblStrWithZero(objIn, 2);
    }

    /**
     * 将double转Str为空时返回0
     * 一般用于图表的生成
     * @param objIn
     * @param carryNum
     * @return
     */
    public static String parseDblStrWithZero(Object objIn, int carryNum)
    {
        if (isEmpty(objIn))
        {
            return "0";
        }
        BigDecimal result = null;
        try
        {
            result = new BigDecimal(parseStr(objIn));
            result = result.setScale(carryNum, BigDecimal.ROUND_HALF_UP);
            return DecimalFormatHolder.getDecimalFormat(carryNum).format(result);
        } catch (Exception e)
        {
            return "0";
        }
    }

    /**
     * 解析浮点型字符串
     * 百分百保留对应的小数位
     */
    public static String parseDblStrWithXsw(Object objIn, int carryNum, boolean bDisplayZero)
    {
        if (isEmpty(objIn))
        {
            return "";
        }
        BigDecimal result = null;
        try
        {
            result = new BigDecimal(parseStr(objIn));
            result = result.setScale(carryNum, BigDecimal.ROUND_HALF_UP);
            return DecimalFormatHolder.getDecimalFormatXsw(carryNum).format(result);
        } catch (Exception e)
        {
            return bDisplayZero ? "0" : "";
        }
    }

    /**
     * 格式化数据
     * @param rable
     * @param carryNum
     */
    public static void parseFeeInfo(DataTable rable, int carryNum)
    {
        if (rable == null || rable.getRows().isEmpty())
        {
            return;
        }
        for (DataRow row : rable.getRows())
        {
            CoreUtil.parseFeeInfo(row.getItemMap(), carryNum);
        }
    }

    /**
     * 格式化数据
     * @param feeInfo
     * @param carryNum
     */
    public static void parseFeeInfo(Map<String, Object> feeInfo, int carryNum)
    {
        if (feeInfo == null || feeInfo.isEmpty())
        {
            return;
        }
        for (Entry<String, Object> entry : feeInfo.entrySet())
        {
            if (isNumber(entry.getValue()))
            {
                feeInfo.put(entry.getKey(), CoreUtil.parseDblStr(entry.getValue(), carryNum));
            }
        }
    }

    /**
     * 转化成百分比形式
     * @param value
     * @return
     */
    public static String parsePercent(Object value)
    {
        return CoreUtil.parsePercent(value, 2);
    }

    /**
     * 转化成百分比形式
     * @param value
     * @return
     */
    public static String parsePercent(Object value, int scale)
    {
        DecimalFormat df = (DecimalFormat) NumberFormat.getInstance();
        df.applyPattern(CoreUtil.getFormatPatternString(scale) + "%");
        try
        {
            return df.format(value);
        } catch (Exception e)
        {
        }
        return "";
    }

    /**
     * 去除所有空格
     * @param source
     * @return
     */
    public static String replaceAllSpace(String source)
    {
        if (isEmpty(source))
        {
            return source;
        }
        return ReGularUtil.NULLPATTERN.matcher(source).replaceAll("");
    }

    /**
     * 格式化
     * @param mapList
     * @param format
     */
    public static void formatMap(List<? extends Map<String, Object>> mapList, Map<String, Integer> format)
    {
        if (mapList == null || mapList.isEmpty() || format == null || format.isEmpty())
        {
            return;
        }
        for (Map<String, Object> map : mapList)
        {
            formatMap(map, format);
        }
    }

    /**
     * 格式化（为0时展示）
     * @param mapList
     * @param format
     */
    public static void formatMapWithZero(List<? extends Map<String, Object>> mapList, Map<String, Integer> format)
    {
        if (mapList == null || mapList.isEmpty() || format == null || format.isEmpty())
        {
            return;
        }
        for (Map<String, Object> map : mapList)
        {
            formatMapWithZero(map, format);
        }
    }

    /**
     * 格式化（为0时展示）
     * @param mapList
     * @param format
     */
    public static void formatDataTableWithZero(DataTable table, Map<String, Integer> format)
    {
        if (table == null || table.getRows() == null || table.getRows().isEmpty() || format == null || format.isEmpty())
        {
            return;
        }
        for (DataRow row : table.getRows())
        {
            formatMapWithZero(row.getItemMap(), format);
        }
    }

    /**
     * 格式化（为0时展示）
     * @param map
     * @param format
     */
    public static void formatMapWithZero(Map<String, Object> map, Map<String, Integer> format)
    {
        if (map == null || map.isEmpty() || format == null || format.isEmpty())
        {
            return;
        }
        for (Entry<String, Integer> entry : format.entrySet())
        {
            // 展示0
            map.put(entry.getKey(), parseDblStr(map.get(entry.getKey()), entry.getValue(), true));
        }
    }

    /**
     * 格式化
     * @param mapList
     * @param format
     */
    public static void formatDataTable(DataTable table, Map<String, Integer> format)
    {
        if (table == null || table.getRows() == null || table.getRows().isEmpty() || format == null || format.isEmpty())
        {
            return;
        }
        for (DataRow row : table.getRows())
        {
            formatMap(row.getItemMap(), format);
        }
    }

    /**
     * 格式化
     * @param map
     * @param format
     */
    public static void formatMap(Map<String, Object> map, Map<String, Integer> format)
    {
        if (map == null || map.isEmpty() || format == null || format.isEmpty())
        {
            return;
        }
        for (Entry<String, Integer> entry : format.entrySet())
        {
            map.put(entry.getKey(), parseDblStr(map.get(entry.getKey()), entry.getValue()));
        }
    }

    /**
     * 序列化Obj
     * @param obj
     * @return
     * @throws Exception
     */
    public static byte[] serializationObject(Object obj) throws Exception
    {
        if (obj == null)
        {
            return null;
        }
        ByteArrayOutputStream bos = null;
        ObjectOutputStream out = null;
        try
        {
            bos = new ByteArrayOutputStream();
            out = new ObjectOutputStream(new BufferedOutputStream(bos));
            out.writeObject(obj);
            out.flush();
            return bos.toByteArray();
        } catch (Exception e)
        {
            throw new Exception("序列化对象失败！");
        } finally
        {
            release(bos);
            release(out);
        }
    }

    /**
     * 反序列化
     * @param bts
     * @param type
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
    public static <T> T unSerializationObject(byte[] bts, Class<T> type) throws Exception
    {
        if (bts == null)
        {
            return null;
        }
        ObjectInputStream inputStream = new ObjectInputStream(new BufferedInputStream(new ByteArrayInputStream(bts)));
        try
        {
            Object obj = inputStream.readObject();
            return (T) obj;
        } catch (Exception e)
        {
            throw new Exception("反序列化对象失败！");
        } finally
        {
            release(inputStream);
        }
    }

    /**
    * 关闭输入流
    * @param rs 输入流
    *
    */
    public static void release(InputStream rs)
    {
        if (rs != null)
        {
            try
            {
                rs.close();
            } catch (Exception e)
            {
                rs = null;
            }
        }
    }

    /**
     * 关闭输出流
     * @param rs 输出流
     *
     */
    public static void release(OutputStream rs)
    {
        if (rs != null)
        {
            try
            {
                rs.close();
            } catch (Exception e)
            {
                rs = null;
            }
        }
    }

    /**
 * 加法运算（准确计算精度）
 * @param args
 * @return
 */
public static double add(Object... args)
{
    try
    {
        if (args == null || args.length == 0)
        {
            return 0.0D;
        } else if (args.length == 1)
        {
            return parseDbl(args[0]);
        }
        BigDecimal result = new BigDecimal(parseSpecialStr(args[0]));
        for (int i = 1; i < args.length; i++)
        {
            result = result.add(new BigDecimal(parseSpecialStr(args[i])));
        }
        if (result != null)
        {
            return result.doubleValue();
        }
    } catch (Exception e)
    {
    }
    return 0.0;
}

    /**
     * 加法运算（准确计算精度）
     * @param args
     * @return
     */
    public static Object addWithNull(Object... args)
    {
        try
        {
            if (args == null || args.length == 0)
            {
                return null;
            } else if (args.length == 1)
            {
                return args[0];
            }
            boolean displayZero = !CoreUtil.isEmpty(args[0]);
            BigDecimal result = new BigDecimal(parseSpecialStr(args[0]));
            for (int i = 1; i < args.length; i++)
            {
                if (displayZero && CoreUtil.isEmpty(args[i]))
                {
                    displayZero = false;
                }
                result = result.add(new BigDecimal(parseSpecialStr(args[i])));
            }
            double local = result.doubleValue();
            return parseDblStr(local, 8, displayZero);
        } catch (Exception e)
        {
        }
        return null;
    }

    /**
     * 减法运算（准确计算精度）
     * @param o1
     * @param o2
     * @return
     */
    public static Object subtractWithNull(Object o1, Object o2)
    {
        try
        {
            boolean displayZero = !CoreUtil.isEmpty(o1);
            if (displayZero)
            {
                displayZero = !CoreUtil.isEmpty(o1);
            }
            BigDecimal b1 = new BigDecimal(parseSpecialStr(o1));
            BigDecimal b2 = new BigDecimal(parseSpecialStr(o2));
            BigDecimal result = b1.subtract(b2);
            double local = result.doubleValue();
            return parseDblStr(local, 8, displayZero);
        } catch (Exception e)
        {
        }
        return null;
    }

    /**
     * 减法运算（准确计算精度）
     * @param o1
     * @param o2
     * @return
     */
    public static double subtract(Object o1, Object o2)
    {
        try
        {
            BigDecimal b1 = new BigDecimal(parseSpecialStr(o1));
            BigDecimal b2 = new BigDecimal(parseSpecialStr(o2));
            BigDecimal result = b1.subtract(b2);
            if (result != null)
            {
                return result.doubleValue();
            }
        } catch (Exception e)
        {
        }
        return 0;
    }

    /**
     * 乘法运算（准确计算精度）
     * @param o1
     * @param o2
     * @return
     */
    public static double multiply(Object o1, Object o2)
    {
        try
        {
            BigDecimal b1 = new BigDecimal(parseSpecialStr(o1));
            BigDecimal b2 = new BigDecimal(parseSpecialStr(o2));
            BigDecimal result = b1.multiply(b2);
            if (result != null)
            {
                return result.doubleValue();
            }
        } catch (Exception e)
        {
        }
        return 0;
    }

    /**
     * 乘法运算（准确计算精度）
     * @param o1
     * @param o2
     * @return
     */
    public static Object multiplyWithNull(Object o1, Object o2)
    {
        try
        {
            boolean displayZero = !CoreUtil.isEmpty(o1);
            if (displayZero)
            {
                displayZero = !CoreUtil.isEmpty(o1);
            }
            BigDecimal b1 = new BigDecimal(parseSpecialStr(o1));
            BigDecimal b2 = new BigDecimal(parseSpecialStr(o2));
            BigDecimal result = b1.multiply(b2);
            double local = result.doubleValue();
            return parseDblStr(local, 8, displayZero);
        } catch (Exception e)
        {
        }
        return null;
    }

    /**
     * 除法运算（准确计算精度）
     * @param o1
     * @param o2
     * @return
     */
    public static double divide(Object o1, Object o2, int... len)
    {
        try
        {
            int i = 12; // 默认取12位
            if (len.length > 0)
            {
                i = len[0];
            }

            BigDecimal b1 = new BigDecimal(parseSpecialStr(o1));
            BigDecimal b2 = new BigDecimal(parseSpecialStr(o2));
            BigDecimal result = b1.divide(b2, i, BigDecimal.ROUND_HALF_UP);
            if (result != null)
            {
                return result.doubleValue();
            }
        } catch (Exception e)
        {
        }
        return 0;
    }

    /**
     * 除法运算（准确计算精度）
     * @param o1
     * @param o2
     * @return
     */
    public static Object divideWithNull(Object o1, Object o2, int... len)
    {
        try
        {
            int i = 8; // 默认取12位
            if (len.length > 0)
            {
                i = len[0];
            }
            boolean displayZero = !CoreUtil.isEmpty(o1);
            if (displayZero)
            {
                displayZero = !CoreUtil.isEmpty(o1);
            }
            BigDecimal b1 = new BigDecimal(parseSpecialStr(o1));
            BigDecimal b2 = new BigDecimal(parseSpecialStr(o2));
            BigDecimal result = b1.divide(b2, i, BigDecimal.ROUND_HALF_UP);
            double local = result.doubleValue();
            return parseDblStr(local, 8, displayZero);
        } catch (Exception e)
        {
        }
        return null;
    }

    /**
     * 解析字符型数据(为空时返回"0")
     */
    private static String parseSpecialStr(Object objIn)
    {
        if (objIn == null)
        {
            return "0";
        }
        String str = objIn.toString().trim();
        if (str.length() != 0 && isNumber(str))
        {
            return str;
        }
        return "0";
    }

    /**
     * 计算表达式（表达式汇总包含绝对值） cb: 暂不支持绝对值符号的嵌套
     * @param exp
     * @param tempValue
     * @return
     */
    public static String calculateExByAviWithAbs(String exp, Map<String, Object> map)
    {
        if (isEmpty(exp))
        {
            return exp;
        }
        // 不包含决定值则不考虑
        if (!exp.contains("|"))
        {
            return calculateExByAvi(exp, map);
        }
        // 查找绝对值区域
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < exp.length(); i++)
        {
            char ch = exp.charAt(i);
            if (ch == '|')
            {
                list.add(i);
            }
        }
        // 不是偶数字认为表达式不合法
        if (list.size() % 2 != 0)
        {
            return null;
        }
        String expTemp = exp;
        try
        {
            for (int i = 0; i < list.size(); i += 2)
            {
                // 把绝对值部分算出来
                int start = list.get(i);
                int end = list.get(i + 1);
                String localOne = expTemp.substring(start, end + 1);
                String localTwo = expTemp.substring(start + 1, end);
                exp = StringUtil.replaceFirst(exp, localOne,
                        CoreUtil.parseStr(Math.abs(CoreUtil.parseDbl(CoreUtil.calculateExByAvi(localTwo, map)))));
            }
        } catch (Exception e)
        {
        	e.printStackTrace();
        }
        // 最后在计算exp
        return CoreUtil.calculateExByAvi(exp, map);
    }

    /**
     * 计算表达式
     * @param exp
     * @param tempValue
     * @return
     */
    @SuppressWarnings("unchecked")
    public static String calculateExByAvi(String exp, Map<String, Object> map)
    {
        if (isEmpty(exp))
        {
            return exp;
        }
        // cb 2018 03 20 如果map中没有这个key，或者这个key的值为null
        // 则当计算结果为0时，则显示 "";
        boolean displayZero = true;
        map = map == null ? Collections.EMPTY_MAP : map;
        Map<String, Object> tempValue = null;
        try
        {
            // cb AviatorEvaluator会自动转型，如两个int相除会取整
            if (checkIsExp(exp))
            {
                // 中引文括号转换
                exp = StringUtil.replace(StringUtil.replace(exp, "（", "("), "）", ")");
                tempValue = new HashMap<String, Object>();
                String[] arr = ReGularUtil.EXPANDKHPATTERN.split(exp);
                // 中文开始符
                int index = 19968;
                String replaceKey;
                char[] charArr = new char[3];
                boolean toDouble = false;
                for (String string : arr)
                {
                    string = string.trim();
                    // 如果string 本身就是常量，则不替换
                    if (!map.containsKey(string) && isNumber(string))
                    {
                        // 转成double
                        // cb 防止传过来特殊符号，计算表达式会报错:如 A1+2A
                        if (!toDouble)
                        {
                            // 保证只能替换一次
                            exp = StringUtil.replaceFirst(exp, string, parseDblStrWithXsw(string, 8));
                            toDouble = true;
                        }
                        continue;
                    }
                    charArr[0] = (char) (index++);
                    charArr[1] = (char) (index++);
                    charArr[2] = (char) (index++);
                    replaceKey = String.valueOf(charArr);
                    // cb 防止传过来特殊符号，计算表达式会报错
                    exp = StringUtil.replaceFirst(exp, string, replaceKey);
                    // 是数字，转成DOUBLE
                    if (isNumber(map.get(string)))
                    {
                        tempValue.put(replaceKey, new BigDecimal(parseStr(map.get(string))));
                    } else
                    {
                        // 非数字当零处理
                        tempValue.put(replaceKey, 0.0D);
                    }
                    if (displayZero && CoreUtil.isEmpty(map.get(string)))
                    {
                        displayZero = false;
                    }
                }
            } else
            {
                tempValue = map;
            }
            return CoreUtil.parseDblStr(AviatorEvaluator.execute(exp, tempValue), 8, displayZero);
        } catch (Exception e)
        {
        }
        return null;
    }

    /**
     * list转DataTable
     * @param list
     * @param columns
     * @return
     */
    public static DataTable list2DataTable(List<? extends Map<String, Object>> list, String... columns)
    {
        if ((columns == null || columns.length == 0) && !isEmpty(list))
        {
            columns = list.get(0).keySet().toArray(new String[list.get(0).keySet().size()]);
        }
        if (columns == null || columns.length == 0)
        {
            return null;
        }
        DataTable result = new DataTable();
        DataColumn newColumn = null;
        for (String column : columns)
        {
            newColumn = new DataColumn(column);
            result.getColumns().add(newColumn);
        }
        if (isEmpty(list))
        {
            return result;
        }
        DataRow newRow = null;
        for (Map<String, Object> row : list)
        {
            newRow = result.newRow();
            newRow.getItemMap().putAll(row);
            result.getRows().add(newRow);
        }
        return result;
    }

    /**
     * 检查是否是表达式
     * @param exp
     * @return
     */
    public static boolean checkIsExp(Object exp)
    {
        if (exp == null)
        {
            return false;
        }
        String str = exp.toString();
        return str.contains("+") || str.contains("-") || str.contains("*") || str.contains("/");
    }

    /**
     * 将toSplit通过delimiter分割
     * @param toSplit
     * @param delimiter
     * @return
     */
    public static String[] split(String toSplit, String delimiter)
    {
        return split(toSplit, delimiter, Integer.MAX_VALUE);
    }

    /**
     * 将toSplit通过delimiter分割
     * @param toSplit
     * @param delimiter
     * @param limit  最大分割个数
     * @return
     */
    public static String[] split(String toSplit, String delimiter, int limit)
    {
        if (!StringUtil.hasLength(toSplit) || !StringUtil.hasLength(delimiter))
        {
            return null;
        }
        List<String> result = new ArrayList<String>();
        int offset = 0;
        int size = 0;
        while ((offset = toSplit.indexOf(delimiter)) > -1)
        {
            result.add(toSplit.substring(0, offset));
            toSplit = toSplit.substring(offset + delimiter.length());
            size++;
            if (size == (limit - 1))
            {
                break;
            }
        }
        if (toSplit.length() > 0)
        {
            result.add(toSplit);
        }
        return result.toArray(new String[result.size()]);
    }

    /**
     * DataTable列名重命名
     * @param table
     * @param renameColumnMap  key 原名， value：新的名称  将原名的值拷贝到新的列名值中
     */
    public static void reNameDatatableColumn(DataTable table, Map<String, String> renameColumnMap)
    {
        if (table == null || table.getColumns() == null || table.getRows() == null || renameColumnMap == null
                || renameColumnMap.isEmpty())
        {
            return;
        }
        for (Entry<String, String> entry : renameColumnMap.entrySet())
        {
            String newCol = entry.getValue();
            String oldCol = entry.getKey();
            // 如果table没有与拿来的列，则不转换
            if (table.getColumn(oldCol) == null)
            {
                continue;
            }
            table.addNewColumn(newCol);
            for (DataRow row : table.getRows())
            {
                row.setValue(newCol, row.getItemMap().get(oldCol));
            }
        }
    }

    /**
     * 将DataTable转化为List<Map<String, Object>>
     * @param dt
     * @return
     */
    public static List<Map<String, Object>> transDataTable(DataTable dt)
    {
        if (dt == null || dt.getRows() == null || dt.getRows().isEmpty())
        {
            return new ArrayList<Map<String, Object>>();
        }
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        for (DataRow row : dt.getRows())
        {
            Map<String, Object> single = new HashMap<String, Object>();
            single.putAll(row.getItemMap());
            result.add(single);
        }
        return result;
    }

    /**
     * 将DataTable转化为List<Map<String, Object>>
     * @param dt
     * @return
     * @throws IllegalAccessException 
     * @throws InstantiationException 
     */
    public static <T extends Map<String, Object>> List<T> transDataTable(DataTable dt, Class<T> c)
            throws InstantiationException, IllegalAccessException
    {
        if (dt == null || dt.getRows() == null || dt.getRows().isEmpty())
        {
            return new ArrayList<T>();
        }
        List<T> result = new ArrayList<T>();
        for (DataRow row : dt.getRows())
        {
            T newT = c.newInstance();
            newT.putAll(row.getItemMap());
            result.add(newT);
        }
        return result;
    }

    /**
     * 检查DataTable是否有数据
     * @param table
     * @return 有列，有行才会返回true
     */
    public static boolean checkDataTableHasData(DataTable table)
    {
        return table != null && table.getColumns() != null && !table.getColumns().isEmpty() && table.getRows() != null
                && !table.getRows().isEmpty();
    }

    /**
     * 检查DataTable是否有列数据
     * @param table
     * @return 有列才会返回true
     */
    public static boolean checkDataTableHasColumns(DataTable table)
    {
        return table != null && table.getColumns() != null && !table.getColumns().isEmpty();
    }

    /**
     * 数组合并
     * @param paramone
     * @param paramtwo
     * @return  合并之后的结果
     */
    public static String[] concat(String[] paramone, String[] paramtwo)
    {
        if (paramone == null)
        {
            return paramtwo;
        } else if (paramtwo == null)
        {
            return paramone;
        }
        int length1 = paramone.length;
        int length2 = paramtwo.length;
        int length = length1 + length2;
        String[] dest = new String[length];
        System.arraycopy(paramone, 0, dest, 0, length1);
        System.arraycopy(paramtwo, 0, dest, length1, length2);
        return dest;
    }

    /**
     * 判断paramone是否包含paramtwo
     * @param paramone
     * @param paramtwo
     * @return
     */
    public static <T> boolean arrayContains(T[] paramone, T paramtwo)
    {
        if (paramone == null || paramone.length == 0)
        {
            return false;
        }
        for (T t : paramone)
        {
            boolean result = (t == null && paramtwo == null) || (t != null && t.equals(paramtwo));
            if (result)
            {
                return result;
            }
        }
        return false;
    }

    /**
     * 将sourceTable中的数据复制到tempTable中，列对应关系在transMap中
     * @param tempTable
     * @param sourceTable
     * @param transMap  key:sourceTable 中的列名  value：tempTable中列名
     */
    public static void transDataTable(DataTable tempTable, DataTable sourceTable, Map<String, String> transMap)
    {
        if (tempTable == null || tempTable.getColumns() == null || tempTable.getColumns().isEmpty()
                || !CoreUtil.checkDataTableHasData(sourceTable) || transMap == null || transMap.isEmpty())
        {
            return;
        }
        DataRow newRow = null;
        for (DataRow row : sourceTable.getRows())
        {
            newRow = tempTable.newRow();
            for (Entry<String, String> entry : transMap.entrySet())
            {
                if (row.getItemMap().containsKey(entry.getKey()))
                {
                    newRow.setValue(entry.getValue(), row.getItemMap().get(entry.getKey()));
                }
            }
            tempTable.getRows().add(newRow);
        }
    }

    /**
     * 集合是否为空
     * @param coll
     * @return
     */
    public static boolean isEmpty(Collection<?> coll)
    {
        return coll == null || coll.isEmpty();
    }

    public static boolean isEmpty(Object obj)
    {
        String str = obj == null ? null : obj.toString();
        return str == null || str.length() == 0 || str.trim().length() == 0;
    }

    public static boolean isEmpty(Object[] arr)
    {
        return arr == null || arr.length == 0;
    }

    /**
     * Map是否为空
     * @param coll
     * @return
     */
    public static boolean isEmpty(Map<?, ?> coll)
    {
        return coll == null || coll.isEmpty();
    }

    /**
     * 创建一个短的UUID
     * @return
     */
    public static String newShortUuId()
    {
        UUID uuid = UUID.randomUUID();
        String result = uuid.toString();
        return result.substring(0, result.indexOf("-"));
    }
    
    /**
     * 创建一个UUID
     * @return
     */
    public static String newUuId()
    {
        UUID uuid = UUID.randomUUID();
        String primaryKey = uuid.toString().replaceAll("-", "");
        return primaryKey;
    }

    /**
     * 查找sourceTable中的行数据
     * @param sourceTable
     * @param queryMap
     * @return
     */
    public static DataRow lookForDataRow(DataTable sourceTable, Map<String, Object> queryMap)
    {
        if (!checkDataTableHasData(sourceTable) || isEmpty(queryMap))
        {
            return null;
        }
        sign: for (DataRow row : sourceTable.getRows())
        {
            for (Entry<String, Object> entry : queryMap.entrySet())
            {
                if (!CoreUtil.checkEqual(entry.getValue(), row.getItemMap().get(entry.getKey())))
                {
                    continue sign;
                }
            }
            return row;
        }
        return null;
    }

    /**
     * 调整时间
     * @param date
     * @param field  调整字段
     * @param amount  调整值
     * @return
     */
    public static Date adjustDate(Date date, int field, int amount)
    {
        if (date == null)
        {
            return date;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(field, amount);
        return calendar.getTime();
    }

    /**
     * 获取时间的指定字段值
     * @param date
     * @param field
     * @return
     */
    public static int getDateFileValue(Object date, int field)
    {
        Date temp = parseDate(date);
        if (temp == null)
        {
            return -1;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(temp);
        return cal.get(field);
    }

    /**
     * 去除所有括号
     * @param param
     * @return
     */
    public static String replaceAllKh(String param)
    {
        return StringUtil.replace(
                StringUtil.replace(StringUtil.replace(StringUtil.replace(param, "（", ""), "）", ""), ")", ""), "(", "");
    }

    /**
     * 中文汉字转数字
     * @param str
     * @return
     */
    public static int chineseToNumber(String str)
    {
        if (isEmpty(str))
        {
            return 0;
        }
        if (str.startsWith("十"))
        {
            str = "一" + str;
        }
        int result = 0;
        // 去除零
        str = str.replace("零", "");
        if (str.contains("亿"))
        {
            String yi = str.substring(0, str.indexOf("亿"));
            result += sectionChinese(yi) * 100000000;
            str = str.substring(str.indexOf("亿") + 1, str.length());
        }
        if (str.contains("万"))
        {
            String wan = str.substring(0, str.indexOf("万"));
            result += sectionChinese(wan) * 10000;
            str = str.substring(str.indexOf("万") + 1, str.length());
        }
        if (str.length() > 0)
        {
            result += sectionChinese(str);
        }
        return result;
    }

    private static int sectionChinese(String str)
    {
        int value = 0;
        int sectionNum = 0;
        for (int i = 0; i < str.length(); i++)
        {
            int v = (int) CHARINTMAP.get(str.charAt(i));
            if (v == 10 || v == 100 || v == 1000)
            {
                // 十位，百位，千位
                sectionNum = v * sectionNum;
                value = value + sectionNum;
            } else if (i == str.length() - 1)
            {
                // 个位
                value = value + v;
            } else
            {
                sectionNum = v;
            }
        }
        return value;
    }

    /**
     * 通过时间秒毫秒数判断两个时间的间隔
     * @param date1
     * @param date2
     * @return  date 2 - data 1  间隔天数
     */
    public static int differentDaysByMillisecond(Date date1, Date date2)
    {
        if (date1 == null || date2 == null)
        {
            return 0;
        }
        int days = (int) ((date2.getTime() - date1.getTime()) / (1000 * 3600 * 24));
        return days;
    }
    
    /**
     * 是否为数字
     * @param objIn
     * @return
     */
    public static boolean isNumber(Object objIn)
    {
        if (objIn == null)
        {
            return false;
        }

        try
        {
            double val = Double.parseDouble(objIn.toString());
            return !Double.isNaN(val);
        } catch (Exception e)
        {
        }
        return false;
    }

    public static DataTable copyDataTable(DataTable resultTable, boolean b)
    {
        return null;
    }

}
