package com.personal.core.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 日志工具类
 * @author cuibo
 *
 */
public final class LogUtil
{
    public static final Log LOG = LogFactory.getLog(LogUtil.class);

    public static final int DEBUG = 0;
    public static final int INFO = 1;
    public static final int WARN = 2;
    public static final int ERROR = 3;

    /**
     * 记录
     * @param e
     */
    public static void log(Exception e)
    {
        log(ERROR, e.getMessage(), e);
    }
    
    public static void debug(Object message, Throwable t)
    {
        log(DEBUG, message, t);
    }
    
    public static void info(Object message, Throwable t)
    {
        log(INFO, message, t);
    }
    
    public static void warn(Object message, Throwable t)
    {
        log(WARN, message, t);
    }
    
    public static void error(Object message, Throwable t)
    {
        log(ERROR, message, t);
    }
    
    /**
     * 记录
     * @param level
     * @param message
     * @param t
     */
    public static void log(int level, Object message, Throwable t)
    {
        switch (level)
        {
        case DEBUG:
            LOG.debug(message, t);
            break;
        case INFO:
            LOG.info(message, t);
            break;
        case WARN:
            LOG.warn(message, t);
            break;
        case ERROR:
            LOG.error(message, t);
            break;
        default:
            break;
        }
    }
}
