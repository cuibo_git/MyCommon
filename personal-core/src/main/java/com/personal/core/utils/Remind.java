package com.personal.core.utils;

import java.util.Collection;
import java.util.Map;

import com.personal.core.bean.MessageException;

/**
 * 提醒判断
 * @author cuibo
 *
 */
public abstract class Remind
{
    
    /**
     * 提醒空
     * @param obj
     * @param errorMessage
     */
    public static void isNullOrEmpty(String obj, String errorMessage) throws MessageException
    {
        if (!CoreUtil.isEmpty(obj))
        {
            throw new MessageException(errorMessage);
        }
    }
    
    /**
     * 是否为false
     * @param expression
     * @param message
     */
    public static void isFalse(boolean expression, String message) throws MessageException
    {
        if (expression)
        {
            throw new MessageException(message);
        }
    }

    /**
     * 提醒非空
     * @param obj
     * @param errorMessage
     */
    public static void isNotNull(Object obj, String errorMessage) throws MessageException
    {
        if (obj == null)
        {
            throw new MessageException(errorMessage);
        }
    }

    /**
     * 提醒集合非空或者非空集合
     * @param obj
     * @param errorMessage
     */
    public static void isNotNullOrEmpty(Collection<?> coll, String errorMessage) throws MessageException
    {
        if (coll == null || coll.isEmpty())
        {
            throw new MessageException(errorMessage);
        }
    }

    /**
     * 提醒Map非空或者非空Map
     * @param obj
     * @param errorMessage
     */
    public static void isNotNullOrEmpty(Map<?, ?> map, String errorMessage) throws MessageException
    {
        if (map == null || map.isEmpty())
        {
            throw new MessageException(errorMessage);
        }
    }

    /**
     * 提醒字符串非空或者非空串
     * @param obj
     * @param errorMessage
     */
    public static void isNotNullOrEmpty(Object obj, String errorMessage) throws MessageException
    {
        if (CoreUtil.isEmpty(obj))
        {
            throw new MessageException(errorMessage);
        }
    }

    /**
     * 提醒Map非空或者非空数据
     * @param obj
     * @param errorMessage
     */
    public static void isNotNullOrEmpty(Object[] objs, String errorMessage) throws MessageException
    {
        if (objs == null || objs.length == 0)
        {
            throw new MessageException(errorMessage);
        }
    }

    /**
     * 是否为true
     * @param expression
     * @param message
     */
    public static void isTrue(boolean expression, String message) throws MessageException
    {
        if (!expression)
        {
            throw new MessageException(message);
        }
    }
}
