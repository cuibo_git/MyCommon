package com.personal.core.utils;

import java.util.Collection;
import java.util.Map;

/**
 * 断言判断
 * @author cuibo
 *
 */
public abstract class Assert
{
    
    /**
     * 断言为空
     * @param obj
     * @param errorMessage
     */
    public static void isNullOrEmpty(String obj, String errorMessage)
    {
        if (!CoreUtil.isEmpty(obj))
        {
            throw new IllegalArgumentException(errorMessage);
        }
    }
    
    /**
     * 是否为false
     * @param expression
     * @param message
     */
    public static void isFalse(boolean expression, String message)
    {
        if (expression)
        {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * 断言非空
     * @param obj
     * @param errorMessage
     */
    public static void isNotNull(Object obj, String errorMessage)
    {
        if (obj == null)
        {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    /**
     * 断言集合非空或者非空集合
     * @param obj
     * @param errorMessage
     */
    public static void isNotNullOrEmpty(Collection<?> coll, String errorMessage)
    {
        if (coll == null || coll.isEmpty())
        {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    /**
     * 断言Map非空或者非空Map
     * @param obj
     * @param errorMessage
     */
    public static void isNotNullOrEmpty(Map<?, ?> map, String errorMessage)
    {
        if (map == null || map.isEmpty())
        {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    /**
     * 断言字符串非空或者非空串
     * @param obj
     * @param errorMessage
     */
    public static void isNotNullOrEmpty(Object obj, String errorMessage)
    {
        if (CoreUtil.isEmpty(obj))
        {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    /**
     * 断言Map非空或者非空数据
     * @param obj
     * @param errorMessage
     */
    public static void isNotNullOrEmpty(Object[] objs, String errorMessage)
    {
        if (objs == null || objs.length == 0)
        {
            throw new IllegalArgumentException(errorMessage);
        }
    }

    /**
     * 是否为true
     * @param expression
     * @param message
     */
    public static void isTrue(boolean expression, String message)
    {
        if (!expression)
        {
            throw new IllegalArgumentException(message);
        }
    }
}
