package com.personal.dataconvert;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.personal.core.data.DataRow;
import com.personal.core.data.DataTable;
import com.personal.core.utils.Assert;
import com.personal.core.utils.CoreUtil;
import com.personal.dataconvert.bean.CombineColumnConfig;
import com.personal.dataconvert.bean.HeaderConfig;
import com.personal.dataconvert.bean.HtmlConfig;
import com.personal.dataconvert.port.Data2Html;
import com.personal.dataconvert.util.ExcelHtmlUtil;

/**
 * 不规则表转Html工具类 只会获取表体的字符串
 * 使用字段DATA,COLINDEX,ROWINDEX,MERGEROWCOUNT,MERGECOLCOUNT ,行号。列号，均从零开始
 * DataTable中的tag设置table 的样式 DataRow 中可以加 columnName + TDSTYLE 设置单元格样式 DataRow
 * 中可以加 columnName + TDCLICK 设置点击事件 有点击事件时会拼接<a> 链接
 * @author cuibo
 */
public class IrregularDataTable2Html implements Data2Html
{
	/** 报表数据 */
	private DataTable table;

	/** 隐藏列 */
	private Set<String> hideColumns;

	/** 合并列配置 */
	private Set<CombineColumnConfig> combineColumnConfigs;

	/** splitFlag 表头分割标记。默认是点 */
	private String splitFlag = ".";

	/** 表头的动态注入 */
	private List<? extends HeaderConfig> headerConfigs = null;

	/** 是否设置title */
	private boolean withTitle = true;

	private IrregularDataTable2Html()
	{
	}

	private IrregularDataTable2Html(Builder builder)
	{
		this.table = builder.table;
		this.hideColumns = builder.hideColumns;
		this.splitFlag = builder.splitFlag;
		this.combineColumnConfigs = builder.combineColumnConfigs;
		this.headerConfigs = builder.headerConfigs;
		this.withTitle = builder.withTitle;
	}

	@Override
	public String createHeader() throws Exception
	{
		// 构建htmlConfig
		HtmlConfig htmlConfig = createHtmlConfig();
		Assert.isNotNull(htmlConfig, "构架htmlConfig失败！");
		List<?> allObjectData = loadAllObjectData();
		Data2Html data2Html = new ExportData2Html.Builder().setAllObjectData(allObjectData).setHtmlConfig(htmlConfig).build();
		return data2Html.createHeader();
	}

	@Override
	public String createBody() throws Exception
	{
		// 构建htmlConfig
		HtmlConfig htmlConfig = createHtmlConfig();
		List<?> allObjectData = loadAllObjectData();
		Assert.isNotNull(htmlConfig, "构架htmlConfig失败！");
		Data2Html data2Html = new ExportData2Html.Builder().setAllObjectData(allObjectData).setHtmlConfig(htmlConfig).build();
		return data2Html.createBody();
	}

	@Override
	public String createHtml() throws Exception
	{
		// 构建htmlConfig
		HtmlConfig htmlConfig = createHtmlConfig();
		List<?> allObjectData = loadAllObjectData();
		Assert.isNotNull(htmlConfig, "构架htmlConfig失败！");
		Data2Html data2Html = new ExportData2Html.Builder().setAllObjectData(allObjectData).setHtmlConfig(htmlConfig).build();
		return data2Html.createHtml();
	}

	private List<?> loadAllObjectData()
	{
		if (table.getRows() == null || table.getRows().isEmpty())
		{
			return null;
		}
		List<Object> result = new ArrayList<Object>();
		for (DataRow object : table.getRows())
		{
			result.add(object.getItemMap());
		}
		return result;
	}

	/**
	 * 构建HtmlConfig
	 * @return
	 * @throws Exception
	 */
	private HtmlConfig createHtmlConfig() throws Exception
	{
		HtmlConfig result = new HtmlConfig();
		result.setName(table.getTableName());
		List<? extends HeaderConfig> headerConfigs = this.headerConfigs == null ? ExcelHtmlUtil.createTrees(table, splitFlag, hideColumns) : this.headerConfigs;
		Assert.isNotNullOrEmpty(headerConfigs, "构建" + table.getTableName() + "的表头节点失败！");
		result.setTableClass(CoreUtil.parseStr(table.getTag()));
		result.setHeaderConfigs(headerConfigs);
		result.setCombineColumnConfigs(combineColumnConfigs);
		result.setWithTitle(withTitle);
		return result;
	}
	

	/**
	 * 建造
	 * @author cuibo
	 */
	public static class Builder
	{
		private DataTable table;
		private String splitFlag = ".";
		private Set<String> hideColumns;
		private Set<CombineColumnConfig> combineColumnConfigs;
		private List<? extends HeaderConfig> headerConfigs = null;
		private boolean withTitle = true;

		public Builder setDataTable(DataTable table)
		{
			this.table = table;
			return this;
		}

		public Builder setSplitFlag(String splitFlag)
		{
			this.splitFlag = splitFlag;
			return this;
		}

		public Builder setWithTitle(boolean withTitle)
		{
			this.withTitle = withTitle;
			return this;
		}

		public Builder setHideColumns(Set<String> hideColumns)
		{
			this.hideColumns = hideColumns;
			return this;
		}

		public Builder setCombineColumnConfigs(Set<CombineColumnConfig> combineColumnConfigs)
		{
			this.combineColumnConfigs = combineColumnConfigs;
			return this;
		}

		public Builder setHeaderConfigs(List<? extends HeaderConfig> headerConfigs)
		{
			this.headerConfigs = headerConfigs;
			return this;
		}

		public IrregularDataTable2Html build()
		{
			return new IrregularDataTable2Html(this);
		}
	}

}
