package com.personal.dataconvert;

import java.io.InputStream;
import java.io.PushbackInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.POIXMLDocument;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.personal.core.utils.CoreUtil;
import com.personal.dataconvert.util.ExcelHtmlUtil;

/**
 * poi-3.8
 * 兼容 xls 和 xlsx
 */
public class Excel2Html
{
    private int sumWidth;
    
    /** 流信息 */
    private InputStream in;
    
    /** Sheet页名称 */
    private String sheetName;
    
    /** 是否需要样式 */
    private boolean hasStyle = true;
    
    
    public Excel2Html(InputStream in, String sheetName)
    {
        this.in = in;
        this.sheetName = sheetName;
    }
    
    public Excel2Html(InputStream in, String sheetName, boolean hasStyle)
    {
        this.in = in;
        this.sheetName = sheetName;
        this.hasStyle = hasStyle;
    }
    
    /**
     * 获取HTML字符串
     * @param in
     * @param sheetName
     * @return
     */
    public StringBuffer getHmtlBuf()
    {
        Workbook workbook = readExcelFile(in);
        String exceltitle = getFirstRowContent(workbook, sheetName);
        StringBuffer htmlbuf = new StringBuffer("");
        htmlbuf.append(headerHtmlStart(exceltitle));
        htmlbuf.append(headerHtmlEnd());
        htmlbuf.append(bodyHtml(workbook));
        htmlbuf.append(bodyHtmlEnd());
        return htmlbuf;
    }

    private String getBorderStyle(String bortype, String borcolor)
    {
        String borstyle = "";
        if ("thin".equals(bortype))
        {
            borstyle = "1px solid " + borcolor;
        } else if ("medium".equals(bortype))
        {
            borstyle = "2px solid " + borcolor;
        } else if ("double".equals(bortype))
        {
            borstyle = "double 2.25pt " + borcolor;
        } else if ("dotted".equals(bortype))
        {
            borstyle = "1px dotted " + borcolor;
        } else if ("dashed".equals(bortype))
        {
            borstyle = "1px dashed " + borcolor;
        }
        return borstyle;
    }

    /**
     * 获取某Sheet，第一行的内容
     *
     * @param workbook
     * @param sheetindex
     * @return
     */
    private String getFirstRowContent(Workbook workbook, String sheetName)
    {
        String exceltitle = "";
        if (workbook != null)
        {
            Sheet sheet = workbook.getSheet(sheetName);
            int firstrownum = sheet.getFirstRowNum();
            Row row = sheet.getRow(firstrownum);
            short fcellnum = row.getFirstCellNum();
            short lcellnum = row.getLastCellNum();
            for (int j = fcellnum; j < lcellnum; j++)
            {
                Cell cell = row.getCell(j);
                exceltitle += getStringCellValue(cell);
            }
        }
        return exceltitle;
    }

    @SuppressWarnings(
    { "rawtypes", "unchecked" })
    private List getRangeRows(Sheet sheet)
    {
        if (sheet != null)
        {
            int rangenum = sheet.getNumMergedRegions();//找到当前sheet单元格中共有多少个合并区域
            ArrayList list = new ArrayList();
            if (rangenum > 0)
            {
                for (int i = 0; i < rangenum; i++)
                {
                    CellRangeAddress range = sheet.getMergedRegion(i);//一个合并单元格代表 CellRangeAddress
                    list.add(range.getFirstRow());
                    list.add(range.getLastRow());
                    list.add(range.getLastColumn());
                    list.add(range.getFirstColumn());

                    // range.
                    // range.isInRange(rowInd, colInd)()

                }
                Collections.sort(list);
                removeDuplicateWithOrder(list);
                return list;
            }
        }
        return null;
    }

    /**
     * 获取Cell的内容
     *
     * @param cell
     * @return
     */
    private String getStringCellValue(Cell cell)
    {
        String cellvalue = "";
        if (cell == null)
        {
            return "";
        }
        switch (cell.getCellType())
        {
        case Cell.CELL_TYPE_STRING:
            cellvalue = cell.getStringCellValue();
            break;
        case Cell.CELL_TYPE_NUMERIC:
            cellvalue = String.valueOf(cell.getNumericCellValue());
            break;
        case Cell.CELL_TYPE_BOOLEAN:
            cellvalue = String.valueOf(cell.getBooleanCellValue());
            break;
        case Cell.CELL_TYPE_BLANK:
            cellvalue = "";
            break;
        default:
            cellvalue = "";
            break;
        }
        if (cellvalue.equals(""))
        {
            return "";
        }
        return cellvalue;
    }

    private int getSumWidth()
    {
        return sumWidth;
    }

    /**
     * 读取excel
     *
     * @param filepath
     *            excel文件地址
     * @return Workbook
     */
    private Workbook readExcelFile(InputStream inputStream)
    {

        Workbook workbook = null;
        try
        {
            //判断输入流是否支持mark()和reset()方法
            if (!inputStream.markSupported())
            {
                inputStream = new PushbackInputStream(inputStream, 8);
            }
            //判断文件是否为excel03文件
            if (POIFSFileSystem.hasPOIFSHeader(inputStream))
            {
                workbook = new HSSFWorkbook(inputStream);
            }
            //判断文件是否为excel07文件
            else if (POIXMLDocument.hasOOXMLHeader(inputStream))
            {
                workbook = new XSSFWorkbook(OPCPackage.open(inputStream));
            }

            if (workbook == null)
            {
                throw new Exception("文件错误，导入的不是标准Excel文件！");
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        } finally
        {
            ExcelHtmlUtil.release(inputStream);
        }
        return workbook;
    }

    /** List order maintained **/
    @SuppressWarnings(
    { "unchecked", "rawtypes" })
    private void removeDuplicateWithOrder(ArrayList arlList)
    {
        Set set = new HashSet();
        List newList = new ArrayList();
        for (Iterator iter = arlList.iterator(); iter.hasNext();)
        {
            Object element = iter.next();
            if (set.add(element))
            {
                newList.add(element);
            }
        }
        arlList.clear();
        arlList.addAll(newList);
    }

    @SuppressWarnings("unused")
    private StringBuffer bodyHtml(Workbook workbook)
    {
        Sheet sheet = workbook.getSheet(sheetName);
        // 显示转换行数
        int lastRowNum = sheet.getLastRowNum();
        if (lastRowNum > 600)
        {
            lastRowNum = 600;
        }
        Row row1 = null;
        int lastColNums = 0;
        for (int rowNum = sheet.getFirstRowNum(); rowNum <= lastRowNum; rowNum++)
        {
            row1 = sheet.getRow(rowNum);
            for (int colNum = 0; colNum < lastColNums; colNum++)
            {
                row1.getCell(colNum);
                int tdwidth = sheet.getColumnWidth(colNum) / 32;
                sumWidth += tdwidth;
            }
            break;
        }

        StringBuffer sb = new StringBuffer("");
        sb.append("<table width=\"" + getSumWidth()
                + "px\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"width:99%\" >\n");
        sb.append("<thead>  \n");
        sb.append("</thead>\n");
        sb.append("<tbody>\n");
        sb.append(excelToHtmlJs(workbook, sheetName, lastRowNum));
        sb.append("</tbody>\n");
        sb.append("</table>\n");
        return sb;
    }

    private StringBuffer bodyHtmlEnd()
    {
        StringBuffer sb = new StringBuffer("");
        sb.append("</body>\n");
        sb.append("</html>\n");
        return sb;
    }

    private String convertAlignToHtml(short alignment)
    {

        String align = "left";

        switch (alignment)
        {

        case CellStyle.ALIGN_LEFT:
            align = "left";
            break;
        case CellStyle.ALIGN_CENTER:
            align = "center";
            break;
        case CellStyle.ALIGN_RIGHT:
            align = "right";
            break;

        default:
            break;
        }

        return align;
    }

    private String convertBorderColorToHtml(short bordercolor)
    {
        String type = "black";
        if (HSSFColor.BLACK.index == bordercolor)
        {
            type = "black";
        } else if (HSSFColor.BLUE.index == bordercolor) 
        {
            type = "blue";
        } else if (HSSFColor.RED.index == bordercolor) 
        {
            type = "red";
        }
        return type;
    }

    private String convertBorderStyleToHtml(short bordertype)
    {
        String type = "none";
        switch (bordertype)
        {

        case CellStyle.BORDER_THIN:
            type = "thin";
            break;
        case CellStyle.BORDER_DOTTED:
            type = "dotted";
            break;
        case CellStyle.BORDER_DASHED:
            type = "dashed";
            break;
        case CellStyle.BORDER_NONE:
            type = "none";
            break;
        case CellStyle.BORDER_MEDIUM:
            type = "medium";
            break;
        case CellStyle.BORDER_DOUBLE:
            type = "double";
            break;

        default:
            break;
        }

        return type;
    }

    private String convertToStardColor(HSSFColor hc)
    {
        StringBuffer sb = new StringBuffer("");
        if (hc != null)
        {

            if (HSSFColor.AUTOMATIC.index == hc.getIndex())
            {

                return null;
            }

            sb.append("#");

            for (int i = 0; i < hc.getTriplet().length; i++)
            {
                sb.append(fillWithZero(Integer.toHexString(hc.getTriplet()[i])));
            }
        }
        return sb.toString();
    }

    private String convertVerticalAlignToHtml(short verticalAlignment)
    {

        String valign = "middle";

        switch (verticalAlignment)
        {

        case CellStyle.VERTICAL_BOTTOM:
            valign = "bottom";
            break;
        case CellStyle.VERTICAL_CENTER:
            valign = "center";
            break;
        case CellStyle.VERTICAL_TOP:
            valign = "top";
            break;
        default:
            break;
        }

        return valign;
    }

    @SuppressWarnings("rawtypes")
    private StringBuffer excelToHtmlJs(Workbook workbook, String sheetName, int lastRowNum)
    {
        Sheet sheet = workbook.getSheet(sheetName);
        Map<String, String>[] map = getRowSpanColSpanMap(sheet);
        Row row = null;
        Cell cell = null;
        List rangennumList = getRangeRows(sheet);
        int rangenum = 2;
        if (rangennumList != null && rangennumList.size() > 0)
        {
            rangenum = rangennumList.size();
        }
        if (rangenum == 2)
        {
            rangenum += 1;
        }
        if (rangenum == 3)
        {
            rangenum += 1;
        }
        if (rangenum == 7)
        {
            rangenum -= 2;
        }
        if (rangenum == 12)
        {
            rangenum -= 6;
        }
        if (rangenum == 13)
        {
            rangenum -= 7;
        }
        if (rangenum == 8)
        {
            rangenum -= 3;
        }
        if (rangenum == 11)
        {
            rangenum -= 5;
        }
        if (rangenum == 10)
        {
            rangenum -= 4;
        }
        if (rangenum == 9)
        {
            rangenum -= 3;
        }
        if (rangenum == 14)
        {
            rangenum -= 8;
        }
        if (rangenum == 18)
        {
            rangenum /= 3;
        }
        if (rangenum == 15)
        {
            rangenum -= 9;
        }
        if (rangenum == 16)
        {
            rangenum -= 10;
        }
        if (rangenum == 17)
        {
            rangenum -= 12;
        }
        StringBuffer strbuf = new StringBuffer("");
        short borderColor = 0;
        short bordertop = 0;
        short borderleft = 0;
        short borderright = 0;
        short borderbottom = 0;
        short colorbottom = 0;
        short colortop = 0;
        short colorleft = 0;
        short colorright = 0;
        String borstyletop = "";
        String borstyleleft = "";
        String borstyleright = "";
        String borstylebottom = "";
        String bortopcolor = "";
        String borleftcolor = "";
        String borrightcolor = "";
        String borbottomcolor = "";
        for (int rowNum = sheet.getFirstRowNum(); rowNum <= lastRowNum; rowNum++)
        {
            row = sheet.getRow(rowNum);
            if (row == null)
            {
                strbuf.append("<tr><td > &nbsp;</td></tr>");
                continue;
            }
            float heightval = row.getHeightInPoints() + 8f;
            strbuf.append("<tr height=\"" + heightval + "px\">");
            int lastColNum = row.getLastCellNum();
            for (int colNum = 0; colNum < lastColNum; colNum++)
            {
                borderColor = 0;
                bordertop = 0;
                borderleft = 0;
                borderright = 0;
                borderbottom = 0;
                colorbottom = 0;
                colortop = 0;
                colorleft = 0;
                colorright = 0;
                borstyletop = "";
                borstyleleft = "";
                borstyleright = "";
                borstylebottom = "";
                bortopcolor = "";
                borleftcolor = "";
                borrightcolor = "";
                borbottomcolor = "";
                cell = row.getCell(colNum);
                int tdwidth = sheet.getColumnWidth(colNum) / 32;
                if (cell == null)
                {
                    cell = row.createCell(colNum);
                    cell.setCellValue("");//strbuf.append("<td>&nbsp;</td>");//continue;
                }
                CellStyle cellStyle = cell.getCellStyle();
                cellStyle.setWrapText(true);
                cell.setCellStyle(cellStyle);
                String stringValue = getCellValue(cell);
                if (map[0].containsKey(rowNum + "," + colNum))
                {
                    String pointString = map[0].get(rowNum + "," + colNum);
                    map[0].remove(rowNum + "," + colNum);
                    int bottomeRow = Integer.valueOf(pointString.split(",")[0]);
                    int bottomeCol = Integer.valueOf(pointString.split(",")[1]);
                    int rowSpan = bottomeRow - rowNum + 1;
                    int colSpan = bottomeCol - colNum + 1;
                    strbuf.append("<td width=\"" + getTdWidth(sheet, colNum, bottomeCol)
                            + "px\" rowspan=\"" + rowSpan + "\" colspan=\"" + colSpan + "\" ");
                    Row tempRow = sheet.getRow(bottomeRow);
                    if (rowSpan > 1 && tempRow != null)
                    {
                        Cell tempcell = tempRow.getCell(colNum);
                        if (tempcell != null)
                        {
                            borderbottom = tempcell.getCellStyle().getBorderBottom();
                            colorbottom = tempcell.getCellStyle().getBottomBorderColor();
                        }
                    }
                    if (colSpan > 1)
                    {
                        Cell tempcell = row.getCell(bottomeCol);
                        if (tempcell != null)
                        {
                            borderright = tempcell.getCellStyle().getBorderRight();
                            colorright = tempcell.getCellStyle().getRightBorderColor();
                        }
                    } 
                } else if (map[1].containsKey(rowNum + "," + colNum))
                { 
                    //被合并掉的单元格
                    map[1].remove(rowNum + "," + colNum);
                    if (rowNum == 20 && colNum == 3)
                    { 
                    }
                    continue;
                } else
                {
                    strbuf.append("<td width=\"" + tdwidth + "px\" ");
                }
                if (cellStyle != null)
                {
                    short alignment = cellStyle.getAlignment();
                    strbuf.append("align=\"" + convertAlignToHtml(alignment) + "\" ");
                    short verticalAlignment = cellStyle.getVerticalAlignment();
                    strbuf.append("valign=\"" + convertVerticalAlignToHtml(verticalAlignment) + "\" ");
                    Font hf = null;
                    if (cellStyle instanceof HSSFCellStyle)
                    {
                        HSSFCellStyle style = (HSSFCellStyle) cellStyle;
                        hf = style.getFont(workbook);
                    } else if (cellStyle instanceof XSSFCellStyle)
                    {
                        XSSFCellStyle style = (XSSFCellStyle) cellStyle;
                        hf = style.getFont();
                    }
                    short boldWeight = hf.getBoldweight();
                    short fontColor = hf.getColor();
                    if (hasStyle)
                    {
                        strbuf.append("style=\"");
                        HSSFPalette palette = null;
                        if (workbook instanceof HSSFWorkbook)
                        {
                            HSSFWorkbook book = (HSSFWorkbook) workbook;
                            palette = book.getCustomPalette();
                        }
                        HSSFColor hc = null;
                        if (palette != null)
                        {
                            hc = palette.getColor(fontColor);
                        }
                        strbuf.append("font-weight:" + boldWeight + ";"); //字体加粗//sb.append("font-size: " + hf.getFontHeight()/2 + "%;");// //字体大小
                        strbuf.append("font-size: " + hf.getFontHeightInPoints() + "pt;");
                        strbuf.append("font-family:" + hf.getFontName() + ";");
                        String fontColorStr = convertToStardColor(hc);
                        if (fontColorStr != null && !"".equals(fontColorStr.trim()))
                        {
                            strbuf.append("color:" + fontColorStr + ";"); // 字体颜色
                        }
                        short bgColor = cellStyle.getFillForegroundColor();
                        if (palette != null)
                        {
                            hc = palette.getColor(bgColor);
                        }
                        String bgColorStr = convertToStardColor(hc);
                        if (bgColorStr != null && !"".equals(bgColorStr.trim()))
                        {
                            strbuf.append("background-color:" + bgColorStr + ";"); // 背景颜色
                        }
                        bordertop = cellStyle.getBorderTop();
                        borderleft = cellStyle.getBorderLeft();
                        if (borderright == 0)
                        {
                            borderright = cellStyle.getBorderRight();
                        }
                        if (borderbottom == 0)
                        {
                            borderbottom = cellStyle.getBorderBottom();
                        }
                        if (colorbottom == 0)
                        {
                            colorbottom = cellStyle.getBottomBorderColor();
                        }
                        colortop = cellStyle.getTopBorderColor();
                        colorleft = cellStyle.getLeftBorderColor();
                        if (colorright == 0)
                        {
                            colorright = cellStyle.getRightBorderColor();
                        }
                        borstyletop = convertBorderStyleToHtml(bordertop);
                        borstyleleft = convertBorderStyleToHtml(borderleft);
                        borstyleright = convertBorderStyleToHtml(borderright);
                        borstylebottom = convertBorderStyleToHtml(borderbottom);
                        bortopcolor = convertBorderColorToHtml(colortop);
                        borleftcolor = convertBorderColorToHtml(colorleft);
                        borrightcolor = convertBorderColorToHtml(colorright);
                        borbottomcolor = convertBorderColorToHtml(colorbottom);
                        strbuf.append("border-top:" + getBorderStyle(borstyletop, bortopcolor) + ";");
                        strbuf.append("border-left:" + getBorderStyle(borstyleleft, borleftcolor) + ";");
                        strbuf.append("border-right:" + getBorderStyle(borstyleright, borrightcolor) + ";");
                        strbuf.append("border-bottom:" + getBorderStyle(borstylebottom, borbottomcolor) + ";");
                        if (palette != null)
                        {
                            hc = palette.getColor(borderColor);
                        }
                        String borderColorStr = convertToStardColor(hc);
                        if (borderColorStr != null && !"".equals(borderColorStr.trim()))
                        {
                        }
                        strbuf.append("\" ");
                    }
                }
                strbuf.append(">");
                if (stringValue == null || "".equals(stringValue.trim()))
                { //             String picStr = getIncludePicStr(rowNum,colNum,workbook);//             strbuf.append(picStr);//                if ( picStr.trim().length() <= 0)//             {//                 strbuf.append(" &nbsp; ");//              }
                } else
                {
                    strbuf.append(stringValue);
                }
                strbuf.append("</td>");
            }
            strbuf.append("</tr>");
        }
        return strbuf;
    }

    private String fillWithZero(String str)
    {

        if (str != null && str.length() < 2)
        {

            return "0" + str;
        }
        return str;
    }

    private String getCellValue(Cell cell)
    {
     // 如果该单元格不存在则返回空
        if (null == cell)
        {
            return "";
        }

        // 根据不同的数据类型，对数据进行格式化
        String rValue = "";
        switch (cell.getCellType())
        {
        // 空单元格
        case Cell.CELL_TYPE_BLANK:
            rValue = "";
            break;
        case Cell.CELL_TYPE_BOOLEAN:
            rValue = String.valueOf(cell.getBooleanCellValue());
            break;
        // 数字或日期单元格
        case Cell.CELL_TYPE_NUMERIC:
            if (DateUtil.isCellDateFormatted(cell))
            {
                rValue = String.format("%tF", cell.getDateCellValue());
            } else
            {
                rValue = String.valueOf(cell.getNumericCellValue());
                if (!CoreUtil.isEmpty(rValue))
                {
                    rValue = new BigDecimal(rValue).toPlainString();
                }
                if (rValue.contains("."))
                {
                    if (rValue.endsWith(".0"))
                    { // 如果本来就是不带小数的数字（解析后会自动加上 .0 ）
                      // 去掉 .0 （wbs编码可能会解析成带.0的数字，必须去掉.0）
                        rValue = rValue.substring(0, rValue.lastIndexOf("."));
                    }
                    // 如果小数点后长度大于2,则进行四舍五入取小数点后两位
                    else if (rValue.length() - rValue.lastIndexOf(".") - 1 > 2)
                    {
                        rValue = new BigDecimal(rValue).setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString();
                    }
                }

            }
            break;
        // 字符串单元格
        case Cell.CELL_TYPE_STRING:
            rValue = cell.getStringCellValue();
            // // 将不合法的字符进行替换
            // rValue = ValueDictionaryApp.replaceInvalidString(rValue);
            break;

        // 公式格式
        case Cell.CELL_TYPE_FORMULA:
            if (Cell.CELL_TYPE_STRING == cell.getCachedFormulaResultType())
            {
                rValue = cell.getStringCellValue();
            } else if (Cell.CELL_TYPE_NUMERIC == cell.getCachedFormulaResultType())
            {
                if (DateUtil.isCellDateFormatted(cell))
                {
                    rValue = String.format("%tF", cell.getDateCellValue());
                } else
                {

                    rValue = String.valueOf(cell.getNumericCellValue());

                    if (!CoreUtil.isEmpty(rValue))
                    {
                        rValue = new BigDecimal(rValue).toPlainString();
                    }

                    if (rValue.contains("."))
                    {
                        if (rValue.endsWith(".0"))
                        { // 如果本来就是不带小数的数字（解析后会自动加上 .0 ）
                          // 去掉 .0 （纯数字单元格可能会解析成带.0的数字，必须去掉.0）
                            rValue = rValue.substring(0, rValue.indexOf("."));
                        }
                        // 如果小数点后长度大于4,则进行四舍五入取小数点后四位
                        if (rValue.length() - rValue.lastIndexOf(".") - 1 > 4)
                        {
                            rValue = new BigDecimal(rValue).setScale(4, BigDecimal.ROUND_HALF_UP).toPlainString();
                        }
                    }
                }
            }
            break;
        default:
            break;
        }
        return rValue;
    }

    @SuppressWarnings(
    { "unchecked", "rawtypes" })
    private Map<String, String>[] getRowSpanColSpanMap(Sheet sheet)
    {

        Map<String, String> map0 = new HashMap<String, String>();//没有合并的单元格
        Map<String, String> map1 = new HashMap<String, String>();//合并的单元格

        int mergedNum = sheet.getNumMergedRegions();
        CellRangeAddress range = null;

        for (int i = 0; i < mergedNum; i++)
        {

            range = sheet.getMergedRegion(i);

            int topRow = range.getFirstRow();

            int topCol = range.getFirstColumn();

            int bottomRow = range.getLastRow();

            int bottomCol = range.getLastColumn();

            map0.put(topRow + "," + topCol, bottomRow + "," + bottomCol);

            int tempRow = topRow;

            while (tempRow <= bottomRow)
            {

                int tempCol = topCol;

                while (tempCol <= bottomCol)
                {

                    map1.put(tempRow + "," + tempCol, "");

                    tempCol++;
                }

                tempRow++;
            }

            map1.remove(topRow + "," + topCol);

        }

        Map[] map =
        { map0, map1 };

        return map;
    }

    //获取td的宽度
    private int getTdWidth(Sheet sheet, int startCol, int endCol)
    {
        int tdwidth = 0;
        for (int i = startCol; i <= endCol; i++)
        {
            int tempwidth = sheet.getColumnWidth(i) / 32;
            tdwidth = tdwidth + tempwidth;

        }
        return tdwidth;
    }

    private StringBuffer headerHtmlEnd()
    {
        StringBuffer sb = new StringBuffer("");
        sb.append("</head>\n");
        sb.append("<body>\n");// 进入页面就刷新下
        return sb;
    }

    private StringBuffer headerHtmlStart(String title)
    {
        StringBuffer sb = new StringBuffer("");
        sb.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
        sb.append("<html>\n");
        sb.append("<head>\n");
        sb.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n");
        sb.append("<META HTTP-EQUIV=\"pragma\" CONTENT=\"no-cache\">\n");
        sb.append("<META HTTP-EQUIV=\"Cache-Control\" CONTENT=\"no-cache, must-revalidate\">\n");
        sb.append("<META HTTP-EQUIV=\"expires\" CONTENT=\"0\">\n");
        sb.append("<title>" + title + "</title>\n");
        return sb;
    }

    //private static  String getIncludePicStr( int rowNum,int colNum,Workbook workbook )
    //{
    //	 String str = "";
    //	 try
    //	 {
    //		if ( map.get(rowNum + ":" + colNum) != null )
    //		{
    //			str = "<img style=\"vertical-align:middle\" align=\"absmiddle\" src=\"" + map.get(rowNum + ":" + colNum) + "\">";
    //		}
    //	} catch (Exception e)
    //	{
    //		e.printStackTrace();
    //	}
    //	 return str;
    // }
}
