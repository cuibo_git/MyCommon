package com.personal.dataconvert.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.personal.core.utils.CoreUtil;

/**
 *
 * @author cuibo
 *
 */
public class WorkBookConfig implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 唯一ID */
    private String id;

    /** Sheet页配置 */
    private List<SheetConfig> sheetConfigs;

    /** 样式配置 */
    private StyleConfig styleConfig;

    public String getId()
    {
        return id;
    }

    /**
     * 通过Sheet名获取SheetConfig
     * @param sheetName
     * @return
     */
    public SheetConfig getSheetConfig(String sheetName)
    {
        if (sheetConfigs == null || sheetConfigs.isEmpty() || CoreUtil.isEmpty(sheetName))
        {
            return null;
        }
        for (SheetConfig sheetConfig : sheetConfigs)
        {
            if (sheetName.equals(sheetConfig.getName()))
            {
                return sheetConfig;
            }
        }
        return null;
    }

    public List<SheetConfig> getSheetConfigs()
    {
        if (sheetConfigs == null)
        {
            sheetConfigs = new ArrayList<SheetConfig>();
        }
        return sheetConfigs;
    }

    public StyleConfig getStyleConfig()
    {
        if (styleConfig == null)
        {
            styleConfig = new StyleConfig();
        }
        return styleConfig;
    }

    public SheetConfig newSheetConfig()
    {
        SheetConfig sheetConfig = new SheetConfig();
        sheetConfig.setBookConfig(this);
        return sheetConfig;
    }

    public SheetConfig newSheetConfig(String sheetName)
    {
        SheetConfig sheetConfig = new SheetConfig();
        sheetConfig.setName(sheetName);
        sheetConfig.setBookConfig(this);
        return sheetConfig;
    }

    public StyleConfig newStyleConfig()
    {
        StyleConfig styleConfig = new StyleConfig();
        styleConfig.setBookConfig(this);
        return styleConfig;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setSheetConfigs(List<SheetConfig> sheetConfigs)
    {
        this.sheetConfigs = sheetConfigs;
    }

    public void setStyleConfig(StyleConfig styleConfig)
    {
        this.styleConfig = styleConfig;
    }

}
