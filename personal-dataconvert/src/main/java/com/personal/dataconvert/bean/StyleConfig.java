package com.personal.dataconvert.bean;

import java.io.Serializable;

import com.personal.dataconvert.IrregularDataTable2Excel;

/**
 * Sheet页样式配置
 * @author cuibo
 *
 */
public class StyleConfig implements Serializable
{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 表体字体：默认Asial */
    private String bodyFont = "Asial";

    /** 表体字体大小：默认11 */
    private int bodyFontSize = 11;

    /** 所属bookConfig */
    private WorkBookConfig bookConfig;

    /** 是否加粗 */
    private boolean headerBold = false;

    /** 表头字体 ：默认 楷体 */
    private String headerFont = "";

    /** 表头字体大小：默认12 */
    private int headerFontSize = 12;
    
    /** 表头高度 */
    private int headerHeight = 0;

    /** 是否加粗 */
    private boolean titleBold = true;

    /** title字体 ：默认 Asial */
    private String titleFont = "Asial";

    /** title字体大小：默认16 */
    private int titleFontSize = 16;

    /** title高度：默认1000 */
    private int titleHeight = 1000;
    
    /** 表体行高度：默认是默认值 */
    private int bodyHeight = 0;

    public String getBodyFont()
    {
        return bodyFont;
    }

    public WorkBookConfig getBookConfig()
    {
        return bookConfig;
    }

    public String getHeaderFont()
    {
        return headerFont;
    }

    public String getTitleFont()
    {
        return titleFont;
    }


    public boolean isHeaderBold()
    {
        return headerBold;
    }

    public boolean isTitleBold()
    {
        return titleBold;
    }

    public void setBodyFont(String bodyFont)
    {
        this.bodyFont = bodyFont;
    }

    public void setBookConfig(WorkBookConfig bookConfig)
    {
        this.bookConfig = bookConfig;
    }

    public void setHeaderBold(boolean headerBold)
    {
        this.headerBold = headerBold;
    }

    public void setHeaderFont(String headerFont)
    {
        this.headerFont = headerFont;
    }

    public void setTitleBold(boolean titleBold)
    {
        this.titleBold = titleBold;
    }

    public void setTitleFont(String titleFont)
    {
        this.titleFont = titleFont;
    }

    public int getHeaderHeight()
    {
        return headerHeight;
    }

    public void setHeaderHeight(int headerHeight)
    {
        this.headerHeight = headerHeight;
    }

    public int getBodyFontSize()
    {
        return bodyFontSize;
    }

    public void setBodyFontSize(int bodyFontSize)
    {
        this.bodyFontSize = bodyFontSize;
    }

    public int getHeaderFontSize()
    {
        return headerFontSize;
    }

    public void setHeaderFontSize(int headerFontSize)
    {
        this.headerFontSize = headerFontSize;
    }

    public int getTitleFontSize()
    {
        return titleFontSize;
    }

    public void setTitleFontSize(int titleFontSize)
    {
        this.titleFontSize = titleFontSize;
    }

    public int getTitleHeight()
    {
        return titleHeight;
    }

    public void setTitleHeight(int titleHeight)
    {
        this.titleHeight = titleHeight;
    }

    public int getBodyHeight()
    {
        return bodyHeight;
    }

    public void setBodyHeight(int bodyHeight)
    {
        this.bodyHeight = bodyHeight;
    }
    
}
