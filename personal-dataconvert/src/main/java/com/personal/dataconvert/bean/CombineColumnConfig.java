package com.personal.dataconvert.bean;

import java.io.Serializable;

/**
 * 规则表转不规则表列配置
 * @author cuibo
 */
public class CombineColumnConfig implements Serializable
{
	public static final String 横向合并 = "横向合并";

	public static final String 纵向合并 = "纵向合并";

	/**
     *
     */
	private static final long serialVersionUID = 870146689883703904L;

	/** 所属表头列的值 */
	private String columnValue;

	/** 合并的最大数量(最大行号或者列号) */
	private int combineCount = Integer.MAX_VALUE;

	/** 列处理类型 */
	private String handleType;

	// 范围采用标准，包含前索引，不包含后索引
	/** 最大行范围 */
	private int maxRowScope = Integer.MAX_VALUE;
	/** 最小行范围 */
	private int minRowScope = -1;

	/** 很特殊的一个配置，一般不会用到，单次最大合并数（也就是说即使符合合并规则，但是超过了单次的合并次数，还是不允许合并） */
	private int singleMaxCount = Integer.MAX_VALUE;

	/** 合并的列号 */
	private int columnIndex;

	public CombineColumnConfig()
	{
		super();
	}

	public CombineColumnConfig(String columnValue, String handleType)
	{
		super();
		this.columnValue = columnValue;
		this.handleType = handleType;
	}

	public CombineColumnConfig(int columnIndex, String handleType)
	{
		super();
		this.columnIndex = columnIndex;
		this.handleType = handleType;
	}

	public String getColumnValue()
	{
		return columnValue;
	}

	/**
	 * 获得最大合并数量
	 * @return
	 */
	public int getCombineCount()
	{
		return combineCount;
	}

	public String getHandleType()
	{
		return handleType;
	}

	public int getMaxRowScope()
	{
		return maxRowScope;
	}

	public int getMinRowScope()
	{
		return minRowScope;
	}

	public int getSingleMaxCount()
	{
		return singleMaxCount;
	}

	public void setColumnValue(String columnValue)
	{
		this.columnValue = columnValue;
	}

	public void setCombineCount(int combineCount)
	{
		this.combineCount = combineCount;
	}

	public void setHandleType(String handleType)
	{
		this.handleType = handleType;
	}

	public void setMaxRowScope(int maxRowScope)
	{
		this.maxRowScope = maxRowScope;
	}

	public void setMinRowScope(int minRowScope)
	{
		this.minRowScope = minRowScope;
	}

	public void setSingleMaxCount(int singleMaxCount)
	{
		this.singleMaxCount = singleMaxCount;
	}

	public int getColumnIndex()
	{
		return columnIndex;
	}

	public void setColumnIndex(int columnIndex)
	{
		this.columnIndex = columnIndex;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + columnIndex;
		result = prime * result + ((columnValue == null) ? 0 : columnValue.hashCode());
		result = prime * result + combineCount;
		result = prime * result + ((handleType == null) ? 0 : handleType.hashCode());
		result = prime * result + maxRowScope;
		result = prime * result + minRowScope;
		result = prime * result + singleMaxCount;
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		CombineColumnConfig other = (CombineColumnConfig) obj;
		if (columnIndex != other.columnIndex)
		{
			return false;
		}
		if (columnValue == null)
		{
			if (other.columnValue != null)
			{
				return false;
			}
		}
		else if (!columnValue.equals(other.columnValue))
		{
			return false;
		}
		if (combineCount != other.combineCount)
		{
			return false;
		}
		if (handleType == null)
		{
			if (other.handleType != null)
			{
				return false;
			}
		}
		else if (!handleType.equals(other.handleType))
		{
			return false;
		}
		if (maxRowScope != other.maxRowScope)
		{
			return false;
		}
		if (minRowScope != other.minRowScope)
		{
			return false;
		}
		if (singleMaxCount != other.singleMaxCount)
		{
			return false;
		}
		return true;
	}

}
