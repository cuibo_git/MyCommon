package com.personal.dataconvert.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * 导出的Html配置信息
 * @author cuibo
 *
 */
public class HtmlConfig
{

	/** 名称 */
	private String name;
	
    /** 表头配置集合 */
    private List<? extends HeaderConfig> headerConfigs;
    
    /** 合并配置 */
    private Set<CombineColumnConfig> combineColumnConfigs;
    
    /** table的样式 */
    private String tableClass;
    
    /** 是否设置title */
    private boolean withTitle = true;

    /** 是否是不规则表 */
    private boolean irregular = false;

    /**
     * 获取所有的叶子节点(包括没有叶子节点的根节点)
     * @return
     */
    public List<HeaderConfig> getLeafHeaderConfigs()
    {
        if (headerConfigs == null || headerConfigs.isEmpty())
        {
            return null;
        }
        List<HeaderConfig> result = new ArrayList<HeaderConfig>();
        List<HeaderConfig> single = null;
        for (HeaderConfig config : headerConfigs)
        {
            single = config.getLeafHeaderWithThis();
            if (single == null || single.isEmpty())
            {
                continue;
            }
            result.addAll(single);
        }
        return result;
    }
    
    public String getName() 
    {
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public List<? extends HeaderConfig> getHeaderConfigs()
    {
        return headerConfigs;
    }

    public void setHeaderConfigs(List<? extends HeaderConfig> headerConfigs)
    {
        this.headerConfigs = headerConfigs;
    }

    public Set<CombineColumnConfig> getCombineColumnConfigs()
    {
        return combineColumnConfigs;
    }

    public void setCombineColumnConfigs(Set<CombineColumnConfig> combineColumnConfigs)
    {
        this.combineColumnConfigs = combineColumnConfigs;
    }

    public String getTableClass()
    {
        return tableClass;
    }

    public void setTableClass(String tableClass)
    {
        this.tableClass = tableClass;
    }

    public boolean isWithTitle()
    {
        return withTitle;
    }

    public void setWithTitle(boolean withTitle)
    {
        this.withTitle = withTitle;
    }

	public boolean isIrregular() 
	{
		return irregular;
	}

	public void setIrregular(boolean irregular)
	{
		this.irregular = irregular;
	}
    
    
}
