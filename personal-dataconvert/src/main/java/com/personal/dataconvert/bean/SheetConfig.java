package com.personal.dataconvert.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.apache.poi.ss.util.CellRangeAddress;

/**
 *
 * @author cuibo
 *
 */
public class SheetConfig implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 所属WorkBook的配置 */
    private WorkBookConfig bookConfig;
    
    /** 是否冻结表头 */
    private boolean freezeHeader = true;
    
    /** 是否创建表头和标题 */
    private boolean createHeader = true;

    /** 表头配置集合 */
    private List<? extends HeaderConfig> headerConfigs;

    /** 索引（可以按照索引重新排列Sheet页顺序） */
    private int index = -1;

    /** 左页眉 */
    private String leftHeader;

    /** 最大深度（即表头的行数） */
    private int maxDeepLength;

    /** Sheet名 */
    private String name;

    /** 右页眉 */
    private String rightHeader;

    /** 标题 */
    private String title;
    
    /** 标题所占行数 */
    private int titleRowCount = 1;
    
    /** 冻结列数 */
    private int freezeColIndex = -1;
    
    /** 列转换配置 */
    private Set<? extends CombineColumnConfig> combineConfigs;
    
    /** 是否是不规则表 */
    private boolean irregular = false;
    
    /** 页脚 */
    private String pagefotter;
    
    /** 合并配置 */
    private List<CellRangeAddress> rangeAddresses;

    public WorkBookConfig getBookConfig()
    {
        return bookConfig;
    }

    public List<? extends HeaderConfig> getHeaderConfigs()
    {
        return headerConfigs;
    }

    public int getMaxDeepLength()
    {
        return maxDeepLength;
    }

    public String getName()
    {
        return name;
    }

    public String getTitle()
    {
        return title;
    }

    /**
     * 创建HeaderConfig
     * @return
     */
    public HeaderConfig newHeaderConfig()
    {
        HeaderConfig result = new HeaderConfig();
        result.setSheetConfig(this);
        return result;
    }

    public void setBookConfig(WorkBookConfig bookConfig)
    {
        this.bookConfig = bookConfig;
    }

    public void setHeaderConfigs(List<? extends HeaderConfig> headerConfigs)
    {
        this.headerConfigs = headerConfigs;
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }

    public void setMaxDeepLength(int maxDeepLength)
    {
        this.maxDeepLength = maxDeepLength;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public int getTitleRowCount()
    {
        return titleRowCount;
    }

    public void setTitleRowCount(int titleRowCount)
    {
        this.titleRowCount = titleRowCount;
    }

    public boolean isFreezeHeader()
    {
        return freezeHeader;
    }

    public void setFreezeHeader(boolean freezeHeader)
    {
        this.freezeHeader = freezeHeader;
    }

    public boolean isCreateHeader()
    {
        return createHeader;
    }

    public void setCreateHeader(boolean createHeader)
    {
        this.createHeader = createHeader;
    }

    public int getFreezeColIndex()
    {
        return freezeColIndex;
    }

    public void setFreezeColIndex(int freezeColIndex)
    {
        this.freezeColIndex = freezeColIndex;
    }

    public Set<? extends CombineColumnConfig> getCombineConfigs()
    {
        return combineConfigs;
    }

    public void setCombineConfigs(Set<? extends CombineColumnConfig> combineConfigs)
    {
        this.combineConfigs = combineConfigs;
    }

	public boolean isIrregular() 
	{
		return irregular;
	}

	public void setIrregular(boolean irregular) 
	{
		this.irregular = irregular;
	}

    public String getPagefotter()
    {
        return pagefotter;
    }

    public void setPagefotter(String pagefotter)
    {
        this.pagefotter = pagefotter;
    }

    public List<CellRangeAddress> getRangeAddresses()
    {
        return rangeAddresses;
    }

    public void setRangeAddresses(List<CellRangeAddress> rangeAddresses)
    {
        this.rangeAddresses = rangeAddresses;
    }

    public String getLeftHeader()
    {
        return leftHeader;
    }

    public void setLeftHeader(String leftHeader)
    {
        this.leftHeader = leftHeader;
    }

    public String getRightHeader()
    {
        return rightHeader;
    }

    public void setRightHeader(String rightHeader)
    {
        this.rightHeader = rightHeader;
    }
  
}
