package com.personal.dataconvert.bean;

import com.personal.core.utils.StringUtil;

/**
 * 
 * @author cuibo
 *
 */
public enum DataConvertStyle
{
    // 使用这种格式因为兼容HTML的样式
    字体颜色红("color:red;"),
    
    背景颜色红("background-color:red;"),
    
    背景颜色粉("background-color:pink;"),
    
    背景颜色宁夏粉("background-color:rgb(248,173,173);"),
    
    背景颜色海南黄("background-color:rgb(255,227,199);"),
    
    背景颜色黄("background-color:yellow;");
    
    private String htmlStyle;
    
    /**
     * 是否是红色字体
     * @param style
     * @return
     */
    public static boolean isFontRed(String style)
    {
        if (StringUtil.isEmpty(style))
        {
            return false;
        }
        style = style.toLowerCase();
        return style.contains(字体颜色红.getHtmlStyle()) && !style.contains(背景颜色红.getHtmlStyle());
    }
    
    /**
     * 背景颜色黄
     * @param style
     * @return
     */
    public static boolean isYellowBackGround(String style)
    {
        if (StringUtil.isEmpty(style))
        {
            return false;
        }
        style = style.toLowerCase();
        return style.contains(背景颜色黄.getHtmlStyle());
    }
    
    /**
     * 粉红色背景
     * @param style
     * @return
     */
    public static boolean isPinkBackGround(String style)
    {
        if (StringUtil.isEmpty(style))
        {
            return false;
        }
        style = style.toLowerCase();
        return style.contains(背景颜色粉.getHtmlStyle());
    }
    
    /**
     * 红色背景
     * @param style
     * @return
     */
    public static boolean isRedBackGround(String style)
    {
        if (StringUtil.isEmpty(style))
        {
            return false;
        }
        style = style.toLowerCase();
        return style.contains(背景颜色红.getHtmlStyle());
    }
    
    /**
     * 宁夏粉红色背景
     * @param style
     * @return
     */
    public static boolean isNxPinkBackGround(String style)
    {
        if (StringUtil.isEmpty(style))
        {
            return false;
        }
        style = style.toLowerCase();
        return style.contains(背景颜色宁夏粉.getHtmlStyle());
    }
    
    /**
     * 宁夏粉红色背景
     * @param style
     * @return
     */
    public static boolean isHnYellowBackGround(String style)
    {
        if (StringUtil.isEmpty(style))
        {
            return false;
        }
        style = style.toLowerCase();
        return style.contains(背景颜色海南黄.getHtmlStyle());
    }
    
    private DataConvertStyle(String htmlStyle)
    {
        this.htmlStyle = htmlStyle;
    }

    public String getHtmlStyle()
    {
        return htmlStyle;
    }

}
