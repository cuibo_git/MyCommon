package com.personal.dataconvert;

import java.io.Serializable;

/**
 * Echart数据模型
 * @author cuibo
 *
 */
public abstract class BaseEchart implements Serializable
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /** 标题 */
    protected String text;
    
    /** 子标题 */
    protected String subText;
    
    /** 是否启用动画效果*/    
    protected boolean animation = true;
    
    /** 宽度 */
    protected int width = 0;
    
    /** 是否在图表上展示数字 */
    protected boolean showLabel = true;
    
    public BaseEchart()
    {
        super();
    }

    public BaseEchart(String text)
    {
        super();
        this.text = text;
    }

    public BaseEchart(String text, String subText)
    {
        super();
        this.text = text;
        this.subText = subText;
    }

    public abstract String toEchartData();

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public String getSubText()
    {
        return subText;
    }

    public void setSubText(String subText)
    {
        this.subText = subText;
    }

	public boolean isAnimation() 
	{
		return animation;
	}

	public void setAnimation(boolean animation)
	{
		this.animation = animation;
	}

    public int getWidth()
    {
        return width;
    }

    public void setWidth(int width)
    {
        this.width = width;
    }

    public boolean isShowLabel()
    {
        return showLabel;
    }

    public void setShowLabel(boolean showLabel)
    {
        this.showLabel = showLabel;
    }
    
}
