package com.personal.dataconvert;

import java.util.List;
import java.util.Map;

import com.personal.core.utils.CoreUtil;
import com.personal.dataconvert.util.ExcelHtmlUtil;

/**
 * Echart柱状，折线模型
 * @author cuibo
 *
 */
public class BarLineEchart extends BaseEchart
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    public static final String BAR = "bar";
    public static final String LINE = "line";
    
    /** 横向数值 */
    private List<XValue> xValues;
    
    /** 纵向数值 */
    private List<YValue> yValues;
    
    /** 多个纵向维度时使用 */
    private List<YAxi> yAxis;
    
    // 单个纵向维度时使用
    private String yName;
    private String yUnit;
    
    /** key:x.y value:值  */
    private Map<String, String> values;
    
    /** 是否生成平均线 */
    private boolean avg = false;
    
    /** 生成最大最小值线 */
    private boolean minmax = false;
    
    public BarLineEchart()
    {
        super();
    }

    public BarLineEchart(String text, String subText)
    {
        super(text, subText);
    }

    public BarLineEchart(String text)
    {
        super(text);
    }

    /**
     * 生成Echart图表
     * @return
     */
    @Override
    public String toEchartData()
    {
        if (xValues == null || yValues == null || xValues.isEmpty() || yValues.isEmpty())
        {
            return null;
        }
        @SuppressWarnings("unused")
        int barCount = 0;
        for (YValue yv : yValues)
        {
            if (BarLineEchart.BAR.equals(yv.getType()))
            {
                barCount ++;
            }
        }
        
        StringBuilder build = new StringBuilder();
        build.append("{\n");
        //animation:false,	动画效果,false没有动画效果
        build.append("\"animation\" : ").append(animation).append(",\n");
        // title
        build.append("\"title\" : {\n");
        if (!CoreUtil.isEmpty(text))
        {
            build.append("\"x\":\"center\",\n");
            build.append("\"text\":\"").append(text).append("\"\n");
        }
        if (!CoreUtil.isEmpty(subText))
        {
            build.append(",");
            build.append("\"subtext\":\"").append(subText).append("\"\n");
        }
        build.append("},\n");
        // grid
        build.append("\"grid\":{\n");
        build.append("\"x\":120,\n");
        build.append("\"y\":60,\n");
        build.append("\"x2:\":80,\n");
        build.append("\"y2\":60\n");
        build.append("},\n");
        // tooltip
        build.append("\"tooltip\":{\n");
        build.append("\"trigger\":\"axis\",\n");
        // 坐标轴指示器，坐标轴触发有效, 默认为直线，可选为：'line' | 'shadow'
        build.append("\"axisPointer\":{\n");
        build.append("\"type\":\"shadow\"\n");
        build.append("}\n");
        build.append("},\n");
        // legend
        build.append("\"legend\":{\n");
        build.append("\"x\":\"10%\",\n");
        build.append("\"data\":[");
        for (YValue y : yValues)
        {
            build.append("\"").append(y.getName()).append("\"").append(",");
        }
        // 删除最后一个逗号
        build.deleteCharAt(build.length() - 1);
        build.append("]\n},\n");
        // toolbox
        build.append("\"toolbox\":{\n").append("\"show\":true,\n").append("\"feature\":{\n")
        .append("\"mark\":{\"show\":false},\n").append("\"dataView\":{\"show\":false,\"readOnly\":false},\n")
        .append("\"magicType\":{\"show\":true,\"type\":[\"line\",\"bar\"]},\n").append("\"restore\":{\"show\":true},\n")
        .append("\"saveAsImage\":{\"show\":true}\n")
        .append("}\n},\n");
        // calculable
        build.append("\"calculable\":true,\n");
        // xAxis
        build.append("\"xAxis\":[\n{\n \"type\":\"category\",\n");
        // 处理横坐标（斜的）
        // int xSize = xValues.size() * barCount;
        int xSize = xValues.size();
        int rotate  = (xSize - 8) / 3 * 15;
        if (rotate < 0)
        {
            rotate = 0;
        } else if (rotate > 90)
        {
            rotate = 90;
        }
        build.append("\"axisLabel\":{\n");
        build.append("\"interval\":0,\n");
        build.append("\"show\":true,\n");
        build.append("\"rotate\":\"").append(rotate).append("\"\n");
        build.append("},");
        build.append("\"data\":[");
        for (XValue x : xValues)
        {
            build.append("\"").append(x.name).append("\",");
        }
        // 删除最后一个逗号
        
        build.deleteCharAt(build.length() - 1);
        build.append("]\n }\n ],\n");
        // yAxis
        build.append("\"yAxis\":[\n");
        if (yAxis == null || yAxis.isEmpty())
        {
            build.append("{\n \"type\":\"value\",\n");
            build.append("\"name\":\"").append(yName == null ? "" : yName).append("\",\n");
            build.append("\"axisLabel\":{\"formatter\": \"{value} ").append(yUnit == null ? "" : yUnit).append("\"}\n}\n],\n");
        } else
        {
            for (YAxi y : yAxis)
            {
                build.append("{\n \"type\":\"value\",\n");
                build.append("\"name\":\"").append(y.getName() == null ? "" : y.getName()).append("\",\n");
                build.append("\"axisLabel\":{\"formatter\": \"{value} ").append(y.getUnit() == null ? "" : y.getUnit()).append("\"}\n},");
            }
            // 删除最后一个逗号
            build.deleteCharAt(build.length() - 1);
            build.append("\n],\n");
        }
        // series
        build.append("\"series\":[\n");
        for (YValue y : yValues)
        {
            build.append("{\n \"name\":\"").append(y.getName()).append("\",\n");
            build.append("\"type\":\"").append(y.getType()).append("\",\n");
            int width = super.width;
            if (width <= 0)
            {
                width = 700;
            }
            if (xSize > 0)
            {
                width = (width / xSize) - 50;
                if (width < 15)
                {
                    width = 15;
                } else if (width > 80)
                {
                    width = 80;
                }
                // 多个柱子先不处理,宽度设置无效
                if (yValues.size() < 2)
                {
                    build.append("\"barWidth\":\"").append(width).append("\",\n");
                }
            }
            build.append("\"barGap\":\"").append("50%").append("\",\n");
            // 柱状图、折线图上方展示数值
            build.append("\"itemStyle\":{\n");
            
            // 柱形变圆滑
            build.append("\"emphasis\":{\n");
            build.append("\"barBorderRadius\": [5, 5, 5, 5]").append("\n},\n");
            
            build.append("\"normal\":{\n");
            
            // 柱形变圆滑
            build.append("\"barBorderRadius\": [5, 5, 5, 5]").append("\n,\n");
            
            build.append("\"label\":{\n");
            if (showLabel)
            {
                build.append("\"show\":").append("\"true\",\n");
            }
            build.append("\"position\":").append("\"top\",\n");
            
            build.append("\"textStyle\":{\n");
            build.append("\"color\":").append("\"black\"\n}\n");
            build.append("}\n}\n},\n");
            
            // 值
            build.append("\"data\":[");
            for (XValue x : xValues)
            {
                String key = x.getKey() + ExcelHtmlUtil.REPLACEPOINTFLAG + y.getKey();
                if (CoreUtil.isNumber(values.get(key)))
                {
                    build.append(values.get(key)).append(",");
                } else
                {
                    build.append(0).append(",");
                }
            }
            // 删除最后一个逗号
            build.deleteCharAt(build.length() - 1);
            build.append("]\n");
            // 平均值
            if (avg)
            {
            	build.append(",");
                build.append("\"markLine\":{");
                build.append("\"data\":[");
                build.append("{\"type\":");
                build.append("\"average\",");
                build.append("\"name\":");
                build.append("\"平均值\"}]}\n");
            }
            if (minmax)
            {
            	build.append(",");
                build.append("\"markPoint\":{");
                build.append("\"data\":[");
                build.append("{\"type\":");
                build.append("\"max\",");
                build.append("\"name\":");
                build.append("\"最大值\"},");
                build.append("{\"type\":");
                build.append("\"min\",");
                build.append("\"name\":");
                build.append("\"最小值\"}]}");
            }
            build.append("},");
        }
        // 删除最后一个逗号
        build.deleteCharAt(build.length() - 1);
        build.append("] \n }");
        
        return build.toString();
    }
    
    public List<XValue> getxValues()
    {
        return xValues;
    }

    public void setxValues(List<XValue> xValues)
    {
        this.xValues = xValues;
    }

    public List<YValue> getyValues()
    {
        return yValues;
    }

    public void setyValues(List<YValue> yValues)
    {
        this.yValues = yValues;
    }

    public List<YAxi> getyAxis()
    {
        return yAxis;
    }

    public void setyAxis(List<YAxi> yAxis)
    {
        this.yAxis = yAxis;
    }

    public String getyName()
    {
        return yName;
    }

    public void setyName(String yName)
    {
        this.yName = yName;
    }

    public String getyUnit()
    {
        return yUnit;
    }

    public void setyUnit(String yUnit)
    {
        this.yUnit = yUnit;
    }

    public Map<String, String> getValues()
    {
        return values;
    }

    public void setValues(Map<String, String> values)
    {
        this.values = values;
    }

    public boolean isAvg()
    {
        return avg;
    }

    public void setAvg(boolean avg)
    {
        this.avg = avg;
    }

    public boolean isMinmax()
    {
        return minmax;
    }

    public void setMinmax(boolean minmax)
    {
        this.minmax = minmax;
    }
    
    
    /**
     * 单个纵向数据
     * @author cuibo
     *
     */
    public class YValue
    {
        /** 名称 */
        private String name;
        
        /** 对应Map中的key(默认值为name) */
        private String key;
        
        /** 类型：bar or line */
        private String type = "bar";
        
        /** 所对应的YAxi序号 */
        private int yAxiIndex = 0;
        
        public YValue()
        {
            super();
        }

        public YValue(String name)
        {
            super();
            this.name = name;
            this.key = name;
        }
        
        public YValue(String name, String type)
        {
            super();
            this.key = name;
            this.name = name;
            this.type = type;
        }
        
        public YValue(String key, String name, String type)
        {
            super();
            this.key = key;
            this.name = name;
            this.type = type;
        }
        
        public YValue(String name, String type, int yAxiIndex)
        {
            super();
            this.name = name;
            this.type = type;
            this.yAxiIndex = yAxiIndex;
            this.key = name;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getType()
        {
            return type;
        }

        public void setType(String type)
        {
            this.type = type;
        }

        public int getyAxiIndex()
        {
            return yAxiIndex;
        }

        public void setyAxiIndex(int yAxiIndex)
        {
            this.yAxiIndex = yAxiIndex;
        }

        public String getKey()
        {
            return key;
        }

        public void setKey(String key)
        {
            this.key = key;
        }
        
    }
    
    /**
     * 单个横向数据
     * @author cuibo
     *
     */
    public class XValue
    {
        /** 名称 */
        private String name;
        
        /** 对应Map中的key(默认值为name) */
        private String key;
        
        public XValue()
        {
            super();
        }

        public XValue(String name)
        {
            super();
            this.name = name;
            this.key = name;
        }
        
        public XValue(String key, String name)
        {
            super();
            this.key = key;
            this.name = name;
        }



        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getKey()
        {
            return key;
        }

        public void setKey(String key)
        {
            this.key = key;
        }
        
    }
    
    
    /**
     * 纵坐标的定义
     * @author cuibo
     *
     */
    public class YAxi
    {
        /** 名称 */
        private String name;
        
        /** 单位 */
        private String unit;

        public YAxi()
        {
            super();
        }

        public YAxi(String name, String unit)
        {
            super();
            this.name = name;
            this.unit = unit;
        }

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getUnit()
        {
            return unit;
        }

        public void setUnit(String unit)
        {
            this.unit = unit;
        }
    }
}
