package com.personal.dataconvert;

import java.util.Map;
import java.util.Map.Entry;

import com.personal.core.utils.CoreUtil;

/**
 * Echart饼图模型
 * @author cuibo
 *
 */
public class PieEchart extends BaseEchart
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /** 所有的值 */
    private Map<String, String> values;
    
    public PieEchart()
    {
        super();
    }

    public PieEchart(String text, String subText)
    {
        super(text, subText);
    }

    public PieEchart(String text)
    {
        super(text);
    }

    /**
     * 生成Echart图表
     * @return
     */
    @Override
    public String toEchartData()
    {
        if (values == null || values.isEmpty())
        {
            return null;
        }
        StringBuilder build = new StringBuilder();
        build.append("{\n");
        //animation:false,	动画效果,false没有动画效果
        build.append("\"animation\" : ").append(animation).append(",\n");
        // title
        build.append("\"title\" : {\n");
        if (null == text)
        {
        	text = "";
        }
        build.append("\"text\":\"").append(text).append("\"\n");
        
        if (!CoreUtil.isEmpty(subText))
        {
        	build.append(",");
            build.append("\"subtext\":\"").append(subText).append("\"\n");
        }
        build.append(",");
        build.append("\"x\":\"center\"\n");
        build.append("},\n");
        // tooltip
        build.append("\"tooltip\":{\n");
        build.append("\"trigger\":\"item\",\n");
        build.append("\"formatter\":\"{a} <br/>{b} : {c} ({d}%)\"\n");
        build.append("},\n");
        // legend
        build.append("\"legend\":{\n");
        build.append("\"orient\":\"vertical\",\n");
        build.append("\"x\":\"left\",\n");
        build.append("\"data\":[");
        for (String key : values.keySet())
        {
            build.append("\"").append(key).append("\"").append(",");
        }
        // 删除最后一个逗号
        build.deleteCharAt(build.length() - 1);
        build.append("]\n},\n");
        // toolbox
//        build.append("\"toolbox\":{\n").append("\"show\":true,\n").append("\"feature\":{\n")
//        .append("\"mark\":{\"show\":true},\n").append("\"dataView\":{\"show\":true,\"readOnly\":false},\n")
//        .append("\"magicType\":{\"show\":true,\"type\":[\"pie\",\"funnel\"],\n").append("\"option\":{\n")
//        .append("\"funnel\":{\n").append("\"x\":\"25%\",\n").append("\"width\":\"50%\",\n")
//        .append("\"funnelAlign\":\"left\",\n").append("\"max\":1548\n}\n}\n},")
//        .append("\"restore\":{\"show\":true},\n")
//        .append("\"saveAsImage\":{\"show\":true}\n}\n},\n");
        // calculable
        build.append("\"calculable\":true,\n");
        // series
        build.append("\"series\":[\n");
        build.append("{\n \"name\":\"").append(text).append("\",\n");
        build.append("\"type\":\"pie\",\n").append("\"radius\":\"55%\",\n")
        .append("\"center\":[\"50%\",\"60%\"],\n");
        // 值
        build.append("\"data\":[");
        for (Entry<String, String> entry : values.entrySet())
        {
            if (CoreUtil.isNumber(entry.getValue()))
            {
                build.append("{\"value\":").append(entry.getValue()).append(",\"name\":\"").append(entry.getKey()).append("\"},");
            } else
            {
                build.append("{\"value\":").append(0.0).append(",\"name\":\"").append(entry.getKey()).append("\"},");
            }
        }
        // 删除最后一个逗号
        build.deleteCharAt(build.length() - 1);
        build.append("]\n");
            
            
        // 删除最后一个逗号
        build.deleteCharAt(build.length() - 1);
        build.append("}\n ]\n }");
        
        return build.toString();
    }

    public void setValues(Map<String, String> values)
    {
        this.values = values;
    }
    
    
}
