package com.personal.dataconvert.port;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 * 数据转Excel接口
 * @author cuibo
 *
 */
public interface Data2Excel
{
    
    // 默认header
    public static final String DEFAULTHEADERFONT = "宋体";
    public static final short DEFAULTHEADERFONTSIZE = 13;
    // 默认title
    public static final String DEFAULTTITLEFONT = "Arial";
    public static final short DEFAULTTITLEFONTSIZE = 16;
    public static final int DEFAULTTITLEHEIGHT = 1000;

    public static final String XLS = "xls";
    public static final String XLSX = "xlsx";
    
    public static final String TRSTYLE = "TRSTYLE";
    public static final String TDSTYLE = "TDSTYLE";
    public static final String TDPOSTIL = "TDPOSTIL";
    public static final String TDBACKGROUND = "TDBACKGROUND";
    
    public static final String TRHIDE = "TRHIDE";
    
    /**
     * 导出Excel
     * @return
     * @throws Exception
     */
    public byte[] exportExcel() throws Exception;

    /**
     * 填充Excel
     * @return
     * @throws Exception
     */
    public Workbook fillExcel() throws Exception;
    
    /**
     * 自定义合并单元格
     * @ClassName: IdefinedRangeAddress 
     * @Description: TODO
     * @author: hudiqing
     * @date: 2017年11月2日 下午5:59:46
     */
    public static class IdefinedRangeAddress
    {
    	private int firstRow;
    	private int lastRow;
    	private int firstCol;
    	private int lastCol;
    	
		public IdefinedRangeAddress(int firstRow, int lastRow, int firstCol, int lastCol)
		{
			super();
			this.firstRow = firstRow;
			this.lastRow = lastRow;
			this.firstCol = firstCol;
			this.lastCol = lastCol;
		}
		
		public CellRangeAddress toCellRangeAddress()
		{
			return new CellRangeAddress(firstRow, lastRow, firstCol, lastCol);
		}
    	
    }
    
}
