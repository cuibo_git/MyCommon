package com.personal.dataconvert.port;


/**
 * 数据转DataGrid
 * @author cuibo
 *
 */
public interface Data2DataGrid
{
    
    /**
     * 创建列
     * @return
     * @throws Exception
     */
    public String createColums() throws Exception;
    
    /**
     * 创建行
     * @return
     * @throws Exception
     */
    public String createRows() throws Exception;
    
}
