package com.personal.dataconvert.port;

import com.personal.core.data.DataTable;

/**
 * 数据构建
 * @author cuibo
 *
 */
public interface DataBuilder
{
    /**
     * 构建DataTable
     * @return
     * @throws Exception
     */
    public DataTable buildAsDataTable() throws Exception;

    /**
     * 构建Html
     * @return
     * @throws Exception
     */
    public String buildAsHtml() throws Exception;

}
