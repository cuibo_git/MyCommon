package com.personal.dataconvert.port;


/**
 * 数据转HTML接口
 * @author cuibo
 *
 */
public interface Data2Html
{
    public static final String TABLECLASS = "TABLECLASS";
    
    public static final String TRSTYLE = "TRSTYLE";
    
    public static final String TRCLASS = "TRCLASS";
    
    public static final String TRCLICK = "TRCLICK";
    
    public static final String TDSTYLE = "TDSTYLE";
    
    public static final String TDCLICK = "TDCLICK";
    
    public static final String TDDATAMAP = "TDDATAMAP";
    
    // 适应生成国网能源局报表
    public static final String TDCLASS = "TDCLASS";
    
    public static final String TDRED = "TDRED";  // td上加 rederror
    
    public static final String TDBLUE = "TDBLUE"; // td上加 blueerror
    
    /**
     * 创建表头
     * @return
     * @throws Exception
     */
    public String createHeader() throws Exception;

    /**
     * 创建表体
     * @return
     * @throws Exception
     */
    public String createBody() throws Exception;
    
    /**
     * 创建HTMl
     * @return
     * @throws Exception
     */
    public String createHtml() throws Exception;
    
}
