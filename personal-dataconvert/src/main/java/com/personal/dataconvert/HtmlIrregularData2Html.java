package com.personal.dataconvert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.personal.core.htmldata.HtmlIrregularData;
import com.personal.core.utils.CoreUtil;
import com.personal.dataconvert.port.Data2Html;

/**
 * 不规则单元数据转Html
 * @author cuibo
 *
 */
public class HtmlIrregularData2Html implements Data2Html
{
    /** 数据集合 */
    private List<HtmlIrregularData> htmlIrregularDatas;
    
    private boolean withTitle = true;
    
    public HtmlIrregularData2Html()
    {
        super();
    }
    
    public HtmlIrregularData2Html(List<HtmlIrregularData> htmlIrregularDatas)
    {
        super();
        this.htmlIrregularDatas = htmlIrregularDatas;
    }
    
    public HtmlIrregularData2Html(List<HtmlIrregularData> htmlIrregularDatas, boolean withTitle)
    {
        super();
        this.htmlIrregularDatas = htmlIrregularDatas;
        this.withTitle = withTitle;
    }
    
    @Override
    public String createHeader() throws Exception
    {
        return "<thead></thead>";
    }

    @Override
    public String createBody() throws Exception
    {
        if (htmlIrregularDatas == null || htmlIrregularDatas.isEmpty())
        {
            return "";
        }
        StringBuilder result = new StringBuilder();
        result.append("<tbody>");
        // 分类，将所有数据，按行号分类
        Map<Integer, List<HtmlIrregularData>> rowMap = classHtmlIrregularDatas();
        if (rowMap != null && !rowMap.isEmpty())
        {
            createBodyRow(rowMap, result);
        }
        result.append("</tbody>");
        return result.toString();
    }

    @Override
    public String createHtml() throws Exception
    {
        if (htmlIrregularDatas == null || htmlIrregularDatas.isEmpty())
        {
            return "";
        }
        StringBuilder result = new StringBuilder();
        result.append("<table border=\"1\" cellspacing=\"0\"  cellpadding=\"2\" class=\"datatable htmldatatable\"> ");
        result.append(createHeader());
        result.append(createBody());
        result.append("</table>");
        return result.toString();
    }
    
    /**
     * 创建表体行
     * @param rowMap
     * @param result
     */
    private void createBodyRow(Map<Integer, List<HtmlIrregularData>> rowMap, StringBuilder result)
    {
        // 查找最大行
        int maxRow = 0;
        for (Entry<Integer, List<HtmlIrregularData>> entry : rowMap.entrySet())
        {
            if (entry.getKey() > maxRow)
            {
                maxRow = entry.getKey();
            }
        }
        List<HtmlIrregularData> datas = null;
        for (int i = 0; i < maxRow + 1; i++)
        {
            datas = rowMap.get(i);
            if (datas == null || datas.isEmpty())
            {
                continue;
            }
            // 按照列号排序
            Collections.sort(datas, new Comparator<HtmlIrregularData>()
            {
                @Override
                public int compare(HtmlIrregularData col1, HtmlIrregularData col2)
                {
                    return col1.getColIndex() - col2.getColIndex();
                }
            });
            result.append("<tr>");
            int rowSpan = handleMultiRowSpanProblem(datas);
            try
            {
                for (HtmlIrregularData data : datas)
                {
                    result.append(data.getTdHtml(withTitle));
                }
            } finally
            {
                if (rowSpan != 1)
                {
                    // 还原
                    for (HtmlIrregularData htmlIrregularData : datas)
                    {
                        htmlIrregularData.setRowSpan(rowSpan);
                    }
                }
            }
            result.append("</tr>");
        }
    }
    
    private int handleMultiRowSpanProblem(List<HtmlIrregularData> result)
    {
        if (CoreUtil.isEmpty(result)) 
        {
            return 1;
        }
        int rowSpan = result.get(0).getRowSpan();
        if (rowSpan == 1)
        {
            return 1;
        }
        for (HtmlIrregularData htmlIrregularData : result)
        {
            if (htmlIrregularData.getRowSpan() != rowSpan)
            {
                rowSpan = htmlIrregularData.getRowSpan();
                return 1;
            }
        }
        // 能运行到这说明需要处理
        for (HtmlIrregularData htmlIrregularData : result)
        {
            htmlIrregularData.setRowSpan(1);
        }
        return rowSpan;
    }
    
    /**
     * 通过行号分类HtmlIrregularData
     * @return
     */
    private Map<Integer, List<HtmlIrregularData>> classHtmlIrregularDatas()
    {
        Map<Integer, List<HtmlIrregularData>> result = new HashMap<Integer, List<HtmlIrregularData>>();
        for (HtmlIrregularData data : htmlIrregularDatas)
        {
            int rowIndex = data.getRowIndex();
            if (result.containsKey(rowIndex))
            {
                result.get(rowIndex).add(data);
            } else
            {
                List<HtmlIrregularData> datas = new ArrayList<HtmlIrregularData>();
                datas.add(data);
                result.put(rowIndex, datas);
            }
        }
        return result;
    }
    
    public List<HtmlIrregularData> getHtmlIrregularDatas()
    {
        return htmlIrregularDatas;
    }

    public void setHtmlIrregularDatas(List<HtmlIrregularData> htmlIrregularDatas)
    {
        this.htmlIrregularDatas = htmlIrregularDatas;
    }

    public boolean isWithTitle()
    {
        return withTitle;
    }

    public void setWithTitle(boolean withTitle)
    {
        this.withTitle = withTitle;
    }
    
    
    
}
