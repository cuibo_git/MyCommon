package com.booway.threadtemp.exception;

/**
 * 机器异常
 * @author cuibo
 *
 */
public class MachineException extends FactoryException 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MachineException(String arg0) 
	{
		super(arg0);
	}

}
