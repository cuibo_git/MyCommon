package com.booway.threadtemp.exception;

/**
 * 工厂异常
 * @author cuibo
 *
 */
public class FactoryException extends Exception 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	public FactoryException(String arg0) 
	{
		super(arg0);
	}

}
