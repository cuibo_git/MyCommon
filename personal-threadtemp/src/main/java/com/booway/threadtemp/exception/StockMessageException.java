package com.booway.threadtemp.exception;

/**
 * 原料处理消息异常
 * 用于在原料处理失败时，需要给出对应的提示信息，但又不想通过原料处理结果来返回的情况
 * 这样可以直接获取原料的错误信息而不需要再通过接口去获取其产品来获取相关的信息
 * @author cuibo
 *
 */
public class StockMessageException extends RuntimeException
{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public StockMessageException()
    {
        super();
    }

    public StockMessageException(String paramString, Throwable paramThrowable)
    {
        super(paramString, paramThrowable);
    }

    public StockMessageException(String paramString)
    {
        super(paramString);
    }

    public StockMessageException(Throwable paramThrowable)
    {
        super(paramThrowable);
    }

    
    
    
}
