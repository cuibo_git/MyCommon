package com.booway.threadtemp.stock;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * 2017 03 15 新增原料包功能
 * @author cuibo
 *
 * @param <S>
 */
public class StockPackage<S>
{
    
    /** 原料包ID */
    private String id;
    
    /** 原料集合 */
    private Collection<S> stocks;
    
    public StockPackage()
    {
        super();
    }

    public StockPackage(String id)
    {
        super();
        this.id = id;
    }

    public StockPackage(String id, List<S> stocks)
    {
        super();
        this.id = id;
        this.stocks = stocks;
    }

    public boolean addStock(S S)
    {
        return getStocks().add(S);
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Collection<S> getStocks()
    {
        if (stocks == null)
        {
            stocks = new ArrayList<S>();
        }
        return stocks;
    }

    public void setStocks(Collection<S> stocks)
    {
        this.stocks = stocks;
    }

    
}
