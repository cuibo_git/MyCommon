package com.booway.threadtemp.stock;

import java.util.Set;

import com.personal.core.port.CallBack;

/**
 * 抽象的添加部分默认功能的原料
 * @author cuibo
 *
 */
public abstract class DefaultStock<S extends DefaultStock<S, P>, P>
{
    /** 前置原料 */
    private Set<S> preStocks;
    
    /** 回调 */
    private CallBack<P> callBack;
    
    /** 百分比 */
    private short percent;
    
    /** 百分比步骤增加 */
    public void addPercent(short step)
    {
        percent += step;
    }

    public Set<S> getPreStocks()
    {
        return preStocks;
    }

    public void setPreStocks(Set<S> preStocks)
    {
        this.preStocks = preStocks;
    }

    public CallBack<P> getCallBack()
    {
        return callBack;
    }

    public void setCallBack(CallBack<P> callBack)
    {
        this.callBack = callBack;
    }

    public short getPercent()
    {
        return percent;
    }

    public void setPercent(short percent)
    {
        this.percent = percent;
    }
    
    
}
