package com.booway.threadtemp.util;

import java.util.Collection;
import java.util.Map;

/**
 * 工具
 * @author cuibo
 *
 */
public class ThreadTempUtil
{
    public static boolean isEmpty(Collection<?> coll)
    {
        return coll == null || coll.isEmpty();
    }

    public static boolean isEmpty(Object obj)
    {
        String str = obj == null ? null : obj.toString();
        return str == null || str.length() == 0 || str.trim().length() == 0;
    }

    public static boolean isEmpty(Object[] arr)
    {
        return arr == null || arr.length == 0;
    }
    
    public static boolean isEmpty(Map<?, ?> coll)
    {
        return coll == null || coll.isEmpty();
    }
}
