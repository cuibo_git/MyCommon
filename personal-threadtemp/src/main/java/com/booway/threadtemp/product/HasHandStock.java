package com.booway.threadtemp.product;

import com.personal.core.delay.CreateAndExpiry;

/**
 * 已经处理完成的原料
 * @author cuibo
 *
 * @param <S>
 * @param <P>
 */
public class HasHandStock<S, P> extends CreateAndExpiry
{
    /** 异常原因 */
    private Throwable cause;

    private S stock;

    private P product;

    public HasHandStock(long createDate, long expirydate, S stock, P product, Throwable cause)
    {
        super(createDate, expirydate);
        this.stock = stock;
        this.product = product;
        this.cause = cause;
    }
    
    /**
     * 是处理出现异常的结果
     * @return
     */
    public boolean isExceptional()
    {
        return cause != null;
    }

    public S getStock()
    {
        return stock;
    }

    public P getProduct()
    {
        return product;
    }

    public Throwable getCause()
    {
        return cause;
    }


}
