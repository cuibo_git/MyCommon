package com.booway.threadtemp.factory;

import java.util.Enumeration;

import com.personal.core.delay.CreateAndExpiry;

/**
 * 
 * 工厂上下文
 * 
 * @author cuibo
 */

public interface FactoryContext
{
    
    public Object getAttribute(String key);
    
    public void setAttribute(String key, Object value);
    
    public void setAttribute(String key, Object value, long expirydate);
    
    public CreateAndExpiry getCreateAndExpiry(String key);
    
    public Object removeAttribute(String key);
    
    public Enumeration<String> getAttributeNames();
    
    public void shutDown();
    
    public void clear();
    
}
