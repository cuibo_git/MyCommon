package com.booway.threadtemp.factoryimpl;

import java.util.concurrent.PriorityBlockingQueue;

/**
 * 原料产品抽象工厂
 * 自定义顺序实现
 * 注意一：此工厂的原料T必须实现comparable接口从而实现自定义顺序
 * 此工厂的添加不会校验队列是否满，如果添加速度一直大于机器的处理速度易造成内存溢出
 * 注意二：该自定义顺序队列只能保证原料提交给机器的顺序，不能保证机器子线程本身的处理完成顺序
 * 即：如 1，2通过compareTo比较之后，1在2前面，该队列只能保证1在2之前提交给机器处理，但不能保证机器百分百让1先于2处理完
 * @author cuibo
 * 
 * @param <S>
 * @param <P>
 *
 */
public abstract class AbstractIdefinedFactory<S, P> extends AbstractFactory<S, P>
{

    public AbstractIdefinedFactory()
    {
        super();
    }
    
    public AbstractIdefinedFactory(int maxSyncCount)
    {
        super(maxSyncCount);
    }

    @Override
    protected void initQueueTypeAndResource()
    {
        stockQueue = new PriorityBlockingQueue<S>();
    }

    @Override
    protected S getNextStock() throws InterruptedException
    {
        return stockQueue.take();
    }
    
    
}

