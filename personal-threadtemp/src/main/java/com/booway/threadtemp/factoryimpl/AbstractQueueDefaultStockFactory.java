package com.booway.threadtemp.factoryimpl;

import java.util.concurrent.LinkedBlockingQueue;

import com.booway.threadtemp.stock.DefaultStock;

/**
 * 原料产品抽象工厂
 * 队列实现（添加的任务是先进先出）
 * 注意：该自定义顺序队列只能保证原料提交给机器的顺序，不能保证机器子线程本身的处理完成顺序
 * 即：如 1在2队头，该队列只能保证1在2之前提交给机器处理，但不能保证机器百分百让1先于2处理完
 * @author cuibo
 * 
 * @param <P>
 *
 */
public abstract class AbstractQueueDefaultStockFactory<S extends DefaultStock<S, P>, P> extends AbstractDefaultStockFactory<S, P>
{
    
    public AbstractQueueDefaultStockFactory()
    {
        super();
    }

    public AbstractQueueDefaultStockFactory(int maxSyncCount)
    {
        super(maxSyncCount);
    }

    @Override
    protected void initQueueTypeAndResource()
    {
        stockQueue = new LinkedBlockingQueue<S>();
    }

    @Override
    protected S getNextStockImpl() throws InterruptedException
    {
        return stockQueue.take();
    }
    
    
}

