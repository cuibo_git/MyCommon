package com.booway.threadtemp.factoryimpl;

import java.util.concurrent.LinkedBlockingDeque;

import com.booway.threadtemp.stock.DefaultStock;

/**
 * 原料产品抽象工厂
 * 栈实现（添加的任务是先进后出，采用双端队列实现）
 * 注意：该自定义顺序队列只能保证原料提交给机器的顺序，不能保证机器子线程本身的处理完成顺序
 * 即：如 1在2栈顶部，该队列只能保证1在2之前提交给机器处理，但不能保证机器百分百让1先于2处理完
 * @author cuibo
 * 
 * @param <P>
 *
 */
public abstract class AbstractStackDefaultStockFactory<S extends DefaultStock<S, P>,P> extends AbstractDefaultStockFactory<S, P>
{

    public AbstractStackDefaultStockFactory()
    {
        super();
    }
    
    public AbstractStackDefaultStockFactory(int maxSyncCount)
    {
        super(maxSyncCount);
    }

    @Override
    protected void initQueueTypeAndResource()
    {
        stockQueue = new LinkedBlockingDeque<S>();
    }

    @Override
    protected S getNextStockImpl() throws InterruptedException
    {
        LinkedBlockingDeque<S> stockQueue = (LinkedBlockingDeque<S>)this.stockQueue;
        return stockQueue.takeLast();
    }
    
    
}

