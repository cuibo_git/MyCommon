package com.booway.threadtemp.factoryimpl;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * 原料产品抽象工厂
 * 队列实现（添加的任务是先进先出）
 * 注意：该自定义顺序队列只能保证原料提交给机器的顺序，不能保证机器子线程本身的处理完成顺序
 * 即：如 1在2队头，该队列只能保证1在2之前提交给机器处理，但不能保证机器百分百让1先于2处理完
 * @author cuibo
 * 
 * @param <S>
 * @param <P>
 *
 */
public abstract class AbstractQueueFactory<S, P> extends AbstractFactory<S, P>
{
    
    public AbstractQueueFactory()
    {
        super();
    }

    public AbstractQueueFactory(int maxSyncCount)
    {
        super(maxSyncCount);
    }

    @Override
    protected void initQueueTypeAndResource()
    {
        stockQueue = new LinkedBlockingQueue<S>();
    }

    @Override
    protected S getNextStock() throws InterruptedException
    {
        return stockQueue.take();
    }
    
    
}

